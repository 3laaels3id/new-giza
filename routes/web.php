<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Front\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. Thesemaster
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['as' => 'front.', 'namespace' => 'Front'], function(){
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::get('/login', [LoginController::class, 'showLoginForm'])->name('sign-in');
    Route::post('/login', [LoginController::class, 'login'])->name('user.submit.login');
    Route::get('/register', [RegisterController::class, 'showRegistrationForm'])->name('show.register');
    Route::post('/register', [RegisterController::class, 'create'])->name('register');
});
