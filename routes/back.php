<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceDoctor within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Back\AdminController;
use App\Http\Controllers\Back\AgeController;
use App\Http\Controllers\Back\SportController;
use App\Http\Controllers\Back\ContactController;
use App\Http\Controllers\Back\DashboardController;
use App\Http\Controllers\Back\RoleController;
use App\Http\Controllers\Back\SettingController;
use App\Http\Controllers\Back\SubscriptionController;
use App\Http\Controllers\Back\TournamentController;
use App\Http\Controllers\Back\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Callbacks\RoutesCallbacks;

Route::namespace('Auth')->group(RoutesCallbacks::AuthAdminRoutes());

# Admin routes
Route::group(['middleware' => ['auth:admin', 'CheckAdminRole', 'CheckAdminStatus']], function ()
{
    // Admin -> home page
    Route::get('/', [DashboardController::class, 'index'])->middleware('SmtpConfigration')->name('admin-panel');

    // admin -> roles Routes
    Route::resource('roles', RoleController::class, ['except' => ['show', 'destroy']]);
    Route::crudRoutes('roles', RoleController::class, true);
    Route::get('/roles/search', [RoleController::class, 'search'])->name('roles.search');

    // admin -> users Routes
    Route::resource('users', UserController::class, ['except' => ['show', 'destroy']]);
    Route::crudRoutes('users', UserController::class, true);
    Route::group(['as' => 'users.'],function(){
        Route::get('/users/{id}/show-user-message', [UserController::class, 'showUserMessage'])->name('show-message');
        Route::post('/users/send-user-message', [UserController::class, 'sendUserMessage'])->name('send-message');
        Route::get('/users/search', [UserController::class, 'search'])->name('search');
    });

    // admin -> Settings Routes
    Route::group(['prefix' => 'settings'], function(){
        Route::get('/', [SettingController::class, 'index'])->name('settings.index');
        Route::post('/update-all', [SettingController::class, 'UpdateAll'])->name('settings.update-all');
    });

    // admin -> Admins Routes
    Route::resource('admins', AdminController::class, ['except' => ['show', 'destroy']]);
    Route::crudRoutes('admins', AdminController::class, true);
    Route::get('/admins/search', [AdminController::class, 'search'])->name('admins.search');
    Route::get('/admins/admin/profile', [AdminController::class, 'adminProfile'])->name('admins.profile');
    Route::post('/admins/admin/profile', [AdminController::class, 'AdminUpdateProfile'])->name('admins.admin-profile-update');
    Route::get('/admins/mails/{id}/show-admin-message', [AdminController::class, 'showAdminMessage'])->name('admins.show-message');
    Route::post('/admins/send-admin-message', [AdminController::class, 'sendAdminMessage'])->name('admins.send-message');

    // admin -> ages Routes
    Route::resource('ages', AgeController::class, ['except' => ['show', 'destroy']]);
    Route::crudRoutes('ages', AgeController::class, true);
    Route::get('/ages/search', [AgeController::class, 'search'])->name('ages.search');

    // admin -> sports Routes
    Route::resource('sports', SportController::class, ['except' => ['show', 'destroy']]);
    Route::crudRoutes('sports', SportController::class, true);
    Route::get('/sports/search', [SportController::class, 'search'])->name('sports.search');

    // admin -> tournaments Routes
    Route::resource('tournaments', TournamentController::class, ['except' => ['show', 'destroy']]);
    Route::crudRoutes('tournaments', TournamentController::class, true);
    Route::get('/tournaments/sport/create', [TournamentController::class, 'sportCreate'])->name('tournaments.sport-create');
    Route::get('/tournaments/sport/{tournament}/edit', [TournamentController::class, 'sportEdit'])->name('tournaments.sport-edit');
    Route::post('/tournaments/sport', [TournamentController::class, 'sportStore'])->name('tournaments.sport-store');
    Route::put('/tournaments/sport/{tournament}/update', [TournamentController::class, 'sportUpdate'])->name('tournaments.sport-update');
    Route::get('/tournaments/search', [TournamentController::class, 'search'])->name('tournaments.search');
    Route::group(['prefix' => 'tournaments'], function(){
        Route::post('/sport/remove', [TournamentController::class, 'sportRemove'])->name('tournaments.ajax.remove.sport');
    });

    // admin -> subscriptions Routes
    Route::resource('subscriptions', SubscriptionController::class,['except' => ['show', 'destroy']]);
    Route::post('/subscriptions/ajax-delete-subscription', [SubscriptionController::class, 'delete'])->name('subscriptions.ajax-delete-subscription');
    Route::get('/subscriptions/export', [SubscriptionController::class, 'export'])->name('subscriptions.export');
    Route::get('/subscriptions/print', [SubscriptionController::class, 'print'])->name('subscriptions.print');
    Route::get('/subscriptions/{subscription}/show', [SubscriptionController::class, 'show'])->name('subscriptions.show');
    Route::get('/subscriptions/search', [SubscriptionController::class, 'search'])->name('subscriptions.search');
    Route::group(['prefix' => 'subscriptions'], function(){
        Route::get('/{invitation}/print-invitation', [SubscriptionController::class, 'printInvitation'])->name('subscriptions.print-invitation');
        Route::post('/ajax-get-sports-by-tournament-id', [SubscriptionController::class, 'getSportsByTournamentId'])->name('subscriptions.ajax-get-sports-by-tournament-id');
        Route::post('/ajax-get-ages-by-sport-id', [SubscriptionController::class, 'getAgesBySportId'])->name('subscriptions.ajax-get-ages-by-sport-id');
        Route::post('/ajax-remove-invitation', [SubscriptionController::class, 'getSportsByTournamentId'])->name('subscriptions.ajax-remove-invitation');
    });

    // admin -> teams Routes
    Route::group(['as' => 'teams.'], function(){
        Route::get('/teams', [SubscriptionController::class, 'teams'])->name('index');
        Route::get('/teams/{team}/show', [SubscriptionController::class, 'showTeam'])->name('show');
        Route::get('/teams/search', [SubscriptionController::class, 'searchTeam'])->name('search');
        Route::get('/teams/export', [SubscriptionController::class, 'teamExport'])->name('export');
        Route::get('/teams/print', [SubscriptionController::class, 'teamsPrint'])->name('print');
    });

    // admin -> Contacts Routes
    Route::group(['as' => 'contacts.', 'prefix' => 'contacts'], function(){
        Route::get('/contacts', [ContactController::class, 'Contacts'])->name('index');
        Route::get('/{contact}/show', [ContactController::class, 'show'])->name('show');
        Route::post('/ajax-delete-contact', [ContactController::class, 'DeleteContact'])->name('ajax-delete-contact');
        Route::get('/{id}/message-details', [ContactController::class, 'showMessageDetails'])->name('message-details');
        Route::get('/id}/send-message', [ContactController::class, 'sendUserMessage'])->name('send-user-message');
        Route::post('/users-send-email', [ContactController::class, 'SendUserReplayMessage'])->name('users-send-email');
        Route::get('/export', [ContactController::class, 'ContactsExport'])->name('export');
        Route::get('/search', [ContactController::class, 'search'])->name('search');
    });
});
