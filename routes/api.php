<?php

use App\Http\Controllers\Api\AppController;
use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\Auth\AuthDoctorController;
use App\Http\Controllers\Api\Doctor\BankController;
use App\Http\Controllers\Api\User\CourseController;
use App\Http\Controllers\Api\User\HomeController;
use App\Http\Controllers\Api\Doctor\DoctorController;
use App\Http\Controllers\Api\Doctor\CourseController as DoctorCourseController;
use App\Http\Controllers\Api\User\SubscriptionController;
use App\Http\Controllers\Api\User\DemandController;
use App\Http\Controllers\Api\ChatController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceDoctor within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->group(function () {
    // Payments Callbacks
    Route::any('payments/success', [AppController::class, 'paymentsSuccess']);
    Route::any('payments/errors', [AppController::class, 'paymentsErrors']);

    Route::any('getAboutApp', [AppController::class, 'getAboutApp']);
    Route::any('getTermsPage', [AppController::class, 'getTermsPage']);
    Route::any('getPrivacyPage', [AppController::class, 'getPrivacyPage']);
    Route::any('setChatMessage', [ChatController::class, 'setChatMessage']);
    Route::any('uploadChatFile', [ChatController::class, 'uploadChatFile']);
    Route::any('contactUsMessage', [AppController::class, 'contactUsMessage']);
    Route::any('getCountriesCodes', [AppController::class, 'getCountriesCodes']);

    // Users Routes;
    Route::prefix('user')->group(function () {
        Route::any('login', [AuthController::class, 'Login']);
        Route::any('register', [AuthController::class, 'register']);
        Route::any('forgetPassword', [AuthController::class, 'forgetPassword']);
        Route::any('checkResetCode', [AuthController::class, 'checkResetCode']);
        Route::any('checkUserActiveCode', [AuthController::class, 'checkUserActiveCode']);
        Route::any('checkUserPhoneExists', [AuthController::class, 'checkUserPhoneExists']);
        Route::any('resetPassword', [AuthController::class, 'resetPassword']);
        Route::any('resendResetCode', [AuthController::class, 'resendResetCode']);
        Route::any('resendUserRegisterActiveCode', [AuthController::class, 'resendUserRegisterActiveCode']);
        Route::any('checkUserLoginActiveCode', [AuthController::class, 'checkUserLoginActiveCode']);

        Route::any('getHomePage', [HomeController::class, 'getHomePage']);
        Route::any('getAllUniversities', [HomeController::class, 'getAllUniversities']);
        Route::any('getAllMainSubjects', [HomeController::class, 'getAllMainSubjects']);
        Route::any('getSubSubjectsByMainId', [HomeController::class, 'getSubSubjectsByMainId']);
        Route::any('getCoursesBySubSubjectId', [CourseController::class, 'getCoursesBySubSubjectId']);
        Route::any('getCoursesByUniversityId', [CourseController::class, 'getCoursesByUniversityId']);
        Route::any('getCourseById', [CourseController::class, 'getCourseById']);
        Route::any('getDoctorProfile', [CourseController::class, 'getDoctorProfile']);
        Route::any('getCourseReviews', [CourseController::class, 'getCourseReviews']);

        Route::any('getFaqsPage', [HomeController::class, 'getFaqsPage']);
        Route::any('search', [HomeController::class, 'search']);

        // auth user routes;
        Route::group(['middleware' => ['assign.guard:api', 'CheckStatus:api']], function () {
            // Profile
            Route::any('updateUserProfile', [AuthController::class, 'updateUserProfile']);
            Route::any('changeUserPassword', [AuthController::class, 'changeUserPassword']);
            Route::any('sendUserNewPhoneCode', [AuthController::class, 'sendUserNewPhoneCode']);
            Route::any('checkUserNewPhoneCode', [AuthController::class, 'checkUserNewPhoneCode']);
            Route::any('getUpdatedProfile', [AuthController::class, 'getUpdatedProfile']);
            Route::any('changePassword', [AuthController::class, 'changePassword']);

            // home
            Route::any('setUserSubjects', [HomeController::class, 'setUserSubjects']);
            Route::any('getUserNotifications', [HomeController::class, 'getUserNotifications']);

            // courses
            Route::any('setUserRateReview', [CourseController::class, 'setUserRateReview']);
            Route::any('setUserDemandedCourse', [DemandController::class, 'setUserDemandedCourse']);

            // subscriptions
            Route::any('getSubscriptionSummary', [SubscriptionController::class, 'getSubscriptionSummary']);
            Route::any('checkUserCoupon', [SubscriptionController::class, 'checkUserCoupon']);
            Route::any('setUserSubscriptionPayment', [SubscriptionController::class, 'setUserSubscriptionPayment']);
            Route::any('getMySubscriptions', [SubscriptionController::class, 'getMySubscriptions']);
            Route::any('getSubscriptionById', [SubscriptionController::class, 'getSubscriptionById']);
            Route::any('setVideoAsWatched', [SubscriptionController::class, 'setVideoAsWatched']);

            // chat
            Route::any('getUserChatList', [ChatController::class, 'getUserChatList']);
            Route::any('getUserChatContentByChatId', [ChatController::class, 'getUserChatContentByChatId']);
        });
    });

    // Doctors Routes;
    Route::prefix('doctor')->group(function () {
        Route::any('login', [AuthDoctorController::class, 'Login']);
        Route::any('checkDoctorLoginActiveCode', [AuthDoctorController::class, 'checkDoctorLoginActiveCode']);
        Route::any('checkDoctorActiveCode', [AuthDoctorController::class, 'checkDoctorActiveCode']);
        Route::any('register', [AuthDoctorController::class, 'register']);
        Route::any('forgetPassword', [AuthDoctorController::class, 'forgetPassword']);
        Route::any('checkResetCode', [AuthDoctorController::class, 'checkResetCode']);
        Route::any('checkDoctorPhoneExists', [AuthDoctorController::class, 'checkDoctorPhoneExists']);
        Route::any('resetPassword', [AuthDoctorController::class, 'resetPassword']);
        Route::any('resendResetCode', [AuthDoctorController::class, 'resendResetCode']);
        Route::any('resendDoctorRegisterActiveCode', [AuthDoctorController::class, 'resendDoctorRegisterActiveCode']);
        Route::any('getFaqsPage', [DoctorController::class, 'getFaqsPage']);

        // auth routes;
        Route::group(['middleware' => ['assign.guard:doctor_api', 'CheckStatus:doctor_api']], function(){
            // Home
            Route::any('getHomePage', [DoctorController::class, 'getHomePage']);
            Route::any('search', [DoctorController::class, 'search']);

            // Profile
            Route::any('updateDoctorProfile', [AuthDoctorController::class, 'updateDoctorProfile']);
            Route::any('sendDoctorNewPhoneCode', [AuthDoctorController::class, 'sendDoctorNewPhoneCode']);
            Route::any('checkDoctorNewPhoneCode', [AuthDoctorController::class, 'checkDoctorNewPhoneCode']);
            Route::any('getUpdatedProfile', [AuthDoctorController::class, 'getUpdatedProfile']);
            Route::any('changeDoctorPassword', [AuthDoctorController::class, 'changeDoctorPassword']);
            Route::any('getDoctorNotifications', [DoctorController::class, 'getDoctorNotifications']);
            Route::any('getRatesPage', [DoctorController::class, 'getRatesPage']);

            // Courses
            Route::any('getCourseById', [DoctorCourseController::class, 'getCourseById']);
            Route::any('getCourseStatistics', [DoctorCourseController::class, 'getCourseStatistics']);

            // Banks
            Route::any('getAllBanks', [BankController::class, 'getAllBanks']);
            Route::any('setNewBankAccount', [BankController::class, 'setNewBankAccount']);
            Route::any('updateBankAccount', [BankController::class, 'updateBankAccount']);
            Route::any('deleteBankAccount', [BankController::class, 'deleteBankAccount']);

            // Wallet
            Route::any('getAllTransactions', [AppController::class, 'getAllTransactions']);
            Route::any('setDoctorBalanceWithdrawal', [AppController::class, 'setDoctorBalanceWithdrawal']);

            // chat
            Route::any('getDoctorChatList', [ChatController::class, 'getDoctorChatList']);
            Route::any('getDoctorChatContentByChatId', [ChatController::class, 'getDoctorChatContentByChatId']);
        });
    });
});
