<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('settings')->insert([
//            self::setRow('map_api', 'Map', 'API', 0, 'AIzaSyBGZZPZWVePr6Ba7SNCwwTSn1YAsV6Smok'),
//            self::setRow('notification_key', 'Notification Key', 'API', 0),
//            self::setRow('tap_company_key', 'Tap Company Key', 'API', 0, 'sk_test_TulXvfotEO3ywQcZp7bY2ksP'),

            self::setRow('smtp', 'driver', 'SMTP', 0, 'smtp'),
            self::setRow('smtp_port', 'SMTP port', 'SMTP', 0, '2525'),
            self::setRow('smtp_host', 'SMTP host', 'SMTP', 0, 'smtp.mailtrap.io'),
            self::setRow('smtp_sender_name', 'SMTP sender name', 'SMTP', 0, config('app.name')),
            self::setRow('smtp_sender_email', 'SMTP sender email', 'SMTP', 0, 'info@site.com'),
            self::setRow('smtp_encryption', 'SMTP encryption', 'SMTP', 0, 'tls'),
            self::setRow('smtp_username', 'SMTP username', 'SMTP', 0, 'efc9853ca1a258'),
            self::setRow('smtp_password', 'SMTP password', 'SMTP', 0, '55b6b6f0114f15'),

//            self::setRow('sms_number', 'SMS Number', 'SMS', 0),
//            self::setRow('sms_password', 'SMS Password', 'SMS', 0),
//            self::setRow('sms_sender_name', 'SMS Sender Name', 'SMS', 0),
//            self::setRow('app_name', 'إسم التطبيق', 'APP', 0, 'APlus'),

//            self::setRow('contact_facebook', 'صفحة الفيسبوك', 'CONTACTS', 0, 'https://facebook.com'),
//            self::setRow('contact_instagram', 'صفحة الانستغرام', 'CONTACTS', 0, 'https://instgram.com'),
//            self::setRow('contact_twitter', 'صفحة تويتر', 'CONTACTS', 0, 'https://twitter.com'),
//            self::setRow('contact_email', 'البريد الالكتروني الخاص بالتطبيق', 'CONTACTS', 0),
//            self::setRow('contact_google_play', 'رابط تحميل التطبيق علي Google Play', 'CONTACTS', 0),
//            self::setRow('contact_app_store', 'رابط تحميل التطبيق علي App Store', 'CONTACTS', 0),

//            self::setRow('contact_site_name_ar', 'إسم الموقع باللغة العربية', 'CONTACTS', 0),
//            self::setRow('contact_site_name_en', 'إسم الموقع باللغة الانجليزية', 'CONTACTS', 0),
//            self::setRow('contact_site_address', 'العنوان', 'CONTACTS', 0),
//            self::setRow('contact_site_description_ar', 'وصف الموقع بالعربية', 'CONTACTS', 0, 'هو مثال لنص يمكن ان يستبدل فى نفس المساحة, لقد تم توليد هذا النص من مولد النص العربي , حيث يمكنك ان تولد مثل هذا النص او العديد من النصوص الأخرى اضافة الى زيادة عدد الحروف التى يولدها التطبيق اذا كنت تحتاج الى عدد كبير من الفقرات يتيح لك مولد النص العربى'),
//            self::setRow('contact_site_description_en', 'وصف الموقع بالانجليزية', 'CONTACTS', 0, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.'),
//            self::setRow('contact_site_location', 'مكان الشركة علي الخريطة', 'CONTACTS', 3, '31.033184321756003,31.38131713867239'),

              self::setRow('sports_max_num_in_tournament', 'أقصي عدد من الالعاب داخل البطولة الواحدة', 'APP', 0, 2)
        ]);
    }

    private static function setRow($key, $name, $type, $input, $value = null)
    {
        return [
            'key'        => $key,
            'name'       => $name,
            'type'       => $type,
            'input'      => $input,
            'value'      => $value,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
