<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('admins')->insert(self::setRow(1,'admin', 'admin@gmail.com','admin123'));
    }

    private static function setRow($role_id, $name, $email, $password, $phone = null)
    {
        return [
            'role_id'    => $role_id,
            'fname'       => $name,
            'lname'       => $name,
            'email'      => $email,
            'password'   => Hash::make($password),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
