<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgesTable extends Migration
{
    public function up()
    {
        Schema::create('ages', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();

            $table->unsignedBigInteger('sport_id')->nullable();
            $table->foreign('sport_id')->references('id')->on('sports')->onDelete('cascade');

            $table->enum('type', ['adult', 'junior'])->nullable();
            $table->string('maximum')->nullable();
            $table->boolean('status')->default(1);

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('age_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->unsignedBigInteger('age_id')->nullable();
            $table->string('locale')->index();
            $table->string('name')->nullable();
            $table->unique(['age_id','locale']);
            $table->foreign('age_id')->references('id')->on('ages')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('age_translations');
        Schema::dropIfExists('ages');
    }
}
