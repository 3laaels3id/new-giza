<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'            => 'يجب قبول حقل :attribute',
    'active_url'          => 'حقل :attribute لا يُمثّل رابطًا صحيحًا',
    'after'               => 'يجب على حقل :attribute أن يكون تاريخًا لاحقًا لـ :date.',
    'after_or_equal'      => 'The :attribute must be a date after or equal to :date.',
    'alpha'               => 'يجب أن لا يحتوي حقل :attribute سوى على حروف',
    'alpha_dash'          => 'يجب أن لا يحتوي حقل :attribute على حروف، أرقام ومطّات.',
    'alpha_num'           => 'يجب أن يحتوي :attribute على حروفٍ وأرقامٍ فقط',
    'array'               => 'يجب أن يكون حقل :attribute ًمصفوفة',
    'before'              => 'يجب على حقل :attribute أن يكون تاريخًا سابقًا لحقل :date.',
    'before_or_equal'     => 'The :attribute must be a date before or equal to :date.',
    'between' => [
        'numeric' => 'يجب أن تكون قيمة :attribute محصورة ما بين :min و :max.',
        'file'    => 'يجب أن يكون حجم الملف :attribute محصورًا ما بين :min و :max كيلوبايت.',
        'string'  => 'يجب أن يكون عدد حروف النّص :attribute محصورًا ما بين :min و :max',
        'array'   => 'يجب أن يحتوي :attribute على عدد من العناصر محصورًا ما بين :min و :max',
    ],
    'boolean'            => 'يجب أن تكون قيمة حقل :attribute إما true أو false ',
    'confirmed'          => 'حقل التأكيد غير مُطابق للحقل :attribute',
    'date'               => 'حقل :attribute ليس تاريخًا صحيحًا',
    'date_equals'        => 'The :attribute must be a date equal to :date.',
    'date_format'        => 'لا يتوافق حقل :attribute مع الشكل :format.',
    'different'          => 'يجب أن يكون حقلان :attribute و :other مُختلفان',
    'digits'             => 'يجب أن يحتوي حقل :attribute على :digits رقمًا/أرقام',
    'digits_between'     => 'يجب أن يحتوي حقل :attribute ما بين :min و :max رقمًا/أرقام ',
    'dimensions'         => 'The :attribute has invalid image dimensions.',
    'distinct'           => 'The :attribute field has a duplicate value.',
    'email'              => 'حقل :attribute يجب ان يكون بريد الكتروني صالح',
    'ends_with'          => 'The :attribute must end with one of the following: :values',
    'exists'             => 'حقل :attribute غير  موجود',
    'file'               => 'حقل :attribute يجب ان يكون ملف',
    'filled'             => 'The :attribute field must have a value.',
    'gt' => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file'    => 'The :attribute must be greater than :value kilobytes.',
        'string'  => 'The :attribute must be greater than :value characters.',
        'array'   => 'The :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file'    => 'The :attribute must be greater than or equal :value kilobytes.',
        'string'  => 'The :attribute must be greater than or equal :value characters.',
        'array'   => 'The :attribute must have :value items or more.',
    ],
    'image'              => 'حقل :attribute يجب ان يكون صورة',
    'in'                 => 'حقل :attribute غير صحيح',
    'in_array'           => 'The :attribute field does not exist in :other.',
    'integer'            => 'حقل :attribute يجب ان يكون رقمي صحيح',
    'ip'                 => 'The :attribute must be a valid IP address.',
    'ipv4'               => 'The :attribute must be a valid IPv4 address.',
    'ipv6'               => 'The :attribute must be a valid IPv6 address.',
    'json'               => 'The :attribute must be a valid JSON string.',
    'lt' => [
        'numeric' => 'The :attribute must be less than :value.',
        'file' => 'The :attribute must be less than :value kilobytes.',
        'string' => 'The :attribute must be less than :value characters.',
        'array' => 'The :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file' => 'The :attribute must be less than or equal :value kilobytes.',
        'string' => 'The :attribute must be less than or equal :value characters.',
        'array' => 'The :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file' => 'حقل :attribute يجب الا يكون بحجم اكبر من :max كيلوبيت',
        'string' => 'The :attribute may not be greater than :max characters.',
        'array' => 'The :attribute may not have more than :max items.',
    ],
    'mimes' => 'حقل :attribute يجب ان يكون بالامتدادات الاتية : :values',
    'mimetypes' => 'حقل :attribute يجب ان يكون بالامتدادات الاتية : :values',
    'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => 'حقل :attribute يجب ان لا يقل عن :min حروف',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'not_in' => 'حقل :attribute يجب ان يكون بقيمة صحيحة',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric' => 'حقل :attribute يجب ان يكون رقمي',
    'present' => 'The :attribute field must be present.',
    'regex' => 'حقل :attribute بصيغة حاطئة',
    'required' => 'حقل :attribute مطلوب.',
    'required_if' => 'حقل :attribute مطلوب في حال ما إذا كان :other يساوي :value.',
    'required_unless' => 'حقل :attribute مطلوب في حال ما لم يكن :other يساوي :values.',
    'required_with' => 'حقل :attribute إذا توفّر :values.',
    'required_with_all' => 'حقل :attribute إذا توفّر :values.',
    'required_without' => 'حقل :attribute إذا لم يتوفّر :values.',
    'required_without_all' => 'حقل :attribute إذا لم يتوفّر :values.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'starts_with' => 'The :attribute must start with one of the following: :values',
    'string' => 'حقل :attribute يجب ان يكون نص',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'حقل :attribute موجود بالفعل',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'حقل :attribute بصيغة غير صحيحة',
    'uuid' => 'The :attribute must be a valid UUID.',

    'mak_words' => 'حقل :attribute يجب ان يكون  رباعي',
    'youtube_link' => 'حقل :attribute يجب ان يكون رابط يوتيوب',
    'is_ar' => 'حقل :attribute يجب ان يكون باللغة العربية',
    'without_spaces' => 'حقل :attribute يجب ان لا يجتوي علي مسافات',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        'ar.answer'                  => 'الجواب باللغة العربية',
        'en.answer'                  => 'الجواب باللغة الانجليزية',

        'ar.question'                => 'السؤال باللغة العربية',
        'en.question'                => 'السؤال باللغة الانجليزية',

        'ar.name'                    => 'الاسم باللغة العربية',
        'en.name'                    => 'الاسم باللغة الانجليزية',

        'ar.desc'                    => 'الوصف باللغة العربية',
        'en.desc'                    => 'الوصف باللغة الانجليزية',

        'ar.description'             => 'الوصف باللغة العربية',
        'en.description'             => 'الوصف باللغة الانجليزية',

        'ar.title'                   => 'العنوان باللغة العربية',
        'en.title'                   => 'العنوان باللغة الانجليزية',

        'ar.content'                 => 'المحتوي باللغة العربية',
        'en.content'                 => 'المحتوي باللغة الانجليزية',

        'ar.body'                    => 'المحتوي باللغة العربية',
        'en.body'                    => 'المحتوي باللغة الانجليزية',

        'video_link'                 => 'رابط الفيديو',
        'cost'                       => 'سعر الحجز',
        'image'                      => 'الصورة',
        'latitude'                   => 'خط الطول',
        'longitude'                  => 'خط العرض',
        'chairs'                     => 'عدد الكراسي',
        'ammount'                    => 'الكمية',
        'price'                      => 'السعر',
        'name'                       => 'الاسم',
        'email'                      => 'البريد الالكتروني',
        'password'                   => 'كلمة المرور',
        'phone'                      => 'رقم الهاتف',
        'commercial_registration_no' => 'رقم السجل التجاري',
        'shop_name'                  => 'اسم المحل',
        'shop_address'               => 'عنوان المحل',
        'date'                       => 'التاريخ',
        'neighborhood'               => 'الحي',
        'street'                     => 'الشارع',
        'bride_name'                 => 'اسم العروسة',
        'userData'                   => 'بيانات العضو',
        'userData.name'              => 'الاسم',
        'userData.email'             => 'البريد الالكتروني',
        'userData.phone'             => 'الهاتف',
        'userData.city_id'           => 'المدينة',

        'reservation_date'           => 'تاريخ الحجز',
        'hall_id'                    => 'رقم القاعة',
        'to'                         => 'المرسل اليه',
        'message'                    => 'الرسالة',
        'subject'                    => 'عنوان الرسالة',
        'code'                       => 'الكود',
        'sms_code'                   => 'كود الرسالة',
        'city_id'                    => 'المدينة',
        'delegate_identity'          => 'المندوب',
        'start_time'                 => 'بداية دوام الفترة الاولي',
        'end_time'                   => 'نهاية دوام الفترة الاولي',
        'identity'                   => 'رقم الموظف',
        'bank_name'                  => 'إسم البنك',
        'bank_account_no'            => 'رقم الحساب البنكي',
        'category_id'                => 'الفئة',
        'lat'                        => 'خط الطول',
        'long'                       => 'خط العرض',
        'market_id'                  => 'المحل',
        'rate'                       => 'التقيم',
        'comment'                    => 'التعليق',
        'ended_at'                   => 'تاريخ الانتهاء',
        'value'                      => 'القيمة',
        'second_start_time'          => 'بداية دوام الفترة الثانية',
        'second_end_time'            => 'نهاية دوام الفترة الثانية',
        'whatsapp'                   => 'رقم الوتس اب',
        'coupon_no'                  => 'الكوبون',
        'state'                      => 'الحي',
        'role_id'                    => 'الصلاحية',
        'database'                   => 'قاعدة البيانات',
        'title'                      => 'العنوان',
        'link'                       => 'الرابط',
        'user_id'                    => 'اللاعب',
        'address'                    => 'العنوان',
        'specialization'             => 'التخصص',
        'crn'                        => 'رقم السجل التجاري',
        'year_founded'               => 'سنة التأسيس',
        'body'                       => 'المحتوي',
        'new_cases'                  => 'الحالات الجديدة',
        'cases_from_pos_to_nig'      => 'الحالات من ايجابي الي سلبي',
        'new_deaths'                 => 'الوافيات الجديدة',
        'total_cases'                => 'اجمالي الحالات المصابة',
        'recovery_cases'             => 'الحالات التي تم شفائها',
        'total_deaths'               => 'اجمالي حالات الوفيات',
        'newPassword'                => 'كلمة المرور الجديدة',
        'start_date'                 => 'تاريخ البدأ',
        'end_date'                   => 'تاريخ الانتهاء',
        'currentPassword'            => 'كلمة المرور الحالية',
        'slug'                       => 'اسم لينك الصفحة',
        'service_name'               => 'إسم الخدمة',
        'service_type'               => 'نوع النشاط',
        'service_desc'               => 'وصف الخدمة المطلوبة',
        'service_extra'              => 'ملحقات خاصة بالخدمة',
        'days'                       => 'مدة الباقة بالايام',
        'bank_id'                    => 'البنك',
        'account_number'             => 'رقم الحساب',
        'iban_number'                => 'رقم iban',
        'desc'                       => 'الوصف',
        'section_id'                 => 'القسم',
        'agency_id'                  => 'المنشأة',
        'country_id'                 => 'البلد',
        'type'                       => 'النوع',

        'gender'                     => 'الجنس',
        'unit_price'                 => 'سعر الوجدة',
        'job'                        => 'الوظيفة',
        'age'                        => 'السن',
        'linkedin'                   => 'حساب لينكدان',
        'program'                    => 'البرنامج',
        'entity'                     => 'الجهة',

        'description'                => 'الوصف',
        'project_id'                 => 'المشروع',
        'country_code'               => 'مفتاح الدولة',
        'zip_code'                   => 'الرمز البريدي',
        'images.*'                   => 'الصور',

        'package_id'                 => 'الباقة',
        'provider_id'                => 'مقدم الخدمة',
        'repair_id'                  => 'طلب الاصلاح',
        'new_password'               => 'كلمة المرور الجديدة',

        'categories'                 => 'الفئات',
        'fname'                      => 'الاسم الاول',
        'lname'                      => 'الاسم الاخير',
        'username'                   => 'اسم المستحدم',
        'course_id'                  => 'الدورة',
        'filename'                   => 'الملف',
        'subject_id'                 => 'المادة الاساسية',
        'sub_subject_id'             => 'المادة الفرعية',
        'into_video'                 => 'الفيديو التجريبي',
        'university_id'              => 'الجامعة',
        'expired_at'                 => 'تاريخ الانتهاء',
        'subjects'                   => 'المواد الاساسية',
        'account_no'                 => 'رقم الحساب البنكي',
        'beneficiary_name'           => 'إسم المستفيد',
        'for'                        => 'الشخص المستهدف',
        'subscription_id'            => 'الاشتراك',
        'video_id'                   => 'الفيديو',
        'doctor_id'                  => 'الدكتور',
        'main_subject_id'            => 'الفئة الرئيسية',
        'sport_id'                   => 'اللعبة الرياضية',
        'maximum'                    => 'أقصي عدد من اللاعبين داخل اللعبة',
        'maximum_sports'             => 'اقصي عدد من الالعاب داخل البطولة',
        'tournament_id'              => 'البطولة',
        'tournament_sports.*'        => 'اللعبة الرياضية',
        'id'                         => 'البطولة',
        'invitations_fname.*'        => 'الاسم الاول',
        'invitations_lname.*'        => 'الاسم الاخير',
        'invitations_age.*'          => 'العمر',
        'invitations_birth_date.*'   => 'تاريح الميلاد',
        'invitations_address.*'      => 'العنوان',
        'invitations_identity.*'     => 'رقم الهوية',
        'invitations_email.*'        => 'البريد الالكتروني',
        'invitations_phone.*'        => 'رقم الهاتف',
        'age_id'                     => 'الفئة العمرية',
        'membership_number'          => 'رقم العضوية',
        'team_name'                  => 'إسم الفرقة',

        'categories.*.maincategory_id' => 'الفئة الرئيسية',
        'categories.*.subcategory_id'  => 'الفئة الفرعية',
    ],
];
