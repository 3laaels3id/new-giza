<!DOCTYPE html>
<html lang="{{ getLocale() }}" dir="{{ direction() }}">
<head>
    <meta charset="utf-8" />
    <title>@yield('title', trans('back.login'))</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    {!! meta('viewport', 'width=device-width, initial-scale=1.0') !!}
    {!! meta('description', 'A fully featured admin theme which can be used to build CRM, CMS, etc.') !!}
    {!! meta('author', 'Coderthemes') !!}
    <!-- App favicon -->
    {!! style('public/admin/images/favicon.png', ['rel' => 'shortcut icon']) !!}

    <!-- App css -->
    {!! style('public/admin/css/bootstrap.css') !!}
    {!! style('public/admin/css/icons.css') !!}
    {!! style('public/admin/css/app-rtl.min.css') !!}
    {!! style('https://fontlibrary.org/face/droid-arabic-kufi', ['media' => 'screen']) !!}
    <style>
        body, h4, h2, h3, h1, h5, h6, label, span {
            font-family: 'DroidArabicKufiRegular', serif;
        }
    </style>
</head>

<body class="authentication-bg">
    <div class="account-pages mt-5 mb-5">
        <div class="container">
            @yield('content')
        </div>
    </div>

    {!! script('public/admin/js/vendor.js') !!}
    {!! script('public/admin/js/app.js') !!}
</body>
</html>
