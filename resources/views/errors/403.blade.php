<!DOCTYPE html>
<html lang="ar">
<head>
    <meta charset="utf-8" />
    <title>403</title>

    {!! meta('viewport', 'width=device-width, initial-scale=1.0') !!}
    {!! meta('description', 'A fully featured admin theme which can be used to build CRM, CMS, etc.') !!}
    {!! meta('author', 'Coderthemes') !!}

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    {!! style('public/admin/images/favicon.ico',['rel'=>'shortcut icon']) !!}

    <!-- App css -->
    {!! style('public/admin/css/bootstrap.css') !!}
    {!! style('public/admin/css/icons.css') !!}
    {!! style('public/admin/css/app-rtl.min.css') !!}
    {!! style('http://fontlibrary.org/face/droid-arabic-kufi',['media'=>'screen']) !!}
    <!-- /global stylesheets -->

    <style>
        body, h1, h2, h3, h4, h5, h6, span, label, p {
            font-family: 'DroidArabicKufiRegular', serif;
        }
    </style>
</head>

<body class="authentication-bg">
    <div class="account-pages mt-5 mb-5">c
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="text-center">
                        <a href="{{ checkUrlHas('admin-panel') ? route('admin-panel') : route('doctor-panel') }}">
                            <span><img src="{{ asset('public/admin/images/logo-dark.png') }}" alt="" height="80"></span>
                        </a>
                        <p class="text-muted mt-2 mb-4">@lang('back.dashboard')</p>
                    </div>
                    <div class="card">
                        <div class="card-body p-4">
                            <div class="text-center">
                                <h1 class="text-error">403</h1>
                                <h4 class="mt-3 mb-2">@lang('back.has-no-permission')</h4>

                                <a href="{{ checkUrlHas('admin-panel') ? route('admin-panel') : route('doctor-panel') }}" class="btn btn-danger waves-effect waves-light">
                                    <i class="fas fa-home mr-1"></i>
                                    العودة للرئيسية
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! script('public/admin/js/vendor.js') !!}

    {!! script('public/admin/js/app.js') !!}
</body>
</html>
