@extends('layouts.auth')

@section('title', trans('back.login'))

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8 col-lg-6 col-xl-5">
            <div class="text-center">
                <a href="{{ route('admin-panel') }}">
                    <span><img src="{{ url('public/admin/images/image.png') }}" alt="" height="100"></span>
                </a>
                <p class="text-muted mt-2 mb-4">@lang('back.dashboard')</p>
            </div>
            <div class="card">

                <div class="card-body p-4">
                    @include('includes.flash')

                    <div class="text-center mb-4">
                        <h4 class="text-uppercase mt-0">@lang('back.login')</h4>
                    </div>

                    <form method="POST" action="{{ route('admin.submit.login') }}">
                        @csrf

                        <div class="form-group mb-3">
                            <label for="emailaddress" style="float: {{ floating('right', 'left') }};">@lang('back.form-email')</label>
                            <input dir="ltr" class="form-control @error('email') is-invalid @enderror" name="email" type="email" id="emailaddress" placeholder="@lang('back.form-email')">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group mb-3">
                            <label for="password" style="float: {{ floating('right', 'left') }};">@lang('back.form-password')</label>
                            <input dir="ltr" class="form-control @error('password') is-invalid @enderror" name="password" type="password" id="password" placeholder="@lang('back.form-password')">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group mb-3" style="float: {{ floating('right', 'left') }};">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="remember" class="custom-control-input" id="checkbox-signin" checked>
                                <label class="custom-control-label" for="checkbox-signin">@lang('back.form-remember-me')</label>
                            </div>
                        </div>

                        <div class="form-group mb-0 text-center">
                            <button class="btn btn-primary btn-block" type="submit">@lang('back.login')</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col-12 text-center">
                    <p dir="ltr"> <a href="{{ route('admin.forget-password') }}" class="text-muted ml-1"><i class="fa fa-lock m-1"></i>@lang('back.form-forget-password')</a></p>
                </div>
            </div>
        </div>
    </div>
@stop
