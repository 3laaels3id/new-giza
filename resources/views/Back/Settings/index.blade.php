@extends('Back.layouts.master')

@section('title', trans('back.settings.settings'))

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin-panel') }}">@lang('back.dashboard')</a></li>
            <li class="breadcrumb-item active" aria-current="page">@lang('back.settings.settings')</li>
        </ol>
    </nav>

    <div class="card-box" dir="{{ direction() }}" style="margin: 20px;">

        <div class="card-header bg-secondary text-white">@lang('back.settings.settings')</div>

        <div class="card-body">
            {{ Form::open(['route' => 'settings.update-all', 'method' => 'POST', 'files' => true, 'id' => 'update-all-form', 'class' => 'ajax edit settings']) }}

            <ul class="nav nav-tabs nav-justified">
                @foreach($settings->pluck('type')->unique()->chunk(2) as $ii => $vv)
                    <li class="nav-item">
                        <a href="#fields-{{$ii}}" data-toggle="tab" aria-expanded="false" class="nav-link {{ $loop->first ? 'active' : '' }}">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                            <span class="d-none d-sm-block">{{ trans('back.page-number', ['var' => $ii + 1]) }}</span>
                        </a>
                    </li>
                @endforeach
            </ul>

            <div class="tab-content">
                @foreach($settings->pluck('type')->unique()->chunk(2) as $jj => $chunk)
                    <div role="tabpanel" class="tab-pane fade {{ $loop->first ? 'show active' : '' }}" id="fields-{{$jj}}">
                        @include('Back.includes.settingsTab', ['chunk' => $chunk, 'settings' => $settings])
                    </div>
                @endforeach
            </div>
            {{ Form::close() }}
            <button type="submit" form="update-all-form" name="submit" class="btn btn-primary">@lang('back.save')</button>
        </div>
    </div>
@stop
