<div class="row">
	<div class="col-md-12">
		<div class="panel panel-flat">
			<div class="panel-body">
                <ul class="nav nav-tabs nav-justified">
                    @foreach(config('sitelangs.langs') as $lang)
                        <li class="nav-item">
                            <a href="#fields-{{ $lang['locale'] }}" data-toggle="tab" aria-expanded="false" class="nav-link {{ $loop->first ? 'active' : '' }}">
                                <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                <span class="d-none d-sm-block nav-tab-title-js">@lang('back.'.$lang['locale'])</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
				<div class="tabbable">
					<div class="tab-content">
						@foreach(config('sitelangs.langs') as $lang)
                            <div role="tabpanel" class="tab-pane fade {{ $loop->first ? 'show active' : '' }}" id="fields-{{ $lang['locale'] }}">
								@foreach ($fields as $key => $field)
									@include('Back.components.langInput', [
										'name'  => $field['name'],
										'type'  => $field['type'],
										'trans' => $field['trans'],
										'lang'  => $lang,
										'other' => isset($others) ? $others : '',
										'class' => 'col-xs-12'
									])
								@endforeach
							</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
