<div class="row">
    @php $z=1;$d=2; @endphp
    @foreach($getAllRoutes as $key => $value)
        @continue($value == 'admin-panel')
        @if(is_array($value))
            @php $chunks = array_chunk($value ?? null, 7); @endphp
            <div class="col-md-4 col-xs-12">
                <div class="card border border-secondary">
                    <div class="card-body" style="height: 712px;">
                        <h4 class="card-title text-secondary"><b>@lang('crud.' . $key . '.' . $key)</b></h4>
                        <ul class="nav nav-tabs nav-justified">
                            @foreach($chunks as $ck => $xx)
                            <li class="nav-item">
                                <a href="#fields-{{$ck.$z.$d}}" data-toggle="tab" aria-expanded="false" class="nav-link {{ $loop->first ? 'active' : '' }}">
                                    <span class="d-block d-sm-none">
                                        <i class="fas fa-home"></i>
                                    </span>
                                    <span class="d-none d-sm-block">
                                        {{ trans('back.page-number', ['var' => $ck + 1]) }}
                                    </span>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach($chunks as $ii => $chunk)
                                <div role="tabpanel" class="tab-pane fade {{ $loop->first ? 'show active' : '' }}" id="fields-{{$ii.$z.$d}}">
                                    @foreach ($chunk as $keys => $field)
                                        <x-permission-list-item :role="$role" :val="$field" :k="$ii.$z.$d.$keys"></x-permission-list-item>
                                    @endforeach
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            @php $z++; @endphp
        @else
            <div class="col-md-4 col-xs-12">
                <div class="card border border-secondary">
                    <div class="card-body" style="height: 712px;">
                        <h3 class="card-title text-secondary"><b>@lang('crud.' . $value)</b></h3>
                        <ul class="nav nav-tabs nav-justified">
                            <li class="nav-item">
                                <a href="#fields-name-1" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                    <span class="d-block d-sm-none">
                                        <i class="fas fa-home"></i>
                                    </span>
                                    <span class="d-none d-sm-block">
                                        {{ trans('back.page-number', ['var' => 1]) }}
                                    </span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade show active" id="fields-name-1">
                                <x-permission-item :role="$role" :value="$value" :key="$key"></x-permission-item>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
</div>
