<script>
    $(document).ready( function () {
        let tableName = '{{ $tableName }}';
        let length = '{{ $length }}';

        $(tableName).DataTable({
            language: languages,
            processing : true,
            autoWidth: false,
            searching: false,
            paging: false,
            info: false,
            columnDefs: [
                { orderable: true, width: '80px', targets: [length - 1] }
            ],
            dom: 'Bfrtip',
            buttons: [],
            drawCallback: function () {
                $(tableName).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function() {
                $(tableName).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            },
        });

        $('.dt-buttons button').addClass('btn btn-secondary');
    });
</script>
