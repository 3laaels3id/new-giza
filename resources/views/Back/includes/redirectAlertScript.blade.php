<script>
    let selectedClass = '{{ $class }}';

    $('.' + selectedClass).on('click', function(e){
        e.preventDefault();

        let href = $(this).attr('href');

        let message = ($(this).data('type') === 'block') ? "@lang('back.confirm-block-account')" : "@lang('back.confirm-unblock-account')";

        swal({title: '@lang('back.a-message')', text: message, icon: "warning", buttons: true, dangerMode: true})
            .then(function(willDelete) {
                if (willDelete) {
                    window.location.href = href;
                } else {
                    swal({ title: "@lang('back.a-message')", text: "@lang('back.operation-terminated')", icon: "success", button: "@lang('back.ok')",});
                }
            });
    });
</script>
