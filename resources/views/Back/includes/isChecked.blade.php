<script>
    let ajaxUrl = '{{ route(str()->plural($model).'.ajax-change-'.$model.'-status') }}';

    function isChecked(value, id) {
        if (value === 'checked')
        {
            let checkBoox = $('#checkbox-'+id);

            checkBoox.html('');

            checkBoox.html(getSwitchRender('true', id));

            $('#active-id-' + id).attr('onclick', 'isChecked("null", "' + id + '")');
        }
        else
        {
            let checkBoox = $('#checkbox-'+id);

            checkBoox.html('');

            checkBoox.html(getSwitchRender('false', id));

            $('#active-id-' + id).attr('onclick', 'isChecked("checked", "' + id + '")');
        }

        $.ajax({
            type: 'POST',
            url: ajaxUrl,
            data: {id, value},
            success: (response) => toastr["success"](response.message),
            error: (x) => crud_handle_server_errors(x),
        });
    }
</script>
