@if($actives > 0 || $deductives > 0)
    <script>
        Morris.Donut({
            element: "{{$htmlID}}",
            data: [
                {
                    label: "{{ trans('back.active-var', ['var' => trans('back.'.$type.'.'.$type)]) }}",
                    value: {{$actives}}
                },
                {
                    label: "{{ trans('back.deductive-var', ['var' => trans('back.'.$type.'.'.$type)]) }}",
                    value: {{$deductives}}
                },
            ],
            resize: !0,
            colors: ["#d9539a", "#5b69bc"]
        });
    </script>

@endif
