@if($type == 'password_confirmation')

    <div class="col-xs-{{ isset($col) ? $col : 12 }}">
        <div class="form-valid floating">
            <label for="password_confirmation">{{ isset($slug) ? ucwords($slug) : '' }}</label>
            {!! Form::password('password_confirmation',['class'=>'form-control form-data','dir'=>direction(),'id'=>'password_confirmation']) !!}
        </div>
    </div>

@elseif($type == 'password')

    <div class="col-xs-{{ isset($col) ? $col : 12 }}">
        <div class="form-valid floating">
            <label for="password">{{ isset($slug) ? ucwords($slug) : '' }}</label>
            {!! Form::password('password', ['class' => 'form-control form-data','dir' => direction(),'id' => 'password']) !!}
        </div>
    </div>

@elseif($type == 'select')

    <div class="col-xs-{{ isset($col) ? $col : 12 }}">
        <div class="form-valid floating">
            <label for="{{ $name }}">{{ isset($slug) ? ucwords($slug) : '' }}</label>
            {!! Form::select($name, $list, null, ['class' => 'form-control select2 form-data', 'dir' => direction(), 'id' => $name]) !!}
        </div>
    </div>

@elseif($type == 'multi-select')

    <div class="col-xs-{{ isset($col) ? $col : 12 }}">
        <div class="form-valid floating">
            <label for="{{ explode('[]', $name)[0] }}">{{ isset($slug) ? ucwords($slug) : '' }}</label>
            {!! Form::select($name, $list, null, getMultiSelectForm($name)) !!}
        </div>
    </div>

@elseif($type == 'file')

    <div class="col-xs-{{ isset($col) ? $col : 12 }}">
        <div class="form-valid">
            <label for="{{$name}}">{{ isset($slug) ? ucwords($slug) : '' }}</label><br>
            <input
                type="file"
                accept="application/pdf"
                data-show-errors="true"
                data-errors-position="outside"
                class="dropify form-data {{ $style }}"
                id="{{$name}}"
                name="{{$name}}"
                data-height="300" />
        </div>
    </div>

@elseif($type == 'image')

    <div class="col-xs-{{ isset($col) ? $col : 12 }}">
        <div class="form-valid">
            <label for="image">{{ isset($slug) ? ucwords($slug) : '' }}</label><br>
            <input type="file" class="file-styled {{ $style }}" id="image" accept="image/*" name="{{$name}}">
        </div>
    </div>

@elseif($type == 'textarea')

    <div class="col-xs-{{ isset($col) ? $col : 12 }}">
        <div class="form-valid floating">
            <label for="{{ $name }}">{{ isset($slug) ? ucwords($slug) : '' }}</label>
            {!! Form::textarea($name, null, ['class' => $style, 'style' => 'resize: vertical;', 'dir' => direction(), 'id' => $name]) !!}
        </div>
    </div>

@elseif($type == 'text')

    <div class="col-xs-{{ isset($col) ? $col : 12 }}">
        <div class="form-valid floating">
            <label for="{{ $name }}">{{ isset($slug) ? ucwords($slug) : '' }}</label>
            {!! Form::text($name, null, ['class'=>$style,'dir'=>direction(),'id' => $name, 'maxlength' => isset($attr) ? $attr : '']) !!}
        </div>
    </div>

@elseif($type == 'number' || $type == 'tel')

    <div class="col-xs-{{ isset($col) ? $col : 12 }}">
        <div class="form-valid floating">
            <label for="{{ $name }}">{{ isset($slug) ? ucwords($slug) : '' }}</label>
            {!! Form::{$type}($name, null, ['class' => $style, 'maxlength' => '12', 'dir' => direction(), 'id' => $name, 'min' => 1]) !!}
        </div>
    </div>

@else

    <div class="col-xs-{{ isset($col) ? $col : 12 }}">
        <div class="form-valid floating">
            <label for="{{ $name }}">{{ isset($slug) ? ucwords($slug) : '' }}</label>
            {!! Form::{$type}($name, null, ['class' => $style, 'id' => $name]) !!}
        </div>
    </div>

@endif

<script>
    $(window).load(function(){
        $('#uniform-image').addClass('form-valid');
        $('#uniform-image span.action.btn.bg-pink-400').html('@lang('back.choose-file')');
        $('#uniform-image span.filename').css({'user-select': 'none','font-size': '12px'});
        $('#uniform-image span.filename').html('@lang('back.no-file-selected')');
    });
</script>
