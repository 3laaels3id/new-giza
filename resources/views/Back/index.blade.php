@extends('Back.layouts.master')

@section('title', trans('back.home'))

@section('content')
    <div class="row">
        @foreach(models() as $model => $value)
            <div class="col-xl-4 col-md-6">
                <x-model-count-element :model="$model" :count="getModelCount($model)" :color="$value['color']"></x-model-count-element>
            </div>
        @endforeach
        <div class="col-xl-4 col-md-6">
            <x-model-count-element model="team" :count="getModelCount('team')" color="secondary"></x-model-count-element>
        </div>
    </div>

    <div class="row">
        @foreach(morrisDonutChart() as $i => $model)
            <div class="col-xl-6">
                <x-morris-donut-chart :collection="$$model" :model="$model" htmlID="morris-donut-example-{{$i}}"></x-morris-donut-chart>
            </div>
        @endforeach
    </div>

{{--    <div class="row">--}}
{{--        <div class="col-xl-6">--}}
{{--            <div class="card-box">--}}
{{--                <h4 class="header-title mb-3">اعلي الدورات اشتراكاً</h4>--}}

{{--                <div class="inbox-widget">--}}

{{--                    <div class="inbox-item">--}}
{{--                        <a href="#">--}}
{{--                            <div class="inbox-item-img">--}}
{{--                                <img src="{{ asset('public/admin/images/users/user-1.jpg') }}" class="rounded-circle" alt="">--}}
{{--                            </div>--}}
{{--                            <h5 class="inbox-item-author mt-0 mb-1">Chadengle</h5>--}}
{{--                            <p class="inbox-item-text">Hey! there I'm available...</p>--}}
{{--                            <p class="inbox-item-date">13:40 PM</p>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
@stop

@section('scripts')
    <script src="{{ asset('public/admin/libs/morris-js/morris.min.js') }}"></script>

    @include('Back.includes.morrisDonutChart', [
        'type'         => 'admins',
        'htmlID'       => 'morris-donut-example-0',
        'actives'      => $admin['active_admins']->count(),
        'deductives'   => $admin['deductive_admins']->count(),
    ])

    @include('Back.includes.morrisDonutChart', [
        'type'         => 'users',
        'htmlID'       => 'morris-donut-example-1',
        'actives'      => $user['active_users']->count(),
        'deductives'   => $user['deductive_users']->count(),
    ])
@stop
