@include('Back.includes.translate', ['fields' => [
    translatedField('name', 'text', 'form-name')
]])

<div class="form-group">
    <x-switch-input :model="$currentModel ?? null"></x-switch-input>
</div>
