<x-model-index-page model="sport" modelType="female" :collection="$sports" columns="6">
    <table class="table table-delete-action-now table-bordered dt-responsive nowrap no-footer dtr-inline text-center f-15" id="sports" style="font-size: 16px;">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.form-name')</th>
            <th>@lang('back.form-status')</th>
            <th>@lang('back.since')</th>
            <th>@lang('back.updated_at')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($sports as $i => $sport)
            <tr>
                <td>{{$i+1}}</td>
                <td><a href="{{ route('sports.show', $sport->id) }}">{!! highlightText($sport->name, $search) !!}</a></td>
                <td><x-table-switch-status :model="$sport"></x-table-switch-status></td>
                <td>{{ $sport->since }}</td>
                <td>{{ $sport->last_update }}</td>
                <td><x-table-actions modelName="sport" :model="$sport"></x-table-actions></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</x-model-index-page>
