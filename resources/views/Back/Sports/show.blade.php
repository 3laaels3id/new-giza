<x-model-show-element :title="$sport->name" model="sport" nameSpace="back">
    <div class="col-md-12">
        <div class="card border border-secondary">
            <div class="card-header bg-secondary text-white">@lang('back.sports.t-sport')</div>
            <div class="card-body">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item">
                        <a href="#sport" data-toggle="tab" aria-expanded="false" class="nav-link active">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                            <span class="d-none d-sm-block">@lang('back.sports.t-sport')</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#ages" data-toggle="tab" aria-expanded="false" class="nav-link">
                            <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                            <span class="d-none d-sm-block">@lang('back.ages.ages') ({{ $sport->ages->count() }})</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#subscriptions" data-toggle="tab" aria-expanded="false" class="nav-link">
                            <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                            <span class="d-none d-sm-block">@lang('back.subscriptions.subscriptions') ({{ $sport->subscriptions->count() }})</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade show active" id="sport">
                        @include('includes.flash')
                        <div class="col-md-12">
                            <div class="well">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <h4 class="text-muted">@lang('back.form-name')</h4>
                                        <h5>{{ $sport->name ?? trans('back.no-value') }}</h5>
                                    </li>

                                    <li class="list-group-item">
                                        <h4 class="text-muted">@lang('back.form-status')</h4>
                                        <h5><x-table-switch-status :model="$sport"></x-table-switch-status></h5>
                                    </li>

                                    <li class="list-group-item">
                                        <h4 class="text-muted">@lang('back.since')</h4>
                                        <h5>{{ $sport->since }}</h5>
                                    </li>

                                    <li class="list-group-item">
                                        <h4 class="text-muted">@lang('back.updated_at')</h4>
                                        <h5>{{ $sport->last_update }}</h5>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="ages">
                        <h4>{{ trans('back.sport-total-max-players-var', ['var' => $sport->ages->sum('maximum')]) }}</h4><br>
                        <div class="table-responsive">
                            <table class="table text-center">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('back.form-name')</th>
                                    <th>@lang('back.form-age-type')</th>
                                    <th>@lang('back.form-maximum-players')</th>
                                    <th>@lang('back.since')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($sport->ages as $i => $age)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td><a href="{{ route('ages.show', $age->id) }}">{{ $age->name }}</a></td>
                                        <td>{{ trans('back.' . $age->type) }}</td>
                                        <td>{{ $age->maximum }}</td>
                                        <td>{{ $age->since }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="10">
                                            <div class="alert alert-info">@lang('back.no-value')</div>
                                        </td>
                                    </tr>
                                @endforelse
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('back.form-name')</th>
                                    <th>@lang('back.form-age-type')</th>
                                    <th>@lang('back.form-maximum-players')</th>
                                    <th>@lang('back.since')</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="subscriptions">
                        <div class="table-responsive">
                            <table class="table text-center">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('back.subscriptions.t-subscription')</th>
                                    <th>@lang('back.users.t-user')</th>
                                    <th>@lang('back.invitations.t-invitation')</th>
                                    <th>@lang('back.tournaments.t-tournament')</th>
                                    <th>@lang('back.since')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($sport->subscriptions as $i => $subscription)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>
                                            <a class="btn btn-dark" data-toggle="tooltip" title="@lang('back.subscriptions.t-subscription')" href="{{ route('subscriptions.show', $subscription->id) }}">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('users.show', $subscription->user_id) }}">
                                                {{ ucwords($subscription->user->full_name) }}
                                            </a>
                                        </td>
                                        <td>{{ $subscription->invitations->count() }}</td>
                                        <td>
                                            <a href="{{ route('tournaments.show', $subscription->id) }}">
                                                {{ ucwords($subscription->tournament->name) }}
                                            </a>
                                        </td>
                                        <td>{{ $subscription->since }}</td>
                                    </tr>
                                @empty
                                    <x-table-alert-no-value></x-table-alert-no-value>
                                @endforelse
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('back.subscriptions.t-subscription')</th>
                                    <th>@lang('back.users.t-user')</th>
                                    <th>@lang('back.invitations.t-invitation')</th>
                                    <th>@lang('back.tournaments.t-tournament')</th>
                                    <th>@lang('back.since')</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-model-show-element>
