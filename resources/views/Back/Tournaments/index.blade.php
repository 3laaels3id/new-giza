<x-model-index-page model="tournament" modelType="female" :collection="$tournaments" columns="8">
    <table class="table table-delete-action-now table-bordered dt-responsive nowrap no-footer dtr-inline text-center f-15" id="tournaments" style="font-size: 16px;">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.form-name')</th>
            <th>@lang('back.form-status')</th>
            <th>@lang('back.form-start-date')</th>
            <th>@lang('back.form-end-date')</th>
            <th>@lang('back.end-status')</th>
            <th>@lang('back.sports.sports')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($tournaments as $i => $tournament)
            <tr>
                <td>{{$i+1}}</td>
                <td><a href="{{ route('tournaments.show', $tournament->id) }}">{!! highlightText($tournament->name, $search) !!}</a></td>
                <td><x-table-switch-status :model="$tournament"></x-table-switch-status></td>
                <td>{{ $tournament->start_date }}</td>
                <td>{{ $tournament->end_date }}</td>
                <td>
                    @if($tournament->formatted_end_date->isPast())
                        <span class="badge badge-success p-2 f-17">@lang('back.finished')</span>
                    @elseif($tournament->formatted_start_date->isFuture())
                        <span class="badge badge-warning p-2 f-17">@lang('back.not-started')</span>
                    @else
                        <span class="badge badge-danger p-2 f-17">@lang('back.started')</span>
                    @endif
                </td>
                <td>
                    <span class="badge badge-{{ $tournament->sports->count() == 0 ? 'secondary' : 'primary' }} p-2 f-16">
                        {{ $tournament->sports->count() }}
                    </span>
                </td>
                <td>
                    @if($tournament->formatted_start_date->isFuture())
                        <a href="{{ route('tournaments.edit', $tournament->id) }}"
                           class="btn btn-success"
                           data-toggle="tooltip" title="@lang('back.edit')"><i class="fa fa-edit fa-1x"></i></a>
                    @endif

                    <a data-id="{{ $tournament->id }}"
                       href="javascript:void(0);"
                       data-toggle="tooltip" title="@lang('back.delete')"
                       class="btn btn-danger delete-action"><i class="fa fa-trash fa-1x"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</x-model-index-page>
