@include('Back.includes.translate', ['fields' => [translatedField('name', 'text', 'form-name')]])

<div class="form-group">
    <x-form-inputs name="maximum_sports" type="number" :slug="trans('back.form-maximum-sports')"></x-form-inputs>
</div>

<div class="form-group">
    <x-form-inputs type="date" name="start_date" :slug="trans('back.form-start-date')"></x-form-inputs>
</div>

<div class="form-group">
    <x-form-inputs type="date" name="end_date" :slug="trans('back.form-end-date')"></x-form-inputs>
</div>

<div class="form-group">
    <x-switch-input :model="$currentModel ?? null"></x-switch-input>
</div>
