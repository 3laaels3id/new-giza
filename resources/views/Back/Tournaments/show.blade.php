<x-model-show-element :title="$tournament->name" model="tournament" nameSpace="back">
    <div class="col-md-12">
        <div class="card border border-secondary">
            <div class="card-header bg-secondary text-white">@lang('back.tournaments.t-tournament')</div>
            <div class="card-body">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item">
                        <a href="#tournament" data-toggle="tab" aria-expanded="false" class="nav-link active">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                            <span class="d-none d-sm-block">@lang('back.tournaments.t-tournament')</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#sports" data-toggle="tab" aria-expanded="false" class="nav-link">
                            <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                            <span class="d-none d-sm-block">@lang('back.sports.sports') ({{ $tournament->sports->count() }})</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade show active" id="tournament">
                        @include('includes.flash')
                        <div class="col-md-12">
                            <div class="well">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <h4 class="text-muted">@lang('back.form-name')</h4>
                                        <h5>{{ $tournament->name ?? trans('back.no-value') }}</h5>
                                    </li>

                                    <li class="list-group-item">
                                        <h4 class="text-muted">@lang('back.form-start-date')</h4>
                                        <h5>{{ $tournament->formatted_start_date->format('Y-m-d') ?? trans('back.no-value') }}</h5>
                                    </li>

                                    <li class="list-group-item">
                                        <h4 class="text-muted">@lang('back.form-end-date')</h4>
                                        <h5>{{ $tournament->formatted_end_date->format('Y-m-d') ?? trans('back.no-value') }}</h5>
                                    </li>

                                    <li class="list-group-item">
                                        <h4 class="text-muted">@lang('back.form-maximum-sports')</h4>
                                        <h5>{{ $tournament->maximum_sports ?? trans('back.no-value') }}</h5>
                                    </li>

                                    <li class="list-group-item">
                                        <h4 class="text-muted">@lang('back.end-status')</h4>
                                        @if($tournament->formatted_end_date->isPast())
                                            <span class="badge badge-success p-2 f-17">@lang('back.finished')</span>
                                        @elseif($tournament->formatted_start_date->isFuture())
                                            <span class="badge badge-warning p-2 f-17">@lang('back.not-started')</span>
                                        @else
                                            <span class="badge badge-danger p-2 f-17">@lang('back.started')</span>
                                        @endif
                                    </li>

                                    <li class="list-group-item">
                                        <h4 class="text-muted">@lang('back.form-status')</h4>
                                        <h5><x-table-switch-status :model="$tournament"></x-table-switch-status></h5>
                                    </li>

                                    <li class="list-group-item">
                                        <h4 class="text-muted">@lang('back.since')</h4>
                                        <h5>{{ $tournament->since }}</h5>
                                    </li>

                                    <li class="list-group-item">
                                        <h4 class="text-muted">@lang('back.updated_at')</h4>
                                        <h5>{{ $tournament->last_update }}</h5>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="sports">
                        @if($tournament->maximum_sports > $tournament->sports->count())
                            <button onclick="window.location.href = '{{ route('tournaments.sport-create') }}';" class="btn btn-primary mb-3" title="{{ transCreate('sport') }}" data-toggle="tooltip">+</button>
                        @endif
                        @if($tournament->sports->count() > 0)
                            <button onclick="window.location.href = '{{ route('tournaments.sport-edit', $tournament->id) }}';" class="btn btn-success mb-3" title="{{ trans('back.tournament-edit-sports') }}" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
                        @endif
                        <div class="table-responsive">
                            <table class="table text-center">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('back.form-name')</th>
                                    <th>@lang('back.since')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($tournament->sports as $i => $sport)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td><a href="{{ route('sports.show', $sport->id) }}">{{ $sport->name }}</a></td>
                                        <td>{{ $sport->since }}</td>
                                    </tr>
                                @empty
                                    <x-table-alert-no-value></x-table-alert-no-value>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-model-show-element>
