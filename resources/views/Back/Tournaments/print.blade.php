<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('Back.layouts.partials.styles')
    <title>Document</title>
</head>
<body>
<div class="container">
    <h3>Tournaments</h3>
    <table class="table table-delete-action-now table-bordered text-center dt-responsive nowrap no-footer dtr-inline f-16" id="admins">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.form-name')</th>
            <th>@lang('back.form-start-date')</th>
            <th>@lang('back.form-end-date')</th>
            <th>@lang('back.sports.sports')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($tournaments as $i => $tournament)
            <tr>
                <td>{{$i+1}}</td>
                <td>{{ $tournament->name }}</td>
                <td>{{ $tournament->start_date }}</td>
                <td>{{ $tournament->end_date }}</td>
                <td>{{ $tournament->sports->count() }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>
@include('Back.layouts.partials.scripts')
<script>
    $(window).on('load', function(){
        window.print();
    });
</script>
</body>
</html>
