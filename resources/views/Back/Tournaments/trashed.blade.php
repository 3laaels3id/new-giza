<x-model-trashed-page model="tournament" modelType="female">
    <table class="table table-bordered dt-responsive nowrap no-footer dtr-inline f-16" id="tournaments">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.form-name')</th>
            <th>@lang('back.form-status')</th>
            <th>@lang('back.since')</th>
            <th>@lang('back.deleted_at')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody>
        @forelse($trashes as $key => $tournament)
            <tr id="sport-row-{{ $tournament->id }}">
                <td>{{ $key+1 }}</td>

                <td>{{ $tournament->name ?? trans('back.no-value') }}</td>

                <td>
                    @if($tournament->status == 1)
                        <label class="badge badge-success f-16">@lang('back.active')</label>
                    @else
                        <label class="badge badge-danger f-16">@lang('back.disactive')</label>
                    @endif
                </td>

                <td>{{ $tournament->created_at->diffForHumans() }}</td>

                <td>{{ $tournament->deleted_at->diffForHumans() }}</td>

                <td class="text-center">
                    <x-trash-menu table="tournaments" :model="$tournament"></x-trash-menu>
                </td>
            </tr>
        @empty
            <x-table-alert-no-value></x-table-alert-no-value>
        @endforelse
        </tbody>
    </table>
</x-model-trashed-page>
