<div class="row">
    @if(Route::is('subscriptions.create'))
        <div class="col-md-12">
            <div class="form-group">
                <x-select-input name="tournament_id" :arr="$tournaments" :slug="trans('back.tournaments.t-tournament')"></x-select-input>
            </div>

            <div class="form-group">
                <x-select-input name="user_id" :arr="$users" :slug="trans('back.users.t-user')"></x-select-input>
            </div>

            <div class="form-group" id="sport_id_el">
                <x-select-input name="sport_id" :arr="$sports" :slug="trans('back.sports.t-sport')"></x-select-input>
            </div>

            <div class="form-group" id="age_id_el">
                <x-select-input name="age_id" :arr="$ages" :slug="trans('back.ages.t-age')"></x-select-input>
            </div>

            <div class="form-group">
                <x-form-inputs type="text" name="team_name" :slug="trans('back.form-team-name')"></x-form-inputs>
            </div>
        </div>
    @endif

    <div class="col-md-12" style="margin: 10px;"></div>

    <div class="col-md-12">
        @if(Route::is('subscriptions.create'))
            <div class="card border border-secondary">
                <div class="card-body text-secondary">
                    <h5 class="card-title text-secondary">
                        @lang('back.invitations.invitation')
                        <button type="button" class="btn btn-primary f-17 m-1" id="addInvitationField">+</button>
                        <span style="margin: 0 5px;">@lang('back.invitations.invitations')</span>
                        <span id="invitationsCounter" class="badge badge-dark p-2 f-17">
                        @isset($currentModel) {{ $currentModel->invitations->count() }} @else 1 @endisset
                    </span>
                        <span style="margin: 0 5px;">@lang('back.total')</span>
                        <span id="invitationsTotalCounter" class="badge badge-danger p-2 f-17">
                        @isset($currentModel) {{ $currentModel->invitations->count() + 1 }} @else 2 @endisset
                    </span>
                    </h5>
                    <div class="form-group">
                        <div class="form-valid floating">
                            <label for="invitations_fname[0]">{{trans('back.form-first-name')}}</label>
                            <input type="text" value="@if(isset($currentModel) && $currentModel->invitations->count() > 0){{ $currentModel->invitations->first()->fname ?? '' }}@endif" class="form-control form-data" id="invitations_fname[0]" name="invitations_fname[0]">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-valid floating">
                            <label for="invitations_lname[0]">{{trans('back.form-last-name')}}</label>
                            <input type="text" value="@if(isset($currentModel) && $currentModel->invitations->count() > 0){{ $currentModel->invitations->first()->lname ?? '' }}@endif" class="form-control form-data" id="invitations_lname[0]" name="invitations_lname[0]">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-valid floating">
                            <label for="invitations_email[0]">{{trans('back.form-email')}}</label>
                            <input type="email" value="@if(isset($currentModel) && $currentModel->invitations->count() > 0){{ $currentModel->invitations->first()->email ?? '' }}@endif" class="form-control form-data" id="invitations_email[0]" name="invitations_email[0]">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-valid floating">
                            <label for="invitations_phone[0]">{{trans('back.form-phone')}}</label>
                            <input type="text" value="@if(isset($currentModel) && $currentModel->invitations->count() > 0){{ $currentModel->invitations->first()->phone ?? '' }}@endif" class="form-control form-data" id="invitations_phone[0]" name="invitations_phone[0]">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-valid floating">
                            <label for="invitations_age[0]">{{trans('back.form-age')}}</label>
                            <input type="text" value="@if(isset($currentModel) && $currentModel->invitations->count() > 0){{ $currentModel->invitations->first()->age ?? '' }}@endif" class="form-control form-data" id="invitations_age[0]" name="invitations_age[0]"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-valid floating">
                            <label for="invitations_identity[0]">{{trans('back.form-identity')}}</label>
                            <input type="text" value="@if(isset($currentModel) && $currentModel->invitations->count() > 0){{ $currentModel->invitations->first()->identity ?? '' }}@endif" class="form-control form-data" id="invitations_identity[0]" name="invitations_identity[0]"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-valid floating">
                            <label for="invitations_birth_date[0]">{{trans('back.form-birth-date')}}</label>
                            <input type="date" value="@if(isset($currentModel) && $currentModel->invitations->count() > 0){{ $currentModel->invitations->first()->birth_date->format('Y-m-d') ?? '' }}@endif" class="form-control form-data" id="invitations_birth_date[0]" name="invitations_birth_date[0]"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="form-valid floating">
                                <label for="invitations_address[0]">{{trans('back.form-address')}}</label>
                                <select name="invitations_address[0]" class="form-control select2 form-data" id="invitations_address[0]">
                                    <option value="" selected>@lang('back.select-a-value')</option>
                                    @forelse($addresses as $key => $name)
                                        <option {{ isset($currentModel) && $currentModel->invitations->count() > 0 ? ($currentModel->invitations->first()->address == $key ? 'selected' : '') : '' }} value="{{ $key }}">{{ $name }}</option>
                                    @empty
                                        <option value="" disabled>@lang('back.no-value')</option>
                                    @endforelse
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            @if(isset($currentModel) && $currentModel->invitations->count() > 0)
                <div class="card border border-secondary">
                    <div class="card-body text-secondary">
                        <h5 class="card-title text-secondary">
                            @lang('back.invitations.invitation')
                            <button type="button" class="btn btn-primary f-17 m-1" id="addInvitationField">+</button>
                            <span style="margin: 0 5px;">@lang('back.invitations.invitations')</span>
                            <span id="invitationsCounter" class="badge badge-dark p-2 f-17">
                        @isset($currentModel) {{ $currentModel->invitations->count() }} @else 1 @endisset
                    </span>
                            <span style="margin: 0 5px;">@lang('back.total')</span>
                            <span id="invitationsTotalCounter" class="badge badge-danger p-2 f-17">
                        @isset($currentModel) {{ $currentModel->invitations->count() + 1 }} @else 2 @endisset
                    </span>
                        </h5>
                        <div class="form-group">
                            <div class="form-valid floating">
                                <label for="invitations_fname[0]">{{trans('back.form-first-name')}}</label>
                                <input type="text" value="@if(isset($currentModel) && $currentModel->invitations->count() > 0){{ $currentModel->invitations->first()->fname ?? '' }}@endif" class="form-control form-data" id="invitations_fname[0]" name="invitations_fname[0]">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-valid floating">
                                <label for="invitations_lname[0]">{{trans('back.form-last-name')}}</label>
                                <input type="text" value="@if(isset($currentModel) && $currentModel->invitations->count() > 0){{ $currentModel->invitations->first()->lname ?? '' }}@endif" class="form-control form-data" id="invitations_lname[0]" name="invitations_lname[0]">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-valid floating">
                                <label for="invitations_email[0]">{{trans('back.form-email')}}</label>
                                <input type="email" value="@if(isset($currentModel) && $currentModel->invitations->count() > 0){{ $currentModel->invitations->first()->email ?? '' }}@endif" class="form-control form-data" id="invitations_email[0]" name="invitations_email[0]">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-valid floating">
                                <label for="invitations_phone[0]">{{trans('back.form-phone')}}</label>
                                <input type="text" value="@if(isset($currentModel) && $currentModel->invitations->count() > 0){{ $currentModel->invitations->first()->phone ?? '' }}@endif" class="form-control form-data" id="invitations_phone[0]" name="invitations_phone[0]">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-valid floating">
                                <label for="invitations_age[0]">{{trans('back.form-age')}}</label>
                                <input type="text" value="@if(isset($currentModel) && $currentModel->invitations->count() > 0){{ $currentModel->invitations->first()->age ?? '' }}@endif" class="form-control form-data" id="invitations_age[0]" name="invitations_age[0]"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-valid floating">
                                <label for="invitations_identity[0]">{{trans('back.form-identity')}}</label>
                                <input type="text" value="@if(isset($currentModel) && $currentModel->invitations->count() > 0){{ $currentModel->invitations->first()->identity ?? '' }}@endif" class="form-control form-data" id="invitations_identity[0]" name="invitations_identity[0]"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-valid floating">
                                <label for="invitations_birth_date[0]">{{trans('back.form-birth-date')}}</label>
                                <input type="date" value="@if(isset($currentModel) && $currentModel->invitations->count() > 0){{ $currentModel->invitations->first()->birth_date->format('Y-m-d') ?? '' }}@endif" class="form-control form-data" id="invitations_birth_date[0]" name="invitations_birth_date[0]"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="form-valid floating">
                                    <label for="invitations_address[0]">{{trans('back.form-address')}}</label>
                                    <select name="invitations_address[0]" class="form-control select2 form-data" id="invitations_address[0]">
                                        <option value="" selected>@lang('back.select-a-value')</option>
                                        @forelse($addresses as $key => $name)
                                            <option {{ isset($currentModel) && $currentModel->invitations->count() > 0 ? ($currentModel->invitations->first()->address == $key ? 'selected' : '') : '' }} value="{{ $key }}">{{ $name }}</option>
                                        @empty
                                            <option value="" disabled>@lang('back.no-value')</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="alert alert-info text-center">@lang('back.subscription-has-no-invitations')</div>
            @endif
        @endif
    </div>

    <div class="col-md-12 row" id="resultInvitations">
        @if(isset($currentModel) && $currentModel->invitations->count() > 0)
            @foreach($currentModel->invitations as $index => $invitation)
                @continue($index === 0)
                <div class="col-md-6" id="invitations-field-row-{{$index}}">
                    <div class="card border border-secondary">
                        <div class="card-body text-secondary">
                            <h5 class="card-title text-secondary">
                                @lang('back.invitations.invitation')
                                <button data-id="{{$index}}" type="button" class="btn btn-danger removeInvitationItem f-17 m-2" style="font-weight: bold;">-</button>
                            </h5>
                            <div class="form-group">
                                <div class="form-valid floating">
                                    <label for="invitations_fname[{{$index}}]">{{trans('back.form-first-name')}}</label>
                                    <input type="text" value="{{ $invitation->fname }}" class="form-control form-data" id="invitations_fname[{{$index}}]" name="invitations_fname[{{$index}}]">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-valid floating">
                                    <label for="invitations_lname[{{$index}}]">{{trans('back.form-last-name')}}</label>
                                    <input type="text" value="{{ $invitation->lname }}" class="form-control form-data" id="invitations_lname[{{$index}}]" name="invitations_lname[{{$index}}]">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-valid floating">
                                    <label for="invitations_email[{{ $index }}]">{{trans('back.form-email')}}</label>
                                    <input type="email" value="{{ $invitation->email }}" class="form-control form-data" id="invitations_email[{{ $index }}]" name="invitations_email[{{ $index }}]">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-valid floating">
                                    <label for="invitations_phone[{{ $index }}]">{{trans('back.form-phone')}}</label>
                                    <input type="text" value="{{ $invitation->phone }}" class="form-control form-data" id="invitations_phone[{{ $index }}]" name="invitations_phone[{{ $index }}]">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-valid floating">
                                    <label for="invitations_age[{{$index}}]">{{trans('back.form-age')}}</label>
                                    <input type="text" value="{{ $invitation->age }}" class="form-control form-data" id="invitations_age[{{$index}}]" name="invitations_age[{{$index}}]"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-valid floating">
                                    <label for="invitations_identity[{{$index}}]">{{trans('back.form-identity')}}</label>
                                    <input type="text" value="{{ $invitation->identity }}" class="form-control form-data" id="invitations_identity[{{$index}}]" name="invitations_identity[{{$index}}]"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-valid floating">
                                    <label for="invitations_birth_date[{{$index}}]">{{trans('back.form-birth-date')}}</label>
                                    <input type="date" value="{{ $invitation->birth_date->format('Y-m-d') }}" class="form-control form-data" id="invitations_birth_date[{{$index}}]" name="invitations_birth_date[{{$index}}]"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="form-valid floating">
                                        <label for="invitations_address[{{$index}}]">{{trans('back.form-address')}}</label>
                                        <select name="invitations_address[{{$index}}]" class="select2-hidden-accessible form-control select2 form-data" id="invitations_address[{{$index}}]">
                                            <option value="" selected>@lang('back.select-a-value')</option>
                                            @forelse($addresses as $key => $name)
                                                <option {{ $invitation->address == $key ? 'selected' : '' }} value="{{ $key }}">{{ $name }}</option>
                                            @empty
                                                <option value="" disabled>@lang('back.no-value')</option>
                                            @endforelse
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</div>

@section('scripts')
    <script>
        let sport_id = $('select#sport_id');
        let age_id = $('select#age_id');
        let counter = @isset($currentModel) {{ $currentModel->invitations->count() + 1 }} @else 1 @endisset;
        let totalCount = @isset($currentModel) {{ $currentModel->invitations->count() + 2 }} @else 2 @endisset;

        $('select#tournament_id').on('change', function(){
            let _token = '{{ csrf_token() }}';
            sport_id.html(``);
            $.ajax({
                method: 'POST',
                url: '{{ route('subscriptions.ajax-get-sports-by-tournament-id') }}',
                data: {"tournament_id": $(this).val(), "_token": _token},
                success: response => {
                    let html = `<option value="" selected disabled>@lang('back.select-a-value')</option>`;
                    $.each(response.data, function (index, el) {
                        html += `<option value="${el.id}">${el.name}</option>`;
                    });
                    sport_id.html(html);
                },
                error: err => console.log(err),
            });
        });

        sport_id.on('change', function(){
            let _token = '{{ csrf_token() }}';
            age_id.html(``);
            $.ajax({
                method: 'POST',
                url: '{{ route('subscriptions.ajax-get-ages-by-sport-id') }}',
                data: {"sport_id": $(this).val(), "_token": _token},
                success: response => {
                    let html = `<option value="" selected disabled>@lang('back.select-a-value')</option>`;
                    $.each(response.data, function (index, el) {
                        html += `<option value="${el.id}">${el.name}</option>`;
                    });
                    age_id.html(html);
                },
                error: err => console.log(err),
            });
        });

        function increment(number) {
            return number++;
        }

        function decrement(number) {
            return --number;
        }

        //======================================================

        @if(Route::is('subscriptions.create'))
            $('#resultInvitations').html(``);
        @endif

        @isset($currentModel)
            $(window).on('load', function(){
                let tournament_id = {{ $currentModel->tournament_id }};
                $.ajax({
                    method: 'POST',
                    url: '{{ route('subscriptions.ajax-get-sports-by-tournament-id') }}',
                    data: {_token: '{{ csrf_token() }}', tournament_id: tournament_id},
                    success: response => {
                        let html = `<option value="" selected disabled>@lang('back.select-a-value')</option>`;
                        $.each(response.data, function (index, el) {
                            html += `<option ${tournament_id === el.id ? 'selected' : ''} value="${el.id}">${el.name}</option>`;
                        });
                        sport_id.html(html);
                    },
                    error: err => console.log(err),
                });
            });
        @endisset

        let count = @if(Route::is('subscriptions.edit')) {{ $currentModel->invitations->count() }} @else 1 @endif;

        let addresses = @json($addresses);

        let html = ``;

        if(jQuery.isEmptyObject(addresses))
        {
            html = `<option value="0" selected disabled>@lang('back.no-value')</option>`;
        }
        else
        {
            $.each(addresses, function (index, el) {
                html += `<option value="${index}">${el}</option>`;
            });
        }

        function dynSportsField(number)
        {
            $('#resultInvitations').append(`
                <div class="col-md-6" id="invitations-field-row-${number}">
                    <div class="card border border-secondary">
                        <div class="card-body text-secondary">
                            <h5 class="card-title text-secondary">
                                @lang('back.invitations.invitation')
                                <button data-id="${number}" type="button" onClick="decrement('${number}');" class="btn btn-danger removeInvitationItem f-17 m-2" style="font-weight: bold;">-</button>
                            </h5>
                            <div class="form-group">
                                <div class="form-valid floating">
                                    <label for="invitations_fname[${number}]">{{trans('back.form-first-name')}}</label>
                                    <input type="text" value="" class="form-control form-data" id="invitations_fname[${number}]" name="invitations_fname[${number}]">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-valid floating">
                                    <label for="invitations_lname[${number}]">{{trans('back.form-last-name')}}</label>
                                    <input type="text" value="" class="form-control form-data" id="invitations_lname[${number}]" name="invitations_lname[${number}]">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-valid floating">
                                    <label for="invitations_email[${number}]">{{trans('back.form-email')}}</label>
                                    <input type="email" value="" class="form-control form-data" id="invitations_email[${number}]" name="invitations_email[${number}]">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-valid floating">
                                    <label for="invitations_phone[${number}]">{{trans('back.form-phone')}}</label>
                                    <input type="text" value="" class="form-control form-data" id="invitations_phone[${number}]" name="invitations_phone[${number}]">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-valid floating">
                                    <label for="invitations_age[${number}]">{{trans('back.form-age')}}</label>
                                    <input type="text" value="" class="form-control form-data" id="invitations_age[${number}]" name="invitations_age[${number}]"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-valid floating">
                                    <label for="invitations_identity[${number}]">{{trans('back.form-identity')}}</label>
                                    <input type="text" value="" class="form-control form-data" id="invitations_identity[${number}]" name="invitations_identity[${number}]"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-valid floating">
                                    <label for="invitations_birth_date[${number}]">{{trans('back.form-birth-date')}}</label>
                                    <input type="date" class="form-control form-data" id="invitations_birth_date[${number}]" name="invitations_birth_date[${number}]"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="form-valid floating">
                                        <label for="invitations_address[${number}]">{{trans('back.form-address')}}</label>
                                        <select name="invitations_address[${number}]" class="form-control select2 form-data" id="invitations_address[${number}]">
                                            <option value="" selected>@lang('back.select-a-value')</option>
                                            ${html}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `);

            $(".select2").select2();
        }

        let removeInvitationItem = function()
        {
            let invitation_id = $(this).data('id');

            let _token = '{{ csrf_token() }}';

            if(invitation_id !== undefined) {
                $.ajax({
                    url: '{{ route('subscriptions.ajax-remove-invitation') }}',
                    method: 'POST',
                    data: { invitation_id, _token },
                    success: () => {
                        swal({
                            title: "@lang('back.a-message')",
                            text: "Done",
                            icon: "success",
                            button: "O.K",
                        });
                    },
                    error: err => console.log(err)
                });
            }

            $('#invitations-field-row-' + $(this).data('id')).remove();

            $('#invitationsCounter').html(--counter);

            $('#invitationsTotalCounter').html(--totalCount);
        };

        $('button#addInvitationField').on('click', function(){
            $('#invitationsCounter').html(++counter);

            $('#invitationsTotalCounter').html(++totalCount);

            dynSportsField(count++);
        });

        $(document).on('click', '.removeInvitationItem', removeInvitationItem);
    </script>
@stop
