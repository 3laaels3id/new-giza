<x-model-show-element :title="$subscription->user->full_name" model="subscription" nameSpace="back">
    <div class="col-md-12">
        <div class="card border border-secondary">
            <div class="card-header bg-secondary text-white">@lang('back.subscriptions.t-subscription')</div>
            <div class="card-body">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item">
                        <a href="#subscription" data-toggle="tab" aria-expanded="false" class="nav-link active">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                            <span class="d-none d-sm-block">@lang('back.subscriptions.t-subscription')</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#invitations" data-toggle="tab" aria-expanded="false" class="nav-link">
                            <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                            <span class="d-none d-sm-block">@lang('back.invitations.invitations') ({{ $subscription->invitations->count() }})</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade show active" id="subscription">
                        @include('includes.flash')
                        <div class="col-md-12">
                            <div class="well">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <h4 class="text-muted">@lang('back.tournaments.t-tournament')</h4>
                                        <h5>
                                            <a href="{{ route('tournaments.show', $subscription->tournament_id) }}">
                                                {{ $subscription->tournament->name ?? trans('back.no-value') }}
                                            </a>
                                        </h5>
                                    </li>

                                    <li class="list-group-item">
                                        <h4 class="text-muted">@lang('back.sports.t-sport')</h4>
                                        <h5>
                                            <a href="{{ route('sports.show', $subscription->sport_id) }}">
                                                {{ $subscription->sport->name ?? trans('back.no-value') }}
                                            </a>
                                        </h5>
                                    </li>

                                    <li class="list-group-item">
                                        <h4 class="text-muted">@lang('back.ages.t-age')</h4>
                                        <h5>
                                            <a href="{{ route('ages.show', $subscription->age_id) }}">
                                                {{ $subscription->age->name ?? trans('back.no-value') }}
                                            </a>
                                        </h5>
                                    </li>

                                    <li class="list-group-item">
                                        <h4 class="text-muted">@lang('back.users.t-user')</h4>
                                        <h5>
                                            <a href="{{ route('users.show', $subscription->user_id) }}">
                                                {{ $subscription->user->full_name ?? trans('back.no-value') }}
                                            </a>
                                        </h5>
                                    </li>

                                    @if($subscription->team)
                                        <li class="list-group-item">
                                            <h4 class="text-muted">@lang('back.form-team-name')</h4>
                                            <h5>{{ ucwords($subscription->team->name ?? trans('back.no-value')) }}</h5>
                                        </li>

                                        <li class="list-group-item">
                                            <h4 class="text-muted">@lang('back.team-total')</h4>
                                            <h5>{{ $subscription->team->total ?? trans('back.no-value') }}</h5>
                                        </li>
                                    @endif

                                    <li class="list-group-item">
                                        <h4 class="text-muted">@lang('back.end-status')</h4>
                                        @if($subscription->tournament->formatted_end_date->isPast())
                                            <span class="badge badge-success p-2 f-17">@lang('back.finished')</span>
                                        @elseif($subscription->tournament->formatted_start_date->isFuture())
                                            <span class="badge badge-warning p-2 f-17">@lang('back.not-started')</span>
                                        @else
                                            <span class="badge badge-danger p-2 f-17">@lang('back.started')</span>
                                        @endif
                                    </li>

                                    <li class="list-group-item">
                                        <h4 class="text-muted">@lang('back.since')</h4>
                                        <h5>{{ $subscription->since }}</h5>
                                    </li>

                                    <li class="list-group-item">
                                        <h4 class="text-muted">@lang('back.updated_at')</h4>
                                        <h5>{{ $subscription->last_update }}</h5>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="invitations">
                        <div class="table-responsive">
                            <table class="table text-center">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('back.form-first-name')</th>
                                    <th>@lang('back.form-last-name')</th>
                                    <th>@lang('back.form-serial-number')</th>
                                    <th>@lang('back.form-age')</th>
                                    <th>@lang('back.print')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($subscription->invitations as $i => $invitation)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>{{ $invitation->fname }}</td>
                                        <td>{{ $invitation->lname }}</td>
                                        <td>{{ $invitation->serial_number }}</td>
                                        <td>{{ $invitation->age }}</td>
                                        <td>
                                            <button onclick="window.location.href='{{ route('subscriptions.print-invitation', $invitation->id) }}';" type="button" class="btn btn-dark">
                                                <i class="fa fa-print"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @empty
                                    <x-table-alert-no-value></x-table-alert-no-value>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-model-show-element>
