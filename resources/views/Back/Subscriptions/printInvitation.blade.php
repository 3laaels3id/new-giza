<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('Back.layouts.partials.styles')
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="card border border-dark" style="margin: 10px;">
            <div class="card-header bg-dark text-white">@lang('back.invitations.t-invitation')</div>
            <div class="card-body" style="padding: 20px;">
                <div class="row">
                    <div class="col-md-4">
                        <h4>Player</h4>
                        <div class="well">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.form-name')</h4>
                                    <h5>{{ $invitation->subscription->user->full_name }}</h5>
                                </li>
                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.form-age')</h4>
                                    <h5>{{ $invitation->subscription->user->age }}</h5>
                                </li>
                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.form-address')</h4>
                                    <h5>{{ trans('back.' . $invitation->subscription->user->address) }}</h5>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h4>Invitation</h4>
                        <div class="well">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.form-name')</h4>
                                    <h5>{{ $invitation->fname . ' ' . $invitation->lname}}</h5>
                                </li>
                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.form-age')</h4>
                                    <h5>{{ $invitation->age }}</h5>
                                </li>
                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.form-serial-number')</h4>
                                    <h5>{{ $invitation->serial_number }}</h5>
                                </li>
                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.form-phone')</h4>
                                    <h5>{{ $invitation->phone }}</h5>
                                </li>
                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.form-email')</h4>
                                    <h5>{{ $invitation->email }}</h5>
                                </li>
                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.form-identity')</h4>
                                    <h5>{{ $invitation->identity }}</h5>
                                </li>
                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.form-address')</h4>
                                    <h5>{{ trans('back.' . $invitation->address) }}</h5>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h4>Tournament</h4>
                        <div class="well">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.tournaments.t-tournament')</h4>
                                    <h5>{{ $invitation->subscription->tournament->name }}</h5>
                                </li>
                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.sports.t-sport')</h4>
                                    <h5>{{ $invitation->subscription->sport->name }}</h5>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('Back.layouts.partials.scripts')
    <script>
        $(window).on('load', function(){
            window.print();
        });
    </script>
</body>
</html>
