<x-model-index-page model="subscription" :collection="$subscriptions" modelType="male" columns="8">
    <table class="table table-delete-action-now table-bordered dt-responsive nowrap no-footer dtr-inline text-center f-16" id="subscriptions">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.subscriptions.t-subscription')</th>
            <th>@lang('back.users.t-user')</th>
            <th>@lang('back.tournaments.t-tournament')</th>
            <th>@lang('back.ages.t-age')</th>
            <th>@lang('back.sports.t-sport')</th>
            <th>@lang('back.form-team-name')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($subscriptions as $i => $subscription)
            <tr>
                <td>{{$i+1}}</td>
                <td>
                    <a href="{{ route('subscriptions.show', $subscription->id) }}">
                        <span class="badge badge-secondary f-17 p-2" data-toggle="tooltip" title="@lang('back.show')"><i class="fa fa-eye"></i></span>
                    </a>
                </td>
                <td>
                    <a href="{{ route('users.show', $subscription->user_id) }}">
                        {!! highlightText($subscription->user->full_name, $search) !!}
                    </a>
                </td>
                <td>
                    <a href="{{ route('tournaments.show', $subscription->tournament_id) }}">
                        {!! highlightText($subscription->tournament->name, $search) !!}
                    </a>
                </td>
                <td>{{ $subscription->age->name }}</td>
                <td>{{ $subscription->sport->name }}</td>
                <td>{{ $subscription->team->name ?? trans('back.no-value') }}</td>
                <td><x-table-actions modelName="subscription" :model="$subscription"></x-table-actions></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</x-model-index-page>
