<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('Back.layouts.partials.styles')
    <title>Document</title>
</head>
<body>
<div class="container">
    <h3>Subscriptions</h3>
    <table class="table table-delete-action-now table-bordered dt-responsive nowrap no-footer dtr-inline text-center f-16" id="subscriptions">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.users.t-user')</th>
            <th>@lang('back.tournaments.t-tournament')</th>
            <th>@lang('back.ages.t-age')</th>
            <th>@lang('back.sports.t-sport')</th>
            <th>@lang('back.form-team-name')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($subscriptions as $i => $subscription)
            <tr>
                <td>{{$i+1}}</td>
                <td>{{ $subscription->user->full_name }}</td>
                <td>{{ $subscription->tournament->name }}</td>
                <td>{{ $subscription->age->name }}</td>
                <td>{{ $subscription->sport->name }}</td>
                <td>{{ $subscription->team->name ?? trans('back.no-value') }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>
@include('Back.layouts.partials.scripts')
<script>
    $(window).on('load', function(){
        window.print();
    });
</script>
</body>
</html>
