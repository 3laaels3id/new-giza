@extends('Back.layouts.master')

@section('title', trans('back.reports.reports'))

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin-panel') }}">@lang('back.dashboard')</a></li>
            <li class="breadcrumb-item active" aria-current="page">@lang('back.reports.reports')</li>
        </ol>
    </nav>

    <div class="card border border-secondary">
        <div class="card-header bg-secondary text-white">@lang('back.submit')</div>
        <div class="card-body">
            @include('Back.includes.flash')

            {!! Form::open(['route' => 'reports.search', 'method' => 'POST']) !!}

            <div class="row">
                <div class="col">
                    <label for="from">@lang('back.form-date-from')</label>
                    <input type="date" id="from" name="from" value="{{ old('from') ? old('from') :  carbon()->yesterday()->format('Y-m-d') }}" class="form-control">
                </div>

                <div class="col">
                    <label for="to">@lang('back.form-date-to')</label>
                    <input type="date" id="to" name="to" value="{{ old('to') ? old('to') : carbon()->now()->format('Y-m-d') }}" class="form-control">
                </div>

                <div class="col">
                    <label for="doctors">@lang('back.doctors.t-doctor')</label>
                    <select name="doctor_id" id="doctors" class="form-control select2">
                        <option value="" disabled selected>@lang('back.select-a-value')</option>
                        @foreach($doctors as $id => $full_name)
                            <option {{ old('doctor_id') == $id ? 'selected' : '' }} value="{{ $id }}">{{ ucwords($full_name) }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col">
                    <input type="submit" style="margin-top: 29px;" class="btn btn-primary" value="@lang('back.submit')">
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

    <div class="card border border-secondary">
        <div class="card-header bg-secondary text-white">@lang('back.reports.reports')</div>
        <div class="card-body">
            <button
                type="button"
                data-toggle="tooltip"
                data-placement="top"
                title="@lang('back.export-csv')"
                onclick="window.location.href='{{ route('reports.export') }}';"
                class="btn btn-success mb-2"><i class="fa fa-file-csv"></i></button>

            <button
                type="button"
                data-toggle="tooltip"
                data-placement="top"
                title="@lang('back.print')"
                class="btn btn-dark mb-2"
                onclick="$('#report').printThis({pageTitle: 'Reports', importCSS: true, header: '<h1>التقرير المالي للأساتذة</h1>' });"
                id="printReport"><i class="fa fa-print"></i></button>

            <table class="table text-center f-18" id="report">
                <thead>
                <tr>
                    <th>#</th>
                    <th>@lang('back.doctors.t-doctor')</th>
                    <th>@lang('back.commissions.t-commission')</th>
                    <th>@lang('back.form-balance')</th>
                </tr>
                </thead>
                <tbody>

                @php $collection = session()->has('commissions') ? session('commissions') : ($commissions ?? ''); @endphp

                @forelse($collection as $i => $commission)
                    <tr>
                        <td>{{ $i+1 }}</td>
                        <td>{{ ucwords($commission->doctor->full_name) }}</td>
                        <td>{{ $commission->total }}</td>
                        <td>{{ $commission->doctor->wallet }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="10">
                            <div class="alert alert-info text-center p-2 f-15">@lang('back.no-value')</div>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div style="margin: 10px;">
                {!! session()->has('commissions') ? session('commissions')->withQueryString()->links() : $commissions->withQueryString()->links() !!}
            </div>
        </div>
    </div>
@stop
