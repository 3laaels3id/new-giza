<x-model-index-page model="notification" modelType="male" :collection="$notifications" columns="4">
    <table class="table table-delete-action-now table-bordered dt-responsive nowrap no-footer dtr-inline text-center f-15" id="notifications" style="font-size: 16px;">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.send-to')</th>
            <th>@lang('back.form-body')</th>
            <th>@lang('back.form-body')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($notifications as $i => $notification)
            <tr>
                <td>{{$i+1}}</td>
                <td>
                    <a href="{{ route(tableName($notification->notificationable_type).'.show', $notification->notificationable_id) }}">
                        {{ $notification->notificationable->full_name ?? trans('back.no-value') }}
                    </a>
                </td>
                <td>{!! highlightText($notification->title, $search) !!}</td>
                <td>{!! highlightText($notification->body, $search) !!}</td>
                <td><a data-id="{{ $notification->id }}" href="javascript:void(0);" class="btn btn-danger delete-action"><i class="fa fa-trash fa-1x"></i></a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</x-model-index-page>
