<div class="form-group">
    <x-form-inputs name="title" type="text" :slug="trans('back.form-title')"></x-form-inputs>
</div>

<div class="form-group">
    <x-form-inputs name="body" type="textarea" :slug="trans('back.form-body')"></x-form-inputs>
</div>

<div class="form-group">
    <x-select-input :arr="$types" name="type" :slug="trans('back.type')"></x-select-input>
</div>

<div class="form-group" id="notificationable" style="display: none;">
    <label for="fcm" id="notificationable_label">Select</label>
    <select name="fcm" id="fcm" class="form-control form-data select2">
        <option value="" selected disabled>@lang('back.select-a-value')</option>
    </select>
</div>

@section('scripts')
    <script>
        $('#type').on('change', function(){
            let selected = $(this).val();
            let notificationable = $('#notificationable');
            let fcm = $('#fcm');
            let notificationable_label = $('#notificationable_label');
            let label_text = selected === 'user' ? '{{ trans('back.users.t-user') }}' : '{{ trans('back.doctors.t-doctor') }}';

            if(selected === 'user' || selected === 'doctor')
            {
                $.ajax({
                    url: '{{ route('notifications.ajax-get-list-by-type') }}',
                    method: 'POST',
                    data: {selected},
                    success: response => {
                        notificationable.css('display', 'inline');
                        notificationable_label.text(label_text);
                        fcm.html('');

                        let Html = `<option value="" selected disabled>@lang('back.select-a-value')</option>`;

                        $.each(response.data, function(index, el){
                            Html += `<option value="${el.fcm}">${el.full_name}</option>`;
                        });

                        fcm.append(Html);
                    },
                    error: err => console.log(err),
                });
            }
            else
            {
                notificationable.hide();
            }
        });
    </script>
@stop
