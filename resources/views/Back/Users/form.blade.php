<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <x-form-inputs type="text" name="fname" :slug="trans('back.form-first-name')"></x-form-inputs>
        </div>
        <div class="form-group">
            <x-form-inputs type="text" name="lname" :slug="trans('back.form-last-name')"></x-form-inputs>
        </div>
        <div class="form-group">
            <x-form-inputs type="email" name="email" :slug="trans('back.form-email')"></x-form-inputs>
        </div>
        <div class="form-group">
            <x-form-inputs type="number" name="age" :slug="trans('back.form-age')"></x-form-inputs>
        </div>
        <div class="form-group">
            <x-form-inputs type="text" name="membership_number" :slug="trans('back.form-membership-number')"></x-form-inputs>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <div class="form-valid floating">
                    <label for="birth_date">@lang('back.form-birth-date')</label>
                    <input type="date" name="birth_date" id="birth_date" class="form-control form-data" value="{{ isset($currentModel) ? $currentModel->birth_date->format('Y-m-d') : '' }}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <x-select-input name="address" :arr="$address" :slug="trans('back.form-address')"></x-select-input>
        </div>
    </div>
</div>

@if(Route::is('users.create'))
{{--    <div class="row">--}}
{{--        <div class="col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <x-form-inputs type="password" name="password" :slug="trans('back.form-password')"></x-form-inputs>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="col-md-12">--}}
{{--            <div class="form-group">--}}
{{--                <x-form-inputs type="password_confirmation" name="password" :slug="trans('back.form-password')"></x-form-inputs>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
@endif

<div class="row">
{{--    <div class="col-md-12">--}}
{{--        <div class="form-group">--}}
{{--            <x-form-input-image name="image" :model="$currentModel ?? null"></x-form-input-image>--}}
{{--        </div>--}}
{{--    </div>--}}
    <div class="col-md-12">
        <div class="form-group">
            <x-switch-input :model="$currentModel ?? null"></x-switch-input>
        </div>
    </div>
</div>
