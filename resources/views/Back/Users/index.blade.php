<x-model-index-page model="user" modelType="male" :collection="$users" columns="7">
    <table class="table table-delete-action-now table-bordered dt-responsive nowrap no-footer dtr-inline text-center f-15" id="users" style="font-size: 16px;">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.form-name')</th>
            <th>@lang('back.form-age')</th>
            <th>@lang('back.form-email')</th>
            <th>@lang('back.form-membership-number')</th>
            <th>@lang('back.form-status')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $i => $user)
            <tr>
                <td>{{$i+1}}</td>
                <td><a href="{{ route('users.show', $user->id) }}">{!! highlightText($user->full_name, $search) !!}</a></td>
                <td>{!! highlightText($user->age, $search) !!}</td>
                <td>{!! highlightText($user->email, $search) !!}</td>
                <td>{!! highlightText($user->membership_number, $search) !!}</td>
                <td><x-table-switch-status :model="$user"></x-table-switch-status></td>
                <td><x-table-actions modelName="user" :model="$user"></x-table-actions></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</x-model-index-page>
