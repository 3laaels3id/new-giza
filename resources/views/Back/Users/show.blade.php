<x-model-show-element :title="$user->full_name" model="user" nameSpace="back">
    <div class="col-md-12">
        <div class="card border border-secondary">
            <div class="card-header bg-secondary text-white">@lang('back.users.t-user')</div>
            <div class="card-body">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item">
                        <a href="#user" data-toggle="tab" aria-expanded="false" class="nav-link active">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                            <span class="d-none d-sm-block">@lang('back.users.t-user')</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#subscriptions" data-toggle="tab" aria-expanded="false" class="nav-link">
                            <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                            <span class="d-none d-sm-block">@lang('back.subscriptions.subscriptions') ({{ $user->subscriptions()->has('sport')->count() }})</span>
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade show active" id="user">
                        @include('includes.flash')
                        <div class="row">
                            <div class="col-md-3">
                                <div class="card text-center">
                                    <img class="card-img-top img-fluid" src="{{ Uploaded::default() }}" alt="{{ $user->full_name }}">
                                    <div class="card-body">
                                        <h4 class="card-title">{{ ucwords($user->full_name) }}</h4>

                                        <a href="{{ route('users.edit', $user->id) }}" class="btn btn-block btn-primary">@lang('back.edit')</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="well">
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item">
                                            <h4 class="text-muted">@lang('back.form-first-name')</h4>
                                            <h5>{{ $user->fname ?? trans('back.no-value')  }}</h5>
                                        </li>

                                        <li class="list-group-item">
                                            <h4 class="text-muted">@lang('back.form-last-name')</h4>
                                            <h5>{{ $user->lname ?? trans('back.no-value')  }}</h5>
                                        </li>

                                        <li class="list-group-item">
                                            <h4 class="text-muted">@lang('back.form-age')</h4>
                                            <h5>{{ $user->age ?? trans('back.no-value')  }}</h5>
                                        </li>

                                        <li class="list-group-item">
                                            <h4 class="text-muted">@lang('back.form-birth-date')</h4>
                                            <h5>{{ $user->birth_date->format('Y-m-d') ?? trans('back.no-value')  }}</h5>
                                        </li>

                                        <li class="list-group-item">
                                            <h4 class="text-muted">@lang('back.form-membership-number')</h4>
                                            <h5>{{ $user->membership_number ?? trans('back.no-value')  }}</h5>
                                        </li>

                                        <li class="list-group-item">
                                            <h4 class="text-muted">@lang('back.form-status')</h4>
                                            <h5><x-table-switch-status :model="$user"></x-table-switch-status></h5>
                                        </li>

                                        <li class="list-group-item">
                                            <h4 class="text-muted">@lang('back.since')</h4>
                                            <h5>{{ $user->since }}</h5>
                                        </li>

                                        <li class="list-group-item">
                                            <h4 class="text-muted">@lang('back.updated_at')</h4>
                                            <h5>{{ $user->last_update }}</h5>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="subscriptions">
                        <div class="table-responsive">
                            <table class="table text-center">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('back.subscriptions.t-subscription')</th>
                                    <th>@lang('back.tournaments.t-tournament')</th>
                                    <th>@lang('back.sports.t-sport')</th>
                                    <th>@lang('back.ages.t-age')</th>
                                    <th>@lang('back.since')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($user->subscriptions()->has('sport')->get() as $i => $subscription)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>
                                            <a data-toggle="tooltip" title="@lang('back.subscriptions.t-subscription')" href="{{ route('subscriptions.show', $subscription->id) }}" class="btn btn-dark">
                                                <i class="fa fa-eye"></i> @lang('back.show')
                                            </a>
                                        </td>
                                        <td>
                                            <a data-toggle="tooltip" title="@lang('back.tournaments.t-tournament')" href="{{ route('tournaments.show', $subscription->tournament_id) }}">
                                                {{ ucwords($subscription->tournament->name) }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('sports.show', $subscription->sport_id) }}">
                                                {{ ucwords($subscription->sport->name) }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('ages.show', $subscription->age_id) }}">
                                                {{ ucwords($subscription->age->name) }}
                                            </a>
                                        </td>
                                        <td>{{ $subscription->since }}</td>
                                    </tr>
                                @empty
                                    <x-table-alert-no-value></x-table-alert-no-value>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="invitations">
                        invitations
                    </div>
                </div>
            </div>
        </div>

{{--        <div id="view_show_user_send_notification" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">--}}
{{--            <div class="modal-dialog">--}}
{{--                <div class="modal-content">--}}
{{--                    <div class="modal-header">--}}
{{--                        <h4 class="modal-title" id="myModalLabel">@lang('back.send-a-message')</h4>--}}
{{--                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
{{--                    </div>--}}
{{--                    <div class="modal-body">--}}
{{--                        <form action="{{ route('users.send-notification') }}" method="POST" id="send-notification-form">--}}
{{--                            @csrf--}}
{{--                            <input type="hidden" name="user_id" value="{{ $user->id }}">--}}
{{--                            <div class="form-group">--}}
{{--                                <label for="title">@lang('back.form-title')</label>--}}
{{--                                <input type="text" name="title" id="title" class="form-control @error('title') is-invalid @enderror">--}}
{{--                                @error('title')--}}
{{--                                <span class="invalid-feedback" role="alert">--}}
{{--                                <strong>{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                            <div class="form-group">--}}
{{--                                <label for="body">@lang('back.form-body')</label>--}}
{{--                                <textarea name="body" id="body" rows="5" class="form-control @error('body') is-invalid @enderror"></textarea>--}}
{{--                                @error('body')--}}
{{--                                <span class="invalid-feedback" role="alert">--}}
{{--                                <strong>{{ $message }}</strong>--}}
{{--                            </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                    <div class="modal-footer">--}}
{{--                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">@lang('back.close')</button>--}}
{{--                        <button type="submit" form="send-notification-form" name="submit" class="btn btn-primary waves-effect waves-light">@lang('back.send')</button>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>

    <x-slot name="scripts">
        @include('Back.includes.showModelScript', ['class' => 'send-user-message', 'modelId' => 'view_show_user_send_message'])
        @include('Back.includes.redirectAlertScript', ['class' => 'blockUser'])
    </x-slot>
</x-model-show-element>
