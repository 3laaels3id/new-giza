<x-model-trashed-page model="user" modelType="male">
    <table class="table table-bordered dt-responsive nowrap no-footer dtr-inline f-16" id="users">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.form-first-name')</th>
            <th>@lang('back.form-last-name')</th>
            <th>@lang('back.form-status')</th>
            <th>@lang('back.since')</th>
            <th>@lang('back.deleted_at')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody>
        @forelse($trashes as $key => $deleted)
            <tr>
                <td>{{ $key+1 }}</td>

                <td>{{ $deleted->fname ?? trans('back.no-value') }}</td>

                <td>{{ $deleted->lname ?? trans('back.no-value') }}</td>

                <td>
                    @if($deleted->status == 1)
                        <label class="badge badge-success f-16 p-2">@lang('back.active')</label>
                    @else
                        <label class="badge badge-danger f-16 p-2">@lang('back.disactive')</label>
                    @endif
                </td>

                <td>{{ $deleted->since }}</td>

                <td>{{ $deleted->deleted_since }}</td>

                <td class="text-center">
                    <x-trash-menu table="users" :model="$deleted"></x-trash-menu>
                </td>
            </tr>
        @empty
            <x-table-alert-no-value></x-table-alert-no-value>
        @endforelse
        </tbody>
    </table>
</x-model-trashed-page>
