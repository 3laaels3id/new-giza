<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <x-form-inputs type="text" name="fname" :slug="trans('back.form-first-name')"></x-form-inputs>
        </div>

        <div class="form-group">
            <x-form-inputs type="text" name="lname" :slug="trans('back.form-last-name')"></x-form-inputs>
        </div>

        <div class="form-group">
            <x-form-inputs type="email" name="email" :slug="trans('back.form-email')"></x-form-inputs>
        </div>

        <div class="form-group">
            <x-form-inputs type="text" name="identity" :slug="trans('back.employee-number')"></x-form-inputs>
        </div>

        <div class="form-group">
            <x-form-inputs type="password" name="password" :slug="trans('back.form-password')"></x-form-inputs>
        </div>

        <div class="form-group">
            <x-form-inputs type="password_confirmation" name="password" :slug="trans('back.form-password')"></x-form-inputs>
        </div>

        <div class="form-group">
            <x-select-input :arr="$roles" name="role_id" :slug="trans('back.permissions.t-permission')"></x-select-input>
        </div>

        <div class="form-group">
            <x-form-input-image name="image" :model="$currentModel ?? null"></x-form-input-image>
        </div>
    </div>
</div>

<div class="form-group">
    <x-switch-input :model="$currentModel ?? null"></x-switch-input>
</div>

@section('scripts')
    <script>
        $('input[name=image]').change(function(){
            readURL(this);
        });
    </script>
@stop
