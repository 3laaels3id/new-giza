<x-model-index-page model="admin" modelType="male" :collection="$admins" columns="8">
    <table class="table table-delete-action-now table-bordered text-center dt-responsive nowrap no-footer dtr-inline f-16" id="admins">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.form-name')</th>
            <th>@lang('back.roles.t-role')</th>
            <th>@lang('back.form-email')</th>
            <th>@lang('back.employee-number')</th>
            <th>@lang('back.form-status')</th>
            <th>@lang('back.since')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($admins as $i => $admin)
            <tr>
                <td>{{ $i+1 }}</td>
                <td>
                    <a href="{{ route('admins.show', $admin->id) }}">
                        {!! highlightText(ucwords($admin->full_name), $search) !!}
                    </a>
                </td>
                <td>
                    <a href="{{ route('roles.show', $admin->role_id) }}">
                        {!! highlightText($admin->role->name, $search) !!}
                    </a>
                </td>
                <td>{!! highlightText($admin->email, $search) !!}</td>
                <td>{!! highlightText($admin->identity, $search) !!}</td>
                <td><x-table-switch-status :model="$admin"></x-table-switch-status></td>
                <td>{{ ucwords($admin->since) }}</td>
                <td><x-table-actions modelName="admin" :model="$admin"></x-table-actions></td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <x-slot name="scripts">
        <script>
            $('table#admins').on('click', 'a.show-admin-mail-sending-btn', function () {
                let clickedMessageAnchor = $(this);
                let ajaxUrl = clickedMessageAnchor.attr('href');
                let siteModal = $('div#site-modals');
                $.ajax({
                    type: 'GET',
                    url: ajaxUrl,
                    success: response => {
                        siteModal.html(response);

                        if (response.requestStatus && response.requestStatus === false) {
                            siteModal.html('');
                        } else {
                            $('#view_show_admin_mail_sending').modal('show');
                        }
                    },
                    error: (x) => crud_handle_server_errors(x),
                });
                return false;
            });
        </script>
    </x-slot>
</x-model-index-page>
