<x-model-trashed-page model="admin" modelType="male">
    <table class="table table-bordered dt-responsive nowrap no-footer dtr-inline f-16" id="admins">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.form-first-name')</th>
            <th>@lang('back.form-last-name')</th>
            <th>@lang('back.form-image')</th>
            <th>@lang('back.form-status')</th>
            <th>@lang('back.since')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody>
        @forelse($trashes as $key => $admin)
            <tr id="admin-row-{{ $admin->id }}">
                <td>{{ $key+1 }}</td>

                <td>{{ $admin->fname ?? trans('back.no-value') }}</td>

                <td>{{ $admin->lname ?? trans('back.no-value') }}</td>

                <td><img width="60" height="60" class="img-circle" src="{{ $admin->image_url }}" alt=""></td>

                <td>
                    @if($admin->status == 1)
                        <label class="badge badge-success f-16 p-2">@lang('back.active')</label>
                    @else
                        <label class="badge badge-danger f-16 p-2">@lang('back.disactive')</label>
                    @endif
                </td>

                <td>{{ $admin->created_at->diffForHumans() }}</td>

                <td class="text-center">
                    <x-trash-menu table="admins" :model="$admin"></x-trash-menu>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="10">
                    <div class="alert alert-info text-center">@lang('back.no-value')</div>
                </td>
            </tr>
        @endforelse
        </tbody>
    </table>
</x-model-trashed-page>
