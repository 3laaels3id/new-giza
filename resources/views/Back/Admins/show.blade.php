<x-model-show-element :title="$admin->full_name" model="admin" nameSpace="back">
    <div class="col-md-12">
        <div class="card border border-secondary">
            <div class="card-header bg-secondary text-white">@lang('back.admins.t-admin')</div>
            <div class="card-body">
                @include('includes.flash')
                <div class="row">
                    <div class="col-md-3">
                        <div class="card">
                            <img class="card-img-top img-fluid" src="{{ $admin->image_url }}" alt="{{ $admin->name }}">
                            <div class="card-body">
                                <h4 class="card-title">{{ ucwords($admin->full_name) }}</h4>
                                <a href="{{ route('admins.edit', $admin->id) }}" class="btn btn-block btn-primary">@lang('back.edit')</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="well">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.form-email')</h4>
                                    <a onclick="event.preventDefault();" class="send-admin-message" href="{{ route('admins.show-message', $admin->id) }}">
                                        {{ $admin->email }}
                                    </a>
                                </li>

                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.form-identity')</h4>
                                    <h5>{{ $admin->identity ?? trans('back.no-value')  }}</h5>
                                </li>

                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.roles.t-role')</h4>
                                    <h5>
                                        <a href="{{ route('roles.show', $admin->role_id) }}">
                                            {{ ucwords($admin->role->name) ?? trans('back.no-value') }}
                                        </a>
                                    </h5>
                                </li>

                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.form-status')</h4>
                                    <h5>
                                        <x-table-switch-status :model="$admin"></x-table-switch-status>
                                    </h5>
                                </li>

                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.since')</h4>
                                    <h5>{{ $admin->since }}</h5>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <x-slot name="scripts">
        @include('Back.includes.showModelScript', ['class' => 'send-admin-message', 'modelId' => 'view_show_admin_send_message'])
    </x-slot>
</x-model-show-element>
