<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('Back.layouts.partials.styles')
    <title>Document</title>
</head>
<body>
<div class="container">
    <h3>Admins</h3>
    <table class="table table-delete-action-now table-bordered text-center dt-responsive nowrap no-footer dtr-inline f-16" id="admins">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.form-name')</th>
            <th>@lang('back.roles.t-role')</th>
            <th>@lang('back.form-email')</th>
            <th>@lang('back.employee-number')</th>
            <th>@lang('back.since')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($admins as $i => $admin)
            <tr>
                <td>{{ $i+1 }}</td>
                <td>{{ ucwords($admin->full_name) }}</td>
                <td>{{ $admin->role->name }}</td>
                <td>{{ $admin->email }}</td>
                <td>{{ $admin->identity ?? trans('back.no-value') }}</td>
                <td>{{ ucwords($admin->since) }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>
@include('Back.layouts.partials.scripts')
<script>
    $(window).on('load', function(){
        window.print();
    });
</script>
</body>
</html>
