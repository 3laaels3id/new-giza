<x-model-index-page model="age" modelType="female" :collection="$ages" columns="6">
    <table class="table table-delete-action-now table-bordered dt-responsive nowrap no-footer dtr-inline text-center f-15" id="ages" style="font-size: 16px;">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.form-name')</th>
            <th>@lang('back.sports.t-sport')</th>
            <th>@lang('back.form-status')</th>
            <th>@lang('back.subscriptions.subscriptions')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($ages as $i => $age)
            <tr>
                <td>{{$i+1}}</td>
                <td><a href="{{ route('ages.show', $age->id) }}">{!! highlightText($age->name, $search) !!}</a></td>
                <td><a href="{{ route('sports.show', $age->sport_id) }}">{!! highlightText($age->sport->name, $search) !!}</a></td>
                <td><x-table-switch-status :model="$age"></x-table-switch-status></td>
                <td>
                    <span class="badge badge-{{ $age->subscriptions->count() == 0 ? 'secondary' : 'primary' }} p-2 f-16">
                        {{ $age->subscriptions->count() }}
                    </span>
                </td>
                <td><x-table-actions modelName="age" :model="$age"></x-table-actions></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</x-model-index-page>
