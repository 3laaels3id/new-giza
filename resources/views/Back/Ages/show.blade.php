<x-model-show-element :title="$age->name" model="age" nameSpace="back">
    <div class="col-md-12">
        <div class="card border border-secondary">
            <div class="card-header bg-secondary text-white">@lang('back.ages.t-age')</div>
            <div class="card-body">
                @include('includes.flash')
                <div class="col-md-12">
                    <div class="well">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <h4 class="text-muted">@lang('back.form-name')</h4>
                                <h5>{{ $age->name ?? trans('back.no-value')  }}</h5>
                            </li>

                            <li class="list-group-item">
                                <h4 class="text-muted">@lang('back.form-age-type')</h4>
                                <h5>{{ $age->type ? trans('back.'.$age->type) : trans('back.no-value') }}</h5>
                            </li>

                            <li class="list-group-item">
                                <h4 class="text-muted">@lang('back.form-maximum-players')</h4>
                                <h5>{{ $age->maximum ?? trans('back.no-value')  }}</h5>
                            </li>

                            <li class="list-group-item">
                                <h4 class="text-muted">@lang('back.sports.t-sport')</h4>
                                <h5>{{ $age->sport->name ?? trans('back.no-value')  }}</h5>
                            </li>

                            <li class="list-group-item">
                                <h4 class="text-muted">@lang('back.form-status')</h4>
                                <h5><x-table-switch-status :model="$age"></x-table-switch-status></h5>
                            </li>

                            <li class="list-group-item">
                                <h4 class="text-muted">@lang('back.since')</h4>
                                <h5>{{ $age->since }}</h5>
                            </li>

                            <li class="list-group-item">
                                <h4 class="text-muted">@lang('back.updated_at')</h4>
                                <h5>{{ $age->last_update }}</h5>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-model-show-element>
