@include('Back.includes.translate', ['fields' => [translatedField('name', 'text', 'form-name')]])

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <x-select-input name="sport_id" :arr="$sports" :slug="trans('back.sports.t-sport')"></x-select-input>
        </div>

        <div class="form-group">
            <x-select-input name="type" :arr="$types" :slug="trans('back.type')"></x-select-input>
        </div>

        <div class="form-group">
            <x-form-inputs name="maximum" type="number" :slug="trans('back.form-maximum-players')"></x-form-inputs>
        </div>
    </div>
</div>

<div class="form-group">
    <x-switch-input :model="$currentModel ?? null"></x-switch-input>
</div>
