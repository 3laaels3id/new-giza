<x-model-index-page model="payment" modelType="female" :collection="$payments" columns="7">
    <table class="table table-delete-action-now text-center table-bordered dt-responsive nowrap no-footer dtr-inline f-16" id="payments">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.users.t-user')</th>
            <th>@lang('back.subscriptions.t-subscription')</th>
            <th>@lang('back.form-amount')</th>
            <th>@lang('back.form-currency')</th>
            <th>@lang('back.since')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($payments as $i => $payment)
            <tr>
                <td>{{ $i+1 }}</td>
                <td>
                    @if(isset($payment->user))
                        <a href="{{ route('users.show', $payment->user_id) }}">
                            {!! highlightText($payment->user->full_name, $search) !!}
                        </a>
                    @else
                        <label class="badge badge-secondary p-2 f-15">@lang('back.no-value')</label>
                    @endif
                </td>
                <td>
                    <a class="btn btn-dark" href="{{ route('subscriptions.show', $payment->paymentable_id) }}">
                        {{ trans('back.show') }} <i class="fa fa-eye"></i>
                    </a>
                </td>
                <td>
                    <label class="badge badge-success p-2 f-13">
                        {!! highlightText($payment->amount, $search) !!}
                    </label>
                </td>
                <td>{!! highlightText($payment->currency, $search) !!}</td>
                <td>{{ $payment->since }}</td>
                <td><x-table-action-delete modelName="payment" :model="$payment"></x-table-action-delete></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</x-model-index-page>
