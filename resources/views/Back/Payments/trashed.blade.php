<x-model-trashed-page model="payment" modelType="female">
    <table class="table table-bordered dt-responsive nowrap no-footer dtr-inline f-16" id="payments">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.users.t-user')</th>
            <th>@lang('back.form-amount')</th>
            <th>@lang('back.since')</th>
            <th>@lang('back.deleted_at')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody>
        @forelse($trashes as $key => $deleted)
            <tr>
                <td>{{ $key+1 }}</td>

                <td>{{ isset($deleted->user) ? $deleted->user->name : trans('back.no-value') }}</td>

                <td>{{ $deleted->amount ?? trans('back.no-value') }}</td>

                <td>{{ $deleted->since }}</td>

                <td>{{ $deleted->deleted_since }}</td>

                <td class="text-center">
                    <x-trash-menu table="payments" :model="$deleted"></x-trash-menu>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="6">
                    <div class="alert alert-info text-center">@lang('back.no-value')</div>
                </td>
            </tr>
        @endforelse
        </tbody>
    </table>
</x-model-trashed-page>
