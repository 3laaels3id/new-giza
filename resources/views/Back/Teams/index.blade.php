<x-model-index-page model="team" :collection="$teams" modelType="male" columns="6">
    <table class="table table-delete-action-now table-bordered dt-responsive nowrap no-footer dtr-inline text-center f-16" id="teams">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.form-team-name')</th>
            <th>@lang('back.users.t-user')</th>
            <th>@lang('back.tournaments.t-tournament')</th>
            <th>@lang('back.since')</th>
            <th>@lang('back.updated_at')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($teams as $i => $team)
            <tr>
                <td>{{$i+1}}</td>
                <td><a href="{{ route('teams.show', $team->id) }}">{{ $team->name }}</a></td>
                <td>
                    <a href="{{ route('users.show', $team->user_id) }}">
                        {!! highlightText($team->user->full_name, $search) !!}
                    </a>
                </td>
                <td>
                    <a href="{{ route('tournaments.show', $team->tournament_id) }}">
                        {!! highlightText($team->tournament->name, $search) !!}
                    </a>
                </td>
                <td>{{ $team->since }}</td>
                <td>{{ $team->last_update }}</td>
                <td>
                    <a href="{{ route('subscriptions.edit', $team->subscription_id) }}"
                       class="btn btn-success"
                       data-toggle="tooltip" title="@lang('back.edit')"><i class="fa fa-edit fa-1x"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</x-model-index-page>
