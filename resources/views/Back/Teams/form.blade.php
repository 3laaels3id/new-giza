<div class="form-group">
    <x-select-input name="tournament_id" :arr="$tournaments" :slug="trans('back.tournaments.t-tournament')"></x-select-input>
</div>

<div class="form-group">
    <x-select-input name="user_id" :arr="$users" :slug="trans('back.users.t-user')"></x-select-input>
</div>

<div class="form-group" id="sport_id_el">
    <x-select-input name="sport_id" :arr="$sports" :slug="trans('back.sports.t-sport')"></x-select-input>
</div>

<div class="form-group" id="age_id_el">
    <x-select-input name="age_id" :arr="$ages" :slug="trans('back.ages.t-age')"></x-select-input>
</div>

<div class="form-group">
    <x-form-inputs type="text" name="team_name" :slug="trans('back.form-team-name')"></x-form-inputs>
</div>
