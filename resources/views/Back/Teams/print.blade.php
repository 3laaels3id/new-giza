<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('Back.layouts.partials.styles')
    <title>Document</title>
</head>
<body>
<div class="container">
    <h3>teams</h3>
    <table class="table table-delete-action-now table-bordered dt-responsive nowrap no-footer dtr-inline text-center f-16" id="teams">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.teams.t-team')</th>
            <th>@lang('back.team-total')</th>
            <th>@lang('back.users.t-user')</th>
            <th>@lang('back.sports.t-sport')</th>
            <th>@lang('back.tournaments.t-tournament')</th>
            <th>@lang('back.since')</th>
            <th>@lang('back.updated_at')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($teams as $i => $team)
            <tr>
                <td>{{$i+1}}</td>
                <td>{{ $team->name }}</td>
                <td>{{ $team->total }}</td>
                <td>{{ $team->user->full_name }}</td>
                <td>{{ $team->sport->name }}</td>
                <td>{{ $team->tournament->name }}</td>
                <td>{{ $team->since }}</td>
                <td>{{ $team->last_update }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>
@include('Back.layouts.partials.scripts')
<script>
    $(window).on('load', function(){
        window.print();
    });
</script>
</body>
</html>
