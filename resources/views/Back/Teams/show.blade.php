<x-model-show-element :title="$team->name" model="team" nameSpace="back">
    @include('includes.flash')
    <div class="col-md-12">
        <div class="card border border-secondary">
            <div class="card-header bg-secondary text-white">@lang('back.teams.t-team')</div>
            <div class="card-body">
                <ul class="nav nav-tabs nav-justified">
                    <li class="nav-item">
                        <a href="#team" data-toggle="tab" aria-expanded="false" class="nav-link active">
                            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                            <span class="d-none d-sm-block">@lang('back.teams.t-team')</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#invitations" data-toggle="tab" aria-expanded="false" class="nav-link">
                            <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                            <span class="d-none d-sm-block">@lang('back.invitations.invitations') ({{ $team->total }})</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade show active" id="team">
                        <div class="well">
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.tournaments.t-tournament')</h4>
                                    <h5>
                                        <a href="{{ route('tournaments.show', $team->tournament_id) }}">
                                            {{ $team->tournament->name ?? trans('back.no-value') }}
                                        </a>
                                    </h5>
                                </li>

                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.sports.t-sport')</h4>
                                    <h5>
                                        <a href="{{ route('sports.show', $team->sport_id) }}">
                                            {{ $team->sport->name ?? trans('back.no-value') }}
                                        </a>
                                    </h5>
                                </li>

                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.users.t-user')</h4>
                                    <h5>
                                        <a href="{{ route('users.show', $team->user_id) }}">
                                            {{ $team->user->full_name ?? trans('back.no-value') }}
                                        </a>
                                    </h5>
                                </li>

                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.team-total')</h4>
                                    <h5>{{ ucwords($team->total) }}</h5>
                                </li>

                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.sports.t-sport')</h4>
                                    <h5>{{ ucwords($team->sport->name) }}</h5>
                                </li>

                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.end-status')</h4>
                                    @if($team->tournament->formatted_end_date->isPast())
                                        <span class="badge badge-success p-2 f-17">@lang('back.finished')</span>
                                    @elseif($team->tournament->formatted_start_date->isFuture())
                                        <span class="badge badge-warning p-2 f-17">@lang('back.not-started')</span>
                                    @else
                                        <span class="badge badge-danger p-2 f-17">@lang('back.started')</span>
                                    @endif
                                </li>

                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.since')</h4>
                                    <h5>{{ $team->since }}</h5>
                                </li>

                                <li class="list-group-item">
                                    <h4 class="text-muted">@lang('back.updated_at')</h4>
                                    <h5>{{ $team->last_update }}</h5>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="invitations">
                        <div class="table-responsive">
                            <table class="table text-center">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('back.form-name')</th>
                                    <th>@lang('back.form-phone')</th>
                                    <th>@lang('back.form-email')</th>
                                    <th>@lang('back.form-serial-number')</th>
                                    <th>@lang('back.form-age')</th>
                                    <th>@lang('back.print')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($team->subscription->invitations as $i => $invitation)
                                    <tr>
                                        <td>{{ $i+1 }}</td>
                                        <td>{{ $invitation->fname . ' ' . $invitation->lname }}</td>
                                        <td>{{ $invitation->phone }}</td>
                                        <td>{{ $invitation->email }}</td>
                                        <td>{{ $invitation->serial_number }}</td>
                                        <td>{{ $invitation->age }}</td>
                                        <td>
                                            <button onclick="window.location.href='{{ route('subscriptions.print-invitation', $invitation->id) }}';" type="button" class="btn btn-dark">
                                                <i class="fa fa-print"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @empty
                                    <x-table-alert-no-value></x-table-alert-no-value>
                                @endforelse
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('back.form-name')</th>
                                    <th>@lang('back.form-phone')</th>
                                    <th>@lang('back.form-email')</th>
                                    <th>@lang('back.form-serial-number')</th>
                                    <th>@lang('back.form-age')</th>
                                    <th>@lang('back.print')</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-model-show-element>
