@extends('Back.layouts.master')

@section('title', transEdit('team'))

@section('style')
    {!! script('public/admin/assets/js/pages/editor_ckeditor.js') !!}
@stop

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin-panel') }}">@lang('back.dashboard')</a></li>
            <li class="breadcrumb-item">
                <a href="{{ route('teams.index') }}">
                    @lang('back.teams.teams')
                </a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">{{ transEdit('team') }}</li>
        </ol>
    </nav>
    <div class="card border border-secondary">
        <div class="card-header bg-secondary text-white">{{ transEdit('team') }}</div>
        {!! Form::model($team, formUpdateArray($team, 'teams')) !!}

        <div class="card-body">
            <x-progress-bar color="bg-primary"></x-progress-bar>

            @include('Back.includes.flash')

            <div class="row">
                <div class="justify-content-md-center col-md-12">
                    @include('Back.Teams.form')
                </div>
            </div>
        </div>

        <div class="card-footer bg-white">
            <input type="submit" name="submit" class="btn btn-primary create-button" value="@lang('back.save')">
        </div>

        {!! Form::close() !!}
    </div>
@stop
