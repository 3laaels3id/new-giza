@extends('Back.layouts.master')

@section('title', transEdit('sport'))

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin-panel') }}">@lang('back.dashboard')</a></li>
            <li class="breadcrumb-item">
                <a href="{{ route('tournaments.index') }}">{{ trans('back.tournaments.tournaments') }}</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">{{ transEdit('sport') }}</li>
        </ol>
    </nav>

    <div class="card border border-secondary">
        <div class="card-header bg-secondary text-white">{{ transEdit('sport') }}</div>

        {!!
            Form::model($currentModel, [
                'url'    => route('tournaments.sport-update', $currentModel->id),
                'method' => 'PUT',
                'id'     => 'tournamentSportForm',
                'class'  => 'form-horizontal push-10-t tournamentSport ajax edit',
                'files'  => true,
            ])
        !!}

        <div class="card-body">
            <x-progress-bar color="bg-primary"></x-progress-bar>

            @include('Back.includes.flash')

            <div class="row">
                <div class="justify-content-md-center col-md-12">
                    @include('Back.TournamentSport.form')
                </div>
            </div>
        </div>

        <div class="card-footer bg-white">
            <input type="submit" name="submit" class="btn btn-primary create-button" value="@lang('back.save')">
        </div>

        {!! Form::close() !!}
    </div>
@stop
