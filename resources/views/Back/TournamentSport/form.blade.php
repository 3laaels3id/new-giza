<div class="form-group">
    <x-select-input name="id" :arr="$tournaments" :slug="trans('back.tournaments.t-tournament')"></x-select-input>
</div>

<div class="form-group">
    <label for="tournament_sports[0]">{{ trans('back.sports.t-sport') }}</label>
    <div class="row">
        <div class="col-md-11">
            <div class="form-valid">
                <select name="tournament_sports[0]" class="form-control select2 form-data" id="tournament_sports[0]">
                    <option value="" selected>@lang('back.select-a-value')</option>
                    @forelse($sports as $id => $name)
                        <option {{ isset($currentModel) ? ($currentModel->first()->id == $id ? 'selected' : '') : '' }} value="{{ $id }}">{{ $name }}</option>
                    @empty
                        <option value="" disabled>@lang('back.no-value')</option>
                    @endforelse
                </select>
            </div>
        </div>
        <div class="col-md-1">
             <button type="button" class="btn btn-primary f-16" id="addSportsField">+</button>
         </div>
     </div>
</div>

<div id="resultTournamentSports">
    @isset($currentModel)
        @foreach($currentModel->sports as $index => $sport)
            @continue($index == 0)
            <div class="row" id="sports-field-row-{{$index}}">
                <div class="col-md-11">
                    <div class="form-valid">
                        <label for="tournament_sports[{{$index}}]">{{trans('back.sports.t-sport')}}</label>
                        <select name="tournament_sports[{{$index}}]" data-index="0" class="form-control select2 form-data" id="tournament_sports[{{$index}}]">
                            <option value="0" disabled selected>@lang('back.select-a-value')</option>
                            @forelse($sports as $id => $name)
                                <option {{ $sport->id == $id ? 'selected' : '' }} value="{{ $id }}">{{ $name }}</option>
                            @empty
                                <option value="0" selected disabled>@lang('back.no-value')</option>
                            @endforelse
                        </select>
                    </div>
                </div>
                <div class="col-md-1">
                    <button data-sportid="{{$sport->id}}" data-id="{{$index}}" type="button" class="btn btn-danger removeSportsField" style="font-size: 17px;margin: 20px 0;font-weight: bold;">-</button>
                </div>
            </div>
        @endforeach
    @endisset
</div>

@section('scripts')
    <script>
        @if(Route::is('tournaments.sportCreate'))
            $('#resultTournamentSports').html('');
        @endif

        let sports_count = @if(Route::is('tournaments.sport-edit')) {{ $currentModel->sports->count() }} @else 1 @endif;

        function dynSportsField(number)
        {
            $('#resultTournamentSports').append(`
                <div class="form-group">
                    <div class="row" id="sports-field-row-${number}">
                        <div class="col-md-11">
                            <div class="form-valid">
                                <label for="tournament_sports[${number}]">{{trans('back.sports.t-sport')}}</label>
                                <select name="tournament_sports[${number}]" data-index="${number}" class="form-control select2 form-data" id="tournament_sports[${number}]">
                                    <option value="0" disabled selected>@lang('back.select-a-value')</option>
                                    @forelse($sports as $id => $name)
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @empty
                                        <option value="0" selected disabled>@lang('back.no-value')</option>
                                    @endforelse
                                </select>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <button data-id="${number}" type="button" class="btn btn-danger removeSportsField" style="font-size: 17px;margin: 20px 0;font-weight: bold;">-</button>
                        </div>
                    </div>
                </div>
            `);
            $(".select2").select2();
        }

        let removeSportItem = function()
        {
            let sport_id = $(this).data('sportid');

            let tournament_id = '{{ isset($currentModel) ? $currentModel->id : 0 }}';

            if(sport_id !== undefined) {
                $.ajax({
                    url: '{{ route('tournaments.ajax.remove.sport') }}',
                    method: 'POST',
                    data: { sport_id, tournament_id },
                    success: (response) => {
                        swal({
                            title: "@lang('back.a-message')",
                            text: "Done",
                            icon: "success",
                            button: "حسنا",
                        });
                    },
                    error: err => console.log(err)
                });
            }

            $('#sports-field-row-' + $(this).data('id')).remove();
        };

        $('#addSportsField').on('click', () => dynSportsField(sports_count++));

        $(document).on('click', '.removeSportsField', removeSportItem);
    </script>
@stop
