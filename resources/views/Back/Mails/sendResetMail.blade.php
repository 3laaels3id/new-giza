@component('mail::message')
# Password Reset Link

<div class="text-center">
    <a href="{{ route('admin.reset-password', ['token' => $token]) }}">
        {{ url('/admin-panel/admin-reset-password/' . $token) }}
    </a>
    <br/>
    <br/>
    <br/>
</div>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
