@component('mail::message')
# Password Reset Link

<div class="text-center">
    <a href="{{ route('doctor.reset-password', ['token' => $token]) }}">
        {{ url('/doctor-panel/doctor-reset-password/' . $token) }}
    </a>
    <br/>
    <br/>
    <br/>
</div>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
