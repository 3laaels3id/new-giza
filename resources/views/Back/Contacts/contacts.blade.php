@extends('Back.layouts.master')

@section('title', trans('back.contacts.contacts'))

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin-panel') }}">@lang('back.dashboard')</a></li>
            <li class="breadcrumb-item active" aria-current="page">@lang('back.contacts.contacts')</li>
        </ol>
    </nav>
    <div class="card-box table-responsive">
        @include('includes.flash')
        <h4 class="mt-0 card-header bg-secondary text-white header-title">@lang('back.contacts.contacts')</h4>

        <form action="{{ route('contacts.search') }}" style="margin-top: 15px;" method="GET">
            <div class="input-group col-md-4">
                <input class="form-control py-2" id="term" name="term" type="search" placeholder="@lang('back.search')">
                <span class="input-group-append">
                    <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>

        <div class="card-body">
            <div class="mb-2">
                <a href="{{ route('contacts.export') }}" data-toggle="tooltip" data-placement="top" title="@lang('back.export-csv')" class="btn btn-success"><i class="fa fa-file-excel"></i></a>
            </div>

            <table class="table table-bordered text-center f-16" id="contacts">
                <thead>
                <tr>
                    <th>#</th>
                    <th>@lang('back.form-name')</th>
                    <th>@lang('back.form-phone')</th>
                    <th>@lang('back.type')</th>
                    <th>@lang('back.form-message')</th>
                    <th>@lang('back.since')</th>
                    <th class="text-center">@lang('back.form-actions')</th>
                </tr>
                </thead>
                <tbody>
                @forelse($contacts as $key => $contact)
                    <tr id="contact-row-{{ $contact->id }}">
                        <td>{{ $key+1 }}</td>

                        <td>
                            <a
                                href="{{route('contacts.message-details', $contact->id)}}"
                                class="btn btn-md btn-default message-details-btn"
                                type="button"
                                style="color: #0a6aa1;"
                                data-toggle="tooltip">{!! highlightText($contact->name, $search) !!}
                            </a>
                        </td>

                        <td>{!! highlightText($contact->mobile_phone, $search) !!}</td>

                        <td>
                            @if($contact->is_seen)
                                @lang('back.old')
                            @else
                                <label class="badge badge-danger p-1">@lang('back.new')</label>
                            @endif
                        </td>

                        <td>{!! highlightText(str()->limit($contact->message, 30), $search) !!}</td>

                        <td>{{ $contact->since }}</td>

                        <td class="text-center">
                            <a data-toggle="tooltip" data-placement="top" title="@lang('back.replay')" href="{{route('contacts.send-user-message', $contact->id)}}" class="send-user-message-btn btn btn-success"><i class="fa fa-envelope fa-1x"></i></a>
                            <a data-id="{{$contact->id}}" data-toggle="tooltip" data-placement="top" title="@lang('back.delete')" href="{{localeUrl('/admin-panel/contacts/'.$contact->id) }}" class="delete-action btn btn-danger"><i class="fa fa-trash fa-1x"></i></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="8">
                            <div class="alert alert-info text-center">@lang('back.no-value')</div>
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div style="margin: 10px;">
                {!! $contacts->withQueryString()->links() !!}
            </div>
        </div>
    </div>
@stop

@section('scripts')
    @include('Back.includes.deleteActionScript', ['model' => 'contact', 'modelType' => 'female', 'deleteType' => 'force'])
    <script>
        let contacts_table = $('table#contacts');

        contacts_table.on('click', 'a.message-details-btn', function () {
            let clickedMessageAnchor = $(this);
            let ajaxUrl = clickedMessageAnchor.attr('href');
            let siteModel = $('div#site-modals');
            $.ajax({
                type: 'GET',
                url: ajaxUrl,
                success: function (response) {
                    siteModel.html(response);

                    if (response.requestStatus && response.requestStatus === false) siteModel.html('');

                    else $('#view_message_details').modal('show');
                },
                error: x => crud_handle_server_errors(x)
            });
            return false;
        });

        contacts_table.on('click', 'a.send-user-message-btn', function () {
            let clickedMessageAnchor = $(this);
            let ajaxUrl = clickedMessageAnchor.attr('href');
            let siteModel = $('div#site-modals');
            $.ajax({
                type: 'GET',
                url: ajaxUrl,
                success: function (response) {
                    siteModel.html(response);

                    if (response.requestStatus && response.requestStatus === false) siteModel.html('');

                    else $('#view_send_message').modal('show');
                },
                error: (x) => crud_handle_server_errors(x)
            });
            return false;
        });
    </script>
@stop
