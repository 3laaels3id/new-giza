<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', getLocale()) }}" dir="{{ direction() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    {!! meta('description', 'A fully featured admin theme which can be used to build CRM, CMS, etc.') !!}
    {!! style('public/admin/images/favicon.png', ['rel' => 'shortcut icon']) !!}
    {!! meta('viewport', 'width=device-width, initial-scale=1.0') !!}
    {!! meta('csrf-token', csrf_token()) !!}
    {!! meta('author', 'Coderthemes') !!}
    {!! meta('http-root', root()) !!}

    <script> window.laravel = @json(['csrfToken' => csrf_token()]) </script>

    <title>@lang('back.dashboard') || @yield('title')</title>

    @include('Back.layouts.partials.styles')

    @yield('style')
</head>
<body class="@yield('bodyclass')">
    <div id="app">
        <div id="wrapper">
            @include('Back.layouts.partials.navbar')
            <div class="left-side-menu">
                <div class="slimscroll-menu">
                    @include('Back.layouts.partials.userBox')
                    @include('Back.layouts.partials.sidebar')
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="content-page">
                <div class="content">
                    <div class="container-fluid">
                        @yield('content')
                    </div>
                </div>
                <footer class="footer">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                {{ date('Y') }} &copy; @lang('back.footer') <a href="{{ url('https://wesal.com.sa/') }}">@lang('back.company-name')</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>

    <div id="site-modals"></div>

    @include('Back.layouts.partials.scripts')

    @yield('scripts')
</body>
</html>
