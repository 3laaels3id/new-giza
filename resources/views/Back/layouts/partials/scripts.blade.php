{!! script('public/admin/js/vendor.min.js') !!}
{!! script('public/admin/libs/jquery-knob/jquery.knob.min.js') !!}
{!! script('public/admin/libs/morris-js/morris.min.js') !!}
{!! script('public/admin/libs/raphael/raphael.min.js') !!}
{!! script('public/admin/assets/js/sweetalert.min.js') !!}
{!! script('public/admin/libs/toastr/toastr.min.js') !!}
{!! script('public/admin/libs/custombox/custombox.min.js') !!}
{!! script('https://malsup.github.io/jquery.form.js') !!}

<!-- Plugins Js -->
{!! script('public/admin/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') !!}
{!! script('public/admin/libs/switchery/switchery.min.js') !!}
{!! script('public/admin/libs/multiselect/jquery.multi-select.js') !!}
{!! script('public/admin/libs/jquery-quicksearch/jquery.quicksearch.min.js') !!}
{!! script('public/admin/libs/select2/select2.min.js') !!}
{!! script('public/admin/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') !!}
{!! script('public/admin/libs/jquery-mask-plugin/jquery.mask.min.js') !!}
{!! script('public/admin/libs/moment/moment.js') !!}
{!! script('public/admin/libs/bootstrap-timepicker/bootstrap-timepicker.min.js') !!}
{!! script('public/admin/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') !!}
{!! script('public/admin/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') !!}
{!! script('public/admin/libs/bootstrap-daterangepicker/daterangepicker.js') !!}
{!! script('public/admin/libs/bootstrap-maxlength/bootstrap-maxlength.min.js') !!}
{!! script('public/admin/libs/dropify/dropify.min.js') !!}
{!! script('public/admin/js/pages/form-fileupload.init.js') !!}
{!! script('public/admin/js/pages/form-advanced.init.js') !!}

{!! script('public/admin/assets/js/crud.js') !!}
{!! script('public/admin/assets/js/scripts.js') !!}
{!! script('public/admin/assets/js/lang.min.js') !!}
{!! script('public/admin/assets/js/plugins/forms/selects/bootstrap_select.min.js') !!}
{!! script('public/admin/assets/js/plugins/forms/selects/select2.min.js') !!}
{!! script('public/admin/assets/js/jquery.print.js') !!}
{!! script('public/admin/js/HoldOn.min.js') !!}
{!! script('public/admin/js/app.min.js') !!}

<script>
    let rootPath = '{{ root() }}/' + '{{ getLocale() }}/admin-panel';

    let languages = {
        "paginate": {
            "previous": "@lang('pagination.previous')",
            "next": "@lang('pagination.next')",
            "first": "@lang('datatables.first')",
            "last": "@lang('datatables.last')",
        },
        "search": "@lang('datatables.search') : ",
        "infoEmpty": "@lang('datatables.emptyshowing')",
        "info": "@lang('datatables.showResult')",
        "emptyTable": "@lang('datatables.no-data-available')",
        "infoFiltered": "@lang('datatables.infoFiltered')",
        "zeroRecords": "@lang('datatables.zeroRecords')",
        "loadingRecords": "&nbsp;",
        "processing"	: "<i class='fa fa-3x fa-asterisk fa-spin'></i>"
    };

    let swalObjectTerminated = {title: "@lang('back.a-message')", text: "@lang('back.operation-terminated')", icon: "success", button: "@lang('back.ok')"};
</script>
