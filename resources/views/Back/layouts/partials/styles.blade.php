<!--Morris Chart-->
{!! style('public/admin/libs/morris-js/morris.css') !!}

<!-- Plugins css -->
{!! style('public/admin/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css') !!}
{!! style('public/admin/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') !!}
{!! style('public/admin/libs/bootstrap-timepicker/bootstrap-timepicker.min.css') !!}
{!! style('public/admin/libs/bootstrap-datepicker/bootstrap-datepicker.css') !!}
{!! style('public/admin/libs/bootstrap-daterangepicker/daterangepicker.css') !!}
{!! style('public/admin/libs/bootstrap-tagsinput/bootstrap-tagsinput.css') !!}
{!! style('public/admin/libs/multiselect/multi-select.css') !!}
{!! style('public/admin/libs/switchery/switchery.min.css') !!}
{!! style('public/admin/libs/switchery/switchery.min.css') !!}
{!! style('public/admin/libs/custombox/custombox.min.css') !!}
{!! style('public/admin/libs/select2/select2.min.css') !!}
{!! style('public/admin/libs/dropify/dropify.min.css') !!}

<!-- Global stylesheets -->
{!! style('public/admin/css/bootstrap.min.css') !!}
{!! style('public/admin/css/icons.min.css') !!}
{!! style('public/admin/css/fontSize.css') !!}

@if(app()->isLocale('ar'))
    {!! style('public/admin/css/app-rtl.min.css') !!}
@else
    {!! style('public/admin/css/app.min.css') !!}
@endif

{!! script('public/admin/assets/js/core/libraries/jquery.min.js') !!}
{!! script('public/admin/assets/js/core/libraries/bootstrap.min.js') !!}
{!! script('public/admin/assets/js/plugins/loaders/blockui.min.js') !!}
{!! script('public/admin/assets/js/plugins/loaders/pace.min.js') !!}
{!! style('public/admin/libs/toastr/toastr.min.css') !!}
{!! script('public/admin/ckeditor/ckeditor.js') !!}
{!! style('public/admin/css/HoldOn.min.css') !!}
{!! style('https://fonts.googleapis.com/css2?family=Open+Sans&display=swap') !!}
{!! style('https://fonts.gstatic.com', ['rel' => 'preconnect']) !!}
{!! style('https://fontlibrary.org/face/droid-arabic-kufi', ['media' => 'screen']) !!}
{!! script('https://www.google.com/jsapi') !!}
<!-- /global stylesheets -->

<style>
    body, h1, h2, h3, h4, h5, h6, span, label {
        font-family: 'DroidArabicKufiRegular', serif;
    }
    .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link{
        color: #eee;
        background-color: #35b8e0;
        border-color: #dee2e6 #dee2e6 #fff;
    }
    .unselectable {
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
</style>
