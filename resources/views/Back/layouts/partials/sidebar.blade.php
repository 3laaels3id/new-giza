<div id="sidebar-menu">
    <ul class="metismenu" id="side-menu">
        <li><a href="{{ route('admin-panel') }}"><i class="mdi mdi-view-dashboard"></i><span> @lang('back.dashboard')</span></a></li>

{{--        @if(permission_route_checker('contacts.index'))--}}
{{--            <li>--}}
{{--                <a href="{{ route('contacts.index') }}">--}}
{{--                    <i class="mdi mdi-email-open"></i>--}}
{{--                    <span>--}}
{{--                    @if($newMessagesCount)--}}
{{--                        <label class="badge badge-danger">{{ $newMessagesCount }}</label>--}}
{{--                    @endif--}}
{{--                    @lang('back.contacts.contacts')--}}
{{--                </span>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--        @endif--}}

        @foreach(models() as $model => $value)
            @if(permission_route_checker(plural($model).'.index'))
                <li>
                    <a href="{{ route(plural($model).'.index') }}">
                        <i class="mdi mdi-{{ $value['icon'] }}"></i>
                        <span> @lang('back.'.plural($model).'.'.plural($model)) </span>
                    </a>
                </li>
            @endif
        @endforeach

        @if(permission_route_checker('teams.index'))
            <li>
                <a href="{{ route('teams.index') }}">
                    <i class="mdi mdi-trophy-award"></i>
                    <span>
                    @lang('back.teams.teams')
                </span>
                </a>
            </li>
        @endif

{{--        @if(permission_route_checker('settings.index'))--}}
{{--            <li>--}}
{{--                <a href="{{ route('settings.index') }}">--}}
{{--                    <i class="mdi mdi-settings-outline"></i>--}}
{{--                    <span> @lang('back.settings.settings') </span>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--        @endif--}}
    </ul>
</div>
