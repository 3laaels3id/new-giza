<div class="navbar-custom">
    <ul class="list-unstyled topnav-menu float-right mb-0">
        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle mr-0 waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                <img src="{{ getFlagLang() }}" alt="lang" width="30">
            </a>

            <div class="dropdown-menu dropdown-menu-right profile-dropdown">
                <a href="{{ LaravelLocalization::getLocalizedURL('en') }}" class="english dropdown-item notify-item">
                    <img src="{{ getFlagLang('gb') }}" alt="">
                    English
                </a>
                <a href="{{ LaravelLocalization::getLocalizedURL('ar') }}" class="arabic dropdown-item notify-item">
                    <img src="{{ getFlagLang('sa') }}" alt="">
                    العربية
                </a>
            </div>
        </li>

        <li class="dropdown notification-list">
            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                <img src="{{ $auth->image_url }}" alt="user-image" class="rounded-circle">
                <span class="pro-user-name ml-1">{{ ucwords($auth->name) }} <i class="mdi mdi-chevron-down"></i></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                <a href="{{ route('admins.profile') }}" class="dropdown-item notify-item"><i class="fe-user"></i><span>@lang('back.my-account')</span></a>

                <div class="dropdown-divider"></div>

                <a href="{{ route('admin.logout') }}"
                   onclick="event.preventDefault();document.getElementById('logout-form-0').submit();" class="dropdown-item notify-item">
                    <i class="fe-log-out"></i><span>@lang('back.logout')</span>
                </a>

                <form id="logout-form-0" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>

    <div class="logo-box">
        <a href="{{ route('admin-panel') }}" class="logo text-center" style="background-color: darkgray;">
            <span class="logo-lg">
                <img src="{{ url('public/admin/images/image.png') }}" alt="" height="60">
            </span>
            <span class="logo-sm">
                <img src="{{ url('public/admin/images/image.png') }}" alt="" height="24">
            </span>
        </a>
    </div>

    <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
        <li><button class="button-menu-mobile disable-btn waves-effect"><i class="fe-menu"></i></button></li>

        <li><h4 class="page-title-main">@lang('back.dashboard')</h4></li>
    </ul>
</div>
