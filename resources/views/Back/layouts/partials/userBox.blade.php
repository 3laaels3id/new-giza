<div class="user-box text-center">
    <img src="{{ $auth->image_url }}" alt="user-img" title="{{ucwords($auth->name)}}" class="rounded-circle img-thumbnail avatar-lg">
    <div class="dropdown">
        <a href="javascript:void(0);" class="text-dark dropdown-toggle h5 mt-2 mb-1 d-block" data-toggle="dropdown">{{ $auth->name }}</a>
        <div class="dropdown-menu user-pro-dropdown">
            <a href="{{ route('admins.profile') }}" class="dropdown-item notify-item">
                <i class="fe-user mr-1"></i>
                <span>@lang('back.my-account')</span>
            </a>

            <a href="{{ route('admin.logout') }}" class="dropdown-item notify-item"
               onclick="event.preventDefault();document.getElementById('logout-form-1').submit();">
                <i class="fe-log-out mr-1"></i> @lang('back.logout')
            </a>

            <form id="logout-form-1" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </div>

    <p class="text-muted">{{ $auth->role->name }}</p>
    <ul class="list-inline">
        <li class="list-inline-item">
            <a href="{{ route('settings.index') }}" class="text-muted">
                <i class="mdi mdi-settings"></i>
            </a>
        </li>
        <li class="list-inline-item">
            <a href="{{ route('admin.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form-3').submit();" class="text-custom">
                <i class="mdi mdi-power"></i>
            </a>
            <form id="logout-form-3" action="{{ route('admin.logout') }}" method="POST"
                  style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</div>
