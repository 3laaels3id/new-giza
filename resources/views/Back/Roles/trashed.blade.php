<x-model-trashed-page model="role" modelType="female">
    <table class="table table-bordered dt-responsive nowrap no-footer dtr-inline f-16" id="permissions">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.form-name')</th>
            <th>@lang('back.form-status')</th>
            <th>@lang('back.since')</th>
            <th>@lang('back.deleted_at')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody>
        @forelse($trashes as $key => $permission)
            <tr id="role-row-{{ $permission->id }}">
                <td>{{ $key+1 }}</td>

                <td>{{ $permission->name ?? trans('back.no-value') }}</td>

                <td>
                    @if($permission->status == 1)
                        <label class="badge badge-success f-16">@lang('back.active')</label>
                    @else
                        <label class="badge badge-danger f-16">@lang('back.disactive')</label>
                    @endif
                </td>

                <td>{{ $permission->created_at->diffForHumans() }}</td>

                <td>{{ $permission->deleted_at->diffForHumans() }}</td>

                <td class="text-center">
                    <x-trash-menu table="roles" :model="$permission"></x-trash-menu>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="6">
                    <div class="alert alert-info text-center">@lang('back.no-value')</div>
                </td>
            </tr>
        @endforelse
        </tbody>
    </table>
</x-model-trashed-page>
