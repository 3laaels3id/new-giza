@include('Back.includes.translate', ['fields' => [
    translatedField('name', 'text', 'form-name')
]])

@include('Back.includes.permissions', ['role' => isset($currentModel) ? $currentModel : null])

<div class="form-group">
    <x-switch-input :model="$currentModel ?? null"></x-switch-input>
</div>
