<x-model-index-page model="role" modelType="female" :collection="$roles" columns="6">
    <table class="table table-delete-action-now table-bordered dt-responsive nowrap no-footer dtr-inline text-center f-15" id="roles" style="font-size: 16px;">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.form-name')</th>
            <th>@lang('back.form-status')</th>
            <th>@lang('back.since')</th>
            <th>@lang('back.updated_at')</th>
            <th class="text-center">@lang('back.form-actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($roles as $i => $role)
            <tr>
                <td>{{$i+1}}</td>
                <td>
                    <a href="{{ route('roles.show', $role->id) }}">
                        {!! highlightText($role->name, $search) !!}
                    </a>
                </td>
                <td><x-table-switch-status :model="$role"></x-table-switch-status></td>
                <td>{{ $role->since }}</td>
                <td>{{ $role->last_update }}</td>
                <td><x-table-actions modelName="role" :model="$role"></x-table-actions></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</x-model-index-page>
