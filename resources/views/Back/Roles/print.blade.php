<!doctype html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('Back.layouts.partials.styles')
    <title>Document</title>
</head>
<body>
<div class="container">
    <h3>Roles</h3>
    <table class="table table-delete-action-now table-bordered dt-responsive nowrap no-footer dtr-inline text-center f-15" id="roles" style="font-size: 16px;">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('back.form-name')</th>
            <th>@lang('back.since')</th>
            <th>@lang('back.updated_at')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($roles as $i => $role)
            <tr>
                <td>{{$i+1}}</td>
                <td>{{ $role->name }}</td>
                <td>{{ $role->since }}</td>
                <td>{{ $role->last_update }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>
@include('Back.layouts.partials.scripts')
<script>
    $(window).on('load', function(){
        window.print();
    });
</script>
</body>
</html>
