<div class="form-group">
    <label for="tournament_sports[0]">
        {{ trans('back.sports.t-sport') }}
        <button type="button" wire:click.prevent="addSportsField" class="btn btn-primary f-16"><i class="fa fa-plus"></i></button>
    </label>

    @foreach($tournamentSports as $index => $sport)
        <div class="row">
            <div class="col-md-11">
                <div class="form-valid">
                    <select wire:model="tournamentSports.{{$index}}" name="tournament_sports[{{ $index }}]" class="form-control select2 form-data" id="tournament_sports[{{ $index }}]">
                        <option value="" selected>@lang('back.select-a-value')</option>
                        @forelse($sports as $id => $name)
                            <option {{ $sport->id === (int)$id ? 'selected' : '' }} value="{{ $id }}">{{ $name }}</option>
                        @empty
                            <option value="" disabled>@lang('back.no-value')</option>
                        @endforelse
                    </select>
                </div>
            </div>
            @if($index != 0)
                <div class="col-md-1">
                    <button type="button" wire:click.prevent="removeSportsField({{ $index }})" class="btn btn-danger f-16"><i class="fa fa-trash"></i></button>
                </div>
            @endif
        </div>
        <br>
    @endforeach
</div>
