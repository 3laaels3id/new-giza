<div class="card-box">
    <h4 class="header-title mt-0 mb-3">@lang('back.' . plural($model) . '.' . plural($model))</h4>

    <div class="widget-box-2">
        <div class="widget-detail-2 text-right">
            <h2 class="font-weight-normal mb-1">{{$collection->count()}}</h2>
        </div>
        <div class="progress progress-bar-alt-success progress-sm">
            <div class="progress-bar bg-{{$color}}" role="progressbar" style="width: 100%;"></div>
        </div>
    </div>
</div>
