<video width="{{$width}}" height="340" playsinline controls preload="auto">
    <source src="{{ $src }}" type="video/mp4">
</video>
