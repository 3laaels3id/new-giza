<li class="list-group-item">
    <div class="row">
        <div class="col-md-9 f-15">
            @lang('crud.'.$val)
        </div>
        <div class="col-md-3">
            <input
                type="checkbox"
                class="form-control form-data"
                data-plugin="switchery"
                @isset($role) {{ edit_permissions($role->permissions, $val) }} @endisset
                value="{{$val}}"
                @if($val == 'admins.profile' || $val == 'admins.admin-profile-update') checked @endif
                name="permissions[]"
                id="permissions_{{$k}}"
                data-color="#00b19d">
        </div>
    </div>
</li>

