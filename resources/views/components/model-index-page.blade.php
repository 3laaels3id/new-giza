@php
    $plural = plural($model ?? '');
@endphp

@extends('Back.layouts.master')

@section('title', trans("back.$plural.$plural"))

@section('style')
    {!! style('public/admin/libs/datatables/dataTables.bootstrap4.css') !!}
    {!! style('public/admin/libs/datatables/responsive.bootstrap4.css') !!}
    {!! style('public/admin/libs/datatables/buttons.bootstrap4.css') !!}
    {!! style('public/admin/libs/datatables/select.bootstrap4.css') !!}
    {{ $styles ?? '' }}
@stop

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin-panel') }}">@lang('back.dashboard')</a></li>
            @if(isset($breadcrumb))
                <li class="breadcrumb-item" aria-current="page">
                    <a href="{{ route("$plural.index") }}">
                        @lang('back.'.$plural.'.'.$plural)
                    </a>
                </li>
                {{ $breadcrumb ?? '' }}
            @else
                <li class="breadcrumb-item active" aria-current="page">@lang('back.'.$plural.'.'.$plural)</li>
            @endif
        </ol>
    </nav>
    <div class="row">
        <div class="col-12">
            <div class="card-box table-responsive">
                @include('includes.flash')

                <h4 class="mt-0 card-header bg-secondary text-white header-title">
                    @lang('back.'.$plural.'.'.$plural) ({{ $collection->total() ?? 0 }})
                </h4>

                {{ $search ?? '' }}

                @if(!isset($search) && $model != 'company' && $model != 'provider')
                    <form action="{{ route("$plural.search") }}" style="margin-top: 15px;" method="GET">
                        <div class="input-group col-md-4">
                            <input class="form-control py-2" name="term" type="search" placeholder="@lang('back.search')">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                @endif

                <div class="card-body">
                    <div class="mb-1 mt-1">
                        @if(!in_array($model, creatRouteNotIn()))
                            <a data-toggle="tooltip" title="{{ transCreate($model) }}" href="{{ route("$plural.create") }}" class="btn btn-primary">+</a>
                        @endif

                        @if(!in_array($model, ['subscription', 'notification', 'team']))
                            <a href="{{ route("$plural.trashed") }}" data-toggle="tooltip" title="@lang('back.trashed')" class="btn btn-danger">
                                <i class="fa fa-trash-restore"></i>
                            </a>
                        @endif

                        <a href="{{ route("$plural.export") }}" data-toggle="tooltip" title="@lang('back.export-csv')" class="btn @if($collection->count() == 0) disabled @endif btn-success">
                            <i class="fa fa-file-excel"></i>
                        </a>

                        <a target="_blank" data-toggle="tooltip" title="@lang('back.print')" href="{{ route(plural($model).'.print') }}" type="button" class="btn btn-dark">
                            <i class="fa fa-print"></i>
                        </a>

                        {{ $buttons ?? '' }}
                    </div>
                    {{ $slot ?? '' }}
                    @if($collection instanceof \Illuminate\Pagination\LengthAwarePaginator)
                        <div style="margin: 10px;">
                            {!! $collection->withQueryString()->links() !!}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <!-- third party js -->
    {!! script('public/admin/libs/datatables/jquery.dataTables.min.js') !!}
    {!! script('public/admin/libs/datatables/dataTables.bootstrap4.js') !!}
    {!! script('public/admin/libs/datatables/dataTables.responsive.min.js') !!}
    {!! script('public/admin/libs/datatables/responsive.bootstrap4.min.js') !!}
    {!! script('public/admin/libs/datatables/dataTables.buttons.min.js') !!}
    {!! script('public/admin/libs/pdfmake/pdfmake.min.js') !!}
    {!! script('public/admin/libs/pdfmake/vfs_fonts.js') !!}
    {!! script('https://cdn.datatables.net/buttons/1.6.4/js/buttons.print.min.js') !!}

    <!-- third party js ends -->
    @include('Back.includes.datatableScript', ['tableName' => 'table#'.$plural, 'length' => $columns])

    @includeWhen(!changeStatusNotIn($model), 'Back.includes.isChecked', ['model' => $model])

    @if(!in_array($model, ['team']))
        <script>
            $(document).on("click", ".delete-action", function() {
                let clickedBtn = $(this);
                let datatable = clickedBtn.parents('table').dataTable();
                let id = clickedBtn.data('id');
                let deleteMessage = '{{ trans('back.delete-message-var', ['var' => trans('back.'.$plural.'.t-'.$model), 'type' => trans('back.for-'.$modelType)]) }}';

                let model = '{{ $model }}';
                let force = '{{ trans('back.force-delete-message-title') }}';
                let soft = '{{ trans('back.delete-message-title') }}';

                let deleteMessageTitle = (model === 'notification') ? force : soft;

                let ajaxUrl = '{{ route($plural.'.ajax-delete-'.$model) }}';

                swal(setAlertDeleteObject(deleteMessage, deleteMessageTitle)).then(function (willDelete) {
                    if (!willDelete) {
                        swal(swalObjectTerminated);
                    } else {
                        $.ajax({
                            type: 'POST',
                            url: ajaxUrl,
                            data: { id },
                            success: response => {
                                if (response.requestStatus) {
                                    clickedBtn.parents('tr').fadeOut('slow', () => datatable.fnDeleteRow($(this)));
                                    swal(response.message, {icon: "success"});
                                }
                            },
                            error: x => crud_handle_server_errors(x)
                        });
                    }
                });
            });
        </script>
    @endif

    {{ $scripts ?? '' }}
@stop
