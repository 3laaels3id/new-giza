@extends(ucfirst($nameSpace).'.layouts.master')

@section('title', $title)

@section('content')
    <div class="content">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                @if($nameSpace == 'doctor')
                    <li class="breadcrumb-item"><a href="{{ route('doctor-panel') }}">@lang('back.dashboard')</a></li>
                    @if($model != 'video')
                        <li class="breadcrumb-item" aria-current="page">
                            <a href="{{ route('doctor.'.plural($model).'.index') }}">
                                @lang('back.'.plural($model).'.'.plural($model))
                            </a>
                        </li>
                    @else
                        <li class="breadcrumb-item" aria-current="page">
                            <a href="{{ url()->previous() }} ">
                                @lang('back.courses.t-course')
                            </a>
                        </li>
                    @endif
                    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
                @else
                    <li class="breadcrumb-item"><a href="{{ route('admin-panel') }}">@lang('back.dashboard')</a></li>
                    @if($model != 'video')
                        @if($model == 'doctor')
                            <li class="breadcrumb-item" aria-current="page">
                                <a href="{{ route(plural($model).'.active') }}">
                                    @lang('back.'.plural($model).'.'.plural($model))
                                </a>
                            </li>
                        @else
                            <li class="breadcrumb-item" aria-current="page">
                                <a href="{{ route(plural($model).'.index') }}">
                                    @lang('back.'.plural($model).'.'.plural($model))
                                </a>
                            </li>
                        @endif
                    @else
                        <li class="breadcrumb-item" aria-current="page">
                            <a href="{{ url()->previous() }} ">
                                @lang('back.courses.t-course')
                            </a>
                        </li>
                    @endif
                    <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
                @endif
            </ol>
        </nav>
        <div class="row">
            {{ $slot }}
        </div>
    </div>
@stop
@section('scripts')
    @includeWhen(!changeStatusNotIn($model), 'Back.includes.isChecked', ['model' => $model])

    {{ $scripts ?? '' }}
@stop
