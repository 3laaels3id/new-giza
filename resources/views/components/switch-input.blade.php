@php
    $status = isset($model) ? $model->status == 1 : true;
@endphp

<input
    type="checkbox"
    class="form-control form-data"
    onclick="isChecked('{{ $status == 1 ? 'checked' : 'null' }}', '{{ isset($model) ? $model->id : 0 }}');"
    data-plugin="switchery"
    id="active-id-{{ isset($model) ? $model->id : 0 }}" {{ $status == 1 ? 'checked' : '' }}
    name="status"
    value="{{isset($model) ? $model->id : true}}"
    data-color="#00b19d"/>

@lang('back.form-status')
