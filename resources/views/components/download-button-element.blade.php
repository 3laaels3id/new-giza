<button
    onclick="window.location.href='{{ $url }}';"
    data-toggle="tooltip"
    data-placement="top"
    title="@lang('back.form-download')" class="btn btn-{{ $color }}">
    <i class="fa fa-download"></i>
</button>
