@if($type == 'password_confirmation')

    @include('Back.includes.inputs', get_input_attributes('password_confirmation', 'password', trans('back.form-password-confirm')))

@elseif($type == 'image')

    @include('Back.includes.inputs', get_input_attributes('image', $name, $slug, 6))

@elseif($type == 'file')

    @include('Back.includes.inputs', get_input_attributes('file', $name, trans('back.form-file'), 6))

@elseif($type == 'ckeditor')

    <textarea class="form-data editorfull" name="{{$name}}" id="editorfullar" rows="4" cols="4"></textarea>

@else

    @include('Back.includes.inputs', get_input_attributes($type, $name, $slug))

@endif
