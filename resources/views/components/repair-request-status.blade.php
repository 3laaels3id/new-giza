@if($status == null)
    <label class="badge badge-warning f-15 p-2">
        @lang('back.pending') <i class="fa fa-clock fa-1x"></i>
    </label>
@elseif($status == 1)
    <label class="badge badge-success f-15 p-2">
        @lang('back.finished')
    </label>
@else
    <label class="badge badge-danger f-15 p-2">
        @lang('back.new')
    </label>
@endif
