<a href="{{ route(plural($modelName).'.edit', $model->id) }}"
   class="btn btn-success"
   data-toggle="tooltip" title="@lang('back.edit')"><i class="fa fa-edit fa-1x"></i></a>

<a data-id="{{ $model->id }}"
   href="javascript:void(0);"
   data-toggle="tooltip" title="@lang('back.delete')"
   class="btn btn-danger delete-action"><i class="fa fa-trash fa-1x"></i></a>
