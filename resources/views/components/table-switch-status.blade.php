<div id="#checkbox-{{$model->id}}">
    <label>
        <input
            type="checkbox"
            class="form-control"
            onclick="isChecked('{{ $model->status == 1 ? 'checked' : 'null' }}', '{{ $model->id }}');"
            data-plugin="switchery"
            id="active-id-{{ $model->id }}" {{ $model->status == 1 ? 'checked' : '' }}
            name="status"
            value="{{ $model->id }}"
            data-color="#00b19d"/>
    </label>
</div>
