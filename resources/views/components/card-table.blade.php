<div class="card border border-secondary">
    <div class="card-header bg-secondary text-white">{{ $title }}</div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-hover mb-0">
                {{ $slot }}
            </table>
        </div>
    </div>
</div>
