<label for="{{$name}}">@lang('back.form-image')</label>

@isset($model)
    <div class="row">
        <div class="col-md-6">
            <input
                type="file"
                accept="image/*"
                data-show-errors="true"
                data-errors-position="outside"
                class="dropify form-data"
                id="{{$name}}"
                name="{{$name}}"
                data-height="300" />
        </div>
        <div class="col-md-6">
            <div class="img-container">
                <img id="viewImage"
                     class="img-responsive"
                     width="300" height="300"
                     src="{{ $model->image_url }}" alt=""/>
            </div>
        </div>
    </div>
@else
    <input
        type="file"
        accept="image/*"
        data-show-errors="true"
        data-errors-position="outside"
        class="dropify form-data"
        id="{{$name}}"
        name="{{$name}}"
        data-height="300" />
@endisset
