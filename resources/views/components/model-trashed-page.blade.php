@extends('Back.layouts.master')

@section('title', trans('back.trashed'))

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('admin-panel') }}">@lang('back.dashboard')</a></li>
            @if($model == 'doctor')
                <li class="breadcrumb-item"><a href="{{ route(plural($model).'.active') }}">@lang('back.'.plural($model).'.'.plural($model))</a></li>
            @else
                <li class="breadcrumb-item"><a href="{{ route(plural($model).'.index') }}">@lang('back.'.plural($model).'.'.plural($model))</a></li>
            @endif
            <li class="breadcrumb-item active" aria-current="page">@lang('back.trashed')</li>
        </ol>
    </nav>
    <div class="card-box table-responsive m-2" dir="{{ direction() }}">
        @include('includes.flash')

        <h4 class="mt-0 header-title">@lang('back.'.plural($model).'.'.plural($model))</h4>

        <div class="card-body">
            {{ $slot }}
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(document).on('click', '.force-delete-btn', function(e){
            e.preventDefault();

            let clickedBtn = $(this);
            let deleteMessage = '{{ trans('back.delete-message-var', ['var' => trans('back.'.plural($model).'.t-'.lower($model)), 'type' => trans('back.for-'.$modelType)]) }}';
            let deleteMessageTitle = '{{ trans('back.force-delete-message-title') }}';
            let href = clickedBtn.attr('href');

            swal(setAlertDeleteObject(deleteMessage, deleteMessageTitle)).then(function (willDelete) {
                (!willDelete) ? swal(swalObjectTerminated) : window.location.href = href;
            });
        });
    </script>
    </script>
    {{ $scripts ?? '' }}
@stop
