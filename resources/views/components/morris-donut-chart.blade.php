@php
    $plural = plural($model ?? '');
@endphp
<div class="card-box">
    <h4 class="header-title mt-0">{{ trans('back.'.$plural.'.'.$plural) }}</h4>

    @if($collection['active_'.$plural]->count() > 0)
        <div class="widget-chart text-center">
            <div id="{{ $htmlID }}" dir="ltr" style="height: 245px;" class="morris-chart"></div>
            <ul class="list-inline chart-detail-list mb-0">
                <li class="list-inline-item">
                    <h5 style="color: #d9539a;">
                        <i class="fa fa-circle mr-1"></i>
                        {{ trans('back.active-var', ['var' => trans('back.'.$plural.'.'.$plural)]) }}
                    </h5>
                </li>
                <li class="list-inline-item">
                    <h5 style="color: #5b69bc;">
                        <i class="fa fa-circle mr-1"></i>
                        {{ trans('back.deductive-var', ['var' => trans('back.'.$plural.'.'.$plural)]) }}
                    </h5>
                </li>
            </ul>
        </div>
    @else
        <div class="alert alert-info text-center" style="margin-top: 225px;">@lang('back.no-value')</div>
    @endif
</div>
