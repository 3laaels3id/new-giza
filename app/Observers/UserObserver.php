<?php

namespace App\Observers;

use App\Models\Fcm;
use App\Models\Token;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserObserver
{
    public function created(User $user)
    {
        Token::createToken($user, Auth::guard('api')->fromUser($user), User::class);

        Fcm::createFcm($user, User::class);
    }

    public function forceDeleted(User $user)
    {
        $user->subscriptions()->delete();
    }
}
