<?php

namespace App\Observers;

use App\Models\Fcm;
use App\Models\Token;
use App\Models\Doctor;
use Illuminate\Support\Facades\Auth;

class DoctorObserver
{
    public function created(Doctor $doctor)
    {
        Token::createToken($doctor, Auth::guard('doctor_api')->fromUser($doctor), Doctor::class);

        Fcm::createFcm($doctor, Doctor::class);
    }

    public function forceDeleted(Doctor $doctor)
    {
        $doctor->subscriptions()->delete();
        $doctor->notifications()->delete();
        $doctor->chats()->delete();
        $doctor->videos()->delete();
        $doctor->files()->delete();
        $doctor->commissions()->delete();
        $doctor->rates()->delete();
        $doctor->banks()->delete();
        $doctor->wallets()->delete();
        $doctor->transactions()->delete();
        $doctor->courses()->delete();
    }
}
