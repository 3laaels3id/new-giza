<?php

namespace App\Observers;

use App\Facade\Firebase;
use App\Models\Message;

class MessageObserver
{
    public function created(Message $message)
    {
        $chat = $message->chat;

        $fcm = request()->sender_type == 'user' ? $chat->doctor->fcm->fcm : $chat->user->fcm->fcm;

        if($fcm) Firebase::pushNotification(self::setNotificationData($message, $chat, $fcm));
    }

    private static function setNotificationData($message, $chat, $fcm)
    {
        return [
            'title'            => trans('back.message'),
            'body'             => $message->message,
            'notificationType' => 'chat',
            'type'             => 'chat',
            'fcm_token'        => $fcm,
            'type_id'          => $chat->id,
        ];
    }
}
