<?php

namespace App\Exports;

use App\Models\Team;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class TeamsExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    public function collection()
    {
        return Team::all();
    }

    public function headings(): array
    {
        return ['#', 'Team Name', 'Total Players', 'Tournament', 'User', 'Sport', 'created at'];
    }

    public function map($row): array
    {
        return [
            $row->id,
            $row->name,
            $row->total,
            $row->tournament->translate('en')->name,
            $row->user->full_name,
            $row->sport->translate('en')->name,
            $row->created_at
        ];
    }
}
