<?php

namespace App\Exports;

use App\Models\Tournament;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class TournamentsExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    public function collection()
    {
        return Tournament::all();
    }

    public function headings(): array
    {
        return ['#', 'Name', 'Sports Game', 'Start Date', 'End Date'];
    }

    public function map($row): array
    {
        return [
            $row->id,
            $row->translate('en')->name,
            $row->sports->count(),
            $row->start_date,
            $row->end_date,
        ];
    }
}
