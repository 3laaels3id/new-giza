<?php

namespace App\Exports;

use App\Models\Role;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class RolesExport implements FromCollection, ShouldAutoSize, WithMapping, WithHeadings
{
    public function collection()
    {
        return Role::all();
    }

    public function headings(): array
    {
        return ['#', 'Name'];
    }

    public function map($row): array
    {
        return [
            $row->id,
            $row->translate('en')->name
        ];
    }
}
