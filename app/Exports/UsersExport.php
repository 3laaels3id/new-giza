<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromCollection;

class UsersExport implements FromCollection, ShouldAutoSize, WithMapping, WithHeadings
{
    public function collection()
    {
        return User::all();
    }

    public function headings(): array
    {
        return ['#', 'Full Name', 'Age', 'Email', 'Membership', 'created at'];
    }

    public function map($row): array
    {
        return [
            $row->id,
            $row->full_name,
            $row->age,
            $row->email,
            $row->membership_number,
            $row->created_at
        ];
    }
}
