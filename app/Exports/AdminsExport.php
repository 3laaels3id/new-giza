<?php

namespace App\Exports;

use App\Models\Admin;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class AdminsExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    public function collection()
    {
        return Admin::where('role_id', '!=', 1)->get();
    }

    public function headings(): array
    {
        return ['#', 'Name', 'The Role', 'Email', 'Employee ID'];
    }

    public function map($row): array
    {
        return [
            $row->id,
            $row->full_name,
            $row->role->name,
            $row->email,
            $row->identity ?? trans('back.no-value'),
        ];
    }
}
