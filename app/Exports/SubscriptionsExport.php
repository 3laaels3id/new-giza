<?php

namespace App\Exports;

use App\Models\Subscription;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class SubscriptionsExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    public function collection()
    {
        return Subscription::all();
    }

    public function headings(): array
    {
        return ['#', 'Player', 'Tournament', 'Age Category', 'Sport Game', 'Team Name'];
    }

    public function map($row): array
    {
        return [
            $row->id,
            $row->user->full_name,
            $row->tournament->translate('en')->name,
            $row->age->translate('en')->name,
            $row->sport->translate('en')->name,
            $row->team->name ?? trans('back.no-value'),
        ];
    }
}
