<?php

namespace App\Exports;

use App\Models\Age;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class AgesExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize
{
    public function collection()
    {
        return Age::all();
    }

    public function headings(): array
    {
        return ['#', 'Name', 'Sport Game', 'Subscriptions'];
    }

    public function map($row): array
    {
        return [
            $row->id,
            $row->translate('en')->name,
            $row->sport->name,
            $row->subscriptions->count(),
        ];
    }
}
