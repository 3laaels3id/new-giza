<?php

namespace App\Repository\Contracts;

use Illuminate\Http\Request;

/**
 * @method all()
 * @method paginate()
 * @method find($id)
 * @method delete($id)
 * @method forceDelete($id)
 * @method index()
 * @method trashed()
 * @method restore($id)
 * @method search($request)
 * @method export()
 */
interface IAdminRepository
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function changeStatus(Request $request);

    /**
     * @return mixed
     */
    public function create();

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id);

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request);

    /**
     * @param Request $request
     * @param $currentModel
     * @return mixed
     */
    public function update(Request $request, $currentModel);

    /**
     * @param $id
     * @return mixed
     */
    public function showAdminMessage($id);

    /**
     * @param Request $request
     * @return mixed
     */
    public function updateAdminProfile(Request $request);

    /**
     * @param Request $request
     * @return mixed
     */
    public function sendAdminMessage(Request $request);

    /**
     * @return mixed
     */
    public function adminProfile();
}
