<?php

namespace App\Repository\Contracts;

use Illuminate\Http\Request;

/**
 * @method all()
 * @method paginate()
 * @method find($id)
 * @method delete($id)
 * @method forceDelete($id)
 * @method index()
 * @method trashed()
 * @method restore($id)
 * @method search($request)
 * @method export()
 */
interface IUserRepository
{
    /**
     * @param $id
     * @return mixed
     */
    public function showUserMessage($id);

    /**
     * @param Request $request
     * @return mixed
     */
    public function sendUserMessage(Request $request);

    /**
     * @param Request $request
     * @return mixed
     */
    public function changeStatus(Request $request);

    /**
     * @return mixed
     */
    public function create();

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id);

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request);

    /**
     * @param Request $request
     * @param $currentModel
     * @return mixed
     */
    public function update(Request $request, $currentModel);
}
