<?php

namespace App\Repository\Contracts;

interface IEloquentRepository
{
    /**
     * @return mixed
     */
    public function index();
    /**
     * @param $id
     */
    public function find($id);

    /**
     * @return mixed
     */
    public function all();

    /**
     * @param int $pages
     * @return mixed
     */
    public function paginate($pages = 10);

    /**
     * @return mixed
     */
    public function export();

    /**
     * @param $id
     * @return mixed
     */
    public function restore($id);

    /**
     * @return mixed
     */
    public function trashed();

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $id
     * @return mixed
     */
    public function show($id);

    /**
     * @param $id
     * @return mixed
     */
    public function forceDelete($id);

    /**
     * @param $request
     * @return mixed
     */
    public function search($request);

    /**
     * @param string $type
     * @param null $currentModel
     * @return mixed
     */
    public function formFields($type = 'create', $currentModel = null);
}
