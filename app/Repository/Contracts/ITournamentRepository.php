<?php

namespace App\Repository\Contracts;

use App\Models\Tournament;
use Illuminate\Http\Request;

/**
 * @method all()
 * @method paginate()
 * @method find($id)
 * @method delete($id)
 * @method forceDelete($id)
 * @method index()
 * @method trashed()
 * @method restore($id)
 * @method search($request)
 * @method export()
 */
interface ITournamentRepository
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function changeStatus(Request $request);

    /**
     * @return mixed
     */
    public function create();

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request);

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id);

    /**
     * @param Request $request
     * @param $currentRole
     * @return mixed
     */
    public function update(Request $request, $currentRole);

    /**
     * @return mixed
     */
    public function sportCreate();

    /**
     * @param Request $request
     * @return mixed
     */
    public function sportStore(Request $request);

    /**
     * @param Request $request
     * @return mixed
     */
    public function sportRemove(Request $request);

    /**
     * @param Tournament $tournament
     * @return mixed
     */
    public function sportEdit(Tournament $tournament);

    /**
     * @param Request $request
     * @param Tournament $tournament
     * @return mixed
     */
    public function sportUpdate(Request $request, Tournament $tournament);
}
