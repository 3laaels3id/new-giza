<?php

namespace App\Repository\Eloquent\Sql;

use App\Facade\Support\Warning;
use App\Models\TournamentSport;
use Exception;
use App\Facade\Support\CrudMessage;
use App\Models\Sport;
use App\Models\Tournament;
use App\Facade\Crud;
use App\Exports\TournamentsExport;
use Illuminate\Http\Request;
use App\Repository\Contracts\ITournamentRepository;
use Illuminate\Support\Facades\DB;

class TournamentRepository extends BaseRepository implements ITournamentRepository
{
    public function __construct(Tournament $model)
    {
        $this->model = $model;

        parent::__construct($model, new TournamentsExport());
    }

    public function store(Request $request)
    {
        $sports_count = Sport::count();

        if((int)$request->maximum_sports > $sports_count) return Warning::tournamentMaxSportsGreaterThanActual();

        $modelData = $request->except(['_token', '_method']);

        $modelData['status'] = $request->has('status') ? $request->status : false;

        return Crud::storeTranslatedModel($this->class, $modelData);
    }

    public function update(Request $request, $currentTournament)
    {
        $sports_count = Sport::count();

        if((int)$request->maximum_sports > $sports_count) return Warning::tournamentMaxSportsGreaterThanActual();

        $modelData = $request->except(['_token', '_method']);

        $modelData['status'] = $request->has('status') ? $request->status : false;

        return Crud::updateTranslatedModel($this->class, $modelData, $currentTournament);
    }

    public function changeStatus(Request $request)
    {
        return Crud::setStatus($this->class, $request);
    }

    public function create()
    {
        return view('Back.Crud.create', self::formFields());
    }

    public function sportCreate()
    {
        return view('Back.TournamentSport.create', [
            'tournaments' => $this->model::getInSelectForm(),
            'sports'      => Sport::getInSelectForm()
        ]);
    }

    public function sportEdit(Tournament $tournament)
    {
        return view('Back.TournamentSport.edit', [
            'tournaments'  => $this->model::getInSelectForm(),
            'sports'       => Sport::getInSelectForm(),
            'currentModel' => $tournament
        ]);
    }

    public function sportStore($request)
    {
        DB::beginTransaction();
        try
        {
            $tournament = Tournament::find($request->id);

            $tournament_sports = array_unique($request->tournament_sports);

            $check = TournamentSport::whereIn('sport_id', $tournament_sports)->where('tournament_id', $request->id)->get();

            if($check->count() > 0) return Warning::sportsAlreadyExists();

            if($tournament->sports->count() === (int)$tournament->maximum_sports) return Warning::sorryTournamentAlreadyHasSports();

            if((int)$tournament->maximum_sports < count($tournament_sports)) return CrudMessage::warning(trans('api.tournament-sports-count-error', ['num' => $tournament->maximum_sports]));

            $tournament->sports()->attach($tournament_sports);

            DB::commit();

            return CrudMessage::successResponse();
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::fails($e);
        }
    }

    public function sportUpdate(Request $request, Tournament $tournament)
    {
        DB::beginTransaction();
        try
        {
            $sports = array_unique($request->tournament_sports);

            if((int)$tournament->maximum_sports < count($sports)) return CrudMessage::warning(trans('api.tournament-sports-count-error', ['num' => $tournament->maximum_sports]));

            $tournament->sports()->sync($sports);

            DB::commit();

            return CrudMessage::successResponse();
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::fails($e);
        }
    }

    public function sportRemove($request)
    {
        DB::beginTransaction();
        try
        {
            TournamentSport::where('sport_id', $request->sport_id)->where('tournament_id', $request->tournament_id)->delete();

            DB::commit();

            return CrudMessage::successResponse();
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::fails($e);
        }
    }

    public function edit($id)
    {
        $currentModel = $this->model::findOrFail($id);

        Crud::load_translated_attrs($currentModel);

        return view('Back.Crud.edit', self::formFields('edit', $currentModel));
    }
}
