<?php

namespace App\Repository\Eloquent\Sql;

use App\Models\Subscription;
use App\Facade\Crud;
use App\Exports\SubscriptionsExport;
use Illuminate\Http\Request;
use App\Repository\Contracts\ISubscriptionRepository;

class SubscriptionRepository extends BaseRepository implements ISubscriptionRepository
{
    public function __construct(Subscription $model)
    {
        $this->model = $model;

        parent::__construct($model, new SubscriptionsExport());
    }

    public function index()
    {
        $data = ['subscriptions' => Subscription::has('sport')->has('age')->has('team')->has('user')->paginate(10), 'search' => false];

        return view('Back.Subscriptions.index', $data);
    }

    public function store(Request $request)
    {
        return Crud::store($this->class, $request);
    }

    public function update(Request $request, $currentSubscription)
    {
        return Crud::update($this->class, $request, $currentSubscription);
    }

    public function changeStatus(Request $request)
    {
        return Crud::setStatus($this->class, $request);
    }

    public function create()
    {
        return view('Back.Crud.create', self::formFields());
    }

    public function edit($id)
    {
        $currentModel = $this->model::findOrFail($id);

        return view('Back.Crud.edit', self::formFields('edit', $currentModel));
    }

    public function formFields($type = 'create', $currentModel = null)
    {
        $data['model'] = $this->name;

        if($type == 'edit') $data['currentModel'] = $currentModel;

        return $data;
    }
}
