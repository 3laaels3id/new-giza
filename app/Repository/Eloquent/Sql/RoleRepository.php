<?php

namespace App\Repository\Eloquent\Sql;

use Exception;
use App\Models\Role;
use App\Facade\Crud;
use App\Exports\RolesExport;
use Illuminate\Http\Request;
use App\Facade\Support\CrudMessage;
use Illuminate\Support\Facades\DB;
use App\Repository\Contracts\IRoleRepository;

class RoleRepository extends BaseRepository implements IRoleRepository
{
    public function __construct(Role $model)
    {
        $this->model = $model;

        parent::__construct($model, new RolesExport());
    }

    public function index()
    {
        $data = ['roles' => $this->model::where('id', '!=', 1)->paginate(10), 'search' => false];

        return view('Back.Roles.index', $data);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try
        {
            $roleData = $request->all();

            $roleData['status'] = $request->has('status') ? $request->status : false;

            $formTranslatedAttrs = only($roleData, array_keys(sitelangs()));

            $roleData = except($roleData, array_keys(sitelangs()));

            $role = new Role(except($roleData, ['_token', 'permissions']));

            Crud::save_translated_attrs($role, $formTranslatedAttrs);

            $role->save();

            DB::table('permissions')->insert(Role::setPermissions($request, $role));

            DB::commit();

            return CrudMessage::add('permission');
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::fails($e);
        }
    }

    public function update(Request $request, $currentRole)
    {
        DB::beginTransaction();
        try
        {
            $roleData = $request->except('permissions');

            $roleData['status'] = $request->has('status') ? $request->status : false;

            $permissions = Role::setPermissions($request, $currentRole);

            $formTranslatedAttrs = only($roleData, array_keys(sitelangs()));

            $roleData = except($roleData, array_keys(sitelangs()));

            Crud::save_translated_attrs($currentRole, $formTranslatedAttrs, $roleData);

            $currentRole->save();

            DB::table('permissions')->where('role_id', $currentRole->id)->delete();

            DB::table('permissions')->insert($permissions);

            DB::commit();

            return CrudMessage::edit('permission');
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::fails($e);
        }
    }

    public function changeStatus(Request $request)
    {
        return Crud::setStatus($this->class, $request);
    }

    public function create()
    {
        return view('Back.Crud.create', self::formFields());
    }

    public function edit($id)
    {
        if($id == 1) return redirect()->route('roles.index');

        $currentModel = $this->model::findOrFail($id);

        Crud::load_translated_attrs($currentModel);

        return view('Back.Crud.edit', self::formFields('edit', $currentModel));
    }
}
