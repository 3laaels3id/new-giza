<?php

namespace App\Repository\Eloquent\Sql;

use App\Models\Sport;
use App\Facade\Crud;
use App\Exports\SportsExport;
use Illuminate\Http\Request;
use App\Repository\Contracts\ISportRepository;

class SportRepository extends BaseRepository implements ISportRepository
{
    public function __construct(Sport $model)
    {
        $this->model = $model;

        parent::__construct($model, new SportsExport());
    }

    public function store(Request $request)
    {
        $modelData = $request->except(['_token', '_method']);

        $modelData['status'] = $request->has('status') ? $request->status : false;

        return Crud::storeTranslatedModel($this->class, $modelData);
    }

    public function update(Request $request, $currentSport)
    {
        $modelData = $request->except(['_token', '_method']);

        $modelData['status'] = $request->has('status') ? $request->status : false;

        return Crud::updateTranslatedModel($this->class, $modelData, $currentSport);
    }

    public function changeStatus(Request $request)
    {
        return Crud::setStatus($this->class, $request);
    }

    public function create()
    {
        return view('Back.Crud.create', self::formFields());
    }

    public function edit($id)
    {
        $currentModel = $this->model::findOrFail($id);

        Crud::load_translated_attrs($currentModel);

        return view('Back.Crud.edit', self::formFields('edit', $currentModel));
    }

    // override the formFields method;
    public function formFields($type = 'create', $currentModel = null)
    {
        $data['model'] = $this->name;

        if($type == 'edit') $data['currentModel'] = $currentModel;

        return $data;
    }
}
