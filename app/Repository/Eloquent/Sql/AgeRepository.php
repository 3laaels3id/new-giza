<?php

namespace App\Repository\Eloquent\Sql;

use App\Models\Age;
use App\Facade\Crud;
use App\Exports\AgesExport;
use App\Models\Sport;
use Illuminate\Http\Request;
use App\Repository\Contracts\IAgeRepository;

class AgeRepository extends BaseRepository implements IAgeRepository
{
    public function __construct(Age $model)
    {
        $this->model = $model;

        parent::__construct($model, new AgesExport());
    }

    public function store(Request $request)
    {
        $modelData = $request->except(['_token', '_method']);

        $modelData['status'] = $request->has('status') ? $request->status : false;

        return Crud::storeTranslatedModel($this->class, $modelData);
    }

    public function update(Request $request, $currentAge)
    {
        $modelData = $request->except(['_token', '_method']);

        $modelData['status'] = $request->has('status') ? $request->status : false;

        return Crud::updateTranslatedModel($this->class, $modelData, $currentAge);
    }

    public function changeStatus(Request $request)
    {
        return Crud::setStatus($this->class, $request);
    }

    public function create()
    {
        return view('Back.Crud.create', self::formFields());
    }

    public function edit($id)
    {
        $currentModel = $this->model::findOrFail($id);

        Crud::load_translated_attrs($currentModel);

        return view('Back.Crud.edit', self::formFields('edit', $currentModel));
    }

    public function formFields($type = 'create', $currentModel = null)
    {
        $data['model'] = $this->name;

        $data['sports'] = Sport::getInSelectForm();

        $data['types'] = $this->model::types();

        if($type == 'edit') $data['currentModel'] = $currentModel;

        return $data;
    }
}
