<?php

namespace App\Repository\Eloquent\Sql;

use App\Facade\Crud;
use Illuminate\Support\Facades\DB;
use App\Facade\Support\CrudMessage;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Database\Eloquent\Model;
use App\Repository\Contracts\IEloquentRepository;

class BaseRepository implements IEloquentRepository
{
    protected $model, $name, $class, $export, $folder;

    public function __construct(Model $model, $export)
    {
        $this->model = $model;

        $this->export = $export;

        $this->class = getClass($model->getTable());

        $this->name = lower(getModelName($this->class));

        $this->folder = plural($this->name)->camel()->ucfirst();
    }

    public function index()
    {
        $data = [(string)$this->folder->camel() => $this->model->latest()->paginate(10), 'search' => false];

        return view('Back.'.(string)$this->folder.'.index', $data);
    }

    public function print()
    {
        $data = [(string)$this->folder->camel() => $this->model->latest()->paginate(10), 'search' => false];

        return view('Back.' . (string)$this->folder . '.print', $data);
    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    public function all()
    {
        return $this->model->all();
    }

    public function paginate($pages = 10)
    {
        return $this->model->paginate($pages);
    }

    public function export()
    {
        return Excel::download($this->export, (string)$this->folder->lower().'.xlsx');
    }

    public function restore($id)
    {
        $model = snake($this->name)->plural();

        DB::table($model)->where('id', $id)->update(['deleted_at' => null]);

        return CrudMessage::restore($this->name);
    }

    public function trashed()
    {
        $trashes = $this->model::onlyTrashed()->get();

        return view('Back.'.(string)$this->folder.'.trashed', compact('trashes'));
    }

    public function delete($id)
    {
        return Crud::delete($this->class, $id);
    }

    public function show($id)
    {
        return view('Back.'.(string)$this->folder.'.show', [(string)$this->name => $this->find($id)]);
    }

    public function forceDelete($id)
    {
        modelForceDelete($this->class, $id, true);

        return CrudMessage::remove($this->name);
    }

    public function search($request)
    {
        $element = (string)$this->folder->camel();

        if(is_null($request->term)) return redirect()->route((string)$element . '.index');

        $data = [$element => $this->model::search($request->term)->paginate(10), 'search' => true];

        return view('Back.' . (string)$this->folder . '.index', $data);
    }

    public function formFields($type = 'create', $currentModel = null)
    {
        $data['model'] = $this->name;

        if($type == 'edit') $data['currentModel'] = $currentModel;

        return $data;
    }
}
