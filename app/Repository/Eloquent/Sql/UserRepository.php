<?php

namespace App\Repository\Eloquent\Sql;

use App\Facade\Crud;
use App\Models\User;
use Illuminate\Http\Request;
use App\Exports\UsersExport;
use App\Facade\Support\CrudMessage;
use App\Repository\Contracts\IUserRepository;

class UserRepository extends BaseRepository implements IUserRepository
{
    public function __construct(User $model)
    {
        $this->model = $model;

        parent::__construct($model, new UsersExport());
    }

    public function store(Request $request)
    {
        $modelData = $request->except(['_token', '_method']);

        $modelData['status'] = $request->has('status') ? $request->status : false;

        return Crud::store($this->class, $modelData);
    }

    public function update(Request $request, $currentModel)
    {
        $modelData = $request->except(['_token', '_method']);

        $modelData['status'] = $request->has('status') ? $request->status : false;

        return Crud::update($this->class, $modelData, $currentModel);
    }

    public function showUserMessage($id)
    {
        if (!$user = $this->model::find($id)) return CrudMessage::warning(trans('responseMessages.product-not-exist'));

        return view('Back.Users.sendUserMessageModal', compact('user'))->render();
    }

    public function show($id)
    {
        return view('Back.'.$this->folder.'.show', [(string)$this->name => $this->find($id)]);
    }

    public function sendUserMessage(Request $request)
    {
        return Crud::sendModelMessage($request);
    }

    public function changeStatus(Request $request)
    {
        return Crud::setStatus($this->class, $request);
    }

    public function create()
    {
        return view('Back.Crud.create', self::formFields());
    }

    public function edit($id)
    {
        return view('Back.Crud.edit', self::formFields('edit', $this->model::findOrFail($id)));
    }

    public function search($request)
    {
        $data['search'] = true;

        if(is_null($request->term)) return redirect()->route('users.index');

        $data['users'] = $this->model::search($request->term)->paginate(10);

        return view('Back.Users.index', $data);
    }

    public function formFields($type = 'create', $currentModel = null)
    {
        $data['model'] = $this->name;

        $data['address'] = $this->model::addresses();

        if($type == 'edit') $data['currentModel'] = $currentModel;

        return $data;
    }
}
