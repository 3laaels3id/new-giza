<?php

namespace App\Repository\Eloquent\Sql;

use App\Facade\Support\Warning;
use Exception;
use App\Facade\Crud;
use App\Models\Role;
use App\Models\Admin;
use App\Jobs\SendEmail;
use App\Mail\SendAdminMail;
use Illuminate\Http\Request;
use App\Exports\AdminsExport;
use App\Facade\Support\CrudMessage;
use Illuminate\Support\Facades\Auth;
use App\Repository\Contracts\IAdminRepository;

class AdminRepository extends BaseRepository implements IAdminRepository
{
    public function __construct(Admin $model)
    {
        $this->model = $model;

        parent::__construct($model, new AdminsExport());
    }

    public function index()
    {
        $data['search'] = false;

        $data['admins'] = Admin::where('role_id', '!=', 1)->with('role')->has('role')->latest()->paginate(10);

        return view('Back.Admins.index', $data);
    }

    public function store($request)
    {
        $modelData = $request->except(['_token', '_method']);

        $modelData['status'] = $request->has('status') ? $request->status : false;

        return Crud::store($this->class, $modelData);
    }

    public function update($request, $currentModel)
    {
        $modelData = $request->except(['password', '_token', 'password_confirmation', '_method']);

        if($request->password) $modelData['password'] = $request->password;

        $modelData['status'] = $request->has('status') ? $request->status : false;

        return Crud::update($this->class, $modelData, $currentModel);
    }

    public function changeStatus(Request $request)
    {
        return Crud::setStatus($this->class, $request);
    }

    public function create()
    {
        return view('Back.Crud.create', self::formFields());
    }

    public function edit($id)
    {
        return view('Back.Crud.edit', self::formFields('edit', Admin::findOrFail($id)));
    }

    public function showAdminMessage($id)
    {
        if (!$admin = Admin::find($id)) return Warning::adminIsNotFound();

        return view('Back.Admins.showAdminMailSendModal', compact('admin'))->render();
    }

    public function sendAdminMessage(Request $request)
    {
        try
        {
            if(is_null($request->title)) return CrudMessage::warningWithInput(trans('api.title-field-is-required'), $request->all());

            if(is_null($request->message)) return CrudMessage::warningWithInput(trans('api.message-field-is-required'), $request->all());

            SendEmail::dispatch($request->email, new SendAdminMail($request->message, $request->title));

            return CrudMessage::success();
        }
        catch (Exception $e)
        {
            return CrudMessage::error($e);
        }
    }

    public function adminProfile()
    {
        return view('Back.Admins.profile', ['admin' => Auth::guard('admin')->user()]);
    }

    public function updateAdminProfile(Request $request)
    {
        $adminData = $request->except(['password', '_token', 'password_confirmation', '_method']);

        if($request->password) $adminData['password'] = $request->password;

        return Crud::update($this->class, $adminData, Auth::guard('admin')->user());
    }

    public function formFields($type = 'create', $currentModel = null)
    {
        $data = ['model' => $this->name, 'roles' => Role::getInSelectForm()];

        if($type == 'edit') $data['currentModel'] = $currentModel;

        return $data;
    }
}
