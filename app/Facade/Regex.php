<?php

namespace App\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * @method static email($value)
 * @method static countryCode($value)
 * @method static arabic($value)
 * @method static english($value)
 * @method static youtube($value)
 * @method static latitude($value)
 * @method static longitude($value)
 * @method static password($value)
 * @method static membership($value)
 * @method static noSpaces($value)
 *
 * @see \App\Facade\Process\RegexProcess
 */
class Regex extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Regex';
    }
}
