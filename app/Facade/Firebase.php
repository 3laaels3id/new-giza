<?php


namespace App\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * @method static pushNotification(array $data)
 * @method static send(array $data)
 * @method static sendByTopic(array $data)
 *
 * @see \App\Facade\Process\FirebaseProcess
 */
class Firebase extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Firebase';
    }
}
