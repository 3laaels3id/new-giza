<?php

namespace App\Facade;

use Illuminate\Support\Facades\Facade;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * @method static login($resource, $request, string $guard = 'api', Boolean $onlyAuth = false)
 * @method static register(string $class, $resource, $request, Boolean $withCreated = false, $other = [])
 * @method static store(string $class, $request, $resource = null, Boolean $imageModel = false)
 * @method static resendModelRegisterActiveCode($request)
 * @method static forgetPassword(string $class, $request)
 * @method static resetPassword(string $class, $request)
 * @method static changePassword(string $class, $request)
 * @method static setNewPassword(string $class, $request)
 * @method static checkResetCode(string $class, $request)
 * @method static resendResetCode(string $class, $request)
 * @method static checkModelPhoneExists($model, $request)
 * @method static updateModelProfile($request, string $guard = 'api', $resource = null)
 * @method static forgetPasswordByPhone(string $class, $request)
 * @method static checkModelActiveCode($request)
 * @method static logout(string $guard = 'api')
 *
 * @see \App\Facade\Process\ApiProcess
 */
class Api extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Api';
    }
}
