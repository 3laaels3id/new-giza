<?php

namespace App\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * @method static tapCompany(array $arr)
 * @method static myFatoorah(array $arr)
 * @method static payTap(array $arr)
 *
 * @see \App\Facade\Process\PayProcess
 */
class Pay extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Pay';
    }
}
