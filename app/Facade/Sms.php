<?php

namespace App\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * @method static hisms($number, $message)
 * @method static malath($number, $message)
 * @method static jawal($number, $message)
 *
 * @see \App\Facade\Process\SmsProcess
 */
class Sms extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Sms';
    }
}
