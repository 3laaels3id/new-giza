<?php

namespace App\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * @method static store(string $class, $request, string $fileName = 'image', array $others = [], $withCreated = false)
 * @method static update(string $class, $request, $currentModel, string $fileName = 'image', array $others = [])
 * @method static storeTranslatedModel(string $class, $request, string $fileName = 'image', $other = [])
 * @method static updateTranslatedModel(string $class, $request, $currentModel, string $fileName = 'image', $other = [])
 * @method static setStatus(string $class, $request)
 * @method static delete(string $class, $id)
 * @method static getModelsInSelectedForm($model, $name, $withIds = true, $exceptedIds = [])
 * @method static sendModelMessage($request)
 * @method static sendPluralModelNotification($modelClass, $request)
 * @method static sendModelNotification($modelClass, $request)
 * @method static block($currentModel)
 * @method static unblock($currentModel)
 * @method static load_translated_attrs($model)
 * @method static getModelWithTokens($model)
 * @method static setBooleanColumnValue($model, $column, $value, $withoutResponse = false)
 * @method static save_translated_attrs($model, $formTranslatedAttrs, $otherAttrs = null, $excepted = [])
 *
 * @see \App\Facade\Process\CrudProcess
 */

class Crud extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Crud';
    }
}
