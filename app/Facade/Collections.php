<?php

namespace App\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * @method static berMonthCount($collection)
 * @method static berDayCount($collection)
 * @method static berMonthAndDaysCount($collection)
 * @method static getTimeSum($collection, $col)
 * @method static getRate($rates)
 * @method static getYearMonthsWithOrWithoutInst($with)
 * @method static getRates($rates)
 *
 * @see \App\Facade\Process\CollectionsProcess
 */
class Collections extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Collections';
    }
}
