<?php

namespace App\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * @method static response($data = [], $type = 'success', $pagination = null, $msg = '', $code = 200, $err = '')
 * @method static warning($message = '', $data = null)
 * @method static warningTrans($key = '', $data = null)
 * @method static success($message = '', $data = null)
 * @method static successTrans($key = '', $data = null)
 * @method static unAuth($message = '', $code = 400, $data = null)
 * @method static validation($message, $data = null)
 * @method static validationTrans($key, $data = null)
 * @method static fails($error = '', $data = null)
 * @method static exceptionFails($e, $data = null)
 * @method static pagination($request, $collection, $resource)
 * @method static dynamicPages($name)
 * @method static notFound($name)
 * @method static authFail($code = 400, $data = null)
 * @method static notActive($name)
 *
 * @see \App\Facade\Process\ApiResponseProcess
 */
class ApiResponse extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'ApiResponse';
    }
}
