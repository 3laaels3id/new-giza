<?php

namespace App\Facade\Support;

use BadMethodCallException;
use App\Facade\ApiResponse;
use Illuminate\Support\Facades\Facade;

/**
 * @method static thisAccountIsBlocked()
 * @method static userStatusIsNotActive()
 * @method static phoneAlreadyExists()
 * @method static userCodeInvalid()
 * @method static userIsNotFoundOrNotActived()
 * @method static passwordNotChanged()
 * @method static pleaseActiveYourPhoneFirst()
 * @method static userNotFound()
 * @method static passwordIsNotMatched()
 * @method static courseIsNotActive()
 * @method static doctorIsNotActive()
 * @method static sorryThisCourseNotYours()
 * @method static userAlreadySubscribeInCourse()
 * @method static sorrySubscriptionNotFound()
 * @method static videoIsAlreadyWatched()
 * @method static videoIsNotFound()
 * @method static sorryThisIsNotYourAccount()
 * @method static providerHasNoBalance()
 * @method static tournamentIsNotFound()
 * @method static sportIsNotFound()
 * @method static userAlreadySubscribeInTournament()
 * @method static tournamentIsFinished()
 * @method static tournamentWillStartToday()
 * @method static adminIsNotFound()
 * @method static tournamentMaxSportsGreaterThanActual()
 * @method static sorryTournamentAlreadyHasSports()
 * @method static sportsAlreadyExists()
 */
class Warning extends Facade
{
    public static function __callStatic($name, $arguments)
    {
        $trans = 'api.' . snake($name)->slug();

        if(trans($trans) === $trans) {
            throw new BadMethodCallException("Method [$name] does not exist on view.");
        }

        if(str()->contains(request()->url(), 'api')) return ApiResponse::warning(trans($trans));

        return CrudMessage::warning(trans($trans));
    }
}
