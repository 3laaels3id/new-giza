<?php

namespace App\Facade\Support;

trait Malath
{
    private static function get_malath_message_by_code($code)
    {
        switch ($code)
        {
            case 0:
                return "Message send successfully";
                break;

            case 101:
                return "Parameter are missing";
                break;

            case 104:
                return "Either user name or password are missing or your Account is on hold.";
                break;

            case 105:
                return "Credit are not available.";
                break;

            case 106:
                return "Wrong Unicode.";
                break;

            case 107:
                return "Blocked Sender Name.";
                break;

            case 108:
                return "Missing Sender name.";
                break;

            case 1010:
                return "SMS Text Grater that 6 part .";
                break;

            default:
                return "Unknown Error !.";
        }
    }

    private static function malathData($username, $password, $number, $sms_sender, $message)
    {
        return [
            'username' => $username, // 'Fixupsms'
            'password' => $password, //'Ka0559667220',
            'mobile'   => $number,
            'unicode'  => 'U',
            'message'  => $message,
            'sender'   => $sms_sender, // Fixup
        ];
    }
}
