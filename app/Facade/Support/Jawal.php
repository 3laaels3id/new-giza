<?php

namespace App\Facade\Support;

trait Jawal
{
    private static function jawalData($username, $password, $number, $sms_sender, $message)
    {
        return [
            'username' => $username,
            'password' => $password,
            'unicode'  => 'E',
            'mobile'   => $number,
            'message'  => $message,
            'sender'   => $sms_sender,
        ];
    }
}
