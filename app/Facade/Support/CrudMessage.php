<?php

namespace App\Facade\Support;

use App\Facade\Traits\CrudResponseTrait;

class CrudMessage
{
    use CrudResponseTrait;

    public static function restore($obj)
    {
        return back()->with('success', translated('restore', $obj));
    }

    public static function remove($obj)
    {
        return back()->with('success', translated('remove', $obj));
    }

    public static function success($message = '')
    {
        $mess = $message == '' ? trans('api.request-done-successfully') : $message;

        return back()->with('success', $mess);
    }

    public static function error($e)
    {
        return back()->with('danger', self::setExceptionMessage($e)['mess']);
    }

    public static function warningWithInput($mess, $inputs)
    {
        return back()->with('danger', $mess)->withInput($inputs);
    }

    private static function setExceptionMessage($e)
    {
        $mess = is_string($e) ? $e : getFormattedException($e);

        if($mess == '') $mess = trans('api.server-internal-error');

        $code = is_string($e) ? 400 : 500;

        return ['mess' => $mess, 'code' => $code];
    }
}
