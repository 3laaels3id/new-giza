<?php

namespace App\Facade\Support;

trait Hisms
{
    private static function get_message_by_code($code)
    {
        switch ($code)
        {
            case 1:
                return "إسم المستخدم غير صحيح";
                break;

            case 2:
                return "كلمة المرور غير صحيحة";
                break;

            case 404:
                return "لم يتم إدخال جميع البرامترات المطلوبة";
                break;

            case 403:
                return "تم تجاوز عدد المحاولات المطلوبة";
                break;

            case 504:
                return "الحساب معطل";
                break;

            case 4:
                return "لا يوجد أرقام";
                break;

            case 5:
                return "لا يوجد رسالة";
                break;

            case 6:
                return "سيندر خطئ";
                break;

            case 7:
                return "سيندر غير مفعل";
                break;

            case 8:
                return "الرسالة تحتوي كلمة ممنوعة";
                break;

            case 9:
                return "لا يوجد رصيد";
                break;

            case 10:
                return "صيغة التاريخ خاطئة";
                break;

            case 11:
                return "صيغة الوقت خاطئة";
                break;

            default:
                return "تم الإرسال";
        }
    }

    private static function hismsData($username, $password, $number, $sms_sender, $message)
    {
        return [
            'send_sms' => '',
            'username' => $username, // '966540060505'
            'password' => $password, //'niksyh-3cogxo-jobQiv',
            'numbers'  => $number,
            'sender'   => $sms_sender, // 'Aplus'
            'message'  => $message,
        ];
    }
}
