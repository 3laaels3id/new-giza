<?php

namespace App\Facade\Support;

trait Yamamah
{
    private static function yamamahData($username, $password, $number, $sms_sender, $message)
    {
        return [
            "Username"        => $username,
            "Password"        => $password,
            "Tagname"         => $sms_sender,
            "Message"         => $message,
            "RecepientNumber" => $number,
        ];
    }
}
