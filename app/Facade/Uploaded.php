<?php

namespace App\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * @method static image($img, $model)
 * @method static images($imgs, $model)
 * @method static file($file, $type = 'pdf', $folder = 'files')
 * @method static defaultImage($model, $folder)
 * @method static defaultVideo($file)
 * @method static updateAndDelete($request, $currentModel, $file_name, $folder)
 * @method static defaultImages($model, $folder)
 * @method static removeImage($id, $model)
 * @method static setOrUpdateModelImage($model, $image, $model_name)
 * @method static setOrUpdateModelImages($model, $images, $model_name)
 * @method static uploadAndCreate($created, $request, $fileName, $model_name)
 * @method static videoUploadWithThumbnail($request, $filename)
 * @method static video($file)
 * @method static fileSize($file_size)
 * @method static storageFilePath($folder, $filename)
 * @method static checkForVideo($request, $filename)
 * @method static default()
 * @method static thumbnail($filename)
 *
 * @see \App\Facade\Process\UploadedProcess
 */
class Uploaded extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Uploaded';
    }
}
