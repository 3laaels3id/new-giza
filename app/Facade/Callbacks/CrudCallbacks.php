<?php

namespace App\Facade\Callbacks;

use App\Facade\Firebase;
use App\Models\Notification;

trait CrudCallbacks
{
    private static function mapOnPlural($request): callable
    {
        return function($model) use($request)
        {
            $notificationData = Notification::setNotificationData($request->title, $request->body,'dash',0);

            $model->notifications()->create($notificationData);

            $notificationData['fcm_token'] = $model->fcm->fcm;

            $notificationData['notificationType'] = 'dash';

            Firebase::pushNotification($notificationData);
        };
    }

    private static function onlyPluralHaveFcm(): callable
    {
        return function ($query){
            return $query->where('fcm', '!=', null);
        };
    }
}
