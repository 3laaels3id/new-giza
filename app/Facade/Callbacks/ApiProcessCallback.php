<?php

namespace App\Facade\Callbacks;

use App\Facade\Regex;

trait ApiProcessCallback
{
    private static function loginChecker($request) : callable
    {
        return function($query) use ($request) {
            return Regex::email($request->email) ? $query->where('email', $request->email) : $query->where('phone', $request->phone);
        };
    }

    private static function mapOnCodes(): callable
    {
        return function ($item){
            return $item->delete();
        };
    }

    private static function credentials($request)
    {
        $type = is_numeric($request->username) ? 'phone' : 'username';

        return [
            $type      => $request->get('username'),
            'password' => $request->password,
        ];
    }

    private static function username($request): array
    {
        $type = is_numeric($request->username) ? 'phone' : 'username';

        return [$type => $request->get('username')];
    }

    private static function setNewTokenAndFcm($request, $auth, $token)
    {
        if($request->has('device_token')) $auth->fcm()->update(['fcm' => $request->device_token]);

        $auth->token()->update(['jwt' => $token, 'ip' => $request->ip()]);
    }
}
