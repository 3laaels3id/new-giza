<?php

namespace App\Facade\Payment;

use Exception;
use App\Models\Payment;
use App\Facade\ApiResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class MyFatoorah
{
    public static function getFatoorahChargeData($arr)
    {
        $user = $arr['user'];

        $data['PaymentMethodId']    = $arr['payment_method_id'];
        $data['CustomerName']       = $user->full_name;
        $data['DisplayCurrencyIso'] = "sar";
        $data['MobileCountryCode']  = $user->country_code;
        $data['CustomerMobile']     = $user->phone;
        $data['CustomerEmail']      = $user->email;
        $data['InvoiceValue']       = $arr['total'];
        $data['CallBackUrl']        = $arr['callBackUrl'];
        $data['ErrorUrl']           = $arr['errorUrl'];
        $data['Language']           = "en";
        $data['UserDefinedField']   = self::getUserDefinedFields($user, $arr);

        $data['SupplierCode']                 = 0;
        $data['InvoiceItems'][0]['ItemName']  = "Course";
        $data['InvoiceItems'][0]['Quantity']  = 1;
        $data['InvoiceItems'][0]['UnitPrice'] = $arr['total'];
        $data['SourceInfo'] = 'String';

        return $data;
    }

    public static function success($request)
    {
        DB::beginTransaction();
        try
        {
            $response = self::getFatoorahPaymentStatus($request->paymentId);

            if (isset($response['ValidationErrors'])) return self::myFatoorahErrors($response);

            $res = self::myFatoorahSuccess($request, $response);

            DB::commit();

            return $res;
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return ApiResponse::exceptionFails($e);
        }
    }

    public static function error($request)
    {
        try
        {
            dd($request->all());
        }
        catch (Exception $e)
        {
            return ApiResponse::exceptionFails($e);
        }
    }

    private static function myFatoorahSuccess($request, $response): string
    {
        app()->setLocale($request->lang);

        $res = Payment::setPayment($response, $request);

        return "<script type='text/javascript'>".json_encode($res)."</script>";
    }

    private static function getUserDefinedFields($user, $arr)
    {
        return json_encode([
            'user_id'   => $user->id,
            'course_id' => $arr['course_id'],
            'doctor_id' => $arr['doctor_id'],
            'price'     => $arr['price'],
            'discount'  => $arr['discount'],
            'total'     => $arr['total'],
            'coupon_id' => $arr['coupon_id'],
        ]);
    }

    private static function getFatoorahPaymentStatus($paymentId)
    {
        $data = ['keyType' => 'PaymentId', 'key' => $paymentId];

        $url = my_fatoorah_base_url()."/v2/GetPaymentStatus";

        $token = getCachedSetting('payment_key')->value;

        return Http::withToken($token)->post($url, $data)->json();
    }

    private static function myFatoorahErrors($response): string
    {
        $payment['success']  = 0;
        $payment['order_id'] = 0;
        $payment['error']    = $response['ValidationErrors'];

        return "<script type=text/javascript>".json_encode($payment)."</script>";
    }
}
