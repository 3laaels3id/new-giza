<?php

namespace App\Facade\Traits;

use Illuminate\Support\Facades\Http;

trait FirebaseNotifications
{
    public static function create_curl_notification($to, $data)
    {
        $token = getCachedSetting('notification_key')->value;

        $url = 'https://fcm.googleapis.com/fcm/send';

        return Http::withToken($token)->post($url, self::setRequestBody($to, $data));
    }

    private static function getData($data)
    {
        return [
            'title'                 => $data['title'],
            'body'                  => $data['body'],
            'notificationType'      => $data['notificationType'],
            'notificationable_type' => $data['type'],
            'notificationable_id'   => $data['type_id'],
            'sound'                 => 'default',
            'click_action'          => 'FCM_PLUGIN_ACTIVITY',
        ];
    }

    private static function setPushNotificationData($data)
    {
        return [
            'data'         => $data,
            'notification' => $data,
            'priority'     => 'high'
        ];
    }

    private static function setRequestBody($to, $data)
    {
        return [
            'to'           => $to,
            'data'         => $data,
            'notification' => $data,
        ];
    }
}
