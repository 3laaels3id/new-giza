<?php

namespace App\Facade\Traits;

use Exception;
use App\Jobs\SendEmail;
use App\Facade\Firebase;
use App\Mail\SendAdminMail;
use App\Models\Notification;
use App\Facade\Support\CrudMessage;
use Illuminate\Support\Facades\DB;

trait CrudTrait
{
    public function block($currentModel)
    {
        DB::beginTransaction();
        try
        {
            $currentModel->update(['is_blocked' => 1]);

            $message = trans('api.this-account-is-blocked');

            // Sms::hisms($currentModel->mobile_phone, $message);

            SendEmail::dispatch($currentModel->email, new SendAdminMail($message, 'رسالة حظر'));

            DB::commit();

            return CrudMessage::success();
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::error($e);
        }
    }

    public function unblock($currentModel)
    {
        DB::beginTransaction();
        try
        {
            $currentModel->update(['is_blocked' => 0]);

            $message = trans('api.user-account-unblocked');

            // Sms::malath($currentModel->mobile_phone, $message);

            SendEmail::dispatch($currentModel->email, new SendAdminMail($message, 'رسالة حظر'));

            DB::commit();

            return CrudMessage::success();
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::error($e);
        }
    }

    public function setStatus($model, $request)
    {
        $model_name = getModelName($model);

        $_model = $model::find($request->id);

        if(!$model) return CrudMessage::fails(trans('api.model-not-found', ['var' => trans('back.' . $model_name)]));

        if (!$updateStatus = updateStatus($request->all(), $_model)) return CrudMessage::fails(trans('api.server-internal-error'));

        return CrudMessage::edit($model_name);
    }

    public function sendModelMessage($request)
    {
        try
        {
            SendEmail::dispatch($request->email, new SendAdminMail($request->message, $request->title));

            return CrudMessage::success();
        }
        catch (Exception $e)
        {
            return CrudMessage::error($e);
        }
    }

    public function sendModelNotification($modelClass, $request)
    {
        DB::beginTransaction();
        try
        {
            $model = $modelClass::find($request->user_id);

            $notificationData = Notification::setNotificationData($request->title, $request->body,'dash',0);

            $model->notifications()->create($notificationData);

            $notificationData['fcm_token'] = $model->fcm->fcm;

            $notificationData['notificationType'] = 'dash';

            Firebase::pushNotification($notificationData);

            DB::commit();

            return CrudMessage::success();
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::error($e);
        }
    }

    public function sendPluralModelNotification($modelClass, $request)
    {
        DB::beginTransaction();
        try
        {
            $modelClass::whereHas('fcm', self::onlyPluralHaveFcm())->get()->map(self::mapOnPlural($request));

            DB::commit();

            return CrudMessage::success();
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::error($e);
        }
    }

    public function save_translated_attrs($model, $formTranslatedAttrs, $otherAttrs = null, $excepted = [])
    {
        foreach (sitelangs() as $lang => $name)
        {
            foreach ($model->translatedAttributes as $attr)
            {
                $model->translateOrNew($lang)->$attr = $formTranslatedAttrs[$lang][$attr];
            }
        }

        if ($otherAttrs != null)
        {
            $otherAttrs = except($otherAttrs, ['_method', '_token'] + $excepted);

            foreach ($otherAttrs as $key => $value)
            {
                $model->$key = $value;
            }
        }
    }

    public function load_translated_attrs($model)
    {
        foreach (sitelangs() as $lang => $name)
        {
            $model->$lang = [];

            foreach ($model->translatedAttributes as $attr)
            {
                $model->$lang = $model->$lang + [$attr => $model->getTranslation($lang)->$attr];
            }
        }
    }

    public function getModelsInSelectedForm($model, $name, $withIds = true, $exceptedIds = [])
    {
        $list = [];

        $modelsDB = $model::whereNotIn('id',$exceptedIds)->where('status', 1)->get();

        if($withIds)
        {
            foreach ($modelsDB as $modelDB)
            {
                $list[$modelDB->id] = ucwords($modelDB->$name);
            }
        }
        else
        {
            foreach ($modelsDB as $modelDB)
            {
                $list[$modelDB->$name] = ucwords($modelDB->$name);
            }
        }

        return $list;
    }

    public function getModelWithTokens($model)
    {
        $collection = $model::whereHas('fcm', self::getUsersWithTokensCallback())->get();

        $withTokens = [];

        foreach ($collection as $index => $result)
        {
            $withTokens[$index]['full_name'] = $result->full_name;
            $withTokens[$index]['fcm']       = $result->fcm->fcm;
        }

        return $withTokens;
    }

    private static function getUsersWithTokensCallback(): callable
    {
        return function($query){
            return $query->where('fcm', '!=', null);
        };
    }

    private static function setBasics($request, $other, $model)
    {
        $modelData = !is_array($request) ? $request->all() + $other : $request;

        $text = lower(getModelName($model));

        $modelData = except($modelData, ['_method', '_token', 'is-main-category', 'password_confirmation']);

        return ['modelData' => $modelData, 'model_name' => $text, 'image_folder' => $text->plural()];
    }
}
