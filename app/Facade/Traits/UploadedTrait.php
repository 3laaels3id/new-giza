<?php

namespace App\Facade\Traits;

use Illuminate\Support\Facades\File;

trait UploadedTrait
{
    public static function storageFilePath($folder, $file)
    {
        return storage_path('app'.ds().'public'.ds().'uploaded'.ds().$folder.ds().$file);
    }

    public static function setOrUpdateModelImage($model, $image)
    {
        if($model->image) return $model->image()->update(['image' => $image]);

        return $model->image()->create(['image' => $image]);
    }

    public static function makeDir($folder)
    {
        $path = storage_path('app'.ds().'public'.ds().'uploaded'.ds().$folder.ds());

        if (!File::exists($path)) mkdir($path, 0777, true);

        return $path;
    }

    public static function getFileName($type, $file)
    {
        $name  = $type . '_'; // file type

        $name .= random(12) . '_'; // random string

        $name .= date('Y-m-d') . '.'; // current date

        $name .= strtolower($file->getClientOriginalExtension()); // file extension

        return $name;
    }

    private static function storageVideoFilePath($file)
    {
        return storage_path('app'.ds().'public'.ds().'uploaded'.ds().'files'.ds().'video'.ds().$file);
    }

    public static function default()
    {
        return asset('public/admin/img/avatar10.jpg');
    }

    private static function getVideoInfo($video_path)
    {
        $file = self::getVideoInSeconds($video_path);

        return [
            'duration' => getTimeFromSeconds($file['playtime_seconds']),
            'width'    => $file['video']['resolution_x'],
            'height'   => $file['video']['resolution_x']
        ];
    }

    private static function getVideoInSeconds($video_path)
    {
        return (new \getID3)->analyze($video_path);
    }

    private static function formatFileSize($size, $type)
    {
        return limit($size, 5, '') . ' ' . trans('back.' . $type);
    }

    private static function getFinalResult($videoName, $thumbnail_image, $getVideoInfo, $file)
    {
        return [
            'file'           => $videoName,
            'size'           => $file->getSize(),
            'width'          => $getVideoInfo['width'],
            'height'         => $getVideoInfo['height'],
            'duration'       => $getVideoInfo['duration'],
            'thumbnail_name' => $thumbnail_image,
        ];
    }
}
