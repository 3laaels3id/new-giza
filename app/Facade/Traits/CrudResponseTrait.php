<?php

namespace App\Facade\Traits;

trait CrudResponseTrait
{
    public static function crudResponse($mess, $requestStatus = true, $code = 200)
    {
        return response()->json(['requestStatus' => $requestStatus, 'message' => $mess], $code);
    }

    public static function warning($mess, $code = 401)
    {
        return self::crudResponse($mess, false, $code);
    }

    public static function fails($e = '')
    {
        $res = self::setExceptionMessage($e);

        return self::crudResponse($res['mess'], false, $res['code']);
    }

    public static function deleteResponse($mess)
    {
        return self::crudDeleteResponse('', $mess, true);
    }

    public static function successResponse($message = '')
    {
        $mess = $message == '' ? trans('api.request-done-successfully') : $message;

        return self::crudResponse($mess);
    }

    public static function edit($obj)
    {
        return self::crudResponse(translated('edit', $obj));
    }

    public static function add(string $obj)
    {
        return self::crudResponse(translated('add', $obj));
    }

    public static function delete($obj)
    {
        return self::crudResponse(translated('delete', $obj));
    }

    public static function deleteResponseFails($e = '')
    {
        return self::crudDeleteResponse(self::setExceptionMessage($e)['mess'], '', false);
    }

    private static function crudDeleteResponse($error, $mess = '', $deleteStatus = true)
    {
        return response()->json(['deleteStatus' => $deleteStatus, 'error' => $error, 'message' => $mess]);
    }
}
