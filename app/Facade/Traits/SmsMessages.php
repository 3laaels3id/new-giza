<?php

namespace App\Facade\Traits;

use App\Models\Setting;
use App\Facade\Support\Hisms;
use App\Facade\Support\Jawal;
use App\Facade\Support\Malath;
use App\Facade\Support\Yamamah;
use Illuminate\Support\Facades\Cache;

trait SmsMessages
{
    use Hisms, Malath, Jawal, Yamamah;

    private static function getCachedSetting()
    {
        return Cache::remember('get_cached_setting', 60*60, function(){
            return Setting::whereType('SMS')->get();
        });
    }
}
