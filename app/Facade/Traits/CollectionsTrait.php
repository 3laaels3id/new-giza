<?php

namespace App\Facade\Traits;

trait CollectionsTrait
{
    private static function getCollectionByDayName($day): callable
    {
        return function ($item) use($day) {
            return $item->created_at->format('l') == $day;
        };
    }

    private static function byMonthName($month): callable
    {
        return function ($item) use($month) {
            return $item->created_at->format('F') == $month;
        };
    }

    private static function mapOnRates(): callable
    {
        return function($item){
            return (int)$item->rate;
        };
    }
}
