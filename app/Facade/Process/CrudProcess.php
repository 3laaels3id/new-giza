<?php

namespace App\Facade\Process;

use Exception;
use App\Facade\Uploaded;
use App\Facade\Traits\CrudTrait;
use Illuminate\Support\Facades\DB;
use App\Facade\Support\CrudMessage;
use App\Facade\Callbacks\CrudCallbacks;

class CrudProcess
{
    use CrudCallbacks, CrudTrait;

    public function store($model, $request, $fileName = 'image', $others = [], $withCreated = false)
    {
        DB::beginTransaction();
        try
        {
            $basics = self::setBasics($request, $others, $model);

            $created = $model::create(except($basics['modelData'], [(string)$fileName]));

            Uploaded::uploadAndCreate($created, request(), $fileName, $basics['model_name']);

            DB::commit();

            return $withCreated ? $created : CrudMessage::add($basics['model_name']);
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::fails($e);
        }
    }

    public function storeTranslatedModel($model, $request, $fileName = 'image', $other = [])
    {
        DB::beginTransaction();
        try
        {
            $basics = self::setBasics($request, $other, $model);

            $modelData = $basics['modelData'];

            $model_name = $basics['model_name'];

            $formTranslatedAttrs = only($modelData, array_keys(sitelangs()));

            $modelData = except($modelData, array_keys(sitelangs()));

            $object = new $model($modelData);

            self::save_translated_attrs($object, $formTranslatedAttrs);

            $object->save();

            if(isset($modelData[$fileName]))
            {
                if(!is_array($modelData[$fileName])) Uploaded::uploadAndCreate($object, $modelData, $fileName, $model_name);

                if (
                    is_array($modelData[$fileName]) &&
                    count($modelData[$fileName]) > 0 &&
                    $modelData[$fileName][0] != null)
                {
                    $object->images()->createMany(Uploaded::images($modelData[$fileName], $model_name));
                }
            }

            if(
                isset($modelData[$fileName]) &&
                is_array($modelData[$fileName]) &&
                count($modelData[$fileName]) > 0 &&
                $modelData[$fileName][0] != null
            )
            {
                Uploaded::setOrUpdateModelImages($object, $modelData[$fileName], $model_name);
            }


            DB::commit();

            return CrudMessage::add(getModelName($model));
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::fails($e);
        }
    }

    public function update($model, $request, $currentModel, $fileName = 'image', $others = [])
    {
        DB::beginTransaction();
        try
        {
            $basics = self::setBasics($request, $others, $model);

            $data = except($basics['modelData'], ['_method', '_token', 'submit']);

            if(isset($data[$fileName])) Uploaded::updateAndDelete($request, $currentModel, $fileName, $basics['image_folder']);

            $currentModel->update($data);

            DB::commit();

            return CrudMessage::edit($basics['model_name']);
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::fails($e);
        }
    }

    public function updateTranslatedModel($model, $request, $currentModel, $fileName = 'image', $other = [])
    {
        DB::beginTransaction();
        try
        {
            $basics = self::setBasics($request, $other, $model);

            $modelData = $basics['modelData'];

            if(isset($modelData[$fileName]))
            {
                if(!is_array($modelData[$fileName])) Uploaded::updateAndDelete($request, $currentModel, $fileName, $basics['image_folder']);

                if (is_array($modelData[$fileName]) && count($modelData[$fileName]) > 0 && $modelData[$fileName][0] != null) {
                    $currentModel->images()->createMany(Uploaded::images($modelData[$fileName], $basics['model_name']));
                }
            }

            $formTranslatedAttrs = only($modelData, array_keys(sitelangs()));

            $modelData = except($modelData, array_keys(sitelangs()));

            $modelData = except($modelData, [(string)$fileName]);

            self::save_translated_attrs($currentModel, $formTranslatedAttrs, $modelData);

            $currentModel->save();

            DB::commit();

            return CrudMessage::edit(getModelName($model));
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::fails($e);
        }
    }

    public function delete($model, $id)
    {
        $model_name = getModelName($model);

        if (!$currentModel = $model::findOrFail($id)) return CrudMessage::fails("Sorry, $model_name is not exists !!");

        DB::beginTransaction();
        try
        {
            $currentModel->delete();

            DB::commit();

            return CrudMessage::delete($model_name);
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::deleteResponseFails($e);
        }
    }

    public function setBooleanColumnValue($model, $column, $value, $withoutResponse = false)
    {
        DB::beginTransaction();
        try
        {
            $result = $model->update([$column => $value]);

            DB::commit();

            return $withoutResponse ? $result : CrudMessage::edit(singular($model->getTable()));
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::fails($e);
        }
    }
}
