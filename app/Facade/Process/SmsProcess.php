<?php

namespace App\Facade\Process;

use Illuminate\Support\Facades\Http;
use App\Facade\Traits\SmsMessages;

class SmsProcess
{
    use SmsMessages;

    protected static $sms_password, $sms_sender, $sms_number;

    public function __construct()
    {
        $sms = self::getCachedSetting();

        static::$sms_number = $sms->where('key', 'sms_number')->first()->value;

        static::$sms_password = $sms->where('key', 'sms_password')->first()->value;

        static::$sms_sender = $sms->where('key', 'sms_sender_name')->first()->value;
    }

    public static function hisms($number, $message)
    {
        $data = self::hismsData(static::$sms_number, static::$sms_password, $number, static::$sms_sender, $message);

        $code = Http::withHeaders(['content-type: multipart/form-data'])->post('http://hisms.ws/api.php', $data);

        return ['code' => $code, 'message' => self::get_message_by_code($code)];
    }

    public static function malath($number, $message)
    {
        $data = self::malathData(static::$sms_number, static::$sms_password, $number, static::$sms_sender, $message);

        $url = 'http://sms.malath.net.sa/httpSmsProvider.aspx';

        $result = Http::withHeaders(['content-type: multipart/form-data'])->post($url, $data);

        $code = (integer)str_replace(" ","", $result);

        return ['code' => $code, 'message' => self::get_malath_message_by_code($code)];
    }

    public static function jawal($number, $message)
    {
        $data = self::jawalData(static::$sms_number, static::$sms_password, $number, static::$sms_sender, $message);

        $url = 'http://www.jawalsms.net/httpSmsProvider.aspx';

        return Http::withHeaders(['content-type: multipart/form-data'])->post($url, $data);
    }

    public static function yamamah($number, $message)
    {
        $data = self::yamamahData(static::$sms_number, static::$sms_password, $number, static::$sms_sender, $message);

        $url = 'http://api.yamamah.com/SendSMS';

        return Http::withHeaders(['content-type: multipart/form-data'])->post($url, $data);
    }
}
