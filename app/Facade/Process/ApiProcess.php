<?php

namespace App\Facade\Process;

use Exception;
use App\Facade\Sms;
use App\Models\Code;
use App\Jobs\SendEmail;
use App\Facade\Uploaded;
use App\Facade\ApiResponse;
use App\Mail\SendResetCodeMail;
use App\Facade\Support\Warning;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Facade\Callbacks\ApiProcessCallback;

class ApiProcess
{
    use ApiProcessCallback;

    public static function login($resource, $request, $guard = 'api', $onlyAuth = false)
    {
        DB::beginTransaction();
        try
        {
            $token = auth()->guard($guard)->attempt(self::credentials($request));

            if(!$token) return ApiResponse::authFail();

            $auth = auth()->guard($guard)->user();

            if($auth->is_blocked) return Warning::thisAccountIsBlocked();

            if(!$auth->status) return Warning::userStatusIsNotActive();

            self::setNewTokenAndFcm($request, $auth, $token);

            DB::commit();

            return $onlyAuth ? $auth : ApiResponse::response(new $resource($auth));
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return ApiResponse::exceptionFails($e);
        }
    }

    public static function register($model, $resource, $request, $withCreated = false, $other = [])
    {
        $data = !is_array($request) ? $request->all() + $other : $request;

        $model_name = getModelName($model);

        DB::beginTransaction();
        try
        {
            $created = $model::create(except($data, ['image']));

            if(isset($request['image'])) Uploaded::uploadAndCreate($created, $request, 'image', $model_name);

            DB::commit();

            return $withCreated ? $created : ApiResponse::response(new $resource($created));
        }
        catch(Exception $e)
        {
            DB::rollBack();
            return ApiResponse::exceptionFails($e);
        }
    }

    public static function resendModelRegisterActiveCode($request)
    {
        DB::beginTransaction();
        try
        {
            $code = generate_rand_numbers();

            $code = 1111;

            Code::create(['phone' => $request->phone, 'country_code' => $request->country_code, 'code' => $code]);

//            Sms::hisms($request->mobile_phone, "كود التفعيل الخاص بك هو : $code");

            DB::commit();

            return ApiResponse::success();
        }
        catch(Exception $e)
        {
            DB::rollBack();
            return ApiResponse::exceptionFails($e);
        }
    }

    public static function checkModelPhoneExists($model, $request)
    {
        try
        {
            $check = $model::wherePhone($request->phone)->whereCountryCode($request->country_code)->first();

            if($check) return Warning::phoneAlreadyExists();

            $code = generate_rand_numbers();

            $code = 1111;

            Code::create(['phone' => $request->phone, 'country_code' => $request->country_code, 'code' => $code]);

//            Sms::hisms($request->mobile_phone, "كود التفعيل الخاص بك هو : $code");

            return ApiResponse::success();
        }
        catch (Exception $e)
        {
            return ApiResponse::exceptionFails($e);
        }
    }

    public static function checkModelActiveCode($request)
    {
        DB::beginTransaction();
        try
        {
            $codes = Code::wherePhone($request->phone)->whereCountryCode($request->country_code)->get();

            $check = $codes->last();

            if(!$check) return Warning::userNotFound();

            if($check->code != $request->code) return Warning::userCodeInvalid();

            $check->update(['is_active' => 1]);

            DB::commit();

            return ApiResponse::successTrans('activation-done-successfully');
        }
        catch(Exception $e)
        {
            DB::rollBack();
            return ApiResponse::exceptionFails($e);
        }
    }

    public static function forgetPassword($model, $request)
    {
        DB::beginTransaction();
        try
        {
            $user = $model::where('email', $request->email)->first();

            $code = generate_rand_numbers();

            $code = 1111;

            $user->update(['code' => $code]);

            $message = 'كود إعادة تعيين كلمة المرور هو : ' . $code;

//            Sms::hisms($user->mobile_phone, $message);

            SendEmail::dispatch($request->email, new SendResetCodeMail($message));

            DB::commit();

            return ApiResponse::successTrans('sms-send-successfully');
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return ApiResponse::exceptionFails($e);
        }
    }

    public static function resendResetCode($model, $request)
    {
        return self::forgetPassword($model, $request);
    }

    public static function changePassword($model, $request)
    {
        DB::beginTransaction();
        try
        {
            $user = $request->user();

            if (!Hash::check($request['old_password'], $user->password)) return Warning::passwordIsNotMatched();

            if (!$user) return Warning::userIsNotFoundOrNotActived();

            $user->update(['password' => $request['new_password']]);

            DB::commit();

            return ApiResponse::success();
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return ApiResponse::exceptionFails($e);
        }
    }

    public static function checkResetCode($model, $request)
    {
        DB::beginTransaction();
        try
        {
            $user = $model::whereEmail($request->email)->whereCode($request->reset_code)->first();

            if(!$user) return Warning::userCodeInvalid();

            $user->update(['code' => null]);

            DB::commit();

            return ApiResponse::successTrans('user-code-valid');
        }
        catch(Exception $e)
        {
            DB::rollBack();
            return ApiResponse::exceptionFails($e);
        }
    }

    public static function setNewPassword($model, $request)
    {
        DB::beginTransaction();
        try
        {
            $user = $model::whereEmail($request->email)->whereStatus(1)->first();

            if (!$user->update(['password' => $request->password])) return Warning::passwordNotChanged();

            DB::commit();

            return ApiResponse::success();
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return ApiResponse::exceptionFails($e);
        }
    }

    public static function updateModelProfile($request, $guard = 'api', $resource = null)
    {
        DB::beginTransaction();
        try
        {
            $data = $request->validated();

            $user = auth()->guard($guard)->user();

            if($guard == 'api') $folder = 'users';

            else if($guard == 'doctor_api') $folder = 'doctors';

            else $folder = plural($guard);

            if ($request->hasFile('image')) Uploaded::updateAndDelete($request, $user, $request->image, $folder);

            $user->update($data);

            if(!$resource) return ApiResponse::success();

            DB::commit();

            return ApiResponse::response(new $resource($user));
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return ApiResponse::exceptionFails($e);
        }
    }

    public static function logout($guard)
    {
        DB::beginTransaction();
        try
        {
            $auth = auth()->guard($guard);

            $auth->user()->token()->update(['is_logged_in' => 0, 'jwt' => null]);

            $auth->logout();

            DB::commit();

            return ApiResponse::success();
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return ApiResponse::exceptionFails($e);
        }
    }
}
