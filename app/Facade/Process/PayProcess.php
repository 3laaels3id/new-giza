<?php

namespace App\Facade\Process;

use App\Facade\ApiResponse;
use App\Facade\Payment\TapCompany;
use App\Facade\Payment\MyFatoorah;
use Illuminate\Support\Facades\Http;

class PayProcess
{
    public static function tapCompany($arr)
    {
        $token = getCachedSetting('payment_key')->value;

        $data = TapCompany::getTapPayChargeData($arr);

        $response = Http::withToken($token)->post('https://api.tap.company/v2/charges', $data)->json();

        if(isset($response['error'])) return ['payment_url' => '', 'error' => $response['error']];

        return ['payment_url' => $response['transaction']['url'], 'error' => ''];
    }

    public static function myFatoorah($arr)
    {
        $token = getSetting('payment_key');

        $baseUrl = my_fatoorah_base_url();

        // InitiatePayment => get the payment methods list;
        $data = MyFatoorah::getFatoorahChargeData($arr);

        $response = Http::withToken($token)->post($baseUrl.'/v2/ExecutePayment', $data)->json();

        if(isset($response['ValidationErrors'])) return ApiResponse::validation('error', ['payment_url' => '', 'error' => $response['ValidationErrors']]);

        return ['payment_url' => $response['Data']['PaymentURL'], 'error' => ''];
    }
}
