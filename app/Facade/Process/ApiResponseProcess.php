<?php

namespace App\Facade\Process;

class ApiResponseProcess
{
    public static function response($data = [], $type = 'success', $pagination = null, $msg = '', $code = 200, $err = '')
    {
        if($type == 'success')
        {
            if($pagination)
            {
                $res['data']         = $data;
                $res['total']        = $pagination['total'];
                $res['last_page']    = $pagination['last_page'];
                $res['perPage']      = $pagination['perPage'];
                $res['current_page'] = (int)request()->page != 0 ? (int)request()->page : 1;
            }
            else
            {
                $res = $data;
            }

            $dataObject = ['data' => $res, 'status' => true, 'error' => ''];

            $dataObject['message'] = ($msg == '') ? trans('api.request-done-successfully') : $msg;

            return response()->json($dataObject, $code);
        }

        if($type == 'warning') {
            return response()->json(['data' => $data, 'status' => false, 'message' => $msg, 'error' => ''], $code);
        }

        $response = ['data' => $data,'status' => false, 'message' => $msg];

        $response['error'] = $err;

        return response()->json($response, $code != 200 ? $code : 500);
    }

    public static function warning($message = '', $data = null)
    {
        return self::response($data,'warning',null, $message,400);
    }

    public static function warningTrans($key, $data = null)
    {
        return self::warning(trans('api.'.$key), $data);
    }

    public static function validation($message, $data = null)
    {
        return self::response($data,'warning',null, $message, 422, '');
    }

    public static function validationTrans($key, $data = null)
    {
        return self::response($data,'warning',null, trans('api.' . $key), 422, '');
    }

    public static function success($message = '', $data = null)
    {
        $msg = ($message == '' || $message == 'success') ? trans('api.request-done-successfully') : $message;

        return response()->json(['data' => $data, 'status' => true, 'message' => $msg, 'error' => '']);
    }

    public static function successTrans($key, $data = null)
    {
        return self::success(trans('api.'.$key), $data);
    }

    public static function unAuth($message = '', $code = 401, $data = null)
    {
        $msg = $message == '' ? trans('auth.failed') : $message;

        return self::response($data,'fails',null, $msg, $code, '');
    }

    public static function authFail($code = 401, $data = null)
    {
        return self::unAuth(trans('auth.failed'), $code, $data);
    }

    public static function fails($error = '', $data = null)
    {
        $errorMessage = ($error == '') ? trans('api.server-internal-error') : $error;

        return self::response($data, 'fails', null, '',500, $errorMessage);
    }

    public static function exceptionFails($e, $data = null)
    {
        $message = is_string($e) ? $e : getFormattedException($e);

        if(contains($message, 'cURL error 6')) return self::response_fails($data,'No Internet Connection');

        return self::response_fails($data, $message);
    }

    public static function pagination($request, $collection, $resource)
    {
        $pagination = self::paginated($request->page ?? 1, $collection);

        return self::response($resource::collection($pagination['paginated']),'success', $pagination);
    }

    public static function dynamicPages($name)
    {
        return self::response(getSetting($name . '_' . app()->getLocale()) ?? '');
    }

    public static function notFound($name)
    {
        return self::warning(trans('api.'.$name.'-is-not-found'));
    }

    public static function notActive($name)
    {
        return self::warning(trans('api.'.$name.'-is-not-active'));
    }

    private static function paginated($page, $model, $perPage = 10)
    {
        $start  = ($page == 1) ? 0 : ($page - 1) * $perPage;

        $paginatedModel = ($page == 0 ? $model : $model->slice($start, $perPage));

        $total = $model->count();

        $count_pages = (int)$total / (int)$perPage;

        $page_counter = is_double($count_pages) ? (int)$count_pages + 1 : $count_pages;

        $last_page =  $count_pages >= 1 ? $page_counter : 1;

        $data['total'] 	   = $total;
        $data['paginated'] = $paginatedModel;
        $data['last_page'] = $last_page;
        $data['perPage']   = $perPage;

        return $data;
    }

    private static function response_fails($data, $err)
    {
        return self::response($data, 'fails', null, '',500, $err);
    }
}
