<?php

namespace App\Facade\Process;

use Exception;
use App\Models\Image;
use Illuminate\Support\Facades\DB;
use App\Facade\Support\CrudMessage;
use Illuminate\Support\Facades\File;
use App\Facade\Traits\UploadedTrait;
use Lakshmaji\Thumbnail\Facade\Thumbnail;
use Intervention\Image\Facades\Image as ImageIntervention;

class UploadedProcess
{
    use UploadedTrait;

    public static function image($img, $model): string
    {
        $filename = self::getFileName(lower($model), $img);

        $folder = plural($model)->lower();

        ImageIntervention::make($img)->save(self::makeDir($folder) . $filename);

        return $filename;
    }

    public static function images($files, $model): array
    {
        $images = [];

        foreach($files as $key => $file)
        {
            $images[$key]['image'] = self::image($file, $model);
        }

        return $images;
    }

    public static function file($file, $type = 'pdf', $folder = 'files')
    {
        if(!is_file($file)) return null;

        $filename = self::getFileName($type, $file);

        self::makeDir($folder);

        $file->storeAs('uploaded'.ds().$folder.ds().$type, $filename, 'public');

        return $filename;
    }

    public static function defaultImage($image, $folder): string
    {
        $imageName = is_object($image) ? $image->image : $image;

        $path = self::storageFilePath($folder, $imageName);

        $url = url('public/storage/uploaded/'.$folder.'/'.$imageName);

        return (!is_null($imageName) && File::exists($path)) ? $url : self::default();
    }

    public static function defaultVideo($file): string
    {
        $path = self::storageVideoFilePath($file);

        $url = url('public/storage/uploaded/files/video/'.$file);

        return (!is_null($file) && File::exists($path)) ? $url : '';
    }

    public static function defaultImages($images, $folder)
    {
        $path = self::storageFilePath($folder, $images->first()->image);

        $is_object = is_object($images);

        $count = count($images->toArray());

        $fileExists = File::exists($path);

        if($is_object && $count > 0 && $fileExists) return url('public/storage/uploaded/'.$folder.'/' . $images->first()->image);

        return self::default();
    }

    public static function removeImage($id, $model)
    {
        DB::beginTransaction();
        try
        {
            $image = Image::find($id);

            storage_unlink(plural($model)->lower(), $image->image);

            $image->delete();

            DB::commit();

            return CrudMessage::crudResponse('done');
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::fails($e);
        }
    }

    public static function updateAndDelete($request, $currentModel, $file_name, $folder)
    {
        $_image = isset($currentModel->image) ? $currentModel->image->image : '';

        $old = $file_name == 'image' ? $_image : $currentModel->$file_name;

        $uploaded = $request[$file_name];

        // return old if there is no uploaded image file;
        if(!isset($uploaded)) return $old;

        // delete the old image;
        if($old != '') storage_unlink($folder, $old);

        // upload image first
        $img = self::image($uploaded, singular($folder));

        // update the table row or create it for relationship image();
        return self::setOrUpdateModelImage($currentModel, $img);
    }

    public static function uploadAndCreate($created, $request, $fileName, $model_name)
    {
        if(is_null($request[$fileName])) return false;

        return $created->image()->create(['image' => self::image($request[$fileName], $model_name)]);
    }

    public static function setOrUpdateModelImages($model, $imgs, $model_name)
    {
        if(!isset($imgs) && count($imgs) == 0) return false;

        return $model->images()->createMany(self::images($imgs, $model_name));
    }

    public static function video($file)
    {
        $videoName = self::file($file, 'video');

        $video_path = self::storageVideoFilePath($videoName);

        $thumbnail_image = $videoName . "_thumbnails" . ".jpg";

        Thumbnail::getThumbnail($video_path, self::makeDir('thumbnails'), $thumbnail_image, 4);

        $getVideoInfo = self::getVideoInfo($video_path);

        $data = self::getFinalResult($videoName, $thumbnail_image, $getVideoInfo, $file);

        return $videoName ? $data : false;
    }

    public static function fileSize($file_size)
    {
        $kilobytes = $file_size / 1024; // convert size from bytes to kilobytes;

        if($kilobytes < 1024) return self::formatFileSize($kilobytes, 'kilobyte');

        if($kilobytes > 1024)
        {
            $megabytes = ($kilobytes / 1024);

            if($megabytes > 1024) return self::formatFileSize($megabytes / 1024, 'gigabyte');

            return self::formatFileSize($megabytes, 'megabyte');
        }

        return $file_size;
    }

    public static function thumbnail($filename)
    {
        $path = self::storageFilePath('thumbnails', $filename);

        $url = url('storage/uploaded/thumbnails/' . $filename);

        return (!is_null($filename) && File::exists($path)) ? $url : self::default();
    }

    public static function checkForVideo($request, $fileName)
    {
        return $request->hasFile($fileName) ? self::video($request->into_video) : [];
    }
}
