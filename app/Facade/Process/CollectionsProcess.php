<?php

namespace App\Facade\Process;

use App\Facade\Traits\CollectionsTrait;

class CollectionsProcess
{
    use CollectionsTrait;

    public static function berMonthCount($collection): array
    {
        return [
            'jan' => $collection->filter(self::byMonthName('January')),
            'feb' => $collection->filter(self::byMonthName('February')),
            'mar' => $collection->filter(self::byMonthName('March')),
            'apr' => $collection->filter(self::byMonthName('April')),
            'may' => $collection->filter(self::byMonthName('May')),
            'jun' => $collection->filter(self::byMonthName('June')),
            'jul' => $collection->filter(self::byMonthName('July')),
            'aug' => $collection->filter(self::byMonthName('August')),
            'sep' => $collection->filter(self::byMonthName('September')),
            'oct' => $collection->filter(self::byMonthName('October')),
            'nov' => $collection->filter(self::byMonthName('November')),
            'dec' => $collection->filter(self::byMonthName('December')),
        ];
    }

    public static function berDayCount($collection): array
    {
        return [
            'saturday'  => $collection->filter(self::getCollectionByDayName('Saturday')),
            'sunday'    => $collection->filter(self::getCollectionByDayName('Sunday')),
            'monday'    => $collection->filter(self::getCollectionByDayName('Monday')),
            'tuesday'   => $collection->filter(self::getCollectionByDayName('Tuesday')),
            'wednesday' => $collection->filter(self::getCollectionByDayName('Wednesday')),
            'thursday'  => $collection->filter(self::getCollectionByDayName('Thursday')),
            'friday'    => $collection->filter(self::getCollectionByDayName('Friday')),
        ];
    }

    public static function berMonthAndDaysCount($collection): array
    {
        return [
            'jan' => self::berDayCount($collection->filter(self::byMonthName('January'))),
            'feb' => self::berDayCount($collection->filter(self::byMonthName('February'))),
            'mar' => self::berDayCount($collection->filter(self::byMonthName('March'))),
            'apr' => self::berDayCount($collection->filter(self::byMonthName('April'))),
            'may' => self::berDayCount($collection->filter(self::byMonthName('May'))),
            'jun' => self::berDayCount($collection->filter(self::byMonthName('June'))),
            'jul' => self::berDayCount($collection->filter(self::byMonthName('July'))),
            'aug' => self::berDayCount($collection->filter(self::byMonthName('August'))),
            'sep' => self::berDayCount($collection->filter(self::byMonthName('September'))),
            'oct' => self::berDayCount($collection->filter(self::byMonthName('October'))),
            'nov' => self::berDayCount($collection->filter(self::byMonthName('November'))),
            'dec' => self::berDayCount($collection->filter(self::byMonthName('December'))),
        ];
    }

    public static function getRate($rates): array
    {
        return ['avg' => self::getRates($rates), 'total' => $rates->count()];
    }

    public static function getRates($rates)
    {
        $_rates = $rates->map(self::mapOnRates())->toArray();

        $count = count($_rates);

        return ($count != 0) ? round(array_sum($_rates) / $count, 1) : 0;
    }

    public static function getTimeSum($collection, $col)
    {
        $res = array_sum($collection->map(self::mapOnTime($col))->toArray());

        $sum = getTimeFromSeconds($res);

        if($res > 3600) return $sum . ' ' . trans('back.time.hour');

        elseif($res < 3600 && $res != 0) return explode('00:', $sum)[1] . ' ' . trans('back.time.minute');

        else return trans('back.no-value');
    }

    private static function mapOnTime($col)
    {
        return function($item) use ($col){
            return getSecondsFromTime($item->$col);
        };
    }

    public static function getYearMonths($without_inst = false)
    {
        if($without_inst) return range(1,12);

        return [
            carbon()->create(date('Y'),1,1),
            carbon()->create(date('Y'),2,1),
            carbon()->create(date('Y'),3,1),
            carbon()->create(date('Y'),4,1),
            carbon()->create(date('Y'),5,1),
            carbon()->create(date('Y'),6,1),
            carbon()->create(date('Y'),7,1),
            carbon()->create(date('Y'),8,1),
            carbon()->create(date('Y'),9,1),
            carbon()->create(date('Y'),10,1),
            carbon()->create(date('Y'),11,1),
            carbon()->create(date('Y'),12,1)
        ];
    }

    public static function getYearMonthsWithOrWithoutInst($with = false)
    {
        return arr()->where(self::getYearMonths($with ? false : true), function ($value, $key) use($with){
            $current_month = (int)now()->format('m');
            return !$with ? $value >= $current_month : (int)$value->format('m') >= $current_month;
        });
    }
}
