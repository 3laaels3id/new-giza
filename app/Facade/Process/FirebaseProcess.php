<?php

namespace App\Facade\Process;

use App\Facade\Traits\FirebaseNotifications;
use Edujugon\PushNotification\PushNotification;

class FirebaseProcess
{
    use FirebaseNotifications;

    public static $key;

    public function __construct()
    {
        static::$key = getCachedSetting('notification_key')->value;
    }

    public function pushNotification($data)
    {
        return self::create_curl_notification($data['fcm_token'], self::getData($data));
    }

    public function send($data)
    {
        $push = new PushNotification('fcm');

        $res = $push->setMessage(self::setPushNotificationData(self::getData($data)))

            ->setApiKey(static::$key)

            ->setDevicesToken($data['fcm_tokens'])

            ->send();

        return $res->feedback;
    }

    public function sendByTopic($data)
    {
        $push = new PushNotification('fcm');

        $res = $push->setMessage(self::setPushNotificationData(self::getData($data)))

            ->setApiKey(static::$key)

            ->setConfig(['dry_run' => false])

            ->setDevicesToken($data['fcm_tokens'])

            ->sendByTopic($data['topic']);

        return $res->feedback;
    }
}
