<?php

namespace App\Models;

use App\Http\Traits\BasicTrait;
use Illuminate\Database\Eloquent\Relations\Pivot;

class TournamentSport extends Pivot
{
    use BasicTrait;

    protected $guarded = ['id'];
}
