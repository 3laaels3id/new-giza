<?php

namespace App\Models;

use App\Http\Traits\BasicTrait;
use App\Http\Traits\TokenTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    use HasFactory, TokenTrait, BasicTrait;

    protected $guarded = ['id'];

    public function tokenable()
    {
        return $this->morphTo();
    }
}
