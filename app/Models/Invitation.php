<?php

namespace App\Models;

use App\Casts\Status;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $casts = ['status' => Status::class];

    protected $dates = ['birth_date'];

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }
}
