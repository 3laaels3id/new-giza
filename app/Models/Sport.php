<?php

namespace App\Models;

use App\Casts\Status;
use App\Http\Traits\SportTrait;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sport extends Model
{
    use HasFactory, SoftDeletes, Translatable, SportTrait;

    protected $guarded = ['id'];

    public $translationModel = SportTranslation::class;

    public $translationForeignKey = 'sport_id';

    public $translatedAttributes = ['name'];

    protected $casts = ['status' => Status::class];

    public function ages()
    {
        return $this->hasMany(Age::class);
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }
}
