<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Notification extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $with = ['notificationable'];

    public function notificationable()
    {
        return $this->morphTo();
    }
}
