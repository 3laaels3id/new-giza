<?php

namespace App\Models;

use App\Casts\Status;
use App\Http\Traits\TeamTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory, TeamTrait;

    protected $guarded = ['id'];

    protected $casts = ['status' => Status::class];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function sport()
    {
        return $this->belongsTo(Sport::class);
    }

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function tournament()
    {
        return $this->belongsTo(Tournament::class);
    }
}
