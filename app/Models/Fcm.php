<?php

namespace App\Models;

use App\Http\Traits\FcmTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Fcm extends Model
{
    use HasFactory, FcmTrait;

    protected $guarded = ['id'];

    public function fcmable()
    {
        return $this->morphTo();
    }
}
