<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TournamentTranslation extends Model
{
    public $timestamps = false;

    protected $table = 'tournament_translations';

    protected $guarded = ['id'];
}
