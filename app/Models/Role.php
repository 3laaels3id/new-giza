<?php

namespace App\Models;

use App\Casts\Status;
use App\Http\Traits\RoleTrait;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use HasFactory, SoftDeletes, Translatable, RoleTrait;

    protected $guarded = ['id'];

    public $translationModel = RoleTranslation::class;

    public $translationForeignKey = 'role_id';

    public $translatedAttributes = ['name'];

    protected $casts = ['status' => Status::class];

    public function permissions()
    {
        return $this->hasMany(Permission::class, 'role_id', 'id');
    }

    public function admins()
    {
        return $this->hasMany(Admin::class, 'role_id', 'id');
    }

    public static function getInSelectForm($exceptedIds = [])
    {
        $roles   = [];

        $rolesDB = Role::whereNotIn('id',$exceptedIds)->where('id','!=',1)->get();

        foreach ($rolesDB as $role) { $roles[$role->id] = ucwords($role->name); }

        return $roles;
    }
}
