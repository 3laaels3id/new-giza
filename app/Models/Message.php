<?php

namespace App\Models;

use App\Facade\Uploaded;
use App\Http\Traits\BasicTrait;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use BasicTrait;

    protected $guarded = ['id'];

    public function getImageUrlAttribute()
    {
        return Uploaded::defaultImage($this->message,'chats');
    }

    public function getFileUrlAttribute()
    {
        return url('/storage/uploaded/chats/pdf/' . $this->message);
    }

    public function chat()
    {
        return $this->belongsTo(Chat::class);
    }
}
