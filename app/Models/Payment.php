<?php

namespace App\Models;

use App\Http\Traits\PaymentTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Payment extends Model
{
    use HasFactory, SoftDeletes, PaymentTrait;

    protected $guarded = ['id'];

    public function paymentable()
    {
        return $this->morphTo();
    }

    public function getTypeAttribute()
    {
        return explode('\\', $this->paymentable)[2];
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
