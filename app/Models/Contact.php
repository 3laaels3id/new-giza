<?php

namespace App\Models;

use App\Http\Traits\ContactTrait;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use ContactTrait;

    protected $guarded = ['id'];

    public function getMobilePhoneAttribute()
    {
        return removePhoneZero($this->phone, $this->country_code);
    }
}
