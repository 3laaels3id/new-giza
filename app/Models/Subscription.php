<?php

namespace App\Models;

use App\Http\Traits\SubscriptionTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    use HasFactory, SubscriptionTrait;

    protected $guarded = ['id'];

    public function invitations()
    {
        return $this->hasMany(Invitation::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function age()
    {
        return $this->belongsTo(Age::class);
    }

    public function sport()
    {
        return $this->belongsTo(Sport::class);
    }

    public function tournament()
    {
        return $this->belongsTo(Tournament::class);
    }

    public function team()
    {
        return $this->hasOne(Team::class);
    }
}
