<?php

namespace App\Models;

use App\Casts\Status;
use App\Http\Traits\TournamentTrait;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tournament extends Model
{
    use HasFactory, SoftDeletes, Translatable, TournamentTrait;

    protected $guarded = ['id'];

    public $translationModel = TournamentTranslation::class;

    public $translationForeignKey = 'tournament_id';

    public $translatedAttributes = ['name'];

    protected $casts = ['status' => Status::class];

    protected $appends = ['formatted_end_date', 'formatted_start_date'];

    public function sports()
    {
        return $this->belongsToMany(Sport::class, 'tournament_sport', 'tournament_id', 'sport_id');
    }

    public function getFormattedStartDateAttribute()
    {
        return carbon()->parse($this->start_date);
    }

    public function getFormattedEndDateAttribute()
    {
        return carbon()->parse($this->end_date);
    }
}
