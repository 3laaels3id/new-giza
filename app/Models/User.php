<?php

namespace App\Models;

use App\Casts\Status;
use App\Facade\Uploaded;
use App\Http\Traits\UserTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable, SoftDeletes, UserTrait;

    public $guard = 'api';

    protected $guarded = ['id'];

    protected $hidden = ['password', 'remember_token'];

    protected $casts = ['email_verified_at' => 'datetime', 'status' => Status::class];

//    protected $appends = ['mobile_phone', 'image_url', 'full_name'];

    protected $dates = ['birth_date'];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getImageUrlAttribute()
    {
        return Uploaded::defaultImage($this->image(),'users');
    }

    public function getFullNameAttribute()
    {
        return "{$this->fname} {$this->lname}";
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function token()
    {
        return $this->morphOne(Token::class, 'tokenable');
    }

//    public function image()
//    {
//        return $this->morphOne(Image::class, 'imageable');
//    }

//    public function fcm()
//    {
//        return $this->morphOne(Fcm::class, 'fcmable');
//    }

//    public function notifications()
//    {
//        return $this->morphMany(Notification::class, 'notificationable');
//    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }
}
