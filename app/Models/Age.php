<?php

namespace App\Models;

use App\Casts\Status;
use App\Http\Traits\AgeTrait;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Age extends Model
{
    use HasFactory, SoftDeletes, Translatable, AgeTrait;

    protected $guarded = ['id'];

    public $translationModel = AgeTranslation::class;

    public $translationForeignKey = 'age_id';

    public $translatedAttributes = ['name'];

    protected $casts = ['status' => Status::class];

    public function sport()
    {
        return $this->belongsTo(Sport::class);
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }
}
