<?php

namespace App\Models;

use App\Casts\Status;
use App\Http\Traits\BasicTrait;
use App\Http\Traits\PaymentMethodTrait;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    use HasFactory, Translatable, BasicTrait, PaymentMethodTrait;

    protected $guarded = ['id'];

    public $translationModel = PaymentMethodTranslation::class;

    public $translationForeignKey = 'payment_method_id';

    public $translatedAttributes = ['name'];

    protected $casts = ['status' => Status::class];
}
