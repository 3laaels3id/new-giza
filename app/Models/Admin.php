<?php

namespace App\Models;

use App\Casts\Status;
use App\Facade\Uploaded;
use App\Http\Traits\AdminTrait;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes, AdminTrait;

    public $guard = 'admin';

    protected $guarded = ['id', 'password_confirmation'];

    protected $hidden = ['password', 'remember_token'];

    protected $casts = ['status' => Status::class];

    protected $with = ['role'];

    public function getImageUrlAttribute()
    {
        return Uploaded::defaultImage($this->image,'admins');
    }

    public function getFullNameAttribute()
    {
        return $this->fname . ' ' . $this->lname;
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function role()
    {
        return $this->belongsTo(Role::class,'role_id','id')->withDefault(['name' => trans('no-value')]);
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
