<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgeTranslation extends Model
{
    public $timestamps = false;

    protected $table = 'age_translations';

    protected $guarded = ['id'];
}
