<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SportTranslation extends Model
{
    public $timestamps = false;

    protected $table = 'sport_translations';

    protected $guarded = ['id'];
}
