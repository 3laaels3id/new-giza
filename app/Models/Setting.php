<?php

namespace App\Models;

use App\Casts\Status;
use App\Http\Traits\SettingTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    use HasFactory, SoftDeletes, SettingTrait;

    protected $guarded = ['id'];

    protected $casts = ['status' => Status::class];
}
