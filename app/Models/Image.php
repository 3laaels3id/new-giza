<?php

namespace App\Models;

use App\Http\Traits\BasicTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory, BasicTrait;

    protected $guarded = ['id'];

    public function imageable()
    {
        return $this->morphTo();
    }
}
