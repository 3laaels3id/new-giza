<?php

namespace App\Rules;

use App\Facade\Regex;
use Illuminate\Contracts\Validation\Rule;

class PasswordRule implements Rule
{
    public function passes($attribute, $value)
    {
        return Regex::password($value);
    }

    public function message()
    {
        return trans('api.password-must-contains-numbers');
    }
}
