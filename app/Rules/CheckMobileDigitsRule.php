<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Str;

class CheckMobileDigitsRule implements Rule
{
    public function passes($attribute, $value)
    {
        return Str::length($value) === self::getNeedle();
    }

    public function message()
    {
        return trans('api.phone_digits_between', ['num' => self::getNeedle()]);
    }

    private static function getNeedle()
    {
        return request()->country_code == '966' ? 10 : 11;
    }
}
