<?php

namespace App\Rules;

use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class AppAccessCodeRule implements Rule
{
    public function passes($attribute, $value)
    {
        return User::where('own_access_code', $value)->first();
    }

    public function message()
    {
        return trans('api.app_access_code');
    }
}
