<?php

namespace App\Rules;

use App\Models\Category;
use Illuminate\Contracts\Validation\Rule;

class MainCategoryRule implements Rule
{
    public function passes($attribute, $value)
    {
        return ($value != 0) ? Category::find($value) : true;
    }

    public function message()
    {
        return trans('api.category-is-not-found');
    }
}
