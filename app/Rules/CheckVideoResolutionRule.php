<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckVideoResolutionRule implements Rule
{
    public $resolution;

    public function __construct($resolution)
    {
        $this->resolution = $resolution;
    }

    public function passes($attribute, $value)
    {
        $getID3 = new \getID3;

        $file = $getID3->analyze($value->getRealPath());

        return $this->resolution >= $file['video']['resolution_y'];
    }

    public function message()
    {
        return trans('api.video-must-be-less-than-var', ['var' => $this->resolution]);
    }
}
