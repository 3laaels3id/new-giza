<?php

namespace App\Rules;

use App\Facade\Regex;
use Illuminate\Contracts\Validation\Rule;

class CountryCodeRule implements Rule
{
    public function passes($attribute, $value)
    {
        return Regex::countryCode($value);
    }

    public function message()
    {
        return trans('back.country-key-checker');
    }
}
