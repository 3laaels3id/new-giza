<?php

namespace App\Rules;

use Illuminate\Support\Str;
use Illuminate\Contracts\Validation\Rule;

class MobilePhoneRule implements Rule
{
    public function passes($attribute, $value)
    {
        return Str::startsWith($value, self::getNeedle());
    }

    public function message()
    {
        return trans('api.phone_start_with_var', ['var' => self::getNeedle()]);
    }

    private static function getNeedle()
    {
        return request()->has('country_code') == '966' ? '05' : '01';
    }
}
