<?php

namespace App\Rules;

use App\Facade\Regex;
use Illuminate\Contracts\Validation\Rule;

class NoSpacesRule implements Rule
{
    public function passes($attribute, $value)
    {
        return Regex::noSpaces($value);
    }

    public function message()
    {
        return trans('api.text-must-not-have-a-spaces');
    }
}
