<?php

namespace App\Rules;

use App\Models\Code;
use Illuminate\Contracts\Validation\Rule;

class CheckActivedMobile implements Rule
{
    public function passes($attribute, $value)
    {
        return !Code::where('phone', request()->phone)->where('country_code', request()->country_code)->first();
    }

    public function message()
    {
        return trans('api.phone-already-exists');
    }
}
