<?php

namespace App\Rules;

use App\Facade\Regex;
use Illuminate\Contracts\Validation\Rule;

class MembershipRule implements Rule
{
    public function passes($attribute, $value)
    {
        return Regex::membership($value);
    }

    public function message()
    {
        return trans('back.membership-number-invalid-format');
    }
}
