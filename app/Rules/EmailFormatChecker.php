<?php

namespace App\Rules;

use App\Facade\Regex;
use Illuminate\Contracts\Validation\Rule;

class EmailFormatChecker implements Rule
{
    public function passes($attribute, $value)
    {
        return Regex::email($value);
    }

    public function message()
    {
        return trans('back.email-format-checker');
    }
}
