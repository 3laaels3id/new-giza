<?php

namespace App\Rules;

use App\Facade\Regex;
use Illuminate\Contracts\Validation\Rule;

class IsEn implements Rule
{
    public function passes($attribute, $value)
    {
        return Regex::english($value);
    }

    public function message()
    {
        return trans('back.text-must-be-english');
    }
}
