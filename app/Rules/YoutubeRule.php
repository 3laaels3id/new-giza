<?php

namespace App\Rules;

use App\Facade\Regex;
use Illuminate\Contracts\Validation\Rule;

class YoutubeRule implements Rule
{
    public function passes($attribute, $value)
    {
        return Regex::youtube($value);
    }

    public function message()
    {
        return 'حقل رابط الفيديو يجب ان يكون رابط يوتيوب صحيح';
    }
}
