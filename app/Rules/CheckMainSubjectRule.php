<?php

namespace App\Rules;

use App\Models\Subject;
use Illuminate\Contracts\Validation\Rule;

class CheckMainSubjectRule implements Rule
{
    public function passes($attribute, $value)
    {
        $subject = Subject::find($value);

        return $subject->parent_id == 0;
    }

    public function message()
    {
        return trans('api.subject-is-not-main');
    }
}
