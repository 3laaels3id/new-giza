<?php

namespace App\Rules;

use App\Facade\Regex;
use Illuminate\Contracts\Validation\Rule;

class IsAr implements Rule
{
    public function passes($attribute, $value)
    {
        return Regex::arabic($value);
    }

    public function message()
    {
        return trans('back.text-must-be-arabic');
    }
}
