<?php

namespace App\Listeners;

use App\Events\EmailResting;
use App\Jobs\SendEmail;
use App\Mail\SendResetingMail;

class SendResetingEmailNotification
{
    public function handle(EmailResting $event)
    {
        SendEmail::dispatch($event->email, new SendResetingMail($event->token));
    }
}
