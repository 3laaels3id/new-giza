<?php

namespace App\Listeners;

use App\Events\SendAdminMailEvent;
use App\Jobs\SendEmail;
use App\Mail\SendAdminMail;
use Illuminate\Queue\InteractsWithQueue;

class SendAdminMailListener
{
    use InteractsWithQueue;

    public function handle(SendAdminMailEvent $event)
    {
        SendEmail::dispatch($event->email, new SendAdminMail($event->message));
    }
}
