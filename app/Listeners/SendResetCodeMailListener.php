<?php

namespace App\Listeners;

use App\Events\SendResetCodeMailEvent;
use App\Jobs\SendEmail;
use App\Mail\SendResetCodeMail;
use Illuminate\Queue\InteractsWithQueue;

class SendResetCodeMailListener
{
    use InteractsWithQueue;

    public function handle(SendResetCodeMailEvent $event)
    {
        SendEmail::dispatch($event->email, new SendResetCodeMail($event->message));
    }
}
