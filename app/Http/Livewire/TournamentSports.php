<?php

namespace App\Http\Livewire;

use App\Models\Sport;
use Livewire\Component;

class TournamentSports extends Component
{
    public $tournamentSports = [], $sports = [];

    public function mount()
    {
        $this->sports = Sport::getInSelectForm();

        $this->tournamentSports = [''];
    }

    public function addSportsField()
    {
        $this->tournamentSports[] = [''];
    }

    public function removeSportsField($index)
    {
        unset($this->tournamentSports[$index]);

        $this->tournamentSports = array_values($this->tournamentSports);
    }

    public function render()
    {
        return view('livewire.tournament-sports');
    }
}
