<?php

namespace App\Http\Livewire;

use App\Models\Sport;
use Livewire\Component;

class TournamentSportUpdate extends Component
{
    public $tournamentSports, $sports = [], $currentModel;

    public function mount($currentModel)
    {
        $this->currentModel = $currentModel;

        $this->sports = Sport::getInSelectForm();

        $this->tournamentSports = $currentModel->sports;
    }

    public function addSportsField()
    {
        $this->tournamentSports->push(collect(['id' => 0]));
    }

    public function removeSportsField($index)
    {
        unset($this->tournamentSports[$index]);

        $this->tournamentSports = array_values($this->tournamentSports);
    }

    public function render()
    {
        return view('livewire.tournament-sport-update');
    }
}
