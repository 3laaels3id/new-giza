<?php

namespace App\Http\Callbacks;

use App\Http\Controllers\Back\Auth\AdminLoginController;
use Illuminate\Support\Facades\Route;

trait RoutesCallbacks
{
    public static function AuthAdminRoutes(): callable
    {
        return function () {
            Route::get('/login', [AdminLoginController::class, 'showAdminLoginForm'])->name('admin.login');
            Route::get('/forget-password', [AdminLoginController::class, 'forgetPassword'])->name('admin.forget-password');
            Route::post('/forget-password-submit', [AdminLoginController::class, 'sendResetMail'])->name('admin.forget-password-submit');
            Route::get('/confirmed-reset-mail', [AdminLoginController::class, 'confirmedResetMail'])->name('admin.confirmedResetMail');
            Route::get('/admin-reset-password/{token}', [AdminLoginController::class, 'adminResetPassword'])->name('admin.reset-password');
            Route::post('/admin-change-password', [AdminLoginController::class, 'adminChangePassword'])->name('admin.changePassword');
            Route::post('/login', [AdminLoginController::class, 'adminLogin'])->name('admin.submit.login');
            Route::post('/logout', [AdminLoginController::class, 'adminLogout'])->name('admin.logout');
        };
    }
}
