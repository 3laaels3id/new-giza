<?php

namespace App\Http\Callbacks;

use App\Models\Contact;
use Illuminate\Support\Facades\Cache;

trait AppCallbacks
{
    public static function getAuth(): callable
    {
        return function ($view) {
            return $view->with('auth', auth()->user());
        };
    }

    public static function getNewMessages(): callable
    {
        return function ($view) {
            return $view->with('newMessagesCount', Cache::remember('get_new_messages', 60*30, function(){
                return Contact::whereIsSeen(0)->count();
            }));
        };
    }

    public static function getAllRoutes(): callable
    {
        return function ($view) {
            return $view->with('getAllRoutes', getAllRoutes());
        };
    }

    public static function getHasPermission(): callable
    {
        return function ($route) {
            return permission_route_checker($route);
        };
    }

    public static function getCrudRoutesCallback(): callable
    {
        return function ($table, $controller, $withShow = false) {
            return crudRoutes($table,$controller, $withShow);
        };
    }

    public static function getSortByIds(): callable
    {
        return function($ids){
            return $this->sortByDesc(function($subject) use ($ids){
                return in_array($subject->id, $ids);
            });
        };
    }
}
