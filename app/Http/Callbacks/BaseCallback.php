<?php

namespace App\Http\Callbacks;

trait BaseCallback
{
    public static function getEditedSince() : callable
    {
        return function ($model){ return $model->since; };
    }

    public static function getEditedUpdatedAt() : callable
    {
        return function ($model){ return $model->last_update; };
    }
}
