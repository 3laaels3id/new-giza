<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ImageResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'image' => self::imageUrl($this),
        ];
    }

    public static function imageUrl($imageable)
    {
        $image = explode('\\', $imageable->imageable_type)[2];

        return asset('public/uploaded/'.lower($image)->plural().'/'.$imageable->image);
    }
}
