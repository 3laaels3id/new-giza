<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserHomePageResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'banners'      => BannerResource::collection($this['banners']),
            'universities' => UniversityResource::collection($this['universities']),
            'subjects'     => self::getSubjects($this['subjects'])
        ];
    }

    private static function getSubjects($subjects): array
    {
        return $subjects->map(self::getSubjectsWithSubs())->toArray();
    }

    private static function getSubjectsWithSubs(): callable
    {
        return function($item){
            return [
                'id'   => $item->id,
                'name' => $item->name,
                'subs' => SubSubjectResource::collection($item->children->take(3)),
            ];
        };
    }
}
