<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'title'                 => $this->title ?? '',
            'body'                  => $this->body ?? '',
            'notificationable_id'   => $this->type,
            'notificationable_type' => $this->type_id,
            "created_at"            => $this->created_at->format('Y-m-d H:i:s'),
        ];
    }
}
