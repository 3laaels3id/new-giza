<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'              => $this->id,
            'fname'           => $this->fname ?? '',
            'lname'           => $this->lname ?? '',
            'username'        => $this->username ?? '',
            'email'           => $this->email ?? '',
            'phone'           => $this->phone ?? '',
            'country_code'    => $this->country_code ?? '',
            'image'           => User::find($request->user()->id)->image_url,
            'jwt'             => $this->token->jwt
        ];
    }
}
