<?php

namespace App\Http\Traits;

use App\Http\Scopes\FaqScopes;
use App\Http\Traits\Api\FaqApi;

trait FaqTrait
{
    use BasicTrait, FaqScopes, FaqApi;

    public static function types()
    {
        return [
            'user'   => translate('user'),
            'doctor' => translate('doctor')
        ];
    }
}
