<?php

namespace App\Http\Traits;

use App\Facade\Crud;
use App\Http\Scopes\TournamentScopes;
use App\Models\Tournament;

trait TournamentTrait
{
    use BasicTrait, TournamentScopes;

    public static function getInSelectForm()
    {
        return Crud::getModelsInSelectedForm(Tournament::class, 'name');
    }

    public static function getNewTournamentsInSelectForm()
    {
        $list = [];

        $modelsDB = Tournament::whereStatus(1)->get()->reject(function($item){
            return $item->formatted_start_date->isPast();
        });

        foreach ($modelsDB as $modelDB)
        {
            $list[$modelDB->id] = ucwords($modelDB->name);
        }

        return $list;
    }
}
