<?php

namespace App\Http\Traits;

use App\Http\Scopes\BasicScopes;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;

/**
 * @method static create(array $array)
 * @method static firstOrcreate(array $array)
 * @method static updateOrCreate(array $array)
 * @method static find(int|array $id)
 * @method static findOrFail(int $id)
 * @method static latest()
 * @method static search(string $term)
 * @method static paginate(int $page)
 * @method static whereStatus(int $int)
 * @method static doesntHave(string $column)
 * @method static whereHas(string $relation, callable $callback)
 * @method static checkForProduct(Request $request, int $cart_id)
 * @method static getCategoriesByType($type = 0)
 * @method static getSubjectsByType($type = 0)
 * @method static getFaqByType(string $type)
 * @method static whenSubcategoryIs(int $subcategoryId)
 * @method static whenProviderIs(int $providerId)
 * @method static getProvidersByFixedDistance(Request $request)
 * @method static whereProviderStatusIs(int $int)
 * @method static whereHasProviderStatusIs(int $int)
 * @method static whenSubSubjectOrUniversityIs(string $col, int $val)
 * @method static whereCompanyId(int $int)
 * @method static whereParentId(int $int)
 * @method static whereUserId(int $int)
 * @method static whereProviderId(int $int)
 * @method static whereProductId(int $int)
 * @method static whereRoleId(int $int)
 * @method static whereIsSeen(int $int)
 * @method static whenPhoneAndCodeIs($request)
 * @method static whereNotIn($column, $arr)
 * @method static whereEmail($value)
 * @method static wherePhone($value)
 * @method static whereCountryCode($value)
 * @method static whenIdIs($id)
 * @method static getCoupon($name)
 * @method static whereName($value)
 * @method static whereType($value)
 * @method static whereAccepted($value)
 * @method static whereDoctorId(int $id)
 * @method static whereCourseId(int $id)
 * @method static whereVideoId(int $id)
 * @method static checkCouponValidation($coupon)
 * @method static getUserVideo(\Illuminate\Http\Request $request, $video)
 * @method static checkUserSubscription(\Illuminate\Http\Request $request)
 * @method static whereBetween($column, array $array)
 * @method static select(string|array $column, Builder $builder = null)
 * @method static reports(\Illuminate\Http\Request $request)
 * @method static allReports()
 * @method static userSearch($term)
 * @method static whereId($id)
 * @method static getDoctorsByType($type)
 */

trait BasicTrait
{
    use BasicScopes;

    public function getSinceAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    public function getLastUpdateAttribute()
    {
        return $this->updated_at->diffForHumans();
    }

    public function getDeletedSinceAttribute()
    {
        return $this->deleted_at->diffForHumans();
    }
}
