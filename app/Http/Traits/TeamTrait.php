<?php

namespace App\Http\Traits;

use App\Http\Scopes\TeamScopes;

trait TeamTrait
{
    use BasicTrait, TeamScopes;
}
