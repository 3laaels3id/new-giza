<?php

namespace App\Http\Traits;

use App\Http\Scopes\PaymentMethodScopes;

trait PaymentMethodTrait
{
    use PaymentMethodScopes;
}
