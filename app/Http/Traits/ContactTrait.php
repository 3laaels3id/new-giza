<?php

namespace App\Http\Traits;

use App\Facade\Support\CrudMessage;
use App\Mail\SendAdminMail;
use Exception;
use App\Facade\Crud;
use App\Models\Contact;
use App\Jobs\SendEmail;
use App\Facade\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Scopes\ContactScopes;
use Illuminate\Support\Facades\DB;

trait ContactTrait
{
    use BasicTrait, ContactScopes;

    public static function deleteMessage($request)
    {
        return Crud::delete(Contact::class, $request);
    }

    public static function createMessage($request)
    {
        DB::beginTransaction();
        try
        {
            Contact::create(self::setData($request));

            DB::commit();

            return CrudMessage::success('تم إرسال رسالتك بنجاح الي الإدارة');
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::error($e);
        }
    }

    public static function apiCreateContactMessage($request)
    {
        DB::beginTransaction();
        try
        {
            Contact::create(self::setData($request));

            DB::commit();

            return ApiResponse::success();
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return ApiResponse::exceptionFails($e);
        }
    }

    public static function sendUserReplayMessage(Request $request)
    {
        try
        {
            if(is_null($request->title)) return CrudMessage::error(trans('api.title-field-is-required'));

            if(is_null($request->message)) return CrudMessage::error(trans('api.message-field-is-required'));

            SendEmail::dispatch($request->email, new SendAdminMail($request->message, $request->title));

            return CrudMessage::success();
        }
        catch (Exception $e)
        {
            return CrudMessage::error($e);
        }
    }

    private static function setData($request)
    {
        return [
            'name'         => $request->name ?? null,
            'email'        => $request->email ?? null,
            'phone'        => $request->phone ?? null,
            'country_code' => $request->country_code ?? '966',
            'message'      => $request->message,
        ];
    }
}
