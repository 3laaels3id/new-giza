<?php

namespace App\Http\Traits;

use App\Facade\Crud;
use App\Http\Scopes\UserScopes;
use App\Http\Traits\Api\UserApi;
use App\Models\User;

trait UserTrait
{
    use BasicTrait, UserApi, UserScopes;

    public static function getInSelectForm()
    {
        return Crud::getModelsInSelectedForm(User::class, 'full_name');
    }

    public static function addresses()
    {
        return [
            'cairo' => trans('back.cairo'),
            'giza'  => trans('back.giza'),
            'other' => trans('back.other'),
        ];
    }
}
