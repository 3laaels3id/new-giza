<?php

namespace App\Http\Traits\Api;

use App\Facade\ApiResponse;
use App\Http\Resources\NotificationResource;

trait NotificationApi
{
    public static function apiGetAuthNotifications($request)
    {
        return ApiResponse::pagination($request, $request->user()->notifications, NotificationResource::class);
    }
}
