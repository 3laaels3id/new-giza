<?php

namespace App\Http\Traits\Api;

use App\Facade\Api;
use App\Facade\Sms;
use App\Facade\Support\Warning;
use App\Models\Code;
use App\Models\User;
use App\Models\Banner;
use App\Models\Subject;
use App\Models\University;
use App\Facade\ApiResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserHomePageResource;

trait UserApi
{
    /**
     * Login
     * @param $request
     * @return mixed
     */
    public static function apiLogin($request)
    {
        // try to attempt the user;
        $response = Api::login(UserResource::class, $request, 'api');

        // if there is an error;
        if(!$response->getData()->status) return $response;

        $code = generate_rand_numbers();

        DB::table('users')->where('id', $response->getData()->data->id)->update(['code' => 1111]);

        $message = 'كود التفعيل الخاص بك هو : ' . $code;

//        Sms::hisms(auth()->guard('api')->user()->mobile_phone, $message);

        return ApiResponse::success();
    }

    public static function apiCheckUserLoginActiveCode($request)
    {
        $user = User::whereStatus(1)->where(self::configUsernameColumn($request))->first();

        if($user->code != $request->code) return Warning::userCodeInvalid();

        $user->update(['code' => null]);

        return ApiResponse::response(new UserResource($user));
    }

    /**
     * Register
     * @param $request
     * @return mixed
     */
    public static function apiCheckUserPhoneExists($request)
    {
        return Api::checkModelPhoneExists(User::class, $request);
    }

    public static function apiResendUserRegisterActiveCode($request)
    {
        return Api::resendModelRegisterActiveCode($request);
    }

    public static function apiCheckUserActiveCode($request)
    {
        return Api::checkModelActiveCode($request);
    }

    public static function apiRegister($request)
    {
        $check = Code::whenPhoneAndCodeIs($request)->get()->last();

        if(!$check || !$check->is_active) return Warning::pleaseActiveYourPhoneFirst();

        return Api::register(User::class,UserResource::class, $request);
    }

    /**
     * Forget Password
     * @param $request
     * @return mixed
     */
    public static function apiForgetPassword($request)
    {
        return Api::forgetPassword(User::class, $request);
    }

    public static function apiCheckResetCode($request)
    {
        return Api::checkResetCode(User::class, $request);
    }

    public static function apiResendResetCode($request)
    {
        return Api::resendResetCode(User::class, $request);
    }

    public static function apiSetNewPassword($request)
    {
        return Api::setNewPassword(User::class,$request);
    }

    /**
     * Profile
     * @param $request
     * @return mixed
     */
    public static function apiUpdateUserProfile($request)
    {
        return Api::updateModelProfile($request);
    }

    public static function apiSendUserNewPhoneCode()
    {
        $auth = auth()->guard('api')->user();

        $code = create_rand_numbers();

        $code = 1111;

        $auth->update(['code' => $code]);

        $message = 'كود التفعيل الخاص بك هو : ' . $code;

//        Sms::hisms($auth->mobile_phone, $message);

        return ApiResponse::success();
    }

    public static function apiCheckUserNewPhoneCode($request)
    {
        $auth = auth()->guard('api')->user();

        if($auth->code != $request->code) return Warning::userCodeInvalid();

        $auth->update(['code' => null, 'phone' => $request->phone, 'country_code' => $request->country_code]);

        return ApiResponse::success();
    }

    public static function apiChangePassword($request)
    {
        return Api::changePassword(User::class, $request);
    }

    public static function apiGetUpdatedProfile($request)
    {
        return ApiResponse::response(new UserResource(auth()->guard('api')->user()));
    }

    private static function configUsernameColumn($request): callable
    {
        return function ($query) use ($request){
            $col = is_numeric($request->username) ? 'phone' : 'username';

            return $query->where($col, $request->username);
        };
    }
}
