<?php

namespace App\Http\Traits\Api;

use App\Models\Faq;
use App\Facade\ApiResponse;
use App\Http\Resources\FaqResource;

trait FaqApi
{
    public static function apiUserFaqsPage($request)
    {
        return ApiResponse::pagination($request, Faq::getFaqByType('user')->get(), FaqResource::class);
    }

    public static function apiDoctorFaqsPage($request)
    {
        return ApiResponse::pagination($request, Faq::getFaqByType('doctor')->get(), FaqResource::class);
    }
}
