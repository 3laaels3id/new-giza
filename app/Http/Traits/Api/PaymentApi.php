<?php

namespace App\Http\Traits\Api;

use Exception;
use App\Facade\Pay;
use App\Models\Chat;
use App\Models\Coupon;
use App\Models\Course;
use App\Models\Subscription;
use App\Facade\Payment\MyFatoorah;
use Illuminate\Support\Facades\DB;
use App\Http\Traits\Other\PaymentHelper;

trait PaymentApi
{
    use PaymentHelper;

    public static function setUserSubscriptionPay($course, $request)
    {
        $coupon = Coupon::whereName($request->coupon)->first();

        // Basics
        $data['payment_method_id'] = $request->payment_method_id ?? 2; // 2 -> visa / master
        $data['total']             = self::getTotalWithDiscount($course, $coupon);
        $data['user']              = $request->user();
        $data['callBackUrl']       = root(). '/api/payments/success?lang=' . $request->header('lang');
        $data['errorUrl']          = root(). '/api/payments/errors?lang=' . $request->header('lang');

        // Optional
        $data['course_id'] = $course->id;
        $data['doctor_id'] = $course->doctor_id;
        $data['price']     = $course->price;
        $data['coupon_id'] = isset($coupon) ? $coupon->id : 0;
        $data['discount']  = isset($coupon) ? get_discount_from_percentage($coupon->value, $course->price) : null;
        $data['total']     = self::getTotalWithDiscount($course, $coupon);

        return Pay::myFatoorah($data); //return payment url;
    }

    public static function apiPaymentsSuccess($request)
    {
        return MyFatoorah::success($request);
    }

    public static function apiPaymentsErrors($request)
    {
        return MyFatoorah::error($request);
    }

    public static function setPayment($response, $request): array
    {
        DB::beginTransaction();

        $data   = json_decode($response['Data']['UserDefinedField']);

        $course = Course::find($data->course_id);

        $coupon = Coupon::find($data->coupon_id);

        $res = ['success' => 0, 'subscription_id' => 0, 'error' => '', 'message' => 'error'];

        try
        {
            $subscription = Subscription::create(self::setSubscriptionData($response));

            $payment = $subscription->payment()->create(self::setPaymentData($subscription, $data->price, 'SAR', $request->paymentId));

            $subscription->update(['payment_id' => $payment->id]);

            if($coupon) $coupon->update(['is_used' => 1, 'user_id' => $subscription->user_id]);

            DB::table('user_video')->insert(self::setUserVideos($subscription));

            Chat::firstOrcreate(['user_id' => $subscription->user_id, 'doctor_id' => $course->doctor->id]);

            // calculate the doctor commission for the app and add the total to his wallet;
            self::setDoctorCommissionAndWallet($subscription, $course->doctor);

            self::sendDoctorNewSubscriptionNotification($subscription, $course->doctor);

            $res = ['success' => 1, 'subscription_id' => $subscription->id, 'error' => '', 'message' => 'success'];

            DB::commit();

            return $res;
        }
        catch (Exception $e)
        {
            DB::rollBack();

            $res['error'] = getFormattedException($e);

            return $res;
        }
    }
}
