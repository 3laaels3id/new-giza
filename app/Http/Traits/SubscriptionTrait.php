<?php

namespace App\Http\Traits;

use App\Http\Scopes\SubscriptionScopes;

trait SubscriptionTrait
{
    use BasicTrait, SubscriptionScopes;

    public static function setInvitationData($request)
    {
        $i = 0;

        $invitations = [];

        foreach ($request->invitations_fname as $index => $fname)
        {
            $invitations[$i]['fname']         = $fname;
            $invitations[$i]['lname']         = $request->invitations_lname[$index];
            $invitations[$i]['age']           = $request->invitations_age[$index];
            $invitations[$i]['birth_date']    = $request->invitations_birth_date[$index];
            $invitations[$i]['address']       = $request->invitations_address[$index];
            $invitations[$i]['identity']      = $request->invitations_identity[$index];
            $invitations[$i]['email']         = $request->invitations_email[$index];
            $invitations[$i]['phone']         = $request->invitations_phone[$index];
            $invitations[$i]['serial_number'] = generate_rand_numbers(8);
            $invitations[$i]['created_at']    = now();
            $invitations[$i]['updated_at']    = now();
            $i++;
        }
        return $invitations;
    }
}
