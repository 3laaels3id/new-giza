<?php

namespace App\Http\Traits;

use App\Http\Scopes\RoleScopes;

trait RoleTrait
{
    use BasicTrait, RoleScopes;

    public static function setPermissions($request, $role)
    {
        $permissions = [];

        $homePage[0] = ['role_id' => $role->id, 'permission' => 'admin-panel', 'created_at' => now(), 'updated_at' => now()];

        foreach ($request->permissions as $key => $permission)
        {
            if ($permission == null) continue;

            $permissions[$key]['role_id']    = $role->id;
            $permissions[$key]['permission'] = $permission;
            $permissions[$key]['created_at'] = now();
            $permissions[$key]['updated_at'] = now();
        }

        return array_merge($permissions, $homePage);
    }
}
