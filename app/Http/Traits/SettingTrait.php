<?php

namespace App\Http\Traits;

use App\Facade\Support\CrudMessage;
use Exception;
use App\Facade\Crud;
use App\Facade\Uploaded;
use App\Models\Setting;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

trait SettingTrait
{
    use BasicTrait;

    public static function createSetting($request)
    {
        $settingData = $request->all();

        DB::beginTransaction();
        try
        {
            if($settingData['input'] == 'file') $settingData['value'] = Uploaded::image($settingData['value'], 'setting');

            Setting::updateOrCreate(except($settingData, ['_token']));

            DB::commit();

            return CrudMessage::add('setting');
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::fails($e);
        }
    }

    public static function updateSetting($request, $currentSetting)
    {
        return Crud::update(Setting::class, $request, $currentSetting);
    }

    public static function deleteSetting($request)
    {
        return Crud::delete(Setting::class, $request->id);
    }

    public static function updateAll($request)
    {
        DB::beginTransaction();
        try
        {
            $settings = Setting::all();

            $settingsArr = $request->except(['_token', '_method', 'submit']);

            if($request->has('app_course_edit_status')) $settingsArr['app_course_edit_status'] = 1;

            elseif(!$request->has('app_course_edit_status')) $settingsArr['app_course_edit_status'] = 0;

            foreach ($settingsArr as $key => $value)
            {
                $setting = $settings->where('key', $key)->first();

                if(!$setting) continue;

                if (is_file($value) && $setting->input == 2)
                {
                    self::handlePdfInputNewFile($setting, $value, $request, $key);
                }
                else
                {
                    $setting->update(['value' => $value]);
                }
            }

            DB::commit();

            return CrudMessage::edit('setting');
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::fails($e);
        }
    }

    public static function setRow($key, $name, $type, $input, $value = null)
    {
        return [
            'key'        => $key,
            'name'       => $name,
            'type'       => $type,
            'input'      => $input,
            'value'      => $value,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }

    private static function handlePdfInputNewFile($setting, $value, $request, $key)
    {
        storage_unlink('settings', $setting->value);

        $up = $value->getClientMimeType() == 'application/pdf' ? Uploaded::file($request, $key,'file') : Uploaded::image($value, 'setting');

        $setting->update(['value' => $up]);
    }
}
