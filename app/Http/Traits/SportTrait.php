<?php

namespace App\Http\Traits;

use App\Facade\Crud;
use App\Http\Scopes\SportScopes;
use App\Models\Sport;

trait SportTrait
{
    use BasicTrait, SportScopes;

    public static function getInSelectForm()
    {
        return Crud::getModelsInSelectedForm(Sport::class, 'name');
    }
}
