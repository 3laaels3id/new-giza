<?php

namespace App\Http\Traits;

use App\Facade\Crud;
use App\Models\Payment;
use App\Http\Traits\Api\PaymentApi;
use App\Http\Scopes\PaymentScopes;

trait PaymentTrait
{
    use BasicTrait, PaymentApi, PaymentScopes;

    public static function deletePayment($id)
    {
        return Crud::delete(Payment::class, $id);
    }

    public static function getCommission($value, $balanceWithCommission = false)
    {
        // Y = P% * X;

        $percentage = getSetting('app_subscriptions_percentage'); // 10%;

        $floatValue = (int)$percentage / 100; // 10 / 100 = 0.1;

        $commission = (int)($value * $floatValue); // 200 * 0.1 = 20;

        if($balanceWithCommission) return $value - $commission;

        return $commission;
    }
}
