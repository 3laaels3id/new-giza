<?php

namespace App\Http\Traits\Other;

use App\Models\Commission;
use App\Models\Notification;

trait PaymentHelper
{
    private static function setPaymentData($subscription, $amount, $currency, $transaction_id): array
    {
        return [
            'user_id'        => $subscription->user_id,
            'amount'         => $amount,
            'currency'       => $currency,
            'transaction_id' => $transaction_id
        ];
    }

    private static function setSubscriptionData($response): array
    {
        $data = json_decode($response['Data']['UserDefinedField']);

        return [
            'price'     => $data->price,
            'user_id'   => $data->user_id,
            'course_id' => $data->course_id,
            'coupon_id' => $data->coupon_id == 0 ? null : $data->coupon_id,
            'doctor_id' => $data->doctor_id,
            'discount'  => $data->discount,
            'total'     => $data->total,
        ];
    }

    private static function createDoctorWallet($balance, $type): array
    {
        return ['balance' => $balance, 'process_type' => $type];
    }

    private static function setDoctorCommissionAndWallet($subscription, $doctor): void
    {
        $balanceWithCommission = self::getCommission($subscription->total, true);

        $onlyCommission = self::getCommission($subscription->total);

        $doctor->wallets()->create(self::createDoctorWallet($balanceWithCommission,'added'));

        $doctor->update(['wallet' => setDoctorWallet($doctor, $balanceWithCommission)]);

        $doctor->commissions()->create(Commission::commissionCreateData($subscription, 'subscription', $onlyCommission));
    }

    private static function getTotalWithDiscount($course, $coupon): int
    {
        if(!$coupon) return $course->price;

        return (string)get_total_from_percentage($coupon->value, $course->price);
    }

    private static function setUserVideos($subscription)
    {
        $videos = $subscription->course->videos->pluck('id')->toArray();

        $data = [];

        foreach ($videos as $i => $video_id)
        {
            $data[$i]['user_id']   = $subscription->user_id;
            $data[$i]['video_id']  = $video_id;
            $data[$i]['course_id'] = $subscription->course_id;
        }

        return $data;
    }

    private static function sendDoctorNewSubscriptionNotification($subscription, $doctor): void
    {
        $body = 'قام ' . $subscription->user->full_name . ' بعمل اشتراك في دورة ' . $subscription->course->title;

        Notification::sendAndSave('إشتراك جديد', $body, $doctor, true, 'course', $subscription->course_id);
    }
}
