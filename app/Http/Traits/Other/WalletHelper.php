<?php

namespace App\Http\Traits\Other;

use App\Facade\Firebase;
use App\Models\Notification;

trait WalletHelper
{
    private static function setTransactionData($request)
    {
        return [
            'doctor_id' => $request->user()->id,
            'amount'    => $request->amount,
            'bank_id'   => $request->bank_id
        ];
    }

    private static function setWalletData($request, $type)
    {
        return [
            'doctor_id'    => $request->user()->id,
            'balance'      => $request->amount,
            'process_type' => $type,
        ];
    }

    private static function setWalletRow($amount, $type)
    {
        return ['balance' => $amount, 'process_type' => $type];
    }

    private static function pushNotificationToDoctor($doctor, $body)
    {
        $title = app()->isLocale('ar') ? 'سحب رصيد' : 'Balance Withdrawal';

        $data = Notification::setNotificationData($title, $body);

        $doctor->notifications()->create($data);

        $data['notificationType'] = 'balance_withdrawal';

        $data['fcm_token'] = $doctor->fcm->fcm;

        Firebase::pushNotification($data);
    }
}
