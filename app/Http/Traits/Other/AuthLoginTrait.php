<?php

namespace App\Http\Traits\Other;

use App\Rules\PasswordRule;

trait AuthLoginTrait
{
    public static function passwordResetData($email, $token)
    {
        return [
            'email'      => $email,
            'token'      => $token,
            'created_at' => now()
        ];
    }

    public static function getValidationRules()
    {
        return [
            'token'        => ['required', 'string'],
            'new_password' => ['required', 'string', new PasswordRule(), 'confirmed']
        ];
    }
}
