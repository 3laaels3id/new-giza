<?php

namespace App\Http\Traits;

use App\Http\Scopes\AdminScopes;

trait AdminTrait
{
    use BasicTrait, AdminScopes;

    protected static function boot()
    {
        parent::boot();

        static::deleting(self::getAdminClouser());
    }
}
