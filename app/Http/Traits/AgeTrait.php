<?php

namespace App\Http\Traits;

use App\Http\Scopes\AgeScopes;

trait AgeTrait
{
    use BasicTrait, AgeScopes;

    public static function types()
    {
        return [
            'adult' => trans('back.adult'),
            'junior' => trans('back.junior'),
        ];
    }
}
