<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Setting;
use Illuminate\Support\Facades\Cache;

class SmtpConfigration
{
    public function handle($request, Closure $next)
    {
        $settings = Cache::remember('settings_collection', 60*60*24, function(){
           return Setting::select(['value', 'key'])->where('type', 'SMTP')->get();
        });

        #SMTP
        config('mail.mailers.smtp.transport', $settings->where('key', 'smtp')->first()->value);
        config('mail.mailers.smtp.host', $settings->where('key', 'smtp_host')->first()->value);
        config('mail.mailers.smtp.port', $settings->where('key', 'smtp_port')->first()->value);
        config('mail.from.address', $settings->where('key', 'smtp_sender_email')->first()->value);
        config('mail.from.name', $settings->where('key', 'smtp_sender_name')->first()->value);
        config('mail.mailers.smtp.encryption', $settings->where('key', 'smtp_encryption')->first()->value);
        config('mail.mailers.smtp.username', $settings->where('key', 'smtp_username')->first()->value);
        config('mail.mailers.smtp.password', $settings->where('key', 'smtp_password')->first()->value);

        return $next($request);
    }
}
