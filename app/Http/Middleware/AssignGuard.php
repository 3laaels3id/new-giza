<?php

namespace App\Http\Middleware;

use Closure;
use App\Facade\ApiResponse;
use Illuminate\Http\Request;

class AssignGuard
{
    public function handle(Request $request, Closure $next, $guard = null)
    {
        if($guard != null) auth()->shouldUse($guard);

        if(auth()->guard($guard)->check()) return $next($request);

        return ApiResponse::unAuth('Unauthenticated');
    }
}
