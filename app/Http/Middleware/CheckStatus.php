<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use App\Facade\ApiResponse;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckStatus
{
    public function handle(Request $request, Closure $next, $guard = null)
    {
        Carbon::setLocale($request->header('lang'));

        $auth = auth()->guard($guard);

        if ($auth->check())
        {
            if (!$auth->user()->status)
            {
                Auth::guard($guard)->logout();

                return self::setRoutesRedirect($request, trans('api.user-status-is-not-active'));
            }

            if ($auth->user()->is_blocked)
            {
                Auth::guard($guard)->logout();

                $message = trans('api.this-account-is-blocked', ['number' => getCachedSetting('contact_mobile_phone')->value]);

                return self::setRoutesRedirect($request, $message);
            }
        }

        return $next($request);
    }

    private static function setRoutesRedirect($request, $message)
    {
        if (self::checkUrl($request,'api')) return ApiResponse::warning($message);

        else if (self::checkUrl($request,'doctor-panel')) return self::setRedirect('doctor.login', $message);

        else if (self::checkUrl($request,'admin-panel')) return self::setRedirect('admin.login', $message);

        else return redirect('/')->with('danger', $message);
    }

    private static function checkUrl($request, $needles)
    {
        return str()->contains($request->url(), $needles);
    }

    private static function setRedirect($route, $message)
    {
        return redirect()->route($route)->with('danger', $message);
    }
}
