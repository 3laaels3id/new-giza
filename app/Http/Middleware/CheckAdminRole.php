<?php

namespace App\Http\Middleware;

use Closure;
use App\Facade\Support\CrudMessage;

class CheckAdminRole
{
    public function handle($request, Closure $next)
    {
        $auth = auth()->guard('admin')->user();

        if ($request->ajax() && !permission_checker($auth)) return CrudMessage::warning(trans('back.has-no-permission'), 403);

        if (!permission_checker($auth)) return abort(403);

        return $next($request);
    }
}
