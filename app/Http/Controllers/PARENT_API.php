<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PARENT_API extends Controller
{
    function __construct(Request $request)
    {
        $lang = $request->header('lang') ? (($request->header('lang') == 'ar') ? 'ar' : 'en') : 'en';

        app()->setLocale($lang);

        carbon()->setLocale($lang);
    }
}
