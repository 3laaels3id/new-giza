<?php

namespace App\Http\Controllers\Api;

use App\Models\Contact;
use App\Models\Payment;
use App\Facade\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\PARENT_API;
use App\Http\Requests\Api\GetExtraPagesRequest;
use App\Http\Requests\Api\User\ContactUsRequest;

class AppController extends PARENT_API
{
    public function getAboutApp()
    {
        return ApiResponse::dynamicPages('about_app');
    }

    public function getTermsPage(GetExtraPagesRequest $request)
    {
        return ApiResponse::dynamicPages('app_'.$request->type.'_terms');
    }

    public function getPrivacyPage(GetExtraPagesRequest $request)
    {
        return ApiResponse::dynamicPages('app_'.$request->type.'_privacy');
    }

    public function contactUsMessage(ContactUsRequest $request)
    {
        return Contact::apiCreateContactMessage($request);
    }

    public function paymentsSuccess(Request $request)
    {
        return Payment::apiPaymentsSuccess($request);
    }

    public function paymentsErrors(Request $request)
    {
        return Payment::apiPaymentsErrors($request);
    }
}
