<?php

namespace App\Http\Controllers\Api\Auth;

use App\Facade\Api;
use App\Models\Doctor;
use Illuminate\Http\Request;
use App\Http\Controllers\PARENT_API;
use App\Http\Requests\Api\User\SendUserActiveCodeRequest;
use App\Http\Requests\Api\User\CheckUserActiveCodeRequest;
use App\Http\Requests\Api\User\ChangePasswordRequest;
use App\Http\Requests\Api\User\CheckUserLoginActiveCodeRequest;
use App\Http\Requests\Api\Doctor\RegisterRequest;
use App\Http\Requests\Api\Doctor\DoctorLoginRequest;
use App\Http\Requests\Api\Doctor\UpdateDoctorProfileRequest;
use App\Http\Requests\Api\Doctor\CheckDoctorNewPhoneCodeRequest;
use App\Http\Requests\Api\Doctor\ForgetPasswordRequest;
use App\Http\Requests\Api\Doctor\CheckResetCodeRequest;
use App\Http\Requests\Api\Doctor\SetNewPasswordRequest;

class AuthDoctorController extends PARENT_API
{
    /**
     * @param DoctorLoginRequest $request
     * @return mixed
     */
    public function login(DoctorLoginRequest $request)
    {
        return Doctor::apiLogin($request);
    }

    public function checkDoctorLoginActiveCode(CheckUserLoginActiveCodeRequest $request)
    {
        return Doctor::apiCheckDoctorLoginActiveCode($request);
    }

    public function resendDoctorRegisterActiveCode(SendUserActiveCodeRequest $request)
    {
        return Doctor::apiResendDoctorRegisterActiveCode($request);
    }

    /**
     * @param CheckUserActiveCodeRequest $request
     * @return mixed
     */
    public function checkDoctorActiveCode(CheckUserActiveCodeRequest $request)
    {
        return Doctor::apiCheckDoctorActiveCode($request);
    }

    public function checkDoctorPhoneExists(SendUserActiveCodeRequest $request)
    {
        return Doctor::apiCheckDoctorPhoneExists($request);
    }

    public function register(RegisterRequest $request)
    {
        return Doctor::apiRegister($request);
    }

    /**
     * @param UpdateDoctorProfileRequest $request
     * @return mixed
     */
    public function updateDoctorProfile(UpdateDoctorProfileRequest $request)
    {
        return Doctor::apiUpdateDoctorProfile($request);
    }

    public function sendDoctorNewPhoneCode()
    {
        return Doctor::apiSendDoctorNewPhoneCode();
    }

    public function checkDoctorNewPhoneCode(CheckDoctorNewPhoneCodeRequest $request)
    {
        return Doctor::apiCheckDoctorNewPhoneCode($request);
    }

    public function getUpdatedProfile(Request $request)
    {
        return Doctor::apiGetUpdatedProfile($request);
    }

    public function changeDoctorPassword(ChangePasswordRequest $request)
    {
        return Doctor::apiChangePassword($request);
    }

    /**
     * @param ForgetPasswordRequest $request
     * @return mixed
     */
    public function forgetPassword(ForgetPasswordRequest $request)
    {
        return Doctor::apiForgetPassword($request);
    }

    public function resendResetCode(ForgetPasswordRequest $request)
    {
        return Doctor::apiResendResetCode($request);
    }

    public function checkResetCode(CheckResetCodeRequest $request)
    {
        return Doctor::apiCheckResetCode($request);
    }

    public function resetPassword(SetNewPasswordRequest $request)
    {
        return Doctor::apiSetNewPassword($request);
    }

    public function Logout()
    {
        return Api::logout('provider');
    }
}
