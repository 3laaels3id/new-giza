<?php

namespace App\Http\Controllers\Api\Auth;

use App\Facade\Api;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\PARENT_API;
use App\Http\Requests\Api\User\RegisterRequest;
use App\Http\Requests\Api\User\UserLoginRequest;
use App\Http\Requests\Api\User\CheckUserLoginActiveCodeRequest;
use App\Http\Requests\Api\User\SendUserActiveCodeRequest;
use App\Http\Requests\Api\User\UpdateUserProfileRequest;
use App\Http\Requests\Api\User\CheckUserNewPhoneCodeRequest;
use App\Http\Requests\Api\User\ChangePasswordRequest;
use App\Http\Requests\Api\User\ForgetPasswordRequest;
use App\Http\Requests\Api\User\SetNewPasswordRequest;
use App\Http\Requests\Api\User\CheckResetCodeRequest;
use App\Http\Requests\Api\User\CheckUserActiveCodeRequest;

class AuthController extends PARENT_API
{
    /**
     * @param UserLoginRequest $request
     * @return mixed
     */
    public function login(UserLoginRequest $request)
    {
        return User::apiLogin($request);
    }

    public function checkUserLoginActiveCode(CheckUserLoginActiveCodeRequest $request)
    {
        return User::apiCheckUserLoginActiveCode($request);
    }

    /**
     * @param SendUserActiveCodeRequest $request
     * @return mixed
     */
    public function resendUserRegisterActiveCode(SendUserActiveCodeRequest $request)
    {
        return User::apiResendUserRegisterActiveCode($request);
    }

    public function checkUserPhoneExists(SendUserActiveCodeRequest $request)
    {
        return User::apiCheckUserPhoneExists($request);
    }

    public function checkUserActiveCode(CheckUserActiveCodeRequest $request)
    {
        return User::apiCheckUserActiveCode($request);
    }

    public function register(RegisterRequest $request)
    {
        return User::apiRegister($request);
    }

    /**
     * @param UpdateUserProfileRequest $request
     * @return mixed
     */
    public function updateUserProfile(UpdateUserProfileRequest $request)
    {
        return User::apiUpdateUserProfile($request);
    }

    public function sendUserNewPhoneCode()
    {
        return User::apiSendUserNewPhoneCode();
    }

    public function checkUserNewPhoneCode(CheckUserNewPhoneCodeRequest $request)
    {
        return User::apiCheckUserNewPhoneCode($request);
    }

    public function getUpdatedProfile(Request $request)
    {
        return User::apiGetUpdatedProfile($request);
    }

    public function changeUserPassword(ChangePasswordRequest $request)
    {
        return User::apiChangePassword($request);
    }

    /**
     * @param ForgetPasswordRequest $request
     * @return mixed
     */
    public function forgetPassword(ForgetPasswordRequest $request)
    {
        return User::apiForgetPassword($request);
    }

    public function resendResetCode(ForgetPasswordRequest $request)
    {
        return User::apiResendResetCode($request);
    }

    public function checkResetCode(CheckResetCodeRequest $request)
    {
        return User::apiCheckResetCode($request);
    }

    public function resetPassword(SetNewPasswordRequest $request)
    {
        return User::apiSetNewPassword($request);
    }

    public function Logout()
    {
        return Api::logout('provider');
    }
}
