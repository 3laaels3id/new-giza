<?php

namespace App\Http\Controllers\Back;

use App\Models\Age;
use App\Http\Requests\Back\EditAgeRequest;
use App\Http\Controllers\RepoController;
use App\Http\Requests\Back\CreateAgeRequest;
use App\Repository\Contracts\IAgeRepository;

class AgeController extends RepoController
{
    public function __construct(IAgeRepository $repository)
    {
        parent::__construct($repository);
    }

    public function index()
    {
        return view('Back.Ages.index', ['ages' => Age::latest()->has('sport')->paginate(10), 'search' => false]);
    }

    public function store(CreateAgeRequest $request)
    {
        return self::repo()->store($request);
    }

    public function update(EditAgeRequest $request, Age $age)
    {
        return self::repo()->update($request, $age);
    }
}
