<?php

namespace App\Http\Controllers\Back;

use Exception;
use App\Models\Team;
use App\Models\Sport;
use App\Models\User;
use App\Models\Invitation;
use App\Models\Tournament;
use App\Models\Subscription;
use App\Http\Controllers\BASE_CONTROLLER;
use App\Http\Requests\Back\EditSubscriptionRequest;
use App\Http\Requests\Back\CreateSubscriptionRequest;
use App\Facade\Support\CrudMessage;
use App\Facade\Support\Warning;
use App\Exports\TeamsExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class SubscriptionController extends BASE_CONTROLLER
{
    public function __construct()
    {
        parent::__construct(Subscription::class);
    }

    public function index()
    {
        $subscriptions = Subscription::with('tournament')
            ->has('tournament')
            ->has('user')
            ->has('sport')
            ->has('team')
            ->has('age')
            ->paginate(10);

        return view('Back.Subscriptions.index', [
            'subscriptions' => $subscriptions,
            'search'        => false
        ]);
    }

    public function create()
    {
        return view('Back.Crud.create', self::formFields());
    }

    public function edit($id)
    {
        return view('Back.Crud.edit', self::formFields('edit', Subscription::findOrFail($id)));
    }

    public function getSportsByTournamentId(Request $request)
    {
        $tournament = Tournament::find($request->tournament_id);

        if(!$tournament) return Warning::tournamentIsNotFound();

        $callback = function($item){
            return ['id' => $item->id,'name' => $item->name];
        };

        $data = $tournament->sports->map($callback);

        return response()->json(['data' => $data->toArray(), 'success' => true]);
    }

    public function getAgesBySportId(Request $request)
    {
        $sport = Sport::find($request->sport_id);

        if(!$sport) return Warning::sportIsNotFound();

        $callback = function($item){
            return ['id' => $item->id,'name' => $item->name];
        };

        $data = $sport->ages->map($callback);

        return response()->json(['data' => $data->toArray(), 'success' => true]);
    }

    public function store(CreateSubscriptionRequest $request)
    {
        DB::beginTransaction();
        try
        {
            $check = Subscription::whereUserId($request->user_id)->whereTournamentId($request->tournament_id)->first();

            if($check)
            {
                if($check->tournament->formatted_start_date->isCurrentDay()) return Warning::userAlreadySubscribeInTournament();

                if($check->tournament->formatted_start_date->isPast()) return Warning::tournamentIsFinished();

                if($check->tournament->formatted_end_date->isPast()) return Warning::tournamentIsFinished();
            }

            $subscriptionData = $request->only(['user_id', 'tournament_id', 'sport_id', 'age_id']);

            $subscription = Subscription::create($subscriptionData);

            if(!is_null($request->invitations_fname[0]) && !is_null($request->invitations_lname[0]) && !is_null($request->invitations_age[0]) && !is_null($request->invitations_birth_date[0]) && !is_null($request->invitations_address[0]))
            {
                $subscription->invitations()->createMany(Subscription::setInvitationData($request));
            }

            $subscription->team()->create(self::getTeamData($request, $subscription));

            DB::commit();

            return CrudMessage::successResponse();
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::fails($e);
        }
    }

    public function printInvitation(Invitation $invitation)
    {
        return view('Back.Subscriptions.printInvitation', compact('invitation'));
    }

    public function update(EditSubscriptionRequest $request, Subscription $subscription)
    {
        DB::beginTransaction();
        try
        {
            if($subscription->tournament->formatted_start_date->isCurrentDay()) return Warning::tournamentWillStartToday();

            if($subscription->tournament->formatted_start_date->isPast()) return Warning::tournamentIsFinished();

            if($subscription->tournament->formatted_end_date->isPast()) return Warning::tournamentIsFinished();

            DB::table('invitations')->where('subscription_id', $subscription->id)->delete();

            $subscription->invitations()->createMany(Subscription::setInvitationData($request));

            $subscription->team->update(['total' => $subscription->invitations->count() + 1]);

            DB::commit();

            return CrudMessage::successResponse();
        }
        catch (Exception $e)
        {
            DB::rollBack();
            return CrudMessage::fails($e);
        }
    }

    public function teams()
    {
        $data = ['teams' => Team::has('user')->has('sport')->has('subscription')->has('tournament')->paginate(10), 'search' => false];

        return view('Back.Teams.index', $data);
    }

    public function showTeam(Team $team)
    {
        return view('Back.Teams.show', compact('team'));
    }

    public function searchTeam(Request $request)
    {
        if(is_null($request->term)) return redirect()->route('teams.index');

        $data = ['teams' => Team::search($request->term)->paginate(10), 'search' => true];

        return view('Back.Team.index', $data);
    }

    public function teamsPrint()
    {
        $callback = function($query){
            return $query->where('start_date', '>', today());
        };

        $data = ['teams' => Team::whereHas('tournament', $callback)->paginate(10), 'search' => false];

        return view('Back.Teams.print', $data);
    }

    public function teamExport()
    {
        return Excel::download(new TeamsExport, 'teams.xlsx');
    }

    public function formFields($type = 'create', $currentModel = null)
    {
        $data = ['model' => 'subscription', 'users' => User::getInSelectForm(), 'tournaments' => Tournament::getNewTournamentsInSelectForm()];

        $data['sports'] = [];

        $data['ages'] = [];

        $data['addresses'] = User::addresses();

        if($type == 'edit') $data['currentModel'] = $currentModel;

        return $data;
    }

    private static function getTeamData($request, $subscription)
    {
        return [
            'user_id'         => $request->user_id,
            'tournament_id'   => $request->tournament_id,
            'sport_id'        => $request->sport_id,
            'name'            => $request->team_name,
            'total'           => $subscription->invitations->count() + 1,
        ];
    }
}
