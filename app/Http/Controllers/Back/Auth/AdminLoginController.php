<?php

namespace App\Http\Controllers\Back\Auth;

use App\Models\Admin;
use App\Jobs\SendEmail;
use Illuminate\Http\Request;
use App\Mail\SendResetingMail;
use Illuminate\Support\Facades\DB;
use App\Facade\Support\CrudMessage;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Traits\Other\AuthLoginTrait;
use App\Http\Requests\Auth\AdminLoginRequest;

class AdminLoginController extends Controller
{
    use AuthLoginTrait;

    public function __construct()
    {
        $this->middleware('guest:admin')->except('adminLogout');
    }

    public function showAdminLoginForm()
    {
        return view('auth.pages.admin.adminLogin');
    }

    public function adminLogin(AdminLoginRequest $request)
    {
        if (!Auth::guard('admin')->attempt($request->only(['email', 'password']), $request->remember))
        {
            return CrudMessage::warningWithInput(trans('back.invalid-account-info'), $request->only('email'));
        }

        if(auth()->guard('admin')->user()->status == 0)
        {
            auth()->guard('admin')->logout();

            return CrudMessage::warningWithInput(trans('back.account-has-been-disactive'), $request->only('email'));
        }

        return redirect()->intended(route('admin-panel'));
    }

    public function forgetPassword()
    {
        return view('auth.pages.admin.adminForgetPassword');
    }

    public function confirmedResetMail()
    {
        return view('auth.pages.admin.adminConfirmedMail');
    }

    public function sendResetMail(Request $request)
    {
        $request->validate(['email' => 'required|email|exists:admins,email']);

        $token = random(60);

        DB::table('password_resets')->insert(self::passwordResetData($request->email, $token));

        SendEmail::dispatch($request->email, new SendResetingMail($token));

        return redirect()->route('admin.confirmedResetMail')->with('email', $request->email);
    }

    public function adminResetPassword($token)
    {
        return view('auth.pages.admin.adminResetForm', compact('token'));
    }

    public function adminChangePassword(Request $request)
    {
        $request->validate(self::getValidationRules());

        $check = DB::table('password_resets')->where('token', $request->token)->latest()->first();

        if(!$check) return redirect()->route('admin.login')->with('danger', 'Invalid Token');

        $admin = Admin::whereEmail($check->email)->first();

        if(!$admin) return redirect()->route('admin.login')->with('danger', 'من فضلك حاول مرة أخري');

        $admin->update(['password' => $request->new_password]);

        DB::table('password_resets')->where('email', $admin->email)->delete();

        return redirect()->route('admin.login')->with('success', trans('api.password-changed-successfully'));
    }

    public function adminLogout()
    {
        Auth::guard('admin')->logout();

        // in case of multi auth, comment this two lines
        // request()->session()->flush();
        // request()->session()->regenerate();

        return redirect()->route('admin-panel');
    }
}
