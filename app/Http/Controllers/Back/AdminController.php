<?php

namespace App\Http\Controllers\Back;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\RepoController;
use App\Http\Requests\Back\EditAdminRequest;
use App\Http\Requests\Back\CreateAdminRequest;
use App\Repository\Contracts\IAdminRepository;
use App\Http\Requests\Back\EditAdminProfileRequest;

class AdminController extends RepoController
{
    public function __construct(IAdminRepository $repository)
    {
        parent::__construct($repository);
    }

    public function showAdminMessage($id)
    {
        return self::repo()->showAdminMessage($id);
    }

    public function sendAdminMessage(Request $request)
    {
        return self::repo()->sendAdminMessage($request);
    }

    public function AdminUpdateProfile(EditAdminProfileRequest $request)
    {
        return self::repo()->updateAdminProfile($request);
    }

    public function adminProfile()
    {
        return self::repo()->adminProfile();
    }

    public function store(CreateAdminRequest $request)
    {
        return self::repo()->store($request);
    }

    public function update(EditAdminRequest $request, Admin $admin)
    {
        return self::repo()->update($request, $admin);
    }
}
