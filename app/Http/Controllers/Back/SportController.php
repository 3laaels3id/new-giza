<?php

namespace App\Http\Controllers\Back;

use App\Models\Sport;
use App\Http\Controllers\RepoController;
use App\Http\Requests\Back\EditSportRequest;
use App\Http\Requests\Back\CreateSportRequest;
use App\Repository\Contracts\ISportRepository;

class SportController extends RepoController
{
    public function __construct(ISportRepository $repository)
    {
        parent::__construct($repository);
    }

    public function store(CreateSportRequest $request)
    {
        return self::repo()->store($request);
    }

    public function update(EditSportRequest $request, Sport $sport)
    {
        return self::repo()->update($request, $sport);
    }
}
