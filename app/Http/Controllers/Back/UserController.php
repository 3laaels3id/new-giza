<?php

namespace App\Http\Controllers\Back;

use App\Models\User;
use Illuminate\Http\Request;
use App\Facade\Support\CrudMessage;
use App\Http\Controllers\RepoController;
use App\Http\Requests\Back\EditUserRequest;
use App\Http\Requests\Back\CreateUserRequest;
use App\Repository\Contracts\IUserRepository;

class UserController extends RepoController
{
    public function __construct(IUserRepository $repository)
    {
        parent::__construct($repository);
    }

    public function showUserMessage($id)
    {
        return self::repo()->showUserMessage($id);
    }

    public function sendUserMessage(Request $request)
    {
        if(is_null($request->message)) return CrudMessage::error(trans('api.message-field-is-required'));

        return self::repo()->sendUserMessage($request);
    }

    public function store(CreateUserRequest $request)
    {
        return self::repo()->store($request);
    }

    public function update(EditUserRequest $request, User $user)
    {
        return self::repo()->update($request, $user);
    }
}
