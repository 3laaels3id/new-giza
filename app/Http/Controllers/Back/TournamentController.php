<?php

namespace App\Http\Controllers\Back;

use App\Models\Tournament;
use Illuminate\Http\Request;
use App\Http\Controllers\RepoController;
use App\Http\Requests\Back\EditTournamentRequest;
use App\Http\Requests\Back\CreateTournamentRequest;
use App\Http\Requests\Back\EditTournamentSportRequest;
use App\Http\Requests\Back\CreateTournamentSportRequest;
use App\Repository\Contracts\ITournamentRepository;

class TournamentController extends RepoController
{
    public function __construct(ITournamentRepository $repository)
    {
        parent::__construct($repository);
    }

    public function store(CreateTournamentRequest $request)
    {
        return self::repo()->store($request);
    }

    public function update(EditTournamentRequest $request, Tournament $tournament)
    {
        return self::repo()->update($request, $tournament);
    }

    public function sportCreate()
    {
        return self::repo()->sportCreate();
    }

    public function sportStore(CreateTournamentSportRequest $request)
    {
        return self::repo()->sportStore($request);
    }

    public function sportEdit(Tournament $tournament)
    {
        return self::repo()->sportEdit($tournament);
    }

    public function sportUpdate(EditTournamentSportRequest $request, Tournament $tournament)
    {
        return self::repo()->sportUpdate($request, $tournament);
    }

    public function sportRemove(Request $request)
    {
        return self::repo()->sportRemove($request);
    }
}
