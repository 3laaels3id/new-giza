<?php

namespace App\Http\Controllers\Back;

use App\Models\User;
use App\Models\Admin;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        $data['users'] = User::latest()->get();

        $data['admins'] = Admin::where('role_id', '!=', 1)->latest()->get();

        return view('Back.index', self::setHomeDonutsCharts($data));
    }

    private static function setHomeDonutsCharts($data)
    {
        return [
            'admin' => self::getModelCount($data['admins'],'admin'),
            'user'  => self::getModelCount($data['users'],'user'),
        ];
    }

    private static function getModelCount($collection, $modelName)
    {
        $model = plural($modelName);

        return [
            'active_' . $model    => $collection->where('status', 1),
            'deductive_' . $model => $collection->where('status', 0)
        ];
    }
}
