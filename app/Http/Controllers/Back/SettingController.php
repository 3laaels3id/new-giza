<?php

namespace App\Http\Controllers\Back;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    public function index()
    {
        $settings = cacheModel('setting', Setting::all());

        return view('Back.Settings.index', compact('settings'));
    }

    public function create()
    {
        return view('Back.Settings.create');
    }

    public function edit(Setting $setting)
    {
        return view('Back.Settings.edit', compact('setting'));
    }

    public function UpdateAll(Request $request)
    {
        return Setting::updateAll($request);
    }
}
