<?php

namespace App\Http\Controllers\Back;

use App\Models\Contact;
use Illuminate\Http\Request;
use App\Exports\ContactsExport;
use App\Facade\Support\CrudMessage;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Http\Requests\Back\SendUserReplayMessageRequest;

class ContactController extends Controller
{
    public function Contacts()
    {
        return view('Back.Contacts.contacts', ['contacts' => Contact::paginate(10), 'search' => false]);
    }

    public function showMessageDetails($id)
    {
        if (!$message = Contact::find($id)) return CrudMessage::fails(trans('responseMessages.product-not-exist'));

        $message->update(['is_seen' => 1]);

        return view('Back.Contacts.messageDetailsModal', compact('message'))->render();
    }

    public function show(Contact $contact)
    {
        return view('Back.Contacts.show', compact('contact'));
    }

    public function sendUserMessage($id)
    {
        if (!$message = Contact::find($id)) return CrudMessage::fails(trans('back.message-not-exists'));

        return view('Back.Contacts.sendUserMessageModal', compact('message'));
    }

    public function SendUserReplayMessage(SendUserReplayMessageRequest $request)
    {
        return Contact::sendUserReplayMessage($request);
    }

    public function ContactsExport()
    {
        return Excel::download(new ContactsExport, 'contacts.xlsx');
    }

    public function DeleteContact(Request $request)
    {
        return Contact::deleteMessage($request);
    }

    public function search(Request $request)
    {
        if(is_null($request->term)) return redirect()->route('contacts.index');

        return view('Back.Contacts.contacts', ['contacts' => Contact::search($request->term)->paginate(10), 'search' => true]);
    }
}
