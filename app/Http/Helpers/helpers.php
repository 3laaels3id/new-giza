<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cache;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

function str()
{
    return new \Illuminate\Support\Str;
}

function arr()
{
    return new \Illuminate\Support\Arr;
}

function carbon()
{
    return new \Carbon\Carbon;
}

function crud()
{
    return new \App\Facade\Crud;
}

function uploaded()
{
    return new \App\Facade\Uploaded;
}

function apiResponse()
{
    return new \App\Facade\ApiResponse;
}

function str_of($text)
{
    return str()->of($text);
}

function singular($plural)
{
    return str()->of($plural)->singular();
}

function random($length)
{
    return str()->random($length);
}

function lower($text)
{
    return str_of($text)->lower();
}

function except($arr, $except)
{
    return arr()->except($arr, $except);
}

function only($array, $keys)
{
    return arr()->only($array, $keys);
}

function limit($string, $limit, $end = '...')
{
    return str()->limit($string, $limit, $end);
}

function direction(): string
{
    return app()->isLocale('ar') ? 'rtl' : 'ltr';
}

function floating($right = 'right', $left = 'left'): string
{
    return app()->isLocale('ar') ? $right : $left;
}

function localeUrl($url, $locale = null): string
{
    return LaravelLocalization::getLocalizedURL($locale, $url);
}

function getSetting($setting_name)
{
    return \App\Models\Setting::whereStatus(1)->where('key', $setting_name)->first()->value ?? null;
}

function str_limit_30($text): string
{
    return ucwords(str()->limit($text, 30, '...'));
}

function isNullable($text): string
{
    return (!isset($text) || $text == null || $text == '') ? trans('back.no-value') : ucwords($text);
}

function getTime($time): string
{
    return carbon()->createFromFormat('H:i', $time)->format('h:i A');
}

function setTimeByFormat($format1, $format2, $time): string
{
    return carbon()->createFromFormat($format1, $time)->format($format2);
}

function create_rand_numbers($digits = 4): int
{
    return rand(pow(10,$digits - 1), pow(10, $digits) -1);
}

function generate_rand_numbers($digits = 4): int
{
    $string_char = '0123456789';

    $ceil = ceil($digits/strlen($string_char));

    $str_repeat = str_repeat($string_char, $ceil);

    $str_shuffle = str_shuffle($str_repeat);

    return (int)substr($str_shuffle,1, $digits);
}

function setActive($url): string
{
    return request()->is(app()->getLocale().'/'.$url) ? 'active' : '';
}

function active($route): bool
{
    return Route::is($route);
}

function isLocalised($lang): bool
{
    return LaravelLocalization::getLocalizedURL($lang);
}

function get_settings_image($setting): string
{
    return ($setting->value)
        ? url('public/uploaded/settings/' . $setting->value)
        : url('public/admin/img/avatar10.jpg');
}

function getIcons($with_null = null): array
{
    // warning this icons works only with fontawesome 4
    $cards  = $with_null == null ? [] : ['' => ''];
    $cards += [
        'fab fa-500px'                               => 'fab fa-500px',
        'fa fa-adjust'                               => 'fa fa-adjust',
        'fa fa-american-sign-language-interpreting'  => 'fa fa-american-sign-language-interpreting',
        'fab fa-adn'                                 => 'fab fa-adn',
        'fa fa-address-book'                         => 'fa fa-address-book',
        'fa fa-address-book-o'                       => 'fa fa-address-book-o',
        'fa fa-address-card'                         => 'fa fa-address-card',
        'fa fa-address-card-o'                       => 'fa fa-address-card-o',
        'fa fa-align-center'                         => 'fa fa-align-center',
        'fa fa-align-justify'                        => 'fa fa-align-justify',
        'fa fa-align-left'                           => 'fa fa-align-left',
        'fa fa-align-right'                          => 'fa fa-align-right',
        'fab fa-amazon'                              => 'fab fa-amazon',
        'fa fa-ambulance'                            => 'fa fa-ambulance',
        'fa fa-anchor'                               => 'fa fa-anchor',
        'fab fa-android'                             => 'fab fa-android',
        'fab fa-angellist'                           => 'fab fa-angellist',
        'fa fa-angle-double-down'                    => 'fa fa-angle-double-down',
        'fa fa-angle-double-left'                    => 'fa fa-angle-double-left',
        'fa fa-angle-double-right'                   => 'fa fa-angle-double-right',
        'fa fa-angle-double-up'                      => 'fa fa-angle-double-up',
        'fa fa-angle-down'                           => 'fa fa-angle-down',
        'fa fa-angle-left'                           => 'fa fa-angle-left',
        'fa fa-angle-right'                          => 'fa fa-angle-right',
        'fa fa-angle-up'                             => 'fa fa-angle-up',
        'fab fa-apple'                               => 'fab fa-apple',
        'fa fa-archive'                              => 'fa fa-archive',
        'fa fa-area-chart'                           => 'fa fa-area-chart',
        'fa fa-arrow-circle-down'                    => 'fa fa-arrow-circle-down',
        'fa fa-arrow-circle-left'                    => 'fa fa-arrow-circle-left',
        'fa fa-arrow-circle-o-down'                  => 'fa fa-arrow-circle-o-down',
        'fa fa-arrow-circle-o-left'                  => 'fa fa-arrow-circle-o-left',
        'fa fa-arrow-circle-o-right'                 => 'fa fa-arrow-circle-o-right',
        'fa fa-arrow-circle-o-up'                    => 'fa fa-arrow-circle-o-up',
        'fa fa-arrow-circle-right'                   => 'fa fa-arrow-circle-right',
        'fa fa-arrow-circle-up'                      => 'fa fa-arrow-circle-up',
        'fa fa-arrow-down'                           => 'fa fa-arrow-down',
        'fa fa-arrow-left'                           => 'fa fa-arrow-left',
        'fa fa-arrow-right'                          => 'fa fa-arrow-right',
        'fa fa-arrow-up'                             => 'fa fa-arrow-up',
        'fa fa-arrows-alt'                           => 'fa fa-arrows-alt',
        'fa fa-arrows-h'                             => 'fa fa-arrows-h',
        'fa fa-arrows-v'                             => 'fa fa-arrows-v',
        'fa fa-arrows'                               => 'fa fa-arrows',
        'fa fa-asl-interpreting'                     => 'fa fa-asl-interpreting',
        'fa fa-assistive-listening-systems'          => 'fa fa-assistive-listening-systems',
        'fa fa-asterisk'                             => 'fa fa-asterisk',
        'fa fa-audio-description'                    => 'fa fa-audio-description',
        'fa fa-automobile'                           => 'fa fa-automobile',
        'fa fa-backward'                             => 'fa fa-backward',
        'fa fa-balance-scale'                        => 'fa fa-balance-scale',
        'fa fa-ban'                                  => 'fa fa-ban',
        'fab fa-bandcamp'                            => 'fab fa-bandcamp',
        'fa fa-bank'                                 => 'fa fa-bank',
        'fa fa-bar-chart'                            => 'fa fa-bar-chart',
        'fa fa-bar-chart-o'                          => 'fa fa-bar-chart-o',
        'fa fa-barcode'                              => 'fa fa-barcode',
        'fa fa-bars'                                 => 'fa fa-bars',
        'fa fa-bath'                                 => 'fa fa-bath',
        'fa fa-bathtub'                              => 'fa fa-bathtub',
        'fa fa-battery-0'                            => 'fa fa-battery-0',
        'fa fa-battery-1'                            => 'fa fa-battery-1',
        'fa fa-battery-2'                            => 'fa fa-battery-2',
        'fa fa-battery-3'                            => 'fa fa-battery-3',
        'fa fa-battery-4'                            => 'fa fa-battery-4',
        'fa fa-battery-empty'                        => 'fa fa-battery-empty',
        'fa fa-battery-full'                         => 'fa fa-battery-full',
        'fa fa-battery-half'                         => 'fa fa-battery-half',
        'fa fa-battery-quarter'                      => 'fa fa-battery-quarter',
        'fa fa-battery-three-quarters'               => 'fa fa-battery-three-quarters',
        'fa fa-bed'                                  => 'fa fa-bed',
        'fa fa-beer'                                 => 'fa fa-beer',
        'fab fa-behance'                             => 'fab fa-behance',
        'fab fa-behance-square'                      => 'fab fa-behance-square',
        'fa fa-bell-o'                               => 'fa fa-bell-o',
        'fa fa-bell-slash'                           => 'fa fa-bell-slash',
        'fa fa-bell-slash-o'                         => 'fa fa-bell-slash-o',
        'fa fa-bicycle'                              => 'fa fa-bicycle',
        'fa fa-binoculars'                           => 'fa fa-binoculars',
        'fa fa-birthday-cake'                        => 'fa fa-birthday-cake',
        'fab fa-bitbucket'                           => 'fab fa-bitbucket',
        'fab fa-bitcoin'                             => 'fab fa-bitcoin',
        'fab fa-black-tie'                           => 'fab fa-black-tie',
        'fa fa-blind'                                => 'fa fa-blind',
        'fab fa-bluetooth'                           => 'fab fa-bluetooth',
        'fab fa-bluetooth-b'                         => 'fab fa-bluetooth-b',
        'fa fa-bold'                                 => 'fa fa-bold',
        'fa fa-bolt'                                 => 'fa fa-bolt',
        'fa fa-bomb'                                 => 'fa fa-bomb',
        'fa fa-book'                                 => 'fa fa-book',
        'fa fa-bookmark'                             => 'fa fa-bookmark',
        'fa fa-bookmark-o'                           => 'fa fa-bookmark-o',
        'fa fa-braille'                              => 'fa fa-braille',
        'fa fa-briefcase'                            => 'fa fa-briefcase',
        'fab fa-btc'                                 => 'fab fa-btc',
        'fa fa-bug'                                  => 'fa fa-bug',
        'fa fa-building'                             => 'fa fa-building',
        'fa fa-building-o'                           => 'fa fa-building-o',
        'fa fa-bullhorn'                             => 'fa fa-bullhorn',
        'fa fa-bullseye'                             => 'fa fa-bullseye',
        'fa fa-bus'                                  => 'fa fa-bus',
        'fab fa-buysellads'                          => 'fab fa-buysellads',
        'fa fa-cab'                                  => 'fa fa-cab',
        'fa fa-calculator'                           => 'fa fa-calculator',
        'fa fa-calendar'                             => 'fa fa-calendar',
        'fa fa-calendar-check-o'                     => 'fa fa-calendar-check-o',
        'fa fa-calendar-minus-o'                     => 'fa fa-calendar-minus-o',
        'fa fa-calendar-plus-o'                      => 'fa fa-calendar-plus-o',
        'fa fa-calendar-times-o'                     => 'fa fa-calendar-times-o',
        'fa fa-calendar-o'                           => 'fa fa-calendar-o',
        'fa fa-camera'                               => 'fa fa-camera',
        'fa fa-camera-retro'                         => 'fa fa-camera-retro',
        'fa fa-car'                                  => 'fa fa-car',
        'fa fa-caret-down'                           => 'fa fa-caret-down',
        'fa fa-caret-left'                           => 'fa fa-caret-left',
        'fa fa-caret-right'                          => 'fa fa-caret-right',
        'fa fa-caret-square-o-down'                  => 'fa fa-caret-square-o-down',
        'fa fa-caret-square-o-left'                  => 'fa fa-caret-square-o-left',
        'fa fa-caret-square-o-right'                 => 'fa fa-caret-square-o-right',
        'fa fa-caret-square-o-up'                    => 'fa fa-caret-square-o-up',
        'fa fa-caret-up'                             => 'fa fa-caret-up',
        'fa fa-cart-arrow-down'                      => 'fa fa-cart-arrow-down',
        'fa fa-cart-plus'                            => 'fa fa-cart-plus',
        'fa fa-cc'                                   => 'fa fa-cc',
        'fab fa-cc-amex'                             => 'fab fa-cc-amex',
        'fab fa-cc-diners-club'                      => 'fab fa-cc-diners-club',
        'fab fa-cc-discover'                         => 'fab fa-cc-discover',
        'fab fa-cc-jcb'                              => 'fab fa-cc-jcb',
        'fab fa-cc-mastercard'                       => 'fab fa-cc-mastercard',
        'fab fa-cc-paypal'                           => 'fab fa-cc-paypal',
        'fab fa-cc-stripe'                           => 'fab fa-cc-stripe',
        'fab fa-cc-visa'                             => 'fab fa-cc-visa',
        'fa fa-certificate'                          => 'fa fa-certificate',
        'fa fa-chain'                                => 'fa fa-chain',
        'fa fa-chain-broken'                         => 'fa fa-chain-broken',
        'fa fa-check'                                => 'fa fa-check',
        'fa fa-check-circle'                         => 'fa fa-check-circle',
        'fa fa-check-circle-o'                       => 'fa fa-check-circle-o',
        'fa fa-check-square'                         => 'fa fa-check-square',
        'fa fa-check-square-o'                       => 'fa fa-check-square-o',
        'fa fa-chevron-circle-down'                  => 'fa fa-chevron-circle-down',
        'fa fa-chevron-circle-left'                  => 'fa fa-chevron-circle-left',
        'fa fa-chevron-circle-right'                 => 'fa fa-chevron-circle-right',
        'fa fa-chevron-circle-up'                    => 'fa fa-chevron-circle-up',
        'fa fa-chevron-down'                         => 'fa fa-chevron-down',
        'fa fa-chevron-left'                         => 'fa fa-chevron-left',
        'fa fa-chevron-right'                        => 'fa fa-chevron-right',
        'fa fa-chevron-up'                           => 'fa fa-chevron-up',
        'fa fa-child'                                => 'fa fa-child',
        'fa fa-chrome'                               => 'fa fa-chrome',
        'fa fa-clone'                                => 'fa fa-clone',
        'fa fa-circle'                               => 'fa fa-circle',
        'fa fa-circle-o'                             => 'fa fa-circle-o',
        'fa fa-circle-o-notch'                       => 'fa fa-circle-o-notch',
        'fa fa-circle-thin'                          => 'fa fa-circle-thin',
        'fa fa-clipboard'                            => 'fa fa-clipboard',
        'fa fa-clock-o'                              => 'fa fa-clock-o',
        'fa fa-close'                                => 'fa fa-close',
        'fa fa-cloud'                                => 'fa fa-cloud',
        'fa fa-cloud-download'                       => 'fa fa-cloud-download',
        'fa fa-cloud-upload'                         => 'fa fa-cloud-upload',
        'fa fa-cny'                                  => 'fa fa-cny',
        'fa fa-code'                                 => 'fa fa-code',
        'fa fa-code-fork'                            => 'fa fa-code-fork',
        'fab fa-codepen'                             => 'fab fa-codepen',
        'fab fa-codiepie'                            => 'fab fa-codiepie',
        'fa fa-coffee'                               => 'fa fa-coffee',
        'fa fa-cog'                                  => 'fa fa-cog',
        'fa fa-cogs'                                 => 'fa fa-cogs',
        'fa fa-columns'                              => 'fa fa-columns',
        'fa fa-comment'                              => 'fa fa-comment',
        'fa fa-comment-o'                            => 'fa fa-comment-o',
        'fa fa-comments'                             => 'fa fa-comments',
        'fa fa-comments-o'                           => 'fa fa-comments-o',
        'fa fa-commenting'                           => 'fa fa-commenting',
        'fa fa-commenting-o'                         => 'fa fa-commenting-o',
        'fa fa-compass'                              => 'fa fa-compass',
        'fa fa-compress'                             => 'fa fa-compress',
        'fab fa-connectdevelop'                      => 'fab fa-connectdevelop',
        'fab fa-contao'                              => 'fab fa-contao',
        'fa fa-copy'                                 => 'fa fa-copy',
        'fa fa-copyright'                            => 'fa fa-copyright',
        'fab fa-creative-commons'                    => 'fab fa-creative-commons',
        'fa fa-credit-card'                          => 'fa fa-credit-card',
        'fa fa-credit-card-alt'                      => 'fa fa-credit-card-alt',
        'fa fa-crop'                                 => 'fa fa-crop',
        'fa fa-crosshairs'                           => 'fa fa-crosshairs',
        'fab fa-css3'                                => 'fab fa-css3',
        'fa fa-cube'                                 => 'fa fa-cube',
        'fa fa-cubes'                                => 'fa fa-cubes',
        'fa fa-cut'                                  => 'fa fa-cut',
        'fa fa-cutlery'                              => 'fa fa-cutlery',
        'fa fa-dashboard'                            => 'fa fa-dashboard',
        'fab fa-dashcube'                            => 'fab fa-dashcube',
        'fa fa-database'                             => 'fa fa-database',
        'fa fa-deaf'                                 => 'fa fa-deaf',
        'fa fa-deafness'                             => 'fa fa-deafness',
        'fa fa-dedent'                               => 'fa fa-dedent',
        'fa fa-delicious'                            => 'fa fa-delicious',
        'fa fa-desktop'                              => 'fa fa-desktop',
        'fa fa-deviantart'                           => 'fa fa-deviantart',
        'fa fa-diamond'                              => 'fa fa-diamond',
        'fa fa-digg'                                 => 'fa fa-digg',
        'fa fa-dollar'                               => 'fa fa-dollar',
        'fa fa-dot-circle-o'                         => 'fa fa-dot-circle-o',
        'fa fa-download'                             => 'fa fa-download',
        'fa fa-dribbble'                             => 'fa fa-dribbble',
        'fa fa-drivers-license'                      => 'fa fa-drivers-license',
        'fa fa-drivers-license-o'                    => 'fa fa-drivers-license-o',
        'fa fa-dropbox'                              => 'fa fa-dropbox',
        'fa fa-drupal'                               => 'fa fa-drupal',
        'fa fa-edge'                                 => 'fa fa-edge',
        'fa fa-edit'                                 => 'fa fa-edit',
        'fa fa-eercast'                              => 'fa fa-eercast',
        'fa fa-eject'                                => 'fa fa-eject',
        'fa fa-ellipsis-h'                           => 'fa fa-ellipsis-h',
        'fa fa-ellipsis-v'                           => 'fa fa-ellipsis-v',
        'fa fa-empire'                               => 'fa fa-empire',
        'fa fa-envelope'                             => 'fa fa-envelope',
        'fa fa-envelope-o'                           => 'fa fa-envelope-o',
        'fa fa-envelope-open'                        => 'fa fa-envelope-open',
        'fa fa-envelope-open-o'                      => 'fa fa-envelope-open-o',
        'fa fa-envelope-square'                      => 'fa fa-envelope-square',
        'fa fa-envira'                               => 'fa fa-envira',
        'fa fa-eraser'                               => 'fa fa-eraser',
        'fa fa-etsy'                                 => 'fa fa-etsy',
        'fa fa-eur'                                  => 'fa fa-eur',
        'fa fa-euro'                                 => 'fa fa-euro',
        'fa fa-exchange'                             => 'fa fa-exchange',
        'fa fa-exclamation'                          => 'fa fa-exclamation',
        'fa fa-exclamation-circle'                   => 'fa fa-exclamation-circle',
        'fa fa-exclamation-triangle'                 => 'fa fa-exclamation-triangle',
        'fa fa-expand'                               => 'fa fa-expand',
        'fa fa-expeditedssl'                         => 'fa fa-expeditedssl',
        'fa fa-external-link'                        => 'fa fa-external-link',
        'fa fa-external-link-square'                 => 'fa fa-external-link-square',
        'fa fa-eye'                                  => 'fa fa-eye',
        'fa fa-eye-slash'                            => 'fa fa-eye-slash',
        'fa fa-eyedropper'                           => 'fa fa-eyedropper',
        'fa fa-facebook'                             => 'fa fa-facebook',
        'fa fa-facebook-f'                           => 'fa fa-facebook-f',
        'fa fa-facebook-official'                    => 'fa fa-facebook-official',
        'fa fa-facebook-square'                      => 'fa fa-facebook-square',
        'fa fa-fast-backward'                        => 'fa fa-fast-backward',
        'fa fa-fast-forward'                         => 'fa fa-fast-forward',
        'fa fa-fax'                                  => 'fa fa-fax',
        'fa fa-female'                               => 'fa fa-female',
        'fa fa-fighter-jet'                          => 'fa fa-fighter-jet',
        'fa fa-file'                                 => 'fa fa-file',
        'fa fa-file-archive-o'                       => 'fa fa-file-archive-o',
        'fa fa-file-audio-o'                         => 'fa fa-file-audio-o',
        'fa fa-file-code-o'                          => 'fa fa-file-code-o',
        'fa fa-file-excel-o'                         => 'fa fa-file-excel-o',
        'fa fa-file-image-o'                         => 'fa fa-file-image-o',
        'fa fa-file-movie-o'                         => 'fa fa-file-movie-o',
        'fa fa-file-o'                               => 'fa fa-file-o',
        'fa fa-file-pdf-o'                           => 'fa fa-file-pdf-o',
        'fa fa-file-photo-o'                         => 'fa fa-file-photo-o',
        'fa fa-file-picture-o'                       => 'fa fa-file-picture-o',
        'fa fa-file-powerpoint-o'                    => 'fa fa-file-powerpoint-o',
        'fa fa-file-sound-o'                         => 'fa fa-file-sound-o',
        'fa fa-file-text'                            => 'fa fa-file-text',
        'fa fa-file-text-o'                          => 'fa fa-file-text-o',
        'fa fa-file-video-o'                         => 'fa fa-file-video-o',
        'fa fa-file-word-o'                          => 'fa fa-file-word-o',
        'fa fa-file-zip-o'                           => 'fa fa-file-zip-o',
        'fa fa-files-o'                              => 'fa fa-files-o',
        'fa fa-film'                                 => 'fa fa-film',
        'fa fa-filter'                               => 'fa fa-filter',
        'fa fa-fire'                                 => 'fa fa-fire',
        'fa fa-fire-extinguisher'                    => 'fa fa-fire-extinguisher',
        'fa fa-firefox'                              => 'fa fa-firefox',
        'fa fa-first-order'                          => 'fa fa-first-order',
        'fa fa-flag'                                 => 'fa fa-flag',
        'fa fa-flag-checkered'                       => 'fa fa-flag-checkered',
        'fa fa-flag-o'                               => 'fa fa-flag-o',
        'fa fa-flash'                                => 'fa fa-flash',
        'fa fa-flask'                                => 'fa fa-flask',
        'fa fa-flickr'                               => 'fa fa-flickr',
        'fa fa-floppy-o'                             => 'fa fa-floppy-o',
        'fa fa-folder'                               => 'fa fa-folder',
        'fa fa-folder-o'                             => 'fa fa-folder-o',
        'fa fa-folder-open'                          => 'fa fa-folder-open',
        'fa fa-folder-open-o'                        => 'fa fa-folder-open-o',
        'fa fa-font'                                 => 'fa fa-font',
        'fa fa-fonticons'                            => 'fa fa-fonticons',
        'fa fa-fort-awesome'                         => 'fa fa-fort-awesome',
        'fa fa-forumbee'                             => 'fa fa-forumbee',
        'fa fa-forward'                              => 'fa fa-forward',
        'fa fa-foursquare'                           => 'fa fa-foursquare',
        'fa fa-free-code-camp'                       => 'fa fa-free-code-camp',
        'fa fa-frown-o'                              => 'fa fa-frown-o',
        'fa fa-futbol-o'                             => 'fa fa-futbol-o',
        'fa fa-gamepad'                              => 'fa fa-gamepad',
        'fa fa-gavel'                                => 'fa fa-gavel',
        'fa fa-gbp'                                  => 'fa fa-gbp',
        'fa fa-ge'                                   => 'fa fa-ge',
        'fa fa-gear'                                 => 'fa fa-gear',
        'fa fa-gears'                                => 'fa fa-gears',
        'fa fa-genderless'                           => 'fa fa-genderless',
        'fa fa-get-pocket'                           => 'fa fa-get-pocket',
        'fa fa-gg'                                   => 'fa fa-gg',
        'fa fa-gg-circle'                            => 'fa fa-gg-circle',
        'fa fa-gift'                                 => 'fa fa-gift',
        'fa fa-git'                                  => 'fa fa-git',
        'fa fa-git-square'                           => 'fa fa-git-square',
        'fa fa-github'                               => 'fa fa-github',
        'fa fa-github-alt'                           => 'fa fa-github-alt',
        'fa fa-github-square'                        => 'fa fa-github-square',
        'fa fa-gitlab'                               => 'fa fa-gitlab',
        'fa fa-gittip'                               => 'fa fa-gittip',
        'fa fa-glass'                                => 'fa fa-glass',
        'fa fa-glide'                                => 'fa fa-glide',
        'fa fa-glide-g'                              => 'fa fa-glide-g',
        'fa fa-globe'                                => 'fa fa-globe',
        'fa fa-google'                               => 'fa fa-google',
        'fa fa-google-plus'                          => 'fa fa-google-plus',
        'fa fa-google-plus-square'                   => 'fa fa-google-plus-square',
        'fa fa-google-wallet'                        => 'fa fa-google-wallet',
        'fa fa-graduation-cap'                       => 'fa fa-graduation-cap',
        'fa fa-gratipay'                             => 'fa fa-gratipay',
        'fa fa-grav'                                 => 'fa fa-grav',
        'fa fa-group'                                => 'fa fa-group',
        'fa fa-h-square'                             => 'fa fa-h-square',
        'fa fa-hacker-news'                          => 'fa fa-hacker-news',
        'fa fa-hand-grab-o'                          => 'fa fa-hand-grab-o',
        'fa fa-hand-lizard-o'                        => 'fa fa-hand-lizard-o',
        'fa fa-hand-paper-o'                         => 'fa fa-hand-paper-o',
        'fa fa-hand-peace-o'                         => 'fa fa-hand-peace-o',
        'fa fa-hand-pointer-o'                       => 'fa fa-hand-pointer-o',
        'fa fa-hand-rock-o'                          => 'fa fa-hand-rock-o',
        'fa fa-hand-scissors-o'                      => 'fa fa-hand-scissors-o',
        'fa fa-hand-spock-o'                         => 'fa fa-hand-spock-o',
        'fa fa-hand-stop-o'                          => 'fa fa-hand-stop-o',
        'fa fa-hand-o-down'                          => 'fa fa-hand-o-down',
        'fa fa-hand-o-left'                          => 'fa fa-hand-o-left',
        'fa fa-hand-o-right'                         => 'fa fa-hand-o-right',
        'fa fa-hand-o-up'                            => 'fa fa-hand-o-up',
        'fa fa-handshake-o'                          => 'fa fa-handshake-o',
        'fa fa-hard-of-hearing'                      => 'fa fa-hard-of-hearing',
        'fa fa-hashtag'                              => 'fa fa-hashtag',
        'fa fa-hdd-o'                                => 'fa fa-hdd-o',
        'fa fa-header'                               => 'fa fa-header',
        'fa fa-headphones'                           => 'fa fa-headphones',
        'fa fa-heart'                                => 'fa fa-heart',
        'fa fa-heart-o'                              => 'fa fa-heart-o',
        'fa fa-heartbeat'                            => 'fa fa-heartbeat',
        'fa fa-history'                              => 'fa fa-history',
        'fa fa-home'                                 => 'fa fa-home',
        'fa fa-hospital-o'                           => 'fa fa-hospital-o',
        'fa fa-hotel'                                => 'fa fa-hotel',
        'fa fa-hourglass'                            => 'fa fa-hourglass',
        'fa fa-hourglass-1'                          => 'fa fa-hourglass-1',
        'fa fa-hourglass-2'                          => 'fa fa-hourglass-2',
        'fa fa-hourglass-3'                          => 'fa fa-hourglass-3',
        'fa fa-hourglass-end'                        => 'fa fa-hourglass-end',
        'fa fa-hourglass-half'                       => 'fa fa-hourglass-half',
        'fa fa-hourglass-o'                          => 'fa fa-hourglass-o',
        'fa fa-hourglass-start'                      => 'fa fa-hourglass-start',
        'fa fa-houzz'                                => 'fa fa-houzz',
        'fa fa-html5'                                => 'fa fa-html5',
        'fa fa-i-cursor'                             => 'fa fa-i-cursor',
        'fa fa-id-badge'                             => 'fa fa-id-badge',
        'fa fa-id-card'                              => 'fa fa-id-card',
        'fa fa-id-card-o'                            => 'fa fa-id-card-o',
        'fa fa-ils'                                  => 'fa fa-ils',
        'fa fa-image'                                => 'fa fa-image',
        'fa fa-imdb'                                 => 'fa fa-imdb',
        'fa fa-inbox'                                => 'fa fa-inbox',
        'fa fa-indent'                               => 'fa fa-indent',
        'fa fa-industry'                             => 'fa fa-industry',
        'fa fa-info'                                 => 'fa fa-info',
        'fa fa-info-circle'                          => 'fa fa-info-circle',
        'fa fa-inr'                                  => 'fa fa-inr',
        'fa fa-instagram'                            => 'fa fa-instagram',
        'fa fa-institution'                          => 'fa fa-institution',
        'fa fa-internet-explorer'                    => 'fa fa-internet-explorer',
        'fa fa-ioxhost'                              => 'fa fa-ioxhost',
        'fa fa-italic'                               => 'fa fa-italic',
        'fa fa-joomla'                               => 'fa fa-joomla',
        'fa fa-jpy'                                  => 'fa fa-jpy',
        'fa fa-jsfiddle'                             => 'fa fa-jsfiddle',
        'fa fa-key'                                  => 'fa fa-key',
        'fa fa-keyboard-o'                           => 'fa fa-keyboard-o',
        'fa fa-krw'                                  => 'fa fa-krw',
        'fa fa-language'                             => 'fa fa-language',
        'fa fa-laptop'                               => 'fa fa-laptop',
        'fa fa-lastfm'                               => 'fa fa-lastfm',
        'fa fa-lastfm-square'                        => 'fa fa-lastfm-square',
        'fa fa-leaf'                                 => 'fa fa-leaf',
        'fa fa-leanpub'                              => 'fa fa-leanpub',
        'fa fa-legal'                                => 'fa fa-legal',
        'fa fa-lemon-o'                              => 'fa fa-lemon-o',
        'fa fa-level-down'                           => 'fa fa-level-down',
        'fa fa-level-up'                             => 'fa fa-level-up',
        'fa fa-life-bouy'                            => 'fa fa-life-bouy',
        'fa fa-life-buoy'                            => 'fa fa-life-buoy',
        'fa fa-life-ring'                            => 'fa fa-life-ring',
        'fa fa-life-saver'                           => 'fa fa-life-saver',
        'fa fa-lightbulb-o'                          => 'fa fa-lightbulb-o',
        'fa fa-line-chart'                           => 'fa fa-line-chart',
        'fa fa-link'                                 => 'fa fa-link',
        'fa fa-linkedin'                             => 'fa fa-linkedin',
        'fa fa-linkedin-square'                      => 'fa fa-linkedin-square',
        'fa fa-linode'                               => 'fa fa-linode',
        'fa fa-linux'                                => 'fa fa-linux',
        'fa fa-list-alt'                             => 'fa fa-list-alt',
        'fa fa-list-ol'                              => 'fa fa-list-ol',
        'fa fa-list-ul'                              => 'fa fa-list-ul',
        'fa fa-location-arrow'                       => 'fa fa-location-arrow',
        'fa fa-lock'                                 => 'fa fa-lock',
        'fa fa-long-arrow-down'                      => 'fa fa-long-arrow-down',
        'fa fa-long-arrow-left'                      => 'fa fa-long-arrow-left',
        'fa fa-long-arrow-right'                     => 'fa fa-long-arrow-right',
        'fa fa-long-arrow-up'                        => 'fa fa-long-arrow-up',
        'fa fa-low-vision'                           => 'fa fa-low-vision',
        'fa fa-magic'                                => 'fa fa-magic',
        'fa fa-magnet'                               => 'fa fa-magnet',
        'fa fa-mail-forward'                         => 'fa fa-mail-forward',
        'fa fa-mail-reply'                           => 'fa fa-mail-reply',
        'fa fa-mail-reply-all'                       => 'fa fa-mail-reply-all',
        'fa fa-male'                                 => 'fa fa-male',
        'fa fa-map'                                  => 'fa fa-map',
        'fa fa-map-marker'                           => 'fa fa-map-marker',
        'fa fa-map-o'                                => 'fa fa-map-o',
        'fa fa-map-pin'                              => 'fa fa-map-pin',
        'fa fa-map-signs'                            => 'fa fa-map-signs',
        'fa fa-mars'                                 => 'fa fa-mars',
        'fa fa-mars-double'                          => 'fa fa-mars-double',
        'fa fa-mars-stroke'                          => 'fa fa-mars-stroke',
        'fa fa-mars-stroke-h'                        => 'fa fa-mars-stroke-h',
        'fa fa-mars-stroke-v'                        => 'fa fa-mars-stroke-v',
        'fa fa-maxcdn'                               => 'fa fa-maxcdn',
        'fa fa-meanpath'                             => 'fa fa-meanpath',
        'fa fa-medium'                               => 'fa fa-medium',
        'fa fa-medkit'                               => 'fa fa-medkit',
        'fa fa-meetup'                               => 'fa fa-meetup',
        'fa fa-meh-o'                                => 'fa fa-meh-o',
        'fa fa-mercury'                              => 'fa fa-mercury',
        'fa fa-microchip'                            => 'fa fa-microchip',
        'fa fa-microphone'                           => 'fa fa-microphone',
        'fa fa-microphone-slash'                     => 'fa fa-microphone-slash',
        'fa fa-minus'                                => 'fa fa-minus',
        'fa fa-minus-circle'                         => 'fa fa-minus-circle',
        'fa fa-minus-square'                         => 'fa fa-minus-square',
        'fa fa-minus-square-o'                       => 'fa fa-minus-square-o',
        'fa fa-mixcloud'                             => 'fa fa-mixcloud',
        'fa fa-mobile'                               => 'fa fa-mobile',
        'fa fa-mobile-phone'                         => 'fa fa-mobile-phone',
        'fa fa-modx'                                 => 'fa fa-modx',
        'fa fa-money'                                => 'fa fa-money',
        'fa fa-moon-o'                               => 'fa fa-moon-o',
        'fa fa-mortar-board'                         => 'fa fa-mortar-board',
        'fa fa-motorcycle'                           => 'fa fa-motorcycle',
        'fa fa-mouse-pointer'                        => 'fa fa-mouse-pointer',
        'fa fa-music'                                => 'fa fa-music',
        'fa fa-navicon'                              => 'fa fa-navicon',
        'fa fa-neuter'                               => 'fa fa-neuter',
        'fa fa-newspaper-o'                          => 'fa fa-newspaper-o',
        'fa fa-object-group'                         => 'fa fa-object-group',
        'fa fa-object-ungroup'                       => 'fa fa-object-ungroup',
        'fa fa-odnoklassniki'                        => 'fa fa-odnoklassniki',
        'fa fa-odnoklassniki-square'                 => 'fa fa-odnoklassniki-square',
        'fa fa-opencart'                             => 'fa fa-opencart',
        'fa fa-openid'                               => 'fa fa-openid',
        'fa fa-opera'                                => 'fa fa-opera',
        'fa fa-optin-monster'                        => 'fa fa-optin-monster',
        'fa fa-outdent'                              => 'fa fa-outdent',
        'fa fa-pagelines'                            => 'fa fa-pagelines',
        'fa fa-paint-brush'                          => 'fa fa-paint-brush',
        'fa fa-paper-plane'                          => 'fa fa-paper-plane',
        'fa fa-paper-plane-o'                        => 'fa fa-paper-plane-o',
        'fa fa-paperclip'                            => 'fa fa-paperclip',
        'fa fa-paragraph'                            => 'fa fa-paragraph',
        'fa fa-paste'                                => 'fa fa-paste',
        'fa fa-pause'                                => 'fa fa-pause',
        'fa fa-pause-circle'                         => 'fa fa-pause-circle',
        'fa fa-pause-circle-o'                       => 'fa fa-pause-circle-o',
        'fa fa-paw'                                  => 'fa fa-paw',
        'fa fa-paypal'                               => 'fa fa-paypal',
        'fa fa-pencil'                               => 'fa fa-pencil',
        'fa fa-pencil-square'                        => 'fa fa-pencil-square',
        'fa fa-pencil-square-o'                      => 'fa fa-pencil-square-o',
        'fa fa-percent'                              => 'fa fa-percent',
        'fa fa-phone'                                => 'fa fa-phone',
        'fa fa-phone-square'                         => 'fa fa-phone-square',
        'fa fa-photo'                                => 'fa fa-photo',
        'fa fa-picture-o'                            => 'fa fa-picture-o',
        'fa fa-pie-chart'                            => 'fa fa-pie-chart',
        'fa fa-pied-piper'                           => 'fa fa-pied-piper',
        'fa fa-pied-piper-alt'                       => 'fa fa-pied-piper-alt',
        'fa fa-pinterest'                            => 'fa fa-pinterest',
        'fa fa-pinterest-p'                          => 'fa fa-pinterest-p',
        'fa fa-pinterest-square'                     => 'fa fa-pinterest-square',
        'fa fa-plane'                                => 'fa fa-plane',
        'fa fa-play'                                 => 'fa fa-play',
        'fa fa-play-circle'                          => 'fa fa-play-circle',
        'fa fa-play-circle-o'                        => 'fa fa-play-circle-o',
        'fa fa-plug'                                 => 'fa fa-plug',
        'fa fa-plus'                                 => 'fa fa-plus',
        'fa fa-plus-circle'                          => 'fa fa-plus-circle',
        'fa fa-plus-square'                          => 'fa fa-plus-square',
        'fa fa-plus-square-o'                        => 'fa fa-plus-square-o',
        'fa fa-podcast'                              => 'fa fa-podcast',
        'fa fa-power-off'                            => 'fa fa-power-off',
        'fa fa-print'                                => 'fa fa-print',
        'fa fa-product-hunt'                         => 'fa fa-product-hunt',
        'fa fa-puzzle-piece'                         => 'fa fa-puzzle-piece',
        'fa fa-qq'                                   => 'fa fa-qq',
        'fa fa-qrcode'                               => 'fa fa-qrcode',
        'fa fa-question'                             => 'fa fa-question',
        'fa fa-question-circle'                      => 'fa fa-question-circle',
        'fa fa-question-circle-o'                    => 'fa fa-question-circle-o',
        'fa fa-quora'                                => 'fa fa-quora',
        'fa fa-quote-left'                           => 'fa fa-quote-left',
        'fa fa-quote-right'                          => 'fa fa-quote-right',
        'fa fa-ra'                                   => 'fa fa-ra',
        'fa fa-random'                               => 'fa fa-random',
        'fa fa-ravelry'                              => 'fa fa-ravelry',
        'fa fa-rebel'                                => 'fa fa-rebel',
        'fa fa-recycle'                              => 'fa fa-recycle',
        'fa fa-reddit'                               => 'fa fa-reddit',
        'fa fa-reddit-alien'                         => 'fa fa-reddit-alien',
        'fa fa-reddit-square'                        => 'fa fa-reddit-square',
        'fa fa-refresh'                              => 'fa fa-refresh',
        'fa fa-registered'                           => 'fa fa-registered',
        'fa fa-remove'                               => 'fa fa-remove',
        'fa fa-renren'                               => 'fa fa-renren',
        'fa fa-reorder'                              => 'fa fa-reorder',
        'fa fa-repeat'                               => 'fa fa-repeat',
        'fa fa-reply'                                => 'fa fa-reply',
        'fa fa-reply-all'                            => 'fa fa-reply-all',
        'fa fa-retweet'                              => 'fa fa-retweet',
        'fa fa-rmb'                                  => 'fa fa-rmb',
        'fa fa-road'                                 => 'fa fa-road',
        'fa fa-rocket'                               => 'fa fa-rocket',
        'fa fa-rouble'                               => 'fa fa-rouble',
        'fa fa-s15'                                  => 'fa fa-s15',
        'fa fa-safari'                               => 'fa fa-safari',
        'fa fa-save'                                 => 'fa fa-save',
        'fa fa-scissors'                             => 'fa fa-scissors',
        'fa fa-scribd'                               => 'fa fa-scribd',
        'fa fa-search'                               => 'fa fa-search',
        'fa fa-search-minus'                         => 'fa fa-search-minus',
        'fa fa-search-plus'                          => 'fa fa-search-plus',
        'fa fa-sellsy'                               => 'fa fa-sellsy',
        'fa fa-send'                                 => 'fa fa-send',
        'fa fa-send-o'                               => 'fa fa-send-o',
        'fa fa-server'                               => 'fa fa-server',
        'fa fa-share'                                => 'fa fa-share',
        'fa fa-share-alt'                            => 'fa fa-share-alt',
        'fa fa-share-alt-square'                     => 'fa fa-share-alt-square',
        'fa fa-share-square'                         => 'fa fa-share-square',
        'fa fa-shower'                               => 'fa fa-shower',
        'fa fa-square-o'                             => 'fa fa-square-o',
        'fa fa-share-square-o'                       => 'fa fa-share-square-o',
        'fa fa-shekel'                               => 'fa fa-shekel',
        'fa fa-sheqel'                               => 'fa fa-sheqel',
        'fa fa-shield'                               => 'fa fa-shield',
        'fa fa-ship'                                 => 'fa fa-ship',
        'fa fa-shirtsinbulk'                         => 'fa fa-shirtsinbulk',
        'fa fa-shopping-bag'                         => 'fa fa-shopping-bag',
        'fa fa-shopping-basket'                      => 'fa fa-shopping-basket',
        'fa fa-shopping-cart'                        => 'fa fa-shopping-cart',
        'fa fa-sign-in'                              => 'fa fa-sign-in',
        'fa fa-sign-out'                             => 'fa fa-sign-out',
        'fa fa-signal'                               => 'fa fa-signal',
        'fa fa-sign-language'                        => 'fa fa-sign-language',
        'fa fa-signing'                              => 'fa fa-signing',
        'fa fa-simplybuilt'                          => 'fa fa-simplybuilt',
        'fa fa-sitemap'                              => 'fa fa-sitemap',
        'fa fa-skyatlas'                             => 'fa fa-skyatlas',
        'fa fa-skype'                                => 'fa fa-skype',
        'fa fa-slack'                                => 'fa fa-slack',
        'fa fa-sliders'                              => 'fa fa-sliders',
        'fa fa-slideshare'                           => 'fa fa-slideshare',
        'fa fa-smile-o'                              => 'fa fa-smile-o',
        'fa fa-snapchat'                             => 'fa fa-snapchat',
        'fa fa-snapchat-ghost'                       => 'fa fa-snapchat-ghost',
        'fa fa-snapchat-square'                      => 'fa fa-snapchat-square',
        'fa fa-snowflake-o'                          => 'fa fa-snowflake-o',
        'fa fa-soccer-ball-o'                        => 'fa fa-soccer-ball-o',
        'fa fa-sort'                                 => 'fa fa-sort',
        'fa fa-sort-alpha-asc'                       => 'fa fa-sort-alpha-asc',
        'fa fa-sort-alpha-desc'                      => 'fa fa-sort-alpha-desc',
        'fa fa-sort-amount-asc'                      => 'fa fa-sort-amount-asc',
        'fa fa-sort-amount-desc'                     => 'fa fa-sort-amount-desc',
        'fa fa-sort-asc'                             => 'fa fa-sort-asc',
        'fa fa-sort-desc'                            => 'fa fa-sort-desc',
        'fa fa-sort-down'                            => 'fa fa-sort-down',
        'fa fa-sort-numeric-asc'                     => 'fa fa-sort-numeric-asc',
        'fa fa-sort-numeric-desc'                    => 'fa fa-sort-numeric-desc',
        'fa fa-sort-up'                              => 'fa fa-sort-up',
        'fa fa-soundcloud'                           => 'fa fa-soundcloud',
        'fa fa-space-shuttle'                        => 'fa fa-space-shuttle',
        'fa fa-spinner'                              => 'fa fa-spinner',
        'fa fa-spoon'                                => 'fa fa-spoon',
        'fa fa-spotify'                              => 'fa fa-spotify',
        'fa fa-square'                               => 'fa fa-square',
        'fa fa-stack-exchange'                       => 'fa fa-stack-exchange',
        'fa fa-stack-overflow'                       => 'fa fa-stack-overflow',
        'fa fa-star'                                 => 'fa fa-star',
        'fa fa-star-half'                            => 'fa fa-star-half',
        'fa fa-star-half-empty'                      => 'fa fa-star-half-empty',
        'fa fa-star-half-full'                       => 'fa fa-star-half-full',
        'fa fa-star-half-o'                          => 'fa fa-star-half-o',
        'fa fa-star-o'                               => 'fa fa-star-o',
        'fa fa-steam'                                => 'fa fa-steam',
        'fa fa-steam-square'                         => 'fa fa-steam-square',
        'fa fa-step-backward'                        => 'fa fa-step-backward',
        'fa fa-step-forward'                         => 'fa fa-step-forward',
        'fa fa-stethoscope'                          => 'fa fa-stethoscope',
        'fa fa-sticky-note'                          => 'fa fa-sticky-note',
        'fa fa-sticky-note-o'                        => 'fa fa-sticky-note-o',
        'fa fa-stop'                                 => 'fa fa-stop',
        'fa fa-stop-circle'                          => 'fa fa-stop-circle',
        'fa fa-stop-circle-o'                        => 'fa fa-stop-circle-o',
        'fa fa-street-view'                          => 'fa fa-street-view',
        'fa fa-strikethrough'                        => 'fa fa-strikethrough',
        'fa fa-stumbleupon'                          => 'fa fa-stumbleupon',
        'fa fa-stumbleupon-circle'                   => 'fa fa-stumbleupon-circle',
        'fa fa-subscript'                            => 'fa fa-subscript',
        'fa fa-subway'                               => 'fa fa-subway',
        'fa fa-suitcase'                             => 'fa fa-suitcase',
        'fa fa-sun-o'                                => 'fa fa-sun-o',
        'fa fa-superpowers'                          => 'fa fa-superpowers',
        'fa fa-superscript'                          => 'fa fa-superscript',
        'fa fa-support'                              => 'fa fa-support',
        'fa fa-table'                                => 'fa fa-table',
        'fa fa-tablet'                               => 'fa fa-tablet',
        'fa fa-tachometer'                           => 'fa fa-tachometer',
        'fa fa-tag'                                  => 'fa fa-tag',
        'fa fa-tags'                                 => 'fa fa-tags',
        'fa fa-tasks'                                => 'fa fa-tasks',
        'fa fa-taxi'                                 => 'fa fa-taxi',
        'fa fa-telegram'                             => 'fa fa-telegram',
        'fa fa-television'                           => 'fa fa-television',
        'fa fa-tencent-weibo'                        => 'fa fa-tencent-weibo',
        'fa fa-terminal'                             => 'fa fa-terminal',
        'fa fa-text-height'                          => 'fa fa-text-height',
        'fa fa-text-width'                           => 'fa fa-text-width',
        'fa fa-th'                                   => 'fa fa-th',
        'fa fa-th-large'                             => 'fa fa-th-large',
        'fa fa-th-list'                              => 'fa fa-th-list',
        'fa fa-themeisle'                            => 'fa fa-themeisle',
        'fa fa-thermometer'                          => 'fa fa-thermometer',
        'fa fa-thermometer-0'                        => 'fa fa-thermometer-0',
        'fa fa-thermometer-1'                        => 'fa fa-thermometer-1',
        'fa fa-thermometer-2'                        => 'fa fa-thermometer-2',
        'fa fa-thermometer-3'                        => 'fa fa-thermometer-3',
        'fa fa-thermometer-4'                        => 'fa fa-thermometer-4',
        'fa fa-thermometer-empty'                    => 'fa fa-thermometer-empty',
        'fa fa-thermometer-full'                     => 'fa fa-thermometer-full',
        'fa fa-thermometer-half'                     => 'fa fa-thermometer-half',
        'fa fa-thermometer-quarter'                  => 'fa fa-thermometer-quarter',
        'fa fa-thermometer-three-quarters'           => 'fa fa-thermometer-three-quarters',
        'fa fa-thumb-tack'                           => 'fa fa-thumb-tack',
        'fa fa-thumbs-down'                          => 'fa fa-thumbs-down',
        'fa fa-thumbs-o-down'                        => 'fa fa-thumbs-o-down',
        'fa fa-thumbs-o-up'                          => 'fa fa-thumbs-o-up',
        'fa fa-thumbs-up'                            => 'fa fa-thumbs-up',
        'fa fa-ticket'                               => 'fa fa-ticket',
        'fa fa-times'                                => 'fa fa-times',
        'fa fa-times-circle'                         => 'fa fa-times-circle',
        'fa fa-times-circle-o'                       => 'fa fa-times-circle-o',
        'fa fa-times-rectangle'                      => 'fa fa-times-rectangle',
        'fa fa-times-rectangle-o'                    => 'fa fa-times-rectangle-o',
        'fa fa-tint'                                 => 'fa fa-tint',
        'fa fa-toggle-down'                          => 'fa fa-toggle-down',
        'fa fa-toggle-left'                          => 'fa fa-toggle-left',
        'fa fa-toggle-off'                           => 'fa fa-toggle-off',
        'fa fa-toggle-on'                            => 'fa fa-toggle-on',
        'fa fa-toggle-right'                         => 'fa fa-toggle-right',
        'fa fa-toggle-up'                            => 'fa fa-toggle-up',
        'fa fa-trademark'                            => 'fa fa-trademark',
        'fa fa-train'                                => 'fa fa-train',
        'fa fa-transgender'                          => 'fa fa-transgender',
        'fa fa-transgender-alt'                      => 'fa fa-transgender-alt',
        'fa fa-trash'                                => 'fa fa-trash',
        'fa fa-trash-o'                              => 'fa fa-trash-o',
        'fa fa-tree'                                 => 'fa fa-tree',
        'fa fa-trello'                               => 'fa fa-trello',
        'fa fa-tripadvisor'                          => 'fa fa-tripadvisor',
        'fa fa-trophy'                               => 'fa fa-trophy',
        'fa fa-truck'                                => 'fa fa-truck',
        'fa fa-try'                                  => 'fa fa-try',
        'fa fa-tty'                                  => 'fa fa-tty',
        'fa fa-tv'                                   => 'fa fa-tv',
        'fa fa-tumblr'                               => 'fa fa-tumblr',
        'fa fa-tumblr-square'                        => 'fa fa-tumblr-square',
        'fa fa-turkish-lira'                         => 'fa fa-turkish-lira',
        'fa fa-twitch'                               => 'fa fa-twitch',
        'fa fa-twitter'                              => 'fa fa-twitter',
        'fa fa-twitter-square'                       => 'fa fa-twitter-square',
        'fa fa-venus'                                => 'fa fa-venus',
        'fa fa-venus-double'                         => 'fa fa-venus-double',
        'fa fa-venus-mars'                           => 'fa fa-venus-mars',
        'fa fa-viacoin'                              => 'fa fa-viacoin',
        'fa fa-viadeo'                               => 'fa fa-viadeo',
        'fa fa-viadeo-square'                        => 'fa fa-viadeo-square',
        'fa fa-video-camera'                         => 'fa fa-video-camera',
        'fa fa-vimeo'                                => 'fa fa-vimeo',
        'fa fa-vimeo-square'                         => 'fa fa-vimeo-square',
        'fa fa-vine'                                 => 'fa fa-vine',
        'fa fa-vk'                                   => 'fa fa-vk',
        'fa fa-volume-control-phone'                 => 'fa fa-volume-control-phone',
        'fa fa-volume-down'                          => 'fa fa-volume-down',
        'fa fa-volume-off'                           => 'fa fa-volume-off',
        'fa fa-volume-up'                            => 'fa fa-volume-up',
        'fa fa-umbrella'                             => 'fa fa-umbrella',
        'fa fa-underline'                            => 'fa fa-underline',
        'fa fa-undo'                                 => 'fa fa-undo',
        'fa fa-university'                           => 'fa fa-university',
        'fa fa-universal-access'                     => 'fa fa-universal-access',
        'fa fa-unlink'                               => 'fa fa-unlink',
        'fa fa-unlock'                               => 'fa fa-unlock',
        'fa fa-unlock-alt'                           => 'fa fa-unlock-alt',
        'fa fa-unsorted'                             => 'fa fa-unsorted',
        'fa fa-upload'                               => 'fa fa-upload',
        'fa fa-usb'                                  => 'fa fa-usb',
        'fa fa-usd'                                  => 'fa fa-usd',
        'fa fa-user'                                 => 'fa fa-user',
        'fa fa-user-circle'                          => 'fa fa-user-circle',
        'fa fa-user-circle-o'                        => 'fa fa-user-circle-o',
        'fa fa-user-md'                              => 'fa fa-user-md',
        'fa fa-user-o'                               => 'fa fa-user-o',
        'fa fa-user-plus'                            => 'fa fa-user-plus',
        'fa fa-user-secret'                          => 'fa fa-user-secret',
        'fa fa-user-times'                           => 'fa fa-user-times',
        'fa fa-users'                                => 'fa fa-users',
        'fa fa-vcard'                                => 'fa fa-vcard',
        'fa fa-vcard-o'                              => 'fa fa-vcard-o',
        'fa fa-warning'                              => 'fa fa-warning',
        'fa fa-wechat'                               => 'fa fa-wechat',
        'fa fa-weibo'                                => 'fa fa-weibo',
        'fa fa-weixin'                               => 'fa fa-weixin',
        'fa fa-whatsapp'                             => 'fa fa-whatsapp',
        'fa fa-wheelchair'                           => 'fa fa-wheelchair',
        'fa fa-wheelchair-alt'                       => 'fa fa-wheelchair-alt',
        'fa fa-wifi'                                 => 'fa fa-wifi',
        'fa fa-wikipedia-w'                          => 'fa fa-wikipedia-w',
        'fa fa-window-close'                         => 'fa fa-window-close',
        'fa fa-window-close-o'                       => 'fa fa-window-close-o',
        'fa fa-window-maximize'                      => 'fa fa-window-maximize',
        'fa fa-window-minimize'                      => 'fa fa-window-minimize',
        'fa fa-window-restore'                       => 'fa fa-window-restore',
        'fa fa-windows'                              => 'fa fa-windows',
        'fa fa-won'                                  => 'fa fa-won',
        'fa fa-wordpress'                            => 'fa fa-wordpress',
        'fa fa-wpbeginner'                           => 'fa fa-wpbeginner',
        'fa fa-wpexplorer'                           => 'fa fa-wpexplorer',
        'fa fa-wpforms'                              => 'fa fa-wpforms',
        'fa fa-wrench'                               => 'fa fa-wrench',
        'fa fa-xing'                                 => 'fa fa-xing',
        'fa fa-xing-square'                          => 'fa fa-xing-square',
        'fa fa-y-combinator'                         => 'fa fa-y-combinator',
        'fa fa-y-combinator-square'                  => 'fa fa-y-combinator-square',
        'fa fa-yahoo'                                => 'fa fa-yahoo',
        'fa fa-yc'                                   => 'fa fa-yc',
        'fa fa-yc-square'                            => 'fa fa-yc-square',
        'fa fa-yelp'                                 => 'fa fa-yelp',
        'fa fa-yen'                                  => 'fa fa-yen',
        'fa fa-yoast'                                => 'fa fa-yoast',
        'fa fa-youtube'                              => 'fa fa-youtube',
        'fa fa-youtube-play'                         => 'fa fa-youtube-play',
        'fa fa-youtube-square'                       => 'fa fa-youtube-square',
    ];
    return $cards;
}

function localizeDate($date)
{
    switch (getLocale())
    {
        case 'ar':
            $smallDays           = ["Sat", "Sun", "Mon", "Tue", "Wed" , "Thu", "Fri"];
            $arDays              = ["السبت", "الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة"];
            $date                = str_replace($smallDays, $arDays, $date);

            $smallMonths         = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            $arMonths            = ["يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"];
            $date                = str_replace($smallMonths, $arMonths, $date);

            header('Content-Type: text/html; charset=utf-8');

            $standard_nums       = ["0","1","2","3","4","5","6","7","8","9"];
            $eastern_arabic_nums = ["٠","١","٢","٣","٤","٥","٦","٧","٨","٩"];
            $date                = str_replace($standard_nums , $eastern_arabic_nums , $date);

            $enAmPm              = ["AM" , "PM"];
            $arAmPm              = ["ص" , "م"];
            $date                = str_replace($enAmPm , $arAmPm , $date);

            return $date;
            break;

        default:
            return $date;
            break;
    }
}

function permission_checker($auth): bool
{
    if($auth->role->id == 1) return true;

    foreach($auth->role->permissions as $permission)
    {
        if(request()->route()->getName() != $permission->permission) continue;

        return true;
    }
    return false;
}

function permission_route_checker($route): bool
{
    $auth = auth()->guard('admin')->user();

    if($auth->role->id == 1) return true;

    foreach($auth->role->permissions as $permission)
    {
        if($route != $permission->permission) continue;

        return true;
    }

    return false;
}

function permission_routes_checker(...$routes): bool
{
    $res = [];

    foreach ($routes as $route)
    {
        $res[] = permission_route_checker($route);
    }

    return in_array(true, $res);
}

function edit_permissions($permissions, $check): string
{
    return in_array($check, $permissions->pluck('permission')->toArray()) ? 'checked' : '';
}

function getAllRoutes(): array
{
    // this function control everything in permissions page routes
    $routes = Route::getRoutes();

    $arr = [];

    foreach ($routes as $key => $value)
    {
        if($value->getName() !== null && !in_array($value->getName(), not_in_routes()))
        {
            $action = $value->getAction();

            $as = $action['as'];

            if(in_array($action['prefix'], ['_debugbar', '_ignition'])) continue;

            if(in_array($action['namespace'], ['Front', 'Auth'])) continue;

            if($action['prefix'] == null) continue;

            if(count(explode('/', $action['prefix'])) > 2) continue;

            $route = explode('.', $as);

            if($as == 'admin-panel') continue;

            if(count($route) >= 2) $arr[head($route)][$key] = $as;

            else $arr[$key] = $as;
        }
    }
    return $arr;
}

function not_in_routes(): array
{
    return [
        'home',
        'front.sign-in',
        'front.register',
        'front.user.submit.login',
        'front.show.register',
        'login',
        'livewire.message',
        'livewire.upload-file',
        'livewire.preview-file',
    ];
}

function models($model = '')
{
    $models = [
        'role'              => iconWithColor('account-key', 'success'),
        'admin'             => iconWithColor('account-group', 'blue'),
        'user'              => iconWithColor('account-multiple', 'dark'),
        'sport'             => iconWithColor('basketball', 'soft-secondary'),
        'age'               => iconWithColor('format-list-bulleted-square', 'pink'),
        'tournament'        => iconWithColor('trophy', 'soft-pink'),
        'subscription'      => iconWithColor('trophy-variant-outline', 'purple'),
//        'changesLog'        => iconWithColor('file-empty', 'secondary'),
//        'accreditationLog'  => iconWithColor('file-text', 'soft-danger'),
//        'course'          => iconWithColor('file-stats2', 'soft-dark'),
//        'paymentMethod'     => iconWithColor('cash', 'soft-purple'),
//        'screen'            => iconWithColor('cellphone-screenshot', 'soft-info'),
//        'faq'               => iconWithColor('comment-question', 'primary'),
//        'banner'            => iconWithColor('image-size-select-actual', 'orange'),
//        'coupon'            => iconWithColor('format-list-checks', 'light'),
//        'university'        => iconWithColor('teach', 'info'),
//        'country'           => iconWithColor('city-variant-outline', 'soft-blue'),
    ];

    return $model != '' ? $models[$model] : $models;
}

function iconWithColor($icon, $color): array
{
    return ['icon' => $icon, 'color' => $color];
}

function GetDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
{
    $latFrom = deg2rad($latitudeFrom);

    $lonFrom = deg2rad($longitudeFrom);

    $latTo = deg2rad($latitudeTo);

    $lonTo = deg2rad($longitudeTo);

    $latDelta = $latTo - $latFrom;

    $lonDelta = $lonTo - $lonFrom;

    $powLat = pow(sin($latDelta / 2), 2);

    $powLng = pow(sin($lonDelta / 2), 2);

    $angle = 2 * asin(sqrt($powLat + cos($latFrom) * cos($latTo) * $powLng));

    return ($angle * $earthRadius) / 1000;
}

function updateStatus($data, $model): bool
{
    // if the value comes with checked that mean we want the reverse value of it;
    return ($data['value'] == 'checked') ? $model->update(['status' => 0]) : $model->update(['status' => 1]);
}

function getModelCount($model, $withDeleted = false): int
{
    $mo = getClass($model);

    $admin = \App\Models\Admin::class;

    $role = \App\Models\Role::class;

    $ttl = 60*60;

	if($withDeleted)
	{
		if($model == 'admin') return $admin::onlyTrashed()->where('role_id', '!=', 1)->count();

		return $mo::onlyTrashed()->count();
	}

	if($model == 'admin') return $admin::where('role_id', '<>', 1)->count();

	if($model == 'role') return $role::where('id', '<>', 1)->count();

	return $mo::count();
}

function modelForceDelete($model, $id, $hasFCM = false, $file = '', $folder = ''): bool
{
    $text  = last(explode('\\', $model));

    $main = str_of($text);

    $model_name = lower($main);

    $image_folder = $model_name->plural();

    $currentModel = $model::onlyTrashed()->where('id', $id)->first();

    if(isset($currentModel->image))
    {
        storage_unlink($image_folder, $currentModel->image->image);

        $currentModel->image()->delete();
    }

    if(isset($currentModel->images))
    {
        foreach ($currentModel->images as $image)
        {
            storage_unlink($image_folder, $image->image);

            $image->delete();
        }
    }

    if(isset($file) && isset($folder))
    {
        storage_unlink($folder, $currentModel->$file);
        storage_unlink('thumbnails', $currentModel->$file.'_thumbnails.jpg');
    }

//    if($hasFCM)
//    {
//        if($model_name == 'user')
//        {
//            $currentModel->fcm()->delete();
//            $currentModel->token()->delete();
//        }
//    }

    return $currentModel->forceDelete();
}

function formCreateArray($table): array
{
    return [
        'route'  => $table.'.store',
        'method' => 'POST',
        'id'     => $table.'Form',
        'class'  => 'form-horizontal push-10-t '.$table.' ajax create',
        'files'  => true
    ];
}

function formUpdateArray($model, $table): array
{
    return [
        'url'    => route($table.'.update', $model->id),
        'method' => 'PUT',
        'id'     => $table.'Form',
        'class'  => 'form-horizontal push-10-t '.$table.' ajax edit',
        'files'  => true,
    ];
}

function getSettingsByType(string $type)
{
    return \App\Models\Setting::select(['key', 'value'])->where('type', $type)->get();
}

function getSettingInCollection($collection, $key): string
{
    return $collection->where('key', $key)->first()->value ?? trans('back.no-value');
}

function getContactUsPageData(): array
{
    $settings = getSettingsByType('CONTACTS');

    $site_location = explode(',', getSettingInCollection($settings, 'contact_site_location'));

    $data['lat'] = $site_location[0];

    $data['lng'] = $site_location[1];

    $data['contact_facebook'] = getSettingInCollection($settings, 'contact_facebook');

    $data['contact_instgram'] = getSettingInCollection($settings, 'contact_instgram');

    $data['contact_twitter'] = getSettingInCollection($settings, 'contact_twitter');

    $data['contact_phone'] = getSettingInCollection($settings, 'contact_phone');

    $data['contact_email'] = getSettingInCollection($settings, 'contact_email');

    $data['contact_site_address'] = getSettingInCollection($settings, 'contact_site_address');

    $data['contact_site_location'] = getSettingInCollection($settings, 'contact_site_location');

    return $data;
}

function crudRoutes($table, $controller, $withShow = false): void
{
    $model = singular($table);

    Route::post("/$table/ajax-delete-$model", [$controller, 'delete'])->name("$table.ajax-delete-$model");

    Route::post("/$table/ajax-change-$model-status", [$controller, 'ChangeStatus'])->name("$table.ajax-change-$model-status");

    if($withShow) Route::get("/$table/{".$model."}/show", [$controller, 'show'])->name("$table.show");

    Route::get("/$table/export", [$controller, 'export'])->name("$table.export");

    Route::get("/$table/print", [$controller, 'print'])->name("$table.print");

    Route::get("$table/get/trashed", [$controller, 'trashed'])->name("$table.trashed");

    Route::get("/$table/restore/{id}/trashed", [$controller, 'restore'])->name("$table.restore");

    Route::get("/$table/delete/{id}/trashed", [$controller, 'forceDelete'])->name("$table.delete");
}

function getTaskStatus($task): string
{
    if($task->actual_start_date->isCurrentDay()) return trans('back.started');

    if($task->actual_start_date->isFuture()) return trans('back.not-started');

    if ($task->actual_start_date->isPast() && $task->actual_end_date->isFuture()) return trans('back.still');

    if ($task->actual_start_date->isPast() && $task->actual_end_date->isPast()) return trans('back.finished');

    return 'Unknown';
}

function removePhoneZero($number, $country_code): string
{
    $replaced = Str::replaceFirst('0','', $number);

    $phone = $number[0] != 0 ? $number : $replaced;

    return $country_code.$phone;
}

function setDoctorWallet($doctor, $balanceWithCommission): int
{
    $currentAmount = is_null($doctor->wallet) ? 0 : $doctor->wallet;

    return (int)$currentAmount + (int)$balanceWithCommission;
}

function morrisDonutChart():array
{
    return ['admin', 'user'];
}

function checkUrlHas($term): bool
{
    return contains(request()->url(), $term);
}

function getFormattedException($e): string
{
    return $e->getMessage() .' in '. $e->getFile() .' at line '. $e->getLine();
}

function getMultiSelectForm($name)
{
    return [
        'class'    => 'form-control select2 select2-multiple form-data',
        'multiple' => 'multiple',
        'dir'      => direction(),
        'id'       => explode('[]', $name)[0]
    ];
}

function translated($type, $obj)
{
    return trans('back.'.$type.'-done',['var' => trans('back.'.plural($obj).'.t-'.$obj)]);
}

function translatedField($name, $type, $trans)
{
    return ['name' => $name, 'type' => $type, 'trans' => 'back.'.$trans];
}

function creatRouteNotIn()
{
    return ['commission', 'team'];
}

function changeStatusNotIn($model)
{
    $data = ['notification', 'team', 'subscription', 'demand', 'contact'];

    return in_array($model, $data);
}

function highlightText($text, $search = false)
{
    if($search) return preg_replace('/(' . request()->term . ')/i', "<mark>".request()->term."</mark>", $text);

    return $text ?? "<label class='badge badge-dark p-2 f-13'>".trans('back.no-value')."</label>";
}

function setValidationLang($lang)
{
    return $lang == 'ar' ? new \App\Rules\IsAr() : new \App\Rules\IsEn();
}

function storage_unlink($folder, $file_name): void
{
    $old = storage_path('app'.ds().'public'.ds().'uploaded'.ds().$folder.ds().$file_name);

    if(($file_name != "" || $file_name != null) && file_exists($old)) unlink($old);
}

function translate($key)
{
    return trans('back.'.plural($key).'.'.$key);
}

function getTimeFromSeconds($seconds)
{
    $hours = floor($seconds / 3600);

    $mins = floor($seconds / 60 % 60);

    $secs = floor($seconds % 60);

    return carbon()->createFromTime($hours, $mins, $secs)->format('H:i:s');
}

function getSecondsFromTime($time)
{
    return carbon()->parse($time)->secondsSinceMidnight();
}

function transByType($type, $model)
{
    return trans('back.'.$type.'-var', ['var' => trans('back.' . plural($model) . '.' . $model)]);
}

function transCreate($model)
{
    return transByType('create', $model);
}

function transEdit($model)
{
    return transByType('edit', $model);
}

function transLatest($tableName)
{
    return transByType('latest', $tableName);
}

function transActive($model)
{
    return transByType('active', $model);
}

function transDeductive($model)
{
    return transByType('deductive', $model);
}

function transNoVar($var)
{
    return trans('back.no-var', ['var' => trans($var)]);
}

function check_if_not_empty($array)
{
    $data = [];

    foreach ($array as $index => $item)
    {
        if(empty($item['count'])) continue;

        else $data[] = true;
    }

    return empty($data);
}

function plural($singular)
{
    return str_of($singular)->plural();
}

function camel($string)
{
    return str_of($string)->camel();
}

function snake($string)
{
    return str_of($string)->snake();
}

function contains($string, $term)
{
    return str()->contains($string, $term);
}

function tableName($polymorphicClass)
{
    $name = explode('\\', $polymorphicClass)[2];

    return lcfirst(plural($name));
}

function check_if_user_finish_course($subscription): bool
{
    return (boolean)$subscription->course->userVideos->where('user_id', $subscription->user_id)->where('is_finish', 0)->count() == 0;
}

function get_who_finish_course_count($course, $isFinish, $withCount = true)
{
    $data = [];

    $videos = isset($course) ? $course->userVideos->groupBy('user_id') : collect([]);

    foreach ($videos as $user_id => $user_video)
    {
        $incomplete = $user_video->where('is_finish', 0)->count();

        $data[] = ['user_id' => $user_id, 'is_finish' => $incomplete == 0];
    }

    $whoFinish = collect($data)->where('is_finish', $isFinish);

    $users = $whoFinish->pluck('user_id')->toArray();

    return (!$withCount) ? \App\Models\User::find($users) : $whoFinish->count();
}

function get_user_course_percentage($subscription): int
{
    if(isset($subscription->course))
    {
        $count = $subscription->course->videos->count();

        $userVideos = $subscription->course->userVideos->where('user_id', $subscription->user_id)->where('is_finish', 1);

        return (int)round(($userVideos->count() / $count) * 100);
    }

    return 0;
}

function convert_to_ar_num($num)
{
    $standard_nums = ["0","1","2","3","4","5","6","7","8","9"];

    $eastern_arabic_nums = ["٠","١","٢","٣","٤","٥","٦","٧","٨","٩"];

    return str_replace($standard_nums, $eastern_arabic_nums, $num);
}

function colors()
{
    $colors = ['primary', 'pink', 'info', 'danger', 'secondary'];

    shuffle($colors);

    return $colors[0];
}

function script($url, $options = [])
{
    return Html::script($url, $options);
}

function style($url, $options = [])
{
    return Html::style($url, $options);
}

function meta($name, $content, $options = [])
{
    $others = "";

    if(count($options) > 0)
    {
        foreach ($options as $key => $option)
        {
            $others .= $key . '=' . $option . ' ';
        }
    }

    return '<meta name="'.$name.'" content="'.$content.'" '.$others.'/>';
}

function admin()
{
    return auth()->guard('admin')->user();
}

function root()
{
    return request()->root();
}

function getLocale()
{
    return app()->getLocale();
}

function isLocale($locale)
{
    return app()->isLocale($locale);
}

function get_total_by_percentage($percent, $value, $isDiscount, $onlyDiscount)
{
    // Y = X * P%;
    $percentage = $percent / 100;

    $discount = ($value * $percentage);

    if($onlyDiscount) return $discount;

    if($isDiscount) return (int)$value - $discount; // in case of we have a discount coupon value;

    return (int)$value + $discount; // in case of we have a tax value;
}

function get_total_from_percentage($percent, $value)
{
    return get_total_by_percentage($percent, $value, true, false);
}

function get_discount_from_percentage($percent, $value)
{
    return get_total_by_percentage($percent, $value, false, true);
}

function get_input_attributes($type, $name, $slug, $col = 12)
{
    return [
        'type'   => $type,
        'name'   => $name,
        'style'  => 'form-control form-data',
        'col'    => $col,
        'slug'   => $slug,
    ];
}

function my_fatoorah_base_url()
{
    return 'https://apitest.myfatoorah.com';
}

function getRate($rates)
{
    return \App\Facade\Collections::getRate($rates);
}

function set_permissions_rows($count)
{
    switch($count)
    {
        case $count <= 20:
            return 'col-md-12';
            break;
        case $count <= 40:
            return 'col-md-6';
            break;
        case $count <= 60:
            return 'col-md-4';
            break;
        case $count <= 80:
            return 'col-md-3';
            break;
        case $count <= 100:
            return 'col-md-2';
            break;
        default:
            return 'col-md-1';
            break;
    }
}

function getModelName($class)
{
    return lcfirst(last(explode('\\', $class)));
}

function getClass($model)
{
    return 'App\Models\\'.singular($model)->ucfirst();
}

function cacheModel($model, $query)
{
    $key = plural($model) . '_collection';

    return Cache::remember($key, 60*60, function () use ($query){
        return $query;
    });
}

function caching()
{
    return new Cache;
}

function ds()
{
    return DIRECTORY_SEPARATOR;
}

function getCachedSetting($key)
{
    return Cache::remember($key, 60*60, function() use($key){
        return getSetting($key);
    });
}

function sitelangs()
{
    return config('sitelangs.locales');
}

function getFlagLang($flag = '')
{
    $lang = app()->isLocale('ar') ? 'sa' : 'gb';

    if($flag != '') $lang = $flag;

    return asset('public/admin/images/flags/'.$lang.'.png');
}
