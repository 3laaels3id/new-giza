<?php

namespace App\Http\Requests\Front;

use App\Rules\EmailFormatChecker;
use Illuminate\Foundation\Http\FormRequest;

class userRegisterRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'     => ['required', 'string'],
            'email'    => ['required','email', new EmailFormatChecker(), 'unique:users,email'],
            'password' => ['required', 'string', 'confirmed', 'min:6']
        ];
    }
}
