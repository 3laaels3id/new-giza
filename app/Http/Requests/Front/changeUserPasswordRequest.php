<?php

namespace App\Http\Requests\Front;

use Illuminate\Foundation\Http\FormRequest;

class changeUserPasswordRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'currentPassword' => ['required', 'string'],
            'newPassword'     => ['required', 'string', 'min:6', 'confirmed'],
        ];
    }
}
