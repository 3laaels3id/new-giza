<?php

namespace App\Http\Requests\Front;

use Illuminate\Foundation\Http\FormRequest;

class contactUsMessageRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'    => ['required', 'string'],
            'email'   => ['required', 'email'],
            'message' => ['required', 'string'],
        ];
    }
}
