<?php

namespace App\Http\Requests\Front;

use Illuminate\Foundation\Http\FormRequest;

class forgetPasswordRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => ['required', 'email', 'exists:users,email'],
        ];
    }
}
