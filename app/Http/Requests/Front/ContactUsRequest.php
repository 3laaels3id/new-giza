<?php

namespace App\Http\Requests\Front;

use App\Rules\MobilePhoneRule;
use App\Rules\EmailFormatChecker;
use Illuminate\Foundation\Http\FormRequest;

class ContactUsRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'    => ['required', 'string'],
            'email'   => ['required', 'email', new EmailFormatChecker],
            'phone'   => ['required', 'numeric', 'digits_between:10,10', new MobilePhoneRule],
            'message' => ['required', 'string']
        ];
    }

    public function messages()
    {
        return [
            'phone.digits_between' => trans('api.phone_digits_between', ['num' => 10]),
        ];
    }
}
