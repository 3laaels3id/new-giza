<?php

namespace App\Http\Requests\Back;

use App\Rules\EmailFormatChecker;
use App\Rules\PasswordRule;
use Illuminate\Foundation\Http\FormRequest;

class EditAdminProfileRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $admin = auth()->guard('admin')->id();

        return [
            'fname'    => ['required', 'string', 'min:3', 'max:255'],
            'lname'    => ['required', 'string', 'min:3', 'max:255'],
            'email'    => ['required', 'email', new EmailFormatChecker(), 'unique:admins,email,'. $admin],
            'identity' => ['nullable', 'numeric', 'unique:admins,identity,' . $admin],
            'password' => ['nullable', 'min:6', new PasswordRule()],
            'image'    => ['nullable', 'image', 'mimes:jpg,png,jpeg', 'max:2048'],
        ];
    }
}
