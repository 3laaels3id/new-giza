<?php

namespace App\Http\Requests\Back;

use Illuminate\Foundation\Http\FormRequest;

class CreateTournamentSportRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id'                  => ['required', 'numeric', 'exists:tournaments,id'],
            'tournament_sports.*' => ['required', 'numeric', 'exists:sports,id']
        ];
    }
}
