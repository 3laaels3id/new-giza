<?php

namespace App\Http\Requests\Back;

use Illuminate\Foundation\Http\FormRequest;

class EditSubscriptionRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'invitations_fname.*'      => ['required', 'string'],
            'invitations_lname.*'      => ['required', 'string'],
            'invitations_age.*'        => ['required', 'numeric', 'min:1', 'max:100'],
            'invitations_email.*'      => ['required', 'email'],
            'invitations_phone.*'      => ['required', 'string'],
            'invitations_identity.*'   => ['nullable', 'numeric', 'digits_between:14,14', 'unique:invitations,identity'],
            'invitations_birth_date.*' => ['required', 'date'],
            'invitations_address.*'    => ['required', 'string'],
        ];
    }

    public function messages()
    {
        return [
            'invitations_identity.*.digits_between' => trans('api.input_digits_between', ['num' => 14]),
        ];
    }
}
