<?php

namespace App\Http\Requests\Back;

use App\Rules\PasswordRule;
use App\Rules\EmailFormatChecker;
use Illuminate\Foundation\Http\FormRequest;

class CreateAdminRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'fname'    => ['required', 'string', 'min:3', 'max:255'],
            'lname'    => ['required', 'string', 'min:3', 'max:255'],
            'role_id'  => ['required', 'numeric', 'exists:roles,id'],
            'email'    => ['required', 'email', new EmailFormatChecker(), 'unique:admins,email'],
            'identity' => ['required', 'numeric', 'unique:admins,identity'],
            'password' => ['required', 'confirmed', 'min:6', new PasswordRule()],
            'image'    => ['nullable', 'image', 'mimes:jpg,png,jpeg', 'max:2048'],
        ];
    }
}
