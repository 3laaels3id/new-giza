<?php

namespace App\Http\Requests\Back;

use Illuminate\Foundation\Http\FormRequest;

class CreateSportRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [];

        foreach (sitelangs() as $lang => $name) {
            $rules[$lang.'.name'] = ['required', 'string', setValidationLang($lang)];
        }

        return $rules;
    }
}
