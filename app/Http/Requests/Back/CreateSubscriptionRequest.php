<?php

namespace App\Http\Requests\Back;

use Illuminate\Foundation\Http\FormRequest;

class CreateSubscriptionRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_id'                  => ['required', 'numeric', 'exists:users,id'],
            'tournament_id'            => ['required', 'numeric', 'exists:tournaments,id'],
            'sport_id'                 => ['required', 'numeric', 'exists:sports,id'],
            'age_id'                   => ['required', 'numeric', 'exists:ages,id'],
            'team_name'                => ['required', 'string', 'unique:teams,name'],
            'invitations_fname.*'      => ['nullable', 'string'],
            'invitations_lname.*'      => ['nullable', 'string'],
            'invitations_age.*'        => ['nullable', 'numeric', 'min:1', 'max:100'],
            'invitations_email.*'      => ['nullable', 'email', 'unique:invitations,email'],
            'invitations_phone.*'      => ['nullable', 'string', 'unique:invitations,phone'],
            'invitations_identity.*'   => ['nullable', 'numeric', 'digits_between:14,14', 'unique:invitations,identity'],
            'invitations_birth_date.*' => ['nullable', 'date'],
            'invitations_address.*'    => ['nullable', 'string'],
        ];
    }

    public function messages()
    {
        return [
            'invitations_identity.*.digits_between' => trans('api.input_digits_between', ['num' => 14]),
        ];
    }
}
