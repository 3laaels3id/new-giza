<?php

namespace App\Http\Requests\Back;

use App\Rules\MembershipRule;
use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'fname'             => ['required', 'string', 'min:3', 'max:255'],
            'lname'             => ['required', 'string', 'min:3', 'max:255'],
            'age'               => ['required', 'string', 'min:1', 'max:100'],
            'birth_date'        => ['required', 'date'],
            'address'           => ['required', 'string', 'in:cairo,giza,other'],
            'membership_number' => ['required', 'string', new MembershipRule, 'unique:users,membership_number'],
//            'password' => ['nullable', 'confirmed', 'min:6', new PasswordRule],
//            'image'    => ['nullable', 'image', 'mimes:jpg,png,jpeg', 'max:2048'],
        ];
    }
}
