<?php

namespace App\Http\Requests\Back;

use Illuminate\Foundation\Http\FormRequest;

class CreateTournamentRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'start_date'     => ['required', 'date', 'after:'.today()->format('Y-m-d')],
            'end_date'       => ['required', 'date', 'after:start_date'],
            'maximum_sports' => ['required', 'numeric', 'min:1', 'max:255'],
        ];

        foreach (sitelangs() as $lang => $name) {
            $rules[$lang.'.name'] = ['required', 'string', setValidationLang($lang)];
        }

        return $rules;
    }
}
