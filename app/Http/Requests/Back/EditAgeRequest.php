<?php

namespace App\Http\Requests\Back;

use Illuminate\Foundation\Http\FormRequest;

class EditAgeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'sport_id' => ['required', 'numeric', 'exists:sports,id'],
            'type'     => ['required', 'string', 'in:adult,junior'],
            'maximum'  => ['required', 'numeric', 'min:1', 'max:255'],
        ];

        foreach (sitelangs() as $lang => $name) {
            $rules[$lang.'.name'] = ['required', 'string', setValidationLang($lang)];
        }

        return $rules;
    }
}
