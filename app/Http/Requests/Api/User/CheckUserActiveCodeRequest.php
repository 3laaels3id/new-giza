<?php

namespace App\Http\Requests\Api\User;

use App\Http\Requests\REQUEST_API_PARENT;
use App\Rules\MobilePhoneRule;

class CheckUserActiveCodeRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'phone'        => ['required', 'numeric', 'digits_between:10,10', new MobilePhoneRule()],
            'country_code' => ['required', 'string'],
            'code'         => ['required', 'numeric'],
        ];
    }

    public function messages()
    {
        return [
            'phone.digits_between' => trans('api.phone_digits_between', ['num' => 10]),
        ];
    }
}
