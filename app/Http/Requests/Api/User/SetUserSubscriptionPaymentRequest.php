<?php

namespace App\Http\Requests\Api\User;

use App\Http\Requests\REQUEST_API_PARENT;

class SetUserSubscriptionPaymentRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'course_id' => ['required', 'numeric', 'exists:courses,id'],
            'coupon'    => ['nullable', 'string'],
        ];
    }
}
