<?php

namespace App\Http\Requests\Api\User;

use App\Http\Requests\REQUEST_API_PARENT;
use App\Rules\EmailFormatChecker;

class UpdateUserProfileRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'fname'        => ['nullable', 'string'],
            'lname'        => ['nullable', 'string'],
            'username'     => ['nullable', 'string', 'min:3', 'max:40'],
            'email'        => ['nullable', 'email', new EmailFormatChecker(), 'unique:users,email,' . request()->user()->id],
            'image'        => ['nullable', 'mimes:jpg,png,jpeg'],
            'device_token' => ['nullable', 'string'],
        ];
    }
}
