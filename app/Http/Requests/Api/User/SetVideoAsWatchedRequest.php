<?php

namespace App\Http\Requests\Api\User;

use App\Http\Requests\REQUEST_API_PARENT;

class SetVideoAsWatchedRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'video_id' => ['required', 'numeric', 'exists:videos,id'],
        ];
    }
}
