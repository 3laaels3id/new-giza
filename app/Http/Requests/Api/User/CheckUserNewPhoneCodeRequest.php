<?php

namespace App\Http\Requests\Api\User;

use App\Http\Requests\REQUEST_API_PARENT;
use App\Rules\MobilePhoneRule;

class CheckUserNewPhoneCodeRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'phone'        => ['required', 'numeric', new MobilePhoneRule()],
            'country_code' => ['required', 'numeric'],
            'code'         => ['required', 'numeric']
        ];
    }
}
