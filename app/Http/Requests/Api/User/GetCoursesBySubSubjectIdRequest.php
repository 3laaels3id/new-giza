<?php

namespace App\Http\Requests\Api\User;

use App\Http\Requests\REQUEST_API_PARENT;
use App\Rules\CheckSubSubjectRule;

class GetCoursesBySubSubjectIdRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'sub_subject_id' => ['required', 'numeric', new CheckSubSubjectRule(), 'exists:subjects,id'],
        ];
    }
}
