<?php

namespace App\Http\Requests\Api\User;

use App\Http\Requests\REQUEST_API_PARENT;
use App\Rules\EmailFormatChecker;
use App\Rules\MobilePhoneRule;
use App\Rules\PasswordRule;

class RegisterRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'fname'           => ['required', 'string', 'min:3', 'max:40'],
            'lname'           => ['required', 'string', 'min:3', 'max:40'],
            'username'        => ['required', 'string', 'min:3', 'max:40'],
            'email'           => ['required', 'email', new EmailFormatChecker(), 'unique:users,email'],
            'phone'           => ['required', 'digits_between:10,10', new MobilePhoneRule(), 'unique:users,phone'],
            'country_code'    => ['required', 'string', 'min:3'],
            'password'        => ['required', 'string', 'min:6', new PasswordRule()],
            'image'           => ['nullable', 'mimes:png,jpg,jpeg', 'max:2048'],
            'accept_terms'    => ['required', 'in:1,0'],
            'device_token'    => ['required', 'string'],
        ];
    }

    public function messages()
    {
        return [
            'password.min'         => trans('api.password-min'),
            'phone.digits_between' => trans('api.number-var-digits-between', ['var' => 10]),
        ];
    }
}
