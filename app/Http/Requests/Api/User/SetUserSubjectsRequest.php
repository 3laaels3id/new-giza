<?php

namespace App\Http\Requests\Api\User;

use App\Http\Requests\REQUEST_API_PARENT;
use App\Rules\CheckMainSubjectRule;

class SetUserSubjectsRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'subjects'   => ['required', 'array'],
            'subjects.*' => ['required', 'numeric', 'exists:subjects,id', new CheckMainSubjectRule()],
        ];
    }
}
