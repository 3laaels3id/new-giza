<?php

namespace App\Http\Requests\Api\User;

use App\Http\Requests\REQUEST_API_PARENT;
use App\Rules\EmailFormatChecker;

class ContactUsRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'         => ['required', 'string'],
            'phone'        => ['required', 'numeric'],
            'country_code' => ['required', 'numeric'],
            'email'        => ['required', 'email', new EmailFormatChecker()],
            'message'      => ['required', 'string'],
        ];
    }
}
