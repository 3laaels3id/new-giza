<?php

namespace App\Http\Requests\Api\User;

use App\Http\Requests\REQUEST_API_PARENT;
use App\Rules\EmailFormatChecker;

class CheckResetCodeRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email'       => ['required', 'email', new EmailFormatChecker()],
            'reset_code'  => ['required', 'numeric']
        ];
    }
}
