<?php

namespace App\Http\Requests\Api\User;

use App\Http\Requests\REQUEST_API_PARENT;
use App\Rules\CheckMainSubjectRule;

class GetSubsBySubjectIdRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'main_subject_id' => ['required', 'numeric', new CheckMainSubjectRule(), 'exists:subjects,id'],
        ];
    }
}
