<?php

namespace App\Http\Requests\Api\User;

use App\Http\Requests\REQUEST_API_PARENT;
use App\Rules\PasswordRule;

class SetNewPasswordRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email'        => ['required', 'email', 'exists:users,email'],
            'password'     => ['required', 'string', new PasswordRule()],
        ];
    }
}
