<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;

class SetUserChatMessageRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'chat_id'     => ['required', 'numeric', 'exists:chats,id'],
            'message'     => ['required', 'string'],
            'sender_type' => ['required', 'string'],
        ];
    }
}
