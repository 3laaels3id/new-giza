<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;

class UploadChatFileRequest extends REQUEST_API_PARENT
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'chat_id'     => ['required', 'numeric', 'exists:chats,id'],
            'sender_type' => ['required', 'string'],
            'file'        => ['required', 'mimes:jpg,png,jpeg,pdf'],
        ];
    }
}
