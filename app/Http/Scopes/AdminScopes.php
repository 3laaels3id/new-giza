<?php

namespace App\Http\Scopes;

trait AdminScopes
{
    public function scopeSearch($query, $term)
    {
        $query->join('role_translations as rt', function($join){
            $join->on('admins.role_id', '=', 'rt.role_id');

            $join->where('rt.role_id', '!=', 1);

            $join->where('rt.locale', app()->getLocale());

            return $join;
        });

        $query->select(['admins.*']);

        $query->where('admins.fname', 'like', "%$term%");

        $query->orWhere('admins.lname', 'like', "%$term%");

        $query->orWhere('admins.email', 'like', "%$term%");

        $query->orWhere('rt.name', 'like', "%$term%");

        return $query;
    }

    private static function getAdminClouser()
    {
        return function ($admin) {
            $admin->image()->delete();
        };
    }
}
