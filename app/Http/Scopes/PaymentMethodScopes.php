<?php

namespace App\Http\Scopes;

trait PaymentMethodScopes
{
    public function scopeSearch($query, $term)
    {
        $query->join('payment_method_translations as pmt', self::joinPaymentMethodTranslations());

        $query->select(['payment_methods.*']);

        $query->where('pmt.name', 'LIKE', "%$term%");

        return $query;
    }

    private static function joinPaymentMethodTranslations(): callable
    {
        return function ($join){
            $join->on('payment_methods.id', '=', 'pmt.payment_method_id')->where('pmt.locale', app()->getLocale());
        };
    }
}
