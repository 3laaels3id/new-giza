<?php

namespace App\Http\Scopes;

trait BasicScopes
{
    public function scopeWhenIdIs($query, $id)
    {
        return $query->where('status', 1)->where('id', $id);
    }
}
