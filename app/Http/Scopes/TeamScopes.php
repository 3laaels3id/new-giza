<?php

namespace App\Http\Scopes;

trait TeamScopes
{
    public function scopeSearch($query, $term)
    {
        $query->join('users', 'users.id', '=', 'team.user_id');

        $query->select(['teams.*']);

        $query->where('teams.name', 'like', "%$term%");

        $query->orWhere('users.fname', 'like', "%$term%");

        $query->orWhere('users.lname', 'like', "%$term%");

        return $query;
    }
}
