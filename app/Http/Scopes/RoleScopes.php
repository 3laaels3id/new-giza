<?php

namespace App\Http\Scopes;

trait RoleScopes
{
    public function scopeSearch($query, $term)
    {
        $query->join('role_translations as rt', function($join){
            $join->on('roles.id', '=', 'rt.role_id');

            $join->where('rt.role_id', '!=', 1);

            $join->where('rt.locale', getLocale());

            return $join;
        });

        $query->select(['roles.*']);

        $query->orWhere('rt.name', 'like', "%$term%");

        return $query;
    }
}
