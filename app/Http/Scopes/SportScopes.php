<?php

namespace App\Http\Scopes;

trait SportScopes
{
    public function scopeSearch($query, $term)
    {
        $query->join('sport_translations as st', function($join){
            $join->on('sports.id', '=', 'st.sport_id')->where('st.locale', getLocale());
        });

        $query->select(['sports.*']);

        $query->where('st.name', 'LIKE', "%$term%");

        return $query;
    }
}
