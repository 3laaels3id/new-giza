<?php

namespace App\Http\Scopes;

trait NotificationScopes
{
    public function scopeSearch($query, $term)
    {
        $query->where('title', 'LIKE', "%$term%");

        $query->orWhere('body', 'LIKE', "%$term%");

        return $query;
    }
}
