<?php

namespace App\Http\Scopes;

trait UploadScopes
{
    public function scopeSearch($query, $term)
    {
        $query->join('courses', self::joinCourseTranslations());

        $query->join('doctors', 'uploads.doctor_id', '=', 'doctors.id');

        $query->select(['uploads.*']);

        $query->where('courses.title', 'LIKE', "%$term%");

        $query->orWhere('doctors.fname', 'LIKE', "%$term%");

        $query->orWhere('doctors.lname', 'LIKE', "%$term%");

        $query->orWhere('uploads.title', 'LIKE', "%$term%");

        return $query;
    }

    private function joinCourseTranslations(): callable
    {
        return function ($join){
            $join->on('uploads.course_id', '=', 'courses.id')->where(self::whereConditionCallback());
        };
    }

    private function whereConditionCallback(): callable
    {
        return function($q){
            if(auth()->user()->guard == 'doctor') {
                return $q->where('uploads.doctor_id', auth()->guard('doctor')->user()->id);
            }
            return $q;
        };
    }
}
