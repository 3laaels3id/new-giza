<?php

namespace App\Http\Scopes;

trait UserScopes
{
    public function scopeSearch($query, $term)
    {
        $query->where('fname', 'LIKE', "%$term%");

        $query->orWhere('lname', 'LIKE', "%$term%");

        return $query;
    }
}
