<?php

namespace App\Http\Scopes;

trait ContactScopes
{
    public function scopeSearch($query, $term)
    {
        $query->where('name', 'like', "%$term%");

        $query->orWhere('email', 'like', "%$term%");

        $query->orWhere('country_code', 'like', "%$term%");

        $query->orWhere('message', 'like', "%$term%");

        $query->orWhere('phone', 'like', "%$term%");

        return $query;
    }
}
