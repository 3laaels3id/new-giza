<?php

namespace App\Http\Scopes;

trait TournamentScopes
{
    public function scopeSearch($query, $term)
    {
        $query->join('tournament_translations as tt', function($join){
            $join->on('tournaments.id', '=', 'tt.tournament_id')->where('tt.locale', getLocale());
        });

        $query->select(['tournaments.*']);

        $query->where('tt.name', 'LIKE', "%$term%");

        return $query;
    }
}
