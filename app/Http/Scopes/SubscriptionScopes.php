<?php

namespace App\Http\Scopes;

trait SubscriptionScopes
{
    public function scopeSearch($query, $term)
    {
        $query->join('users', 'subscriptions.user_id', '=', 'users.id');

        $query->join('tournament_translations as tt', function($join){
            $join->on('subscriptions.tournament_id', '=', 'tt.tournament_id')->where('tt.locale', getLocale());
        });

        $query->select(['subscriptions.*']);

        $query->where('tt.name', 'LIKE', "%$term%");

        $query->orWhere('users.fname', 'LIKE', "%$term%");

        $query->orWhere('users.lname', 'LIKE', "%$term%");

        return $query;
    }
}
