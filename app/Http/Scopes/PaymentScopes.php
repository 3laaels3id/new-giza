<?php

namespace App\Http\Scopes;

trait PaymentScopes
{
    public function scopeSearch($query, $term)
    {
        $query->join('users', 'payments.user_id', '=', 'users.id');

        $query->select(['payments.*', 'users.name']);

        $query->where('users.name', 'LIKE', "%$term%");

        $query->orWhere('payments.amount', 'LIKE', "%$term%");

        $query->orWhere('payments.currency', 'LIKE', "%$term%");

        return $query;
    }
}
