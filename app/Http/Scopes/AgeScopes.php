<?php

namespace App\Http\Scopes;

trait AgeScopes
{
    public function scopeSearch($query, $term)
    {
        $query->join('age_translations as at', function($join){
            $join->on('ages.id', '=', 'at.age_id')->where('at.locale', getLocale());
        });

        $query->join('sport_translations as st', function($join){
            $join->on('ages.sport_id', '=', 'st.sport_id')->where('st.locale', getLocale());
        });

        $query->select(['ages.*']);

        $query->where('at.name', 'LIKE', "%$term%");

        $query->orWhere('st.name', 'LIKE', "%$term%");

        return $query;
    }
}
