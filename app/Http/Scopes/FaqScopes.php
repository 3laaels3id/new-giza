<?php

namespace App\Http\Scopes;

trait FaqScopes
{
    public function scopeGetFaqByType($query, $type)
    {
        return $query->where('status', 1)->where('type', $type);
    }

    public function scopeSearch($query, $term)
    {
        $query->join('faq_translations as ft', self::joinFaqTranslations());

        $query->select(['faqs.*']);

        $query->where('ft.question', 'LIKE', "%$term%");

        $query->orWhere('ft.answer', 'LIKE', "%$term%");

        return $query;
    }

    private function joinFaqTranslations(): callable
    {
        return function ($join){
            $join->on('faqs.id', '=', 'ft.faq_id')->where('ft.locale', app()->getLocale());
        };
    }
}
