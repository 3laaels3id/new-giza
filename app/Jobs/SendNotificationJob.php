<?php

namespace App\Jobs;

use App\Facade\Firebase;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $push, $model;

    public function __construct($push, $model)
    {
        $this->push = $push;

        $this->model = $model;
    }

    public function handle()
    {
        if(isset($this->model->fcm) && $this->model->fcm->fcm) Firebase::pushNotification($this->push);
    }
}
