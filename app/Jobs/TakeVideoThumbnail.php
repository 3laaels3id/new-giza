<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Lakshmaji\Thumbnail\Facade\Thumbnail;

class TakeVideoThumbnail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $video_path, $thumbnail_path, $thumbnail_image;

    public function __construct($video_path, $thumbnail_path, $thumbnail_image)
    {
        $this->video_path      = $video_path;
        $this->thumbnail_path  = $thumbnail_path;
        $this->thumbnail_image = $thumbnail_image;
    }

    public function handle()
    {
        Thumbnail::getThumbnail($this->video_path, $this->thumbnail_path, $this->thumbnail_image, 4);
    }
}
