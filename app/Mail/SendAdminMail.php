<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendAdminMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $message;

    public function __construct($message, $title)
    {
        $this->message = $message;

        $this->subject = $title;
    }

    public function build()
    {
        return $this->markdown('emails.sendAdminMail');
    }
}
