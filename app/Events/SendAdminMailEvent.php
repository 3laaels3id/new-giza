<?php

namespace App\Events;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SendAdminMailEvent implements ShouldQueue
{
    use Dispatchable, SerializesModels;

    public $message;

    public $email;

    public function __construct($message, $email)
    {
        $this->message = $message;
        $this->email = $email;
    }
}
