<?php

namespace App\Providers;

use Illuminate\Support\Str;
use Illuminate\Support\ServiceProvider;
use App\Repository\Eloquent\Sql\BaseRepository;
use App\Repository\Contracts\IEloquentRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        foreach (self::getModels(app_path('Models')) as $model)
        {
            $abs = "App\Repository\Contracts\I".$model."Repository";

            $con = "App\Repository\Eloquent\Sql\\".$model."Repository";

            $this->app->bind($abs, $con);
        }

        $this->app->bind(IEloquentRepository::class, BaseRepository::class);
    }

    public function boot()
    {
        //
    }

    private static function getModels($path)
    {
        $out = [];

        $results = scandir($path);

        $notIn = ['.', '..', 'Token.php', 'Fcm.php', 'Image.php'];

        foreach ($results as $result)
        {
            if (in_array($result, $notIn)) continue;

            $filePath = $path . DIRECTORY_SEPARATOR . $result;

            if(Str::contains($result, 'Translation')) continue;

            if (is_dir($filePath)) $out = array_merge($out, self::getModels($filePath));

            else $out[] = substr($result,0,-4);
        }

        return $out;
    }
}
