<?php

namespace App\Providers;

use App\Facade\Process\PayProcess;
use Illuminate\Support\ServiceProvider;

class PayServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Pay', function () {
            return new PayProcess();
        });
    }

    public function boot()
    {
        //
    }
}
