<?php

namespace App\Providers;

use App\Facade\Process\ApiResponseProcess;
use Illuminate\Support\ServiceProvider;

class ApiResponseServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('ApiResponse', function () {
            return new ApiResponseProcess();
        });
    }

    public function boot()
    {
        //
    }
}
