<?php

namespace App\Providers;

use App\Facade\Process\CrudProcess;
use Illuminate\Support\ServiceProvider;

class CrudServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Crud', function () {
            return new CrudProcess();
        });
    }

    public function boot()
    {
        //
    }
}
