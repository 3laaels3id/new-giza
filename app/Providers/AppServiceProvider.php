<?php

namespace App\Providers;

use App\Models\User;
use App\Models\Message;
use App\Observers\UserObserver;
use App\Observers\MessageObserver;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use App\Http\Callbacks\AppCallbacks;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Collection;

class AppServiceProvider extends ServiceProvider
{
    use AppCallbacks;

    public function register()
    {
        View::composer('*', self::getAuth());

        View::composer('Back.layouts.partials.sidebar', self::getNewMessages());

        Collection::macro('sortByDescByIds', self::getSortByIds());

        Paginator::useBootstrap();
    }

    public function boot()
    {
        View::composer('*', self::getAllRoutes());

        Blade::if('hasPermission', self::getHasPermission());

        User::observe(UserObserver::class);

        Message::observe(MessageObserver::class);
    }
}
