<?php

namespace App\Providers;

use App\Facade\Process\SmsProcess;
use Illuminate\Support\ServiceProvider;

class SmsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Sms', function () {
            return new SmsProcess();
        });
    }

    public function boot()
    {
        //
    }
}
