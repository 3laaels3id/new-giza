<?php

namespace App\Providers;

use App\Facade\Process\ApiProcess;
use App\Http\Callbacks\AppCallbacks;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    use AppCallbacks;

    public function register()
    {
        $this->app->singleton('Api', function () {
            return new ApiProcess();
        });

        Route::macro('crudRoutes', self::getCrudRoutesCallback());
    }

    public function boot()
    {
        //
    }
}
