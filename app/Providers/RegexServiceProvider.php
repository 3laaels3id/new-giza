<?php

namespace App\Providers;

use App\Facade\Process\RegexProcess;
use Illuminate\Support\ServiceProvider;

class RegexServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Regex', function () {
            return new RegexProcess();
        });
    }

    public function boot()
    {
        //
    }
}
