<?php

namespace App\Providers;

use App\Events\SendAdminMailEvent;
use Illuminate\Auth\Events\Registered;
use App\Listeners\SendAdminMailListener;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        Registered::class           => [SendEmailVerificationNotification::class],
        SendAdminMailEvent::class   => [SendAdminMailListener::class],
    ];

    public function boot()
    {
        //
    }
}
