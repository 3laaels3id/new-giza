<?php

namespace App\Providers;

use App\Facade\Process\UploadedProcess;
use Illuminate\Support\ServiceProvider;

class UploadedServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Uploaded', function () {
            return new UploadedProcess();
        });
    }

    public function boot()
    {
        //
    }
}
