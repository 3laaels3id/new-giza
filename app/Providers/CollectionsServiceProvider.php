<?php

namespace App\Providers;

use App\Facade\Process\CollectionsProcess;
use Illuminate\Support\ServiceProvider;

class CollectionsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Collections', function () {
            return new CollectionsProcess();
        });
    }

    public function boot()
    {
        //
    }
}
