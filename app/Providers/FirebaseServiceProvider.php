<?php

namespace App\Providers;

use App\Facade\Process\FirebaseProcess;
use Illuminate\Support\ServiceProvider;

class FirebaseServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('Firebase', function () {
            return new FirebaseProcess();
        });
    }

    public function boot()
    {
        //
    }
}
