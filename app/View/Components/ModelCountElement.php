<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ModelCountElement extends Component
{
    public $model, $count, $color;

    public function __construct($model, $count, $color)
    {
        $this->model = $model;
        $this->count = $count;
        $this->color = $color;
    }

    public function render()
    {
        return view('components.model-count-element');
    }
}
