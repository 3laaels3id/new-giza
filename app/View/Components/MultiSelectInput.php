<?php

namespace App\View\Components;

use Illuminate\View\Component;

class MultiSelectInput extends Component
{
    public array $arr;
    public string $name;
    public string $slug;

    public function __construct($arr, $name, $slug)
    {
        $this->arr = $arr;
        $this->name = $name;
        $this->slug = $slug;
    }

    public function render()
    {
        return view('components.multi-select-input');
    }
}
