<?php

namespace App\View\Components;

use Illuminate\View\Component;

class FormInputMap extends Component
{
    public $model;

    public function __construct($model)
    {
        $this->model = $model;
    }

    public function render()
    {
        return view('components.form-input-map');
    }
}
