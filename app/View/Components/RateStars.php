<?php

namespace App\View\Components;

use Illuminate\View\Component;

class RateStars extends Component
{
    public $rates;

    public function __construct($rates)
    {
        $this->rates = $rates;
    }

    public function render()
    {
        return view('components.rate-stars');
    }
}
