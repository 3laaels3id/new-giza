<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ModelVideoPlayer extends Component
{
    public $src, $width;

    public function __construct($src, $width)
    {
        $this->src = $src;
        $this->width = $width;
    }

    public function render()
    {
        return view('components.model-video-player');
    }
}
