<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ModelShowElement extends Component
{
    public $model, $title, $nameSpace;

    public function __construct($model, $title, $nameSpace)
    {
        $this->model = $model;
        $this->title = $title;
        $this->nameSpace = $nameSpace;
    }

    public function render()
    {
        return view('components.model-show-element');
    }
}
