<?php

namespace App\View\Components;

use Illuminate\View\Component;

class TableActionDelete extends Component
{
    public $modelName;
    public $model;

    public function __construct($modelName, $model)
    {
        $this->modelName = $modelName;
        $this->model = $model;
    }

    public function render()
    {
        return view('components.table-action-delete');
    }
}
