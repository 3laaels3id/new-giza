<?php

namespace App\View\Components;

use Illuminate\View\Component;

class DownloadButtonElement extends Component
{
    public $url, $color;

    public function __construct($url, $color)
    {
        $this->url = $url;
        $this->color = $color;
    }

    public function render()
    {
        return view('components.download-button-element');
    }
}
