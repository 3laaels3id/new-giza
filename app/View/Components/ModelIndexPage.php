<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ModelIndexPage extends Component
{
    public $model, $collection, $modelType, $columns;

    public function __construct($model, $modelType, $collection, $columns)
    {
        $this->model      = $model;
        $this->modelType  = $modelType;
        $this->collection = $collection;
        $this->columns    = $columns;
    }

    public function render()
    {
        return view('components.model-index-page');
    }
}
