<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ModelCountCard extends Component
{
    public $model, $collection, $color;

    public function __construct($model, $collection, $color)
    {
        $this->model      = $model;
        $this->collection = $collection;
        $this->color      = $color;
    }

    public function render()
    {
        return view('components.model-count-card');
    }
}
