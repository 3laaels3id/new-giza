<?php

namespace App\View\Components;

use Illuminate\View\Component;

class TrashedTableHead extends Component
{
    public $table;
    public $collection;

    public function __construct($table, $collection)
    {
        $this->table = $table;
        $this->collection = $collection;
    }

    public function render()
    {
        return view('components.trashed-table-head');
    }
}
