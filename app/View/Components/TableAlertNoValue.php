<?php

namespace App\View\Components;

use Illuminate\View\Component;

class TableAlertNoValue extends Component
{
    public function render()
    {
        return view('components.table-alert-no-value');
    }
}
