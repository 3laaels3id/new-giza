<?php

namespace App\View\Components;

use Illuminate\View\Component;

class MorrisDonutChart extends Component
{
    public $model, $htmlID, $collection;

    public function __construct($model, $htmlID, $collection)
    {
        $this->model      = $model;
        $this->htmlID     = $htmlID;
        $this->collection = $collection;
    }

    public function render()
    {
        return view('components.morris-donut-chart');
    }
}
