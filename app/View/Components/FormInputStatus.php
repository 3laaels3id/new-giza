<?php

namespace App\View\Components;

use Illuminate\View\Component;

class FormInputStatus extends Component
{
    public $model;
    public $col;
    public $trans;

    public function __construct($model, $col, $trans)
    {
        $this->model = $model;
        $this->col = $col;
        $this->trans = $trans;
    }

    public function render()
    {
        return view('components.form-input-status');
    }
}
