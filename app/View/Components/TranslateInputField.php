<?php

namespace App\View\Components;

use Illuminate\View\Component;

class TranslateInputField extends Component
{
    public $type;
    public $name;
    public $slug;

    public function __construct($name, $type, $slug)
    {
        $this->type = $type;
        $this->name = $name;
        $this->slug = $slug;
    }

    public function render()
    {
        return view('components.translate-input-field');
    }
}
