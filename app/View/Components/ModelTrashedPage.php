<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ModelTrashedPage extends Component
{
    public $model, $modelType;

    public function __construct($model, $modelType)
    {
        $this->model = $model;

        $this->modelType = $modelType;
    }

    public function render()
    {
        return view('components.model-trashed-page');
    }
}
