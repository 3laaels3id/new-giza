<?php

namespace App\View\Components;

use Illuminate\View\Component;

class PercentageElement extends Component
{
    public $fgColor, $bgColor, $value;

    public function __construct($fgColor, $bgColor, $value)
    {
        $this->value   = $value;
        $this->fgColor = $fgColor;
        $this->bgColor = $bgColor;
    }

    public function render()
    {
        return view('components.percentage-element');
    }
}
