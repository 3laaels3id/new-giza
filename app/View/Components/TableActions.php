<?php

namespace App\View\Components;

use Illuminate\View\Component;

class TableActions extends Component
{
    public $modelName;
    public $model;

    public function __construct($model, $modelName)
    {
        $this->model = $model;
        $this->modelName = $modelName;
    }

    public function render()
    {
        return view('components.table-actions');
    }
}
