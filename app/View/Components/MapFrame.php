<?php

namespace App\View\Components;

use Illuminate\View\Component;

class MapFrame extends Component
{
    public $lat;
    public $lng;

    public function __construct($lat, $lng)
    {
        $this->lat = $lat;
        $this->lng = $lng;
    }

    public function render()
    {
        return view('components.map-frame');
    }
}
