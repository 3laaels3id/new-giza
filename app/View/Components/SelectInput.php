<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SelectInput extends Component
{
    public $arr;
    public $name;
    public $slug;

    public function __construct($arr, $name, $slug)
    {
        $this->arr    = $arr;
        $this->name   = $name;
        $this->slug   = $slug;
    }

    public function render()
    {
        return view('components.select-input');
    }
}
