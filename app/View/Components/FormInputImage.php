<?php

namespace App\View\Components;

use Illuminate\View\Component;

class FormInputImage extends Component
{
    public $name, $model;

    public function __construct($model, $name)
    {
        $this->name = $name;
        $this->model = $model;
    }

    public function render()
    {
        return view('components.form-input-image');
    }
}
