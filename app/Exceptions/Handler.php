<?php

namespace App\Exceptions;

use Error;
use Throwable;
use ErrorException;
use BadMethodCallException;
use App\Facade\ApiResponse;
use Illuminate\Database\QueryException;
use Illuminate\Auth\AuthenticationException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Contracts\Container\BindingResolutionException;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function render($request, Throwable $e)
    {
        if (str()->contains($request->url(), 'api'))
        {
            if($e instanceof TokenInvalidException) return ApiResponse::unAuth('Token invalid');

            elseif($e instanceof BadMethodCallException) return self::setHandlerResponse($e);

            elseif($e instanceof BindingResolutionException) return self::setHandlerResponse($e);

            elseif($e instanceof FileNotFoundException) return self::setHandlerResponse($e);

            elseif($e instanceof QueryException) return self::setHandlerResponse($e);

            elseif($e instanceof NotFoundHttpException) return self::setHandlerResponse($e, [], 'Not Found',404);

            elseif ($e instanceof ErrorException) return self::setHandlerResponse($e,null);

            elseif($e instanceof Error) return self::setHandlerResponse($e,null);
        }

        return parent::render($request, $e);
    }

    public function unauthenticated($request, AuthenticationException $exception)
    {
        $guard = arr()->get($exception->guards(), 0);

        switch ($guard)
        {
            case 'admin':
                $login = 'admin.login';
                break;

            case 'doctor':
                $login = 'doctor.login';
                break;

            default:
                $login = 'login';
                break;
        }

        return redirect()->guest(route($login));
    }

    private static function setHandlerResponse($e, $data = [], $message = '', $code = 500)
    {
        $exception = $e->getMessage() .' in '. $e->getFile() .' at line '. $e->getLine();

        $data['data']    = $data;

        $data['status']  = false;

        $data['message'] = $message;

        $data['error']   = $message == '' ? $exception : '';

        return response()->json($data, $code);
    }
}
